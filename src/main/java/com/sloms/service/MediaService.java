package com.sloms.service;

import java.security.Principal;
import java.util.List;

import com.sloms.entity.*;

public interface MediaService {
	void saveMedia(
			Media mediaDAO,
			MediaDetails mediaDetailsDAO,
			MediaClientDetails mediaClientDetailsDAO,
			Notifications notificationsDAO,
			Principal principal);
	
	void updateMedia(
			Media mediaDAO,
			MediaDetails mediaDetailsDAO,
			MediaClientDetails mediaClientDetailsDAO);
	
	List<Media> getAllMedia();
	List<Media> getAllMediaOfLoggedInUser(Principal principal);
	Media getAllMediaByMediaIdAndThreadNotesId(Integer media_id, Integer notes_id);
	Media getMediaById(Integer mediaID);
}
