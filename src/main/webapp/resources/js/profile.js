var stompClient = null;
var money = value => currency(value, { formatWithSymbol: true });

function connectWebSocket(){
    var socket = new SockJS('/sloms_web_socket');
    stompClient = Stomp.over(socket);

    stompClient.connect({}, function(frame) {
		stompClient.subscribe("/sloms/errors", function(message) {
            console.log("Web Socket Error Message : " + message.body);
            connectWebSocket();
        });
        
		stompClient.subscribe("/sloms/all_feedbacks_of_logged_in_user", function(message) {
            $('#orderedList_myFeedback').html('');
            myFeedback();
		});
	},function(error) {
        console.log("Web Socket Error Message : " + error);
    });
}

connectWebSocket();

$.getJSON("/available_groups", function(response) {
	$.each(response, function(key,data) {
		$('#cmbMedia_group').append("<option value='" + data.group_id + "'>" + data.group_name + "</option>");
	});

	$('#cmbMedia_group').selectpicker('refresh');
	$('#cmbMedia_group').selectpicker('render');
});

function myAccount(){
    $("#frmChangePassword").validate({
        ignore : [],
        rules : {
            user_password : {
                required : true,
                minlength : 8,
                maxlength : 16
            },
            're-password' : {
                required : true,
                minlength : 8,
                maxlength : 16,
                equalTo : "#password2"
            }
        }
    });

    function updateUserProfilePicture(){
		$('#frmUpdateProfilePicture').submit(function(event) {
			event.preventDefault();
			var formEl = $(this);

			swal({
				title: "Are you sure?",
					text: "Once updated, you will not be able to undo this action!",
					icon: "warning",
					buttons: ["No, not sure yet!", "Yes, I'm Sure!"],
					dangerMode: true
			})
			.then((willUpdate) => {
				if (willUpdate) {
					$.ajax({
						type: 'POST',
						dataType : 'JSON',
						url: formEl.prop('action'),
						data: formEl.serialize(),
						// if received a response from the server
						success: function( data, textStatus, jqXHR) {
							if(data.isUpdateUserProfilePictureSuccess){
								swal("Profile picture has been successfully updated!", {
									icon: "success"
								}).then((willUpdate) => {
									if(willUpdate){
										location.reload();
									}
								});
							}else{
								iziToast.error({
									title: 'Oops!',
									theme: 'light', // dark
									color: 'blue', // blue, red, green, yellow
									position: 'bottomRight', // bottomRight, bottomLeft, topRight, topLeft, topCenter, bottomCenter, center
									message: 'Failed to update profile picture. Please try again!'
								});
							}
						},
						error: function(e){
							console.log(e);
						}
					});
				}
			});
		});
    }

    function updateUsername(){
        $('#frmChangeUsername').submit(function(event) {
			event.preventDefault();
			var formEl = $(this);

			swal({
				title: "Are you sure?",
					text: "Once updated, you will not be able to undo this action!",
					icon: "warning",
					buttons: ["No, not sure yet!", "Yes, I'm Sure!"],
					dangerMode: true
			})
			.then((willUpdate) => {
				if (willUpdate) {
					$.ajax({
						type: 'POST',
						dataType : 'JSON',
						url: formEl.prop('action'),
						data: formEl.serialize(),
						// if received a response from the server
						success: function( data, textStatus, jqXHR) {
							if(data.isUpdateUsernameSuccess){
								swal("Username has been successfully updated!", {
									icon: "success"
								}).then((willUpdate) => {
									if(willUpdate){
										location.replace("http://localhost:8080/logout");
									}
								});
							}else{
								iziToast.error({
									title: 'Oops!',
									theme: 'light', // dark
									color: 'blue', // blue, red, green, yellow
									position: 'bottomRight', // bottomRight, bottomLeft, topRight, topLeft, topCenter, bottomCenter, center
									message: 'Failed to update username. Please try again!'
								});
							}
						},
						error: function(e){
							console.log(e);
						}
					});
				}
			});
		});
    }

    function updateUserPassword(){
        $('#frmChangePassword').submit(function(event) {
			event.preventDefault();
			var formEl = $(this);

			swal({
				title: "Are you sure?",
					text: "Once updated, you will not be able to undo this action!",
					icon: "warning",
					buttons: ["No, not sure yet!", "Yes, I'm Sure!"],
					dangerMode: true
			})
			.then((willUpdate) => {
				if (willUpdate) {
					$.ajax({
						type: 'POST',
						dataType : 'JSON',
						url: formEl.prop('action'),
						data: formEl.serialize(),
						// if received a response from the server
						success: function( data, textStatus, jqXHR) {
							if(data.isUpdatePasswordSuccess){
								swal("Password has been successfully updated!", {
									icon: "success"
								}).then((willUpdate) => {
									if(willUpdate){
										location.reload();
									}
								});
							}else{
								iziToast.error({
									title: 'Oops!',
									theme: 'light', // dark
									color: 'blue', // blue, red, green, yellow
									position: 'bottomRight', // bottomRight, bottomLeft, topRight, topLeft, topCenter, bottomCenter, center
									message: 'Failed to update password. Please try again!'
								});
							}
						},
						error: function(e){
							console.log(e);
						}
					});
				}
			});
		});
    }

    function updateProfileInformation(){
        $('#frmUpdateProfileInformation').submit(function(event) {
			event.preventDefault();
			var formEl = $(this);

			swal({
				title: "Are you sure?",
					text: "Once updated, you will not be able to undo this action!",
					icon: "warning",
					buttons: ["No, not sure yet!", "Yes, I'm Sure!"],
					dangerMode: true
			})
			.then((willUpdate) => {
				if (willUpdate) {
					$.ajax({
						type: 'POST',
						dataType : 'JSON',
						url: formEl.prop('action'),
						data: formEl.serialize(),
						// if received a response from the server
						success: function( data, textStatus, jqXHR) {
							if(data.isUpdateProfileInformationSuccess){
								swal("Profile information has been successfully updated!", {
									icon: "success"
								}).then((willUpdate) => {
									if(willUpdate){
										location.reload();
									}
								});
							}else{
								iziToast.error({
									title: 'Oops!',
									theme: 'light', // dark
									color: 'blue', // blue, red, green, yellow
									position: 'bottomRight', // bottomRight, bottomLeft, topRight, topLeft, topCenter, bottomCenter, center
									message: 'Failed to update profile information. Please try again!'
								});
							}
						},
						error: function(e){
							console.log(e);
						}
					});
				}
			});
		});
    }

    updateUserProfilePicture();
    updateUsername();
    updateUserPassword();
    updateProfileInformation();
}

function myMedia(){
    $.getJSON("/groups", function(response) {
        $.each(response, function(key,data) {
            $('#frmUpdateDeleteMedia_cmbMediaGroup').append("<option value='" + data.group_id + "'>" + data.group_name + "</option>");
        });
        
        $('#frmUpdateDeleteMedia_cmbMediaGroup').selectpicker('refresh');
        $('#frmUpdateDeleteMedia_cmbMediaGroup').selectpicker('render');
    });

    function retrieveDataToUpdateGroupModal(data){
        //label
        $('#frmUpdateDeleteMedia_lblMediaName').text(data.media_name);
        
        $('#frmUpdateDeleteMedia_lblMedia_id').val(data.media_id);
        
        //image
        if(data.media_preview_image == ""){
            $('#frmUpdateDeleteMedia_imgMediaPreviewImage').attr('src',"/resources/img/no-image-available.jpg");	
        }else{
            $('#frmUpdateDeleteMedia_imgMediaPreviewImage').attr('src',"/SLOMS_server" + data.media_preview_image);
        }

        //textboxes,textarea
        $('#frmUpdateDeleteMedia_txtMediaName').val(data.media_name);
        $('#frmUpdateDeleteMedia_txtMediaCode').val(data.media_code);
        $('#frmUpdateDeleteMedia_txtMediaDescription').val(data.mediaDetails.media_description);
        $('#frmUpdateDeleteMedia_txtMediaPrice').val(data.mediaDetails.media_price);
        $('#frmUpdateDeleteMedia_txtMediaLocation').val(data.mediaDetails.media_location);
        $('#frmUpdateDeleteMedia_txtMediaRegion').val(data.mediaDetails.media_region);
        $('#frmUpdateDeleteMedia_txtMediaCity').val(data.mediaDetails.media_city);
        $('#frmUpdateDeleteMedia_txtMediaSize_h').val(data.mediaDetails.media_size_h);
        $('#frmUpdateDeleteMedia_txtMediaSize_w').val(data.mediaDetails.media_size_w);
        $('#frmUpdateDeleteMedia_txtMediaClient').val(data.mediaClientDetails.media_client);

        $('#frmUpdateDeleteMedia_txtMediaContractedRate').val(data.mediaClientDetails.media_contracted_rate);
        $('#frmUpdateDeleteMedia_txtMediaContractStart').val(data.mediaClientDetails.media_contract_start);
        $('#frmUpdateDeleteMedia_txtMediaExpiration').val(data.mediaClientDetails.media_expiration);

        String.prototype.replaceAll = function (searchText, replacementText) {
            return this.split(searchText).join(replacementText);
        };
        
        
        //select
        $('#frmUpdateDeleteUser_cmbMedia_Status')
        .removeAttr('selected')
        .find('option[value="' + data.media_status + '"]')
        .attr("selected",true)
        .change();
        
        $('#frmUpdateDeleteMedia_cmbMediaGroup')
            .removeAttr('selected')
            .find('option[value="' + data.mediaDetails.media_group + '"]')
            .attr("selected",true)
            .change();
        
        $('#frmUpdateDeleteMedia_cmbMediaRoadPosition')
            .removeAttr('selected')
            .find("option[value='" + data.mediaDetails.media_road_position.replaceAll(" ","_") + "']")
            .attr("selected",true)
            .change();
        
        $('#frmUpdateDeleteMedia_cmbMediaPanelOrientation')
            .removeAttr('selected')
            .find('option[value="' + data.mediaDetails.media_panel_orientation.replaceAll(" ","_") + '"]')
            .attr("selected",true)
            .change();
        
        $('#frmUpdateDeleteMedia_cmbMediaIllumination')
            .removeAttr('selected')
            .find('option[value="' + data.mediaDetails.media_illumination + '"]')
            .attr("selected",true)
            .change();
        
        $('#frmUpdateDeleteMedia_cmbMediaStructureType')
            .removeAttr('selected')
            .find('option[value="' + data.mediaDetails.media_structure_type.replaceAll(" ","_") + '"]')
            .attr("selected",true)
            .change();
        
        $('#frmUpdateDeleteMedia_cmbMediaTrafficDetails')
            .removeAttr('selected')
            .find('option[value="' + data.mediaDetails.media_traffic_details.replaceAll(" ","_") + '"]')
            .attr("selected",true)
            .change();
    }

    function showMediaDetails(d) {
        retrieveDataToUpdateGroupModal(d);

        var table = "";
    
        table = '<table cellpadding="5" cellspacing="0" border="0" style="padding-left:50px;" class="table table-bordered table-striped display">'+
                    '<tr align="center">';
    
                        var image = "";
    
                        if(d.media_preview_image == null){
                            image = "resources/img/no-image-available.jpg";
                        }else{
                            image = "/SLOMS_server" + d.media_preview_image;
                        }
    
            table += '<td colspan="2"><img src="' + image + '" style="width: 200px; height: 200px;"/></td>'+
                    '</tr>'+
                    '<tr>'+
                        '<td>Media Code:</td>'+
                        '<td><a href="/media_details?media_id=' + d.media_id + '" style="cursor: pointer;" target="_blank">' + d.media_code + '</a></td>'+
                    '</tr>'+
                    '<tr>'+
                        '<td>Location:</td>'+
                        '<td>'+ d.mediaDetails.media_location +'</td>'+
                    '</tr>'+
                    '<tr>'+
                        '<td>Dimensions:</td>'+
                        '<td>'+ d.mediaDetails.media_size_h + 'x' + d.mediaDetails.media_size_w + ' ft.</td>'+
                    '</tr>'+
                    '<tr>'+
                        '<td>Region:</td>'+
                        '<td>'+ d.mediaDetails.media_region +'</td>'+
                    '</tr>'+
                    '<tr>'+
                        '<td>City:</td>'+
                        '<td>'+ d.mediaDetails.media_city +'</td>'+
                    '</tr>'+
                    '<tr>'+
                        '<td>Price:</td>'+
                        '<td>₱' + money(d.mediaDetails.media_price).format(false) + '</td>'+
                    '</tr>'+
                    '<tr>'+
                        '<td>Created:</td>'+
                        '<td>'+ moment(new Date(d.media_dateAndTime_created)).format('LLLL') +'</td>'+
                    '</tr>'+
                    '<tr>'+
                        '<td>Status:</td>'+
                        '<td>'+ d.media_status +'</td>'+
                    '</tr>'+
                    '<tr>';
    
                    var access = "";
    
                    access = '<button type="button" class="btn btn-info btn-lg" data-toggle="modal" data-target="#view_edit_media-modal"><span class="glyphicon glyphicon-edit"></span> Edit Media Information</button>';
    
            table += '<td colspan="2" style="text-align: center;">' + access + '</td>'+
                    '</tr>'+
                '</table>';
    
        return table;
    }
    
    function showAllMyMedia(){
        $("#tblMyMedia tbody").empty();
    
        /* DataTables instantiation. */
        var table = $("#tblMyMedia").DataTable({
            "ajax": {
                "url": "/my_media",
                "dataSrc": ""
            },
            "columns": [
                {
                    "className": 'details-control',
                    "orderable": false,
                    "data": null,
                    "defaultContent": ''
                },
                { 
                    "data": null,
                    render: function (data,type,row) {
                        return '<img src="/SLOMS_server' + data.media_preview_image + '" style="width: 30px; height: 30px; border-radius: 50%;" />&nbsp;&nbsp;<a href="/media_details?media_id=' + data.media_id + '" target="_blank">' + data.media_name + '</a>';
                    }
                }
            ],
            "iDisplayLength": 5,
            "aLengthMenu": [[5, 10, 25, 50, 100, -1], [5, 10, 25, 50, 100, "All"]],
            "pagingType": "full_numbers"
        });
    
        // Add event listener for opening and closing details
        $('#tblMyMedia tbody').on('click', 'td.details-control', function () {
            var tr = $(this).closest('tr');
            var row = table.row(tr);
     
            if (row.child.isShown()) {
                // This row is already open - close it
                row.child.hide();
                tr.removeClass('shown');
            }
            else {
                // Open this row
                row.child(showMediaDetails(row.data())).show();
                tr.addClass('shown');
            }
        });
    }

    function addMedia(){
        $('#frmAddMedia').submit(function(event) {
            event.preventDefault();
            var formEl = $(this);
        
            swal({
                title: "Are you sure?",
                text: "Once added, you will not be able to undo this action!",
                icon: "warning",
                buttons: ["No, not sure yet!", "Yes, I'm Sure!"],
                dangerMode: true
            })
            .then((willAdd) => {
                if (willAdd) {
                    $.ajax({
                        type: 'POST',
                        dataType : 'JSON',
                        url: formEl.prop('action'),
                        data: formEl.serialize(),
                        // if received a response from the server
                        success: function(data) {
                            if(data.status == "mediaNameExists"){
                                 swal("We're sorry!", "There is already a media registered with the media name provided!", "error");
                            }else if(data.status == "mediaCodeExists"){
                                 swal("We're sorry!", "There is already a media registered with the media code provided!", "error");
                            }else if(data.status){
                                iziToast.success({
                                    title: 'Congratulations!',
                                    theme: 'light', // dark
                                    color: 'green', // blue, red, green, yellow
                                    position: 'bottomRight', // bottomRight, bottomLeft, topRight, topLeft, topCenter, bottomCenter, center
                                    message: "Media has been added successfully!"
                                });
        
                                $("#add_new_media-modal").modal('toggle');
                                $('#tblMyMedia').DataTable().ajax.reload();
                            }
                        },
                        error: function(e){
                            console.log(e);
                        }
                    });
                }
            });
        });
    }

    function updateSpecificMedia(){
        $('.frmUpdateDeleteMedia').submit(function(event) {
            event.preventDefault();
            var formEl = $(this);
            
            swal({
                  title: "Are you sure?",
                  text: "Once updated, you will not be able to undo this action!",
                  icon: "warning",
                  buttons: ["No, not sure yet!", "Yes, I'm Sure!"],
                  dangerMode: true
            })
            .then((willUpdate) => {
                if (willUpdate) {
                    $.ajax({
                        type: 'POST',
                        dataType : 'JSON',
                        url: "/update-media",
                        data: formEl.serialize(),
                        // if received a response from the server
                        success: function( data, textStatus, jqXHR) {
                             if(data.isUpdateMediaSuccess){
                                iziToast.success({
                                    title: 'Congratulations!',
                                    theme: 'light', // dark
                                    color: 'green', // blue, red, green, yellow
                                    position: 'bottomRight', // bottomRight, bottomLeft, topRight, topLeft, topCenter, bottomCenter, center
                                    message: "Media has been updated successfully!"
                                });
        
                                $("#view_edit_media-modal").modal('toggle');
                                $('#tblMyMedia').DataTable().ajax.reload();
                             }
                        },
                        error: function(e){
                            console.log(e);
                        }
                    });
                }
            });
        });
    }

    showAllMyMedia();
    addMedia();
    updateSpecificMedia();
}

function myGroup(){
    function showGroupDetails(d) {
        // `d` is the original data object for the row
        var table = "";
        
        $('#frmUpdateGroup_lblGroup_id').val(d.group_id);
        $('#frmUpdateGroup_txtGroup_name').val(d.group_name);
        $('#frmUpdateGroup_txtGroup_description').val(d.group_description);
        $('#frmUpdateGroup_lblGroup_name').text(d.group_name);
        
        $('#frmUpdateGroup_cmbGroup_status')
            .removeAttr('selected')
            .find('option[value="' + d.group_status + '"]')
            .attr("selected",true)
            .change();

        table = '<table cellpadding="5" cellspacing="0" border="0" style="padding-left:50px;" class="table table-bordered table-striped display">'+
                    '<tr align="center">'+
                        '<td colspan="2"><img src="/SLOMS_server'+ d.group_preview_image + '" style="width: 100px; height: 100px;"/></td>'+
                    '</tr>'+
                    '<tr>'+
                        '<td>Description:</td>'+
                        '<td>'+d.group_description+'</td>'+
                    '</tr>'+
                    '<tr>'+
                        '<td>Created:</td>'+
                        '<td>'+ moment(new Date(d.group_dateAndTime_created)).format('LLLL') +'</td>'+
                    '</tr>'+
                    '<tr>'+
                        '<td>Modified:</td>' +
                        '<td>'+ ((d.group_modified == null) ? "None" : moment(new Date(d.group_modified)).format('LLLL')) +'</td>'+
                    '</tr>'+
                    '<tr>'+
                        '<td>Status:</td>'+
                        '<td>'+ d.group_status +'</td>'+
                    '</tr>'+
                    '<tr>';

                    var access = "";

                    access = '<button type="button" class="btn btn-info btn-lg" data-toggle="modal" data-target="#view_edit_group-modal"><span class="glyphicon glyphicon-edit"></span> Edit Group Information</button>';

            table += '<td colspan="2" style="text-align: center;">' + access + '</td>'+
                    '</tr>'+
                '</table>';

        return table;
    }

    function showAllMyGroup(){
        $("#tblMyGroup tbody").empty();

        /* DataTables instantiation. */
        var table = $("#tblMyGroup").DataTable({
            "ajax": {
                "url": "/my_group",
                "dataSrc": ""
            },
            "columns": [
                {
                    "className": 'details-control',
                    "orderable": false,
                    "data": null,
                    "defaultContent": ''
                },
                { 
                    "data": null,
                    render: function (data,type,row) {
                        return '<img src="/SLOMS_server' + data.group_preview_image + '" style="width: 30px; height: 30px;" />&nbsp;&nbsp;' + data.group_name;
                    }
                }
            ],
            "iDisplayLength": 5,
            "aLengthMenu": [[5, 10, 25, 50, 100, -1], [5, 10, 25, 50, 100, "All"]],
            "pagingType": "full_numbers"
        });

        // Add event listener for opening and closing details
        $('#tblMyGroup tbody').on('click', 'td.details-control', function () {
            var tr = $(this).closest('tr');
            var row = table.row(tr);
    
            if (row.child.isShown()) {
                // This row is already open - close it
                row.child.hide();
                tr.removeClass('shown');
            }
            else {
                // Open this row
                row.child(showGroupDetails(row.data())).show();
                tr.addClass('shown');
            }
        });
    }

    function addGroup(){
        $('#frmAddGroup').submit(function(event) {
            event.preventDefault();
            var formEl = $(this);
        
            swal({
                title: "Are you sure?",
                text: "Once added, you will not be able to undo this action!",
                icon: "warning",
                buttons: ["No, not sure yet!", "Yes, I'm Sure!"],
                dangerMode: true
            })
            .then((willAdd) => {
                if (willAdd) {
                    $.ajax({
                        type: 'POST',
                        dataType : 'JSON',
                        url: formEl.prop('action'),
                        data: formEl.serialize(),
                        // if received a response from the server
                        success: function( data, textStatus, jqXHR) {
                            if(data.status == "groupNameExists"){
                                swal("We're sorry!", "Group name exists!", "error");
                            }else if(data.status){
                                iziToast.success({
                                    title: 'Congratulations!',
                                    theme: 'light', // dark
                                    color: 'green', // blue, red, green, yellow
                                    position: 'bottomRight', // bottomRight, bottomLeft, topRight, topLeft, topCenter, bottomCenter, center
                                    message: "Group has been added successfully!"
                                });
        
                                $("#add_new_group-modal").modal('toggle');
                                $('#tblMyGroup').DataTable().ajax.reload();
                            }
                        },
                        error: function(e){
                            console.log(e);
                        }
                    });
                }
            });
        });
    }

    function updateSpecificGroup(){
        $('#frmUpdateGroup').submit(function(event) {
            event.preventDefault();
            var formEl = $(this);
            
            swal({
                title: "Are you sure?",
                text: "Once updated, you will not be able to undo this action!",
                icon: "warning",
                buttons: ["No, not sure yet!", "Yes, I'm Sure!"],
                dangerMode: true
            })
            .then((willUpdate) => {
                if (willUpdate) {
                    $.ajax({
                        type: 'POST',
                        dataType : 'JSON',
                        url: formEl.prop('action'),
                        data: formEl.serialize(),
                        // if received a response from the server
                        success: function( data, textStatus, jqXHR) {
                            if(data.isUpdateGroupSuccess == "groupNameExists"){
                                swal("We're sorry!", "Group name exists!", "error");
                            }else if(data.isUpdateGroupSuccess){
                                iziToast.success({
                                    title: 'Congratulations!',
                                    theme: 'light', // dark
                                    color: 'green', // blue, red, green, yellow
                                    position: 'bottomRight', // bottomRight, bottomLeft, topRight, topLeft, topCenter, bottomCenter, center
                                    message: "Group has been updated successfully!"
                                });
        
                                $("#view_edit_group-modal").modal('toggle');
                                $('#tblMyGroup').DataTable().ajax.reload();
                            }
                        },
                        error: function(e){
                            console.log(e);
                        }
                    });
                }
            });
        });
    }

    showAllMyGroup();

    addGroup();
    updateSpecificGroup();
}

function myThread(){
    $.getJSON("/activated_notes", function(response) {
        $.each(response, function(key,data) {
            $('#viewEdit_cmbNotes_type').append("<option value='" + data.notes_id + "'>" + data.notes_type + "</option>");	
        });
        
        $('#viewEdit_cmbNotes_type').selectpicker('refresh');
        $('#viewEdit_cmbNotes_type').selectpicker('render');
    });

    function retrieveDataToUpdateThreadModal(data){
        $('#lblThread_name').text("Edit " + data.thread_caption + "?");
			
        $('#viewEdit_cmbNotes_type')
            .removeAttr('selected')
            .find('option[value="' + data.thread_notes_id.notes_id + '"]')
            .attr("selected",true)
            .change();
        
        $('#txtThread_caption').val(data.thread_caption);
        
        $('#cmbThread_status')
            .removeAttr('selected')
            .find('option[value="' + data.thread_status + '"]')
            .attr("selected",true)
            .change();
        
        $('#frmUpdateDeleteThread_lblThread_id').val(data.thread_id);
    }

    function showThreadDetails(d) {
        
        retrieveDataToUpdateThreadModal(d);

        var table = "";
    
        table = '<table cellpadding="5" cellspacing="0" border="0" style="padding-left:50px;" class="table table-bordered table-striped display">'+
                    '<tr align="center">';

                    var imageSrc = '';

                    if(d.thread_preview_image == "" || d.thread_preview_image == null){
                        imageSrc = '/resources/img/no-image-available.jpg';
                    }else{
                        imageSrc = 'SLOMS_server' + d.thread_preview_image;
                    }

        table += '<td colspan="2"><img class="img-responsive img-text" src="'+ imageSrc + '" style="width: 400px; height: 200px;"/></td>'+
                    '</tr>'+
                    '<tr>'+
                        '<td>Billboard or Media:</td>'+
                        '<td><a href="/media_details?media_id=' + d.thread_billboard.media_id + '" target="_blank">' + d.thread_billboard.media_name + '</a></td>'+
                    '</tr>'+
                    '<tr>'+
                        '<td>Notes Type:</td>'+
                        '<td>' + d.thread_notes_id.notes_type + '</td>'+
                    '</tr>'+
                    '<tr>'+
                        '<td>Created:</td>'+
                        '<td>'+ moment(new Date(d.thread_dateAndTimeCreated)).format('LLLL') +'</td>'+
                    '</tr>'+
                    '<tr>'+
                        '<td>Modified:</td>' +
                        '<td>'+ ((d.thread_modified == null) ? "None" : moment(new Date(d.thread_modified)).format('LLLL')) +'</td>'+
                    '</tr>'+
                    '<tr>'+
                        '<td>Status:</td>'+
                        '<td>'+ d.thread_status +'</td>'+
                    '</tr>'+
                    '<tr>';

                    var access = "";

                    access = '<button type="button" class="btn btn-info btn-lg" data-toggle="modal" data-target="#view_edit_thread-modal"><span class="glyphicon glyphicon-edit"></span> Edit Thread Information</button>';

            table += '<td colspan="2" style="text-align: center;">' + access + '</td>'+
                    '</tr>'+
                '</table>';

        return table;
    }

    function showAllMyThread(){
        $("#tblMyThread tbody").empty();

        /* DataTables instantiation. */
        var table = $("#tblMyThread").DataTable({
            "ajax": {
                "url": "/my_thread",
                "dataSrc": ""
            },
            "columns": [
                {
                    "className": 'details-control',
                    "orderable": false,
                    "data": null,
                    "defaultContent": ''
                },
                { 
                    "data": null,
                    render: function (data,type,row) {
                        var imageSrc = '';

                        if(data.thread_preview_image == "" || data.thread_preview_image == null){
                            imageSrc = '/resources/img/no-image-available.jpg';
                        }else{
                            imageSrc = 'SLOMS_server' + data.thread_preview_image;
                        }

                        return '<img src="' + imageSrc + '" style="width: 40px; height: 40px; border-radius: 50%;" />&nbsp;&nbsp;' + data.thread_caption;
                    }
                }
            ],
            "iDisplayLength": 5,
            "aLengthMenu": [[5, 10, 25, 50, 100, -1], [5, 10, 25, 50, 100, "All"]],
            "pagingType": "full_numbers"
        });

        // Add event listener for opening and closing details
        $('#tblMyThread tbody').on('click', 'td.details-control', function () {
            var tr = $(this).closest('tr');
            var row = table.row(tr);
    
            if (row.child.isShown()) {
                // This row is already open - close it
                row.child.hide();
                tr.removeClass('shown');
            }
            else {
                // Open this row
                row.child(showThreadDetails(row.data())).show();
                tr.addClass('shown');
            }
        });
    }

    function updateSpecificThread(){
        $('#frmUpdateDeleteThread').submit(function(event) {
            event.preventDefault();
            var formEl = $(this);
            
            swal({
                  title: "Are you sure?",
                  text: "Once updated, you will not be able to undo this action!",
                  icon: "warning",
                  buttons: ["No, not sure yet!", "Yes, I'm Sure!"],
                  dangerMode: true
            })
            .then((willUpdate) => {
                if (willUpdate) {
                    $.ajax({
                        type: 'POST',
                        dataType : 'JSON',
                        url: formEl.prop('action'),
                        data: formEl.serialize(),
                        // if received a response from the server
                        success: function(data) {
                            if(data.isUpdatingThreadSuccess){
                                iziToast.success({
                                    title: 'Congratulations!',
                                    theme: 'light', // dark
                                    color: 'green', // blue, red, green, yellow
                                    position: 'bottomRight', // bottomRight, bottomLeft, topRight, topLeft, topCenter, bottomCenter, center
                                    message: "Thread has been updated successfully!"
                                });
        
                                $("#view_edit_thread-modal").modal('toggle');
                                $('#tblMyThread').DataTable().ajax.reload();
                            }
                        },
                        error: function(e){
                            console.log(e);
                        }
                    });
                }
            });
        });
    }

    showAllMyThread();
    updateSpecificThread();
}

function myFeedback(){
    function showAllMyFeedback(){
        $.ajax({
            url: '/my_feedback',
            dataType: 'json',
            mimeType: 'json',
            success: function(response) {
                $.each(response, function(i, data) {
                    var orderedList = '';
    
                    orderedList = '<li>' + data.feedback + '<br>';
                    orderedList += '<span class="fa fa-comment"></span> ' + data.feedback_type + '<br>';
                    orderedList += '<span class="fa fa-clock-o"></span> ' + timeago().format(data.feedback_dateAndTimeCreated);
                    orderedList += '</li>';
    
                    $('#orderedList_myFeedback').append(orderedList);
                });
            },
            error: function(e){
                console.log(e);
            }
        });
    }
    
    showAllMyFeedback();
}

myAccount();
myMedia();
myGroup();
myThread();
myFeedback();