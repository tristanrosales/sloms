package com.sloms.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.sloms.entity.Users;
import com.sloms.service.MediaService;
import com.sloms.service.NotesService;
import com.sloms.service.UsersService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.logout.SecurityContextLogoutHandler;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.security.Principal;

@Controller
public class MainController{

	@Autowired private UsersService usersService;
	@Autowired private MediaService mediaService;
	@Autowired private NotesService notesService;

	@GetMapping("/")
	public String login (
			Principal principal,
			Model model,
			@RequestParam(value="error", required=false) String error,
			@RequestParam(value="logout", required=false) String logout
	) {

		if(error != null)
			model.addAttribute("msg", "Invalid username and password!");

		if (logout != null)
			model.addAttribute("msg","You've been logged out successfully!");

		return principal == null ? "admin-login-page" : "redirect:/home";
	}

	@GetMapping("/registration_page")
	public String registration_page(Model model){
		int randomPIN = (int)(Math.random()*9000)+1000;
		String randomPassword = "sloms" + randomPIN;

		model.addAttribute("randomPassword",randomPassword);

		return "admin-register-page";
	}

	@GetMapping("/forgot_password_page")
	public String forgot_password_page(){
		return "admin-forgot-password-page";
	}

	@GetMapping("/logout")
	public String logout(HttpServletRequest request, HttpServletResponse response) {
		Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
		
		if(authentication != null) {
			new SecurityContextLogoutHandler().logout(request, response, authentication);
		}

		return "redirect:/?logout";
	}

	@GetMapping("/home")
	public String home(Model model, Principal principal) {
		Users user = usersService.findUserByUsername(principal.getName());
		model.addAttribute("user",user);

		return "home";
	}

	@GetMapping("/media_page")
	public String media_page(Model model, Principal principal) {
		Users user = usersService.findUserByUsername(principal.getName());
		model.addAttribute("user",user);
		model.addAttribute("mediaList",mediaService.getAllMedia());
		model.addAttribute("notes",notesService.getAllNotes());

		return "media-page";
	}

	@GetMapping("/users_page")
	public String users_page(Model model, Principal principal) {
		Users user = usersService.findUserByUsername(principal.getName());
		model.addAttribute("user",user);

		return "users_page";
	}

	@GetMapping("/groups_page")
	public String groups_page(Model model, Principal principal) {
		Users user = usersService.findUserByUsername(principal.getName());
		model.addAttribute("user",user);

		return "groups-page";
	}

	@GetMapping("/threads_page")
	public String threads_page(Model model, Principal principal) {
		Users user = usersService.findUserByUsername(principal.getName());
		model.addAttribute("user",user);

		return "threads-page";
	}

	@GetMapping("/media_details")
	public String mediaDetails_page(Model model, Principal principal) {
		Users user = usersService.findUserByUsername(principal.getName());
		model.addAttribute("user",user);
		model.addAttribute("notes",notesService.getAllNotes());

		return "media-details";
	}

	@GetMapping("/faqs_page")
	public String FAQs_page(Model model, Principal principal) {
		Users user = usersService.findUserByUsername(principal.getName());
		model.addAttribute("user",user);

		return "FAQs-page";
	}

	@GetMapping("/feedbacks_and_help")
	public String feedbacks_and_help_page(Model model, Principal principal){
		Users user = usersService.findUserByUsername(principal.getName());
		model.addAttribute("user",user);

		return "feedbacks_and_help-page";
	}

	@GetMapping("/about")
	public String about_page(Model model, Principal principal) {
		Users user = usersService.findUserByUsername(principal.getName());
		model.addAttribute("user",user);

		return "about-page";
	}

	@GetMapping("/profile")
	public String profile_page(Model model, Principal principal) {
		Users user = usersService.findUserByUsername(principal.getName());
		model.addAttribute("user",user);

		return "profile-page";
	}
}
