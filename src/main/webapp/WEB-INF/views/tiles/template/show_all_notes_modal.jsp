<div class="modal" id="show_all_notes-modal" tabindex="-1" role="dialog" aria-labelledby="largeModalHead" aria-hidden="true" data-backdrop="static" data-keyboard="false">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">
					<span aria-hidden="true">&times;</span><span class="sr-only">Close</span>
				</button>
				<h3 class="modal-title" id="largeModalHead">All Notes</h3>
			</div>
			<div class="modal-body" style="font-size: 15px;">
				<div class="page-content-wrap">
					<div class="row">
						<div class="col-md-12">
							<table id="tblNotes" class="display" style="width:100%;">
								<thead style="background-color: #1caf9a; color: white;">
									<tr>
										<th style="text-align: center;">Details</th>
										<th style="text-align: center;">Type</th>
									</tr>
								</thead>

								<tfoot style="background-color: #1caf9a; color: white;">
									<tr>
										<th style="text-align: center;">Details</th>
										<th style="text-align: center;">Type</th>
									</tr>
								</tfoot>
							</table>
						</div>
					</div>
				</div>
			</div>
			
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
			</div>
		</div>
	</div>
</div>