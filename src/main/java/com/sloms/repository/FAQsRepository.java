package com.sloms.repository;

import java.util.Date;
import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.sloms.entity.FAQs;

@Repository
public interface FAQsRepository extends JpaRepository<FAQs, Integer>{
	@Query("SELECT f FROM FAQs f WHERE f.faqs_status='Enabled' ORDER BY f.faqs_id DESC")
	List<FAQs> getAllFAQs();
	
	@Query("SELECT f FROM FAQs f WHERE f.faqs_question=:faqs_question")
	FAQs findFAQsByFAQsQuestion(@Param("faqs_question") String faqs_question);
	
	@Transactional
	@Modifying
	@Query("UPDATE FAQs f SET "
			+ "f.faqs_question=:faqs_question,"
			+ "f.faqs_answer=:faqs_answer,"
			+ "f.faqs_status=:faqs_status,"
			+ "f.faqs_dateAndTimeModified=:faqs_dateAndTimeModified "
			+ "WHERE f.faqs_id=:faqs_id")
	void updateFAQsInformation(
			@Param("faqs_question") String faqs_question,
			@Param("faqs_answer") String faqs_answer,
			@Param("faqs_status") String faqs_status,
			@Param("faqs_dateAndTimeModified") Date faqs_dateAndTimeModified,
			@Param("faqs_id") Integer faqs_id);
}
