var stompClient = null;
var userID = document.getElementById("loggedInUserId").innerText;
var userRole = document.getElementById("loggedInUserRole").innerText;

var IT_ADMIN = "IT_ADMIN";
var SYS_ADMIN = "SYS_ADMIN";
var SYS_SPECIALIST = "SYS_SPECIALIST";

function connectWebSocket(){
    var socket = new SockJS('/sloms_web_socket');
    stompClient = Stomp.over(socket);

    stompClient.connect({}, function(frame) {
		stompClient.subscribe("/sloms/errors", function(message) {
            console.log("Web Socket Error Message : " + message.body);
            connectWebSocket();
        });
        
		stompClient.subscribe("/sloms/all_notes", function(message) {
			$('#tblNotes').DataTable().ajax.reload();
			$('ul.unorderedList_threads').text("");
			notesFunctionality();
		});

		stompClient.subscribe("/sloms/all_activated_notes", function(message) {
			
		});

		stompClient.subscribe("/sloms/thread", function(message) {
			$('#tblAllDiscussions').DataTable().ajax.reload();
			getThreadsOfSelectedMedia();
		});
		
		stompClient.subscribe("/sloms/thread_status", function(message) {
			$('#tblAllDiscussions').DataTable().ajax.reload();
			getThreadsOfSelectedMedia();
		});
	},function(error) {
        console.log("Web Socket Error Message : " + error);
    });
}

connectWebSocket();

var notes_tabs = "";
var notes_content = "";
var threads_content = "";
var notes_tab_pane = "";
var emptyThreads = "";

//show media details by parameter media_id in url
var getUrlParameter = function getUrlParameter(sParam) {
    var sPageURL = decodeURIComponent(window.location.search.substring(1)),
        sURLVariables = sPageURL.split('&'),
        sParameterName,
        i;

    for (i = 0; i < sURLVariables.length; i++) {
        sParameterName = sURLVariables[i].split('=');

        if (sParameterName[0] === sParam) {
            return sParameterName[1] === undefined ? true : sParameterName[1];
        }
    }
};

var media_id = getUrlParameter('media_id');

String.prototype.replaceAll = function (searchText, replacementText) {
    return this.split(searchText).join(replacementText);
};

$.getJSON('/notes/media_id/' + media_id, function(response) {
	$.each(response, function(key,data) {
		$('#cmbNotes_type').append("<option value='" + data.notes_id + "'>" + data.notes_type + "</option>");
	});

	$('#cmbNotes_type').selectpicker('refresh');
	$('#cmbNotes_type').selectpicker('render');
});

function notesFunctionality(){
	function getAllNotes(){
		$.ajax({
			url: '/notes/media_id/' + media_id,
			dataType: 'json',
			success: function(response) {
				if(response.length <= 0){
					$('#btnAddNewThread').css("display","none");
				}else{
					$('#btnAddNewThread').css("display","inline");
				}

				$.each(response, function(i, data) {
					notes_tabs += "<li>" +
										"<a href='#" + data.notes_type.replaceAll(" ","_").toLowerCase() + "'>" + data.notes_type.toUpperCase() + "</a>" +
									"</li>";
					
					notes_content += "<div class='tab-pane' id='" + data.notes_type.replaceAll(" ","_").toLowerCase() + "' style='font-size: 15px;'>";

					notes_content += '<ul id="unorderedList_' + data.notes_type.replaceAll(" ","_").toLowerCase() + '" class="unorderedList_threads" style="list-style-type: none;"></ul>';
					
					notes_content += "</div>";
				});
	
				getThreadsOfSelectedMedia();
		
				$('#notes_tabs').append(notes_tabs);
				$('#notes_content').append(notes_content);
			},
			error: function(e){
				console.log(e);
			}
		});
	}

	getAllNotes();
}

function threadsFunctionality(){
	$("#frmAddThread").validate({
		ignore : [],
		rules : {
			thread_notes_id: {
				required : true
			},
			thread_caption: {
				required : true,
				minlength : 5,
				maxlength : 50
			}
		},
		submitHandler: function(form) {
			addThread(form);
		}
	});

	function addThread(form){
		var formEl = $(form);
		
		swal({
			title: "Do you still want to continue?",
			icon: "warning",
			buttons: ["No!", "Yes!"],
			dangerMode: true
		})
		.then((willAdd) => {
			if (willAdd) {
				$('body').loadingModal({
					position: 'auto',
					text: 'Please wait while creating this thread...',
					color: '#fff',
					opacity: '0.7',
					backgroundColor: 'rgb(0,0,0)',
					animation: 'doubleBounce'
				});

				$.ajax({
					type: 'POST',
					dataType : 'JSON',
					url: formEl.prop('action'),
					data: formEl.serialize(),
					success: function(data) {
                        $('body').loadingModal('destroy');

						if(data.isAddingThreadSuccess == "threadCaptionExists"){
                            iziToast.error({
                                title: 'Oops! There is an error!',
                                theme: 'light',
                                color: 'red',
                                position: 'bottomRight',
                                message: "There is already a thread registered with the thread caption provided!"
                            });
						}else if(data.isAddingThreadSuccess){
							iziToast.success({
								title: 'Congratulations!',
								theme: 'light', // dark
								color: 'green', // blue, red, green, yellow
								position: 'bottomRight', // bottomRight, bottomLeft, topRight, topLeft, topCenter, bottomCenter, center
								message: "Thread has been added successfully!"
							});

							$("#add_new_thread-modal").modal('toggle');
						}
					},
					error: function(e){
						console.log(e);
                        $('body').loadingModal('destroy');
                        iziToast.error({
                            title: 'Oops! There is an error!',
                            theme: 'light',
                            color: 'red',
                            position: 'bottomRight',
                            message: "Failed to create this thread. Please try again!"
                        });
					}
				});
			}
		});
	}

	function viewOrEditThread(){
		$('body').on('click', '.btnViewEditThread',function(){
			$.ajax({
				url: "/threads/" + $(this).attr('data-id'),
				dataType: 'json',
				mimeType: 'json',
				success: function(data) {
					$('#lblThread_name').text("Edit or Delete " + data.thread_caption + "?");
					
					$('#viewEdit_cmbNotes_type')
						.removeAttr('selected')
						.find('option[value="' + data.thread_notes_id.notes_id + '"]')
						.attr("selected",true)
						.change();
					
					$('#txtThread_caption').val(data.thread_caption);
					
					$('#viewEdit_cmbThread_billboard')
						.removeAttr('selected')
						.find('option[value="' + data.thread_billboard.media_id + '"]')
						.attr("selected",true)
						.change();
					
					$('#cmbThread_status')
						.removeAttr('selected')
						.find('option[value="' + data.thread_status + '"]')
						.attr("selected",true)
						.change();
					
					$('#frmUpdateDeleteThread_lblThread_id').val(data.thread_id);
					$('#frmUpdateDeleteThread_lblThread_notes_id').val(data.thread_notes_id.notes_id);
					$('#frmUpdateDeleteThread_lblThread_createdBy').val(data.thread_created_by.user_id);
					$('#frmUpdateDeleteThread_lblThread_dateAndTimeCreated').val(moment(new Date(data.thread_dateAndTimeCreated)).format('LLLL'));			
				},
				error: function(e){
					console.log(e);
				}
			});
		});
	}

	function deleteThread(){
		swal({
			  title: "Are you sure?",
			  text: "Once deleted, you will not be able to recover this group!",
			  icon: "warning",
			  buttons: ["No, not sure yet!", "Yes, I'm Sure!"],
			  dangerMode: true,
		})
		.then((willDelete) => {
			if (willDelete) {
				$.ajax({
				type: 'POST',
				dataType : 'JSON',
				url: "/frmDeleteThread",
				data: $('#frmUpdateDeleteThread').serialize(),
				// if received a response from the server
				success: function( data, textStatus, jqXHR) {
						 if(data.status){
							 swal("Thread has been deleted successfully!", {
								  icon: "success"
							   }).then((willDelete) => {
								   if(willDelete){
									  location.reload(); 
								   }
							   });
						 }
				},
				error: function(e){
					console.log(e.responseText);
				}
			});
			}
		});
	}

	function updateThread(){
		$('#frmUpdateDeleteThread').submit(function(event) {
			event.preventDefault();
			var formEl = $(this);
			
			swal({
				  title: "Are you sure?",
				  text: "Once updated, you will not be able to undo this action!",
				  icon: "warning",
				  buttons: ["No, not sure yet!", "Yes, I'm Sure!"],
				  dangerMode: true
			})
			.then((willUpdate) => {
				if (willUpdate) {
					$.ajax({
						type: 'POST',
						dataType : 'JSON',
						url: "/frmUpdateThread",
						data: formEl.serialize(),
						// if received a response from the server
						success: function( data, textStatus, jqXHR) {
							if(data.status){
								swal("Thread has been updated successfully!", {
									icon: "success"
								}).then((willUpdate) => {
									if(willUpdate){
										location.reload(); 
									}
								});
							}
						},
						error: function(e){
							console.log(e);
						}
					});
				}
			});
		});
	}

	function updateThreadPreviewImage(){
		$('#frmUpdateThreadPreviewImage').submit(function(event) {
			event.preventDefault();
			var formEl = $(this);
			
			swal({
				  title: "Are you sure?",
				  text: "Once updated, you will not be able to undo this action!",
				  icon: "warning",
				  buttons: ["No, not sure yet!", "Yes, I'm Sure!"],
				  dangerMode: true
			})
			.then((willUpdate) => {
				if (willUpdate) {
					$.ajax({
						type: 'POST',
						dataType : 'JSON',
						url: formEl.prop('action'),
						data: formEl.serialize(),
						// if received a response from the server
						success: function(data) {
							if(data.isUpdatingThreadPreviewImage){
								iziToast.success({
									title: 'Congratulations!',
									theme: 'light', // dark
									color: 'green', // blue, red, green, yellow
									position: 'bottomRight', // bottomRight, bottomLeft, topRight, topLeft, topCenter, bottomCenter, center
									message: "Thread preview image has been updated successfully!"
								});
								
								$("#update_thread_preview_image-modal").modal('toggle');
								$('#tblAllDiscussions').DataTable().ajax.reload();
								getThreadsOfSelectedMedia();
							}
						},
						error: function(e){
							console.log(e);
						}
					});
				}
			});
		});
	}


	function showThreadDetails(d) {
		var table,access;
	
		table = '<table cellpadding="5" cellspacing="0" border="0" style="padding-left:50px;" class="table table-bordered table-striped display">' +
					'<tr>'+
						'<td>Notes Type:</td>'+
						'<td>' + d.thread_notes_id.notes_type + '</td>'+
					'</tr>'+
					'<tr>'+
						'<td>Creator:</td>'+
						'<td>'+d.thread_created_by.user_firstname + " " + d.thread_created_by.user_lastname +'</td>'+
					'</tr>'+
					'<tr>'+
						'<td>Created:</td>'+
						'<td>'+ moment(new Date(d.thread_dateAndTimeCreated)).format('LLLL') +'</td>'+
					'</tr>'+
					'<tr>'+
						'<td>Modified:</td>' +
						'<td>'+ ((d.thread_modified == null) ? "None" : moment(new Date(d.thread_modified)).format('LLLL')) +'</td>'+
					'</tr>'+
					'<tr>'+
						'<td>Status:</td>'+
						'<td>'+ d.thread_status +'</td>'+
					'</tr>' +
					'<tr>' +
						'<td colspan="2" style="text-align: center;">';

							if(userRole == SYS_ADMIN || userRole == SYS_SPECIALIST){
								if(d.thread_status == "Open"){
									access = '<button type="button" class="btn btn-warning btn-lg btn-rounded" style="font-size: 15px; padding: 10px;" onclick="closeSelectedThread(' + d.thread_id + ')"><span class="fa fa-lock"></span> Close This Thread</button> &nbsp;';
								}else if(d.thread_status == "Closed"){
									access = '<button type="button" class="btn btn-danger btn-lg btn-rounded" style="font-size: 15px; padding: 10px;" onclick="openSelectedThread(' + d.thread_id + ')"><span class="fa fa-unlock"></span> Open This Thread</button> &nbsp;';
								}
							}

					table += access;

					table += '<button type="button" class="btn btn-info btn-lg btn-rounded" style="font-size: 15px; padding: 10px;" data-toggle="modal" data-target="#update_thread_preview_image-modal"><span class="fa fa-picture-o"></span> Update Thread Preview Image</button>&nbsp;';

					table += "<button type='button' id='btnViewComments' style='font-size: 15px; padding: 10px;' class='btn btn-info btn-lg btn-rounded' " +
								" data-toggle='modal' data-target='#view_comments-modal' data-id='" + d.thread_id + "'>" +
								"<span class='fa fa-eye'></span> View Comments" +
							"</button>" +
						'</td>' +
					'</tr>' +
				'</table>';
	
		return table;
	}
	
	function showAllDiscussions(){
		$("#tblAllDiscussions tbody").empty();
	
		/* DataTables instantiation. */
		var table = $("#tblAllDiscussions").DataTable({
			"ajax": {
				"url": "/media/" + media_id,
				"dataSrc": "threads",
				error: function(){
					$('#tblAllDiscussions').parents('div.dataTables_wrapper').first().hide();
					$('#no-data-available-for-this-selection').css("display","inline");
				}
			},
			"columns": [
				{
					"className": 'details-control',
					"orderable": false,
					"data": null,
					"defaultContent": '',
					"searchable": false
				},
				{ 
                    "data": null,
                    render: function (data,type,row) {
                        var imageSrc = '';

                        if(data.thread_preview_image == "" || data.thread_preview_image == null){
                            imageSrc = '/resources/img/no-image-available.jpg';
                        }else{
                            imageSrc = 'SLOMS_server' + data.thread_preview_image;
                        }

                        return '<img src="' + imageSrc + '" style="width: 30px; height: 30px; border-radius: 50%;" />&nbsp;&nbsp;' + data.thread_caption;
                    }
                }
			],
			"iDisplayLength": 5,
			"aLengthMenu": [[5, 10, 25, 50, 100, -1], [5, 10, 25, 50, 100, "All"]],
			"pagingType": "full_numbers"
		});
	
		// Add event listener for opening and closing details
		$('#tblAllDiscussions tbody').on('click', 'td.details-control', function () {
			var tr = $(this).closest('tr');
			var row = table.row(tr);
	 
			if (row.child.isShown()) {
				row.child.hide();
				tr.removeClass('shown');
			}
			else {
				row.child(showThreadDetails(row.data())).show();
				tr.addClass('shown');
			}
		});
	}

	viewOrEditThread();
	viewOrEditThread();
	updateThread();
	updateThreadPreviewImage();

	showAllDiscussions();
}

function getThreadsOfSelectedMedia(){
	$('ul.unorderedList_threads').text("");

	$.ajax({
		url: '/media/' + media_id,
		dataType: 'json',
		success: function(response) {
			$.each(response.threads, function(index, element) {
				var imageSource = '';
				var thread_status = '';
				var access = "";

				if(element.thread_preview_image == "" || element.thread_preview_image == null){
					imageSource = '/resources/img/no-image-available.jpg';
				}else{
					imageSource = '/SLOMS_server' + element.thread_preview_image;
				}

				if(element.thread_status == "Open"){
					thread_status = '<span class="fa fa-unlock"></span> ' + element.thread_status;
				}else if(element.thread_status == "Closed"){
					thread_status = '<span class="fa fa-lock"></span> ' + element.thread_status;
				}

				if(userRole == "SYS_ADMIN" || userRole == "SYS_SPECIALIST"){
					if(element.thread_status == "Open"){
						access = '&nbsp;<button type="button" class="btn btn-warning" onclick="closeSelectedThread(' + element.thread_id + ')"><span class="fa fa-lock"></span> Close This Thread</button> &nbsp;';
					}else if(element.thread_status == "Closed"){
						access = '&nbsp;<button type="button" class="btn btn-danger" onclick="openSelectedThread(' + element.thread_id + ')"><span class="fa fa-unlock"></span> Open This Thread</button> &nbsp;';
					}
				}

				threads_content = 
						'<li style="padding: 10px; overflow: auto; border: groove; border-color: #1caf9a; margin-bottom: 10px;" class="list_threads">' +
							'<img src="' + imageSource + '" class="img-responsive" style="margin-right: 1.5em; float: left; max-height: 50px; height: 50px; max-width: 50px; width: 50px;">' +
							'<h4><strong>' + element.thread_caption + '</strong> by <b>' + element.thread_created_by.user_firstname + " " + element.thread_created_by.user_lastname + '</b></h4>' +
							'<p style="overflow: hidden;"><span class="glyphicon glyphicon-time"></span> ' + moment(new Date(element.thread_dateAndTimeCreated)).format('LLLL') + ' | <b>' + timeago().format(element.thread_dateAndTimeCreated) + '</b>' +
							'<br>' + thread_status + "" + access +						
							'<button type="button" id="btnViewComments" class="btn btn-default btn-rounded pull-right" data-toggle="modal" data-target="#view_comments-modal" data-id="' + element.thread_id + '"><span class="fa fa-eye"></span> View Comments &RightArrow;</button></p>' +					
						'</li>'

				$("#unorderedList_" + element.thread_notes_id.notes_type.replaceAll(" ","_").toLowerCase()).append(threads_content);				
			});
		},
		error: function(e){
			console.log(e);
		}
	});
}

notesFunctionality();
threadsFunctionality();

function openSelectedThread(thread_id){
	var values = {
		'thread_status': 'Open',
		'thread_id': thread_id
	};

	swal({
		title: "Do you still want to continue?",
		icon: "warning",
		buttons: ["No!", "Yes!"],
		dangerMode: true
	})
	.then((willUpdate) => {
		if (willUpdate) {
			$.ajax({
				url: "/update-thread-status",
				type: "POST",
				dataType : 'JSON',
				data: values,
				success: function(data) {
					if(data.isUpdatingThreadStatusSuccess){
						iziToast.success({
							title: 'Congratulations!',
							theme: 'light', // dark
							color: 'green', // blue, red, green, yellow
							position: 'bottomRight', // bottomRight, bottomLeft, topRight, topLeft, topCenter, bottomCenter, center
							message: "Thread has been opened successfully!"
						});
					}
				},
				error: function(e){
					console.log(e);
				}
			});
		}
	});
}

function closeSelectedThread(thread_id){
	var values = {
		'thread_status': 'Closed',
		'thread_id': thread_id
	};

	swal({
		title: "Do you still want to continue?",
		icon: "warning",
		buttons: ["No!", "Yes!"],
		dangerMode: true
	})
	.then((willUpdate) => {
		if (willUpdate) {
			$.ajax({
				url: "/update-thread-status",
				type: "POST",
				dataType : 'JSON',
				data: values,
				success: function(data) {
					if(data.isUpdatingThreadStatusSuccess){
						iziToast.success({
							title: 'Congratulations!',
							theme: 'light', // dark
							color: 'green', // blue, red, green, yellow
							position: 'bottomRight', // bottomRight, bottomLeft, topRight, topLeft, topCenter, bottomCenter, center
							message: "Thread has been closed successfully!"
						});
					}
				},
				error: function(e){
					console.log(e);
				}
			});
		}
	});
}

$('#panel_tabs').on('click','#notes_tabs a',function (e) {
 	e.preventDefault();
 	var url = $(this).attr("a href");

	if (typeof url !== "undefined") {
		var pane = $(this), href = this.hash;

		// ajax load from data-url
		$(href).load(url,function(result){      
			pane.tab('show');
		});
	} else {
		$(this).tab('show');
	}
});

$('body').on('click', '#btnViewComments',function(){
	$('#frmAddComment_lblThread_id').val($(this).attr('data-id'));
});