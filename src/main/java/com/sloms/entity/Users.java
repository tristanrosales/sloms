package com.sloms.entity;

import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.*;
import org.hibernate.validator.constraints.NotEmpty;

@Getter
@Setter
@ToString
@EqualsAndHashCode
@NoArgsConstructor
@Entity
public class Users{
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Integer user_id;
	@NotNull @Column(unique = true) private String username;
	@NotNull private String user_password;
	@NotNull private String user_firstname;
	@NotNull private String user_lastname;
	@NotNull @Column(unique = true) private String user_email_address;
	private String user_phone_number;
	@NotNull private String user_gender;
	@Column(length=5000) private String user_profile_picture;
	@NotNull private String user_role;
	private String user_status;
	private String user_sms_notif;
	private String user_email_notif;

	@Temporal(TemporalType.TIMESTAMP)
	private Date user_dateAndTimeApproved;

	@Temporal(TemporalType.TIMESTAMP)
	private Date user_dateAndTimeCreated;

	@OneToMany(cascade = CascadeType.ALL,
            fetch = FetchType.LAZY,
            mappedBy = "notes_created_by")
	@JsonIgnoreProperties("notes_created_by")
	@JsonIgnore
	private List<Notes> notes;
	
	@OneToMany(cascade = CascadeType.ALL,
            fetch = FetchType.LAZY,
            mappedBy = "thread_created_by")
	@JsonIgnoreProperties("thread_created_by")
	@JsonIgnore
	private List<Threads> threads;
	
	@OneToMany(cascade = CascadeType.ALL,
            fetch = FetchType.LAZY,
            mappedBy = "thread_comment_by")
	@JsonIgnoreProperties("thread_comment_by")
	@JsonIgnore
	private List<ThreadComments> threadComments;
	
	@OneToMany(cascade = CascadeType.ALL,
            fetch = FetchType.LAZY,
            mappedBy = "thread_reply_by")
	@JsonIgnoreProperties("thread_reply_by")
	@JsonIgnore
	private List<ThreadCommentReplies> threadCommentReplies;
	
	@OneToMany(cascade = CascadeType.ALL,
            fetch = FetchType.LAZY,
            mappedBy = "media_created_by")
	@JsonIgnoreProperties("media_created_by")
	@JsonIgnore
	private List<Media> medias;
	
	@OneToMany(cascade = CascadeType.ALL,
            fetch = FetchType.LAZY,
            mappedBy = "group_created_by")
	@JsonIgnoreProperties("group_created_by")
	@JsonIgnore
	private List<Groups> groups;
	
	@OneToMany(cascade = CascadeType.ALL,
            fetch = FetchType.LAZY,
            mappedBy = "feedback_created_by")
	@JsonIgnoreProperties("feedback_created_by")
	@JsonIgnore
	private List<Feedbacks> feedbacks;
	
	@OneToMany(cascade = CascadeType.ALL,
            fetch = FetchType.LAZY,
            mappedBy = "faqs_created_by")
	@JsonIgnoreProperties("faqs_created_by")
	@JsonIgnore
	private List<FAQs> faqs;
	
	@OneToMany(cascade = CascadeType.ALL,
            fetch = FetchType.LAZY,
            mappedBy = "changelog_added_by")
	@JsonIgnoreProperties("changelog_added_by")
	@JsonIgnore
	private List<Changelog> changelogs;
	
	@OneToMany(
			cascade = CascadeType.ALL,
			fetch = FetchType.LAZY,
			mappedBy = "users")
	@JsonIgnore
	private List<Notifications> notifications;

}
