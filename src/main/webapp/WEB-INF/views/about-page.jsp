<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<!-- PAGE CONTENT -->
<div class="page-content">

	<tiles:insertAttribute name="x_navigationVertical" />

	<!-- START BREADCRUMB -->
	<ul class="breadcrumb">
		<li><a href="${pageContext.request.contextPath}/home">Home</a></li>
		<li class="active">About</li>
	</ul>
	<!-- END BREADCRUMB -->

	<!-- PAGE TITLE -->
	<div class="page-title">
		<h2>
			<span class="fa fa-info-circle"></span> About The System
		</h2>
	</div>
	<!-- END PAGE TITLE -->

	<!-- PAGE CONTENT WRAPPER -->
	<div class="page-content-wrap">
		<div class="row">
			<div class="col-md-12" style="text-align: center;">
				<p style="font-size: 18px; text-align: justify; text-indent: 50px;">
					<strong>Sign Language Outdoor Media System, Inc. (SLOMS, Inc.)</strong>	currently uses Microsoft Excel to monitor their media and groups by manually 
					browsing through inputted data, by rows or columns, which can be tedious for most employees. When an employee needs to check the status 
					of a billboard, one needs to open a different file or document, which goes the same for checking on current groups and contract details. 
					Communication between employees are strewn from different mediums; through a short messaging system, an e-mail, a chat message from a web 
					or mobile application, even through voice or video call.
				</p>

				<p style="font-size: 18px; text-align: justify; text-indent: 50px;">
					The proponents have been offered to create a management system that can revolutionize the company's business flow in 
					terms of media and group management. It will also enable employees to communicate with each other with ease and structure, 
					which can be referenced when needed. Through SMS or e-mail notifications, employees will be given the convenience of being 
					updated with whatever changes that may occur within the website, a user of the website, a media, a group, or a recent matter at hand.
				</p>

				<hr width="100%" style="border: solid;">

				<p style="font-size: 18px; text-align: center;">
					<b>The Researchers:</b><br><br>
					Maria Cristina A. Hewell <b>(Project Manager)</b><br>
					George Edward P. Dongallo <b>(Content Writer | Software Tester)</b><br>
					Tristan Jules B. Rosales <b>(Web Developer | Web Designer)</b><br>
				</p>

				<hr width="100%" style="border: solid;">

			</div>
		</div>

	</div>
	<!-- END PAGE CONTENT WRAPPER -->

</div>
<!-- END PAGE CONTENT -->