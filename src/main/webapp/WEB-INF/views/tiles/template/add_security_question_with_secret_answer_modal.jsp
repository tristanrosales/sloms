<div class="modal" id="add_security_question_with_secret_answer-modal" tabindex="-1" role="dialog" aria-labelledby="largeModalHead" aria-hidden="true" data-backdrop="static" data-keyboard="false">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<form class="form-horizontal frmAddSecurityQuestionWithSecretAnswer" role="form" action="/add-security-question-with-secret-answer" method="POST">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">
						<span aria-hidden="true">&times;</span><span class="sr-only">Close</span>
					</button>
					<h3 class="modal-title" id="largeModalHead"><span class="fa fa-lock"></span> Add Security Question with Secret Answer</h3>
				</div>

				<div class="modal-body" style="max-height: calc(100vh - 200px); overflow-y: auto;">
					<input type="hidden" id="lblProfile_user_id" name="user_id">
					
					<div class="form-group">
						<label class="col-md-2 control-label">Security question</label>
						<div class="col-md-10">
							<select class="form-control select" data-live-search="true"
								required="required" name="user_security_question">
								<option disabled="disabled" selected="selected">---CHOOSE SECURITY QUESTION---</option>
								<option value="In_which_city_where_you_born?">In which city where you born?</option>
								<option value="In_which_state_where_you_born?">In which state where you born?</option>
								<option value="In_which_province_were_you_born?">In which province were you born?</option>
								<option value="What_is_the_name_of_your_favorite_cousin?">What is the name of your favorite cousin?</option>
								<option value="What_is_the_name_of_the_street_where_you_grew_up?">What is the name of the street where you grew up?</option>
								<option value="Who_was_your_childhood_hero?">Who was your childhood hero?</option>
								<option value="What_was_the_name_of_your_elementary_school?">What was the name of your elementary school?</option>
								<option value="What_was_the_name_of_your_primary_school?">What was the name of your primary school?</option>
							</select> <span class="help-block">Required security question</span>
						</div>
					</div>
					
					<div class="form-group">
						<label class="col-md-2 control-label">Secret answer</label>
						<div class="col-md-10">
							<input 
								type="password" 
								class="form-control" 
								placeholder="Enter secret answer..." 
								required="required" 
								name="user_secret_answer" 
								style="background-color:white !important;"/>

								<span class="help-block">Required secret answer</span>
						</div>
					</div>
				</div>

				<div class="modal-footer">
					<button
						type="submit" 
						class="btn btn-info btn-lg btn-rounded" 
						style="padding: 10px; font-size: 15px; background-color:#1caf9a; color: white;">
						<strong>
							<span class="glyphicon glyphicon-hand-right"></span> Submit
						</strong>
					</button>

					<button 
						type="button" 
						class="btn btn-warning btn-rounded" 
						style="padding: 10px; font-size: 15px; background-color: orange; color: white;" 
						data-dismiss="modal">
						<strong>
							<span class="glyphicon glyphicon-remove-sign"></span> Close
						</strong>
					</button>
				</div>
			</form>
		</div>
	</div>
</div>