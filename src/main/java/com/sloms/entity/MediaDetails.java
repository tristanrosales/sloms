package com.sloms.entity;

import lombok.*;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.validation.constraints.NotNull;

@Getter
@Setter
@ToString
@EqualsAndHashCode
@NoArgsConstructor
@Entity
public class MediaDetails{
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Integer id;

	@NotNull
	@OneToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="media_id",nullable = false)
	private Media media;

	@NotNull
	private String media_group;

	@NotNull
	@Column(length=5000)
	private String media_description;

	@NotNull
	@Column(length=2000)
	private String media_location;

	@NotNull
	private String media_region;

	@NotNull
	private String media_province;

	@NotNull
	private String media_city;

	@NotNull
	private String media_size_h;

	@NotNull
	private String media_size_w;

	@NotNull
	private String media_unit;

	@NotNull
	private String media_price;

	@NotNull
	private String media_road_position;

	@NotNull
	private String media_panel_orientation;

	@NotNull
	private String media_illumination;

	@NotNull
	private String media_structure_type;

	@NotNull
	private String media_traffic_details;
}
