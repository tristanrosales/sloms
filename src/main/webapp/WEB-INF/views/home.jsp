<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<div class="page-content">

	<tiles:insertAttribute name="x_navigationVertical" />

	<!-- START BREADCRUMB -->
	<ul class="breadcrumb">
		<li><a href="/home">Home</a></li>
		<li class="active">Dashboard</li>
	</ul>
	<!-- END BREADCRUMB -->

	<!-- PAGE CONTENT WRAPPER -->
	<div class="page-content-wrap">
		<div class="row">
			<div class="col-md-8">
				<!-- START TABS -->
				<div class="panel panel-default tabs">
					<ul class="nav nav-tabs" role="tablist">
						<li class="active"><a href="#bulletin_board" role="tab" data-toggle="tab"><span class="fa fa-thumb-tack"></span> BULLETIN BOARD</a></li>
						<li><a href="#all_media" role="tab" data-toggle="tab">ALL MEDIA</a></li>
						<li><a href="#available_media" role="tab" data-toggle="tab">AVAILABLE MEDIA</a></li>
						<li><a href="#changelog" role="tab" data-toggle="tab">CHANGELOG</a></li>
					</ul>
					<div class="panel-body tab-content">
						<div class="tab-pane active" id="bulletin_board">
							<h2>
								<span class="fa fa-thumb-tack"></span> Bulletin Board
								<!--
								<button
									type="button" 
									class="btn btn-info btn-lg btn-rounded" 
									style="padding: 10px; font-size: 15px; background-color:#1caf9a; color: white;" 
									onclick="refreshBulletinBoard()">
									<strong>
										<span class="fa fa-refresh"></span> Refresh
									</strong>
								</button>
								-->
	
								<span id="lblNotificationCount" class="fa fa-bell" style="float: right;">&nbsp;<strong>0</strong></span>
							</h2>

							<div class="panel-body posts" style="background: url('/resources/img/bulletin_board.jpg') no-repeat center; background-size:100% 100%; -webkit-background-size:100% 100%; -moz-background-size:100% 100%; -o-background-size: 100% 100%;">
								<div class="panel-body list-group list-group-contacts scroll" style="max-height: 350px; overflow: scroll; overflow-x: hidden; overflow-y: hidden; height: auto;">
									<div id="bulletin_board_row" class="row">
									<!--
									<a href="#" class="list-group-item">
										<img src="/resources/img/no-image-available.jpg" class="pull-left"/>
										<p><b>Tristan Rosales</b> added a new billboard.</p>
										<span class="contacts-title">February 22, 2018</span>
									</a>
									-->
									</div>
								</div>	
							</div>
						</div>

						<div class="tab-pane" id="all_media">
							<h2>All Media</h2>

							<div class="panel-body posts">
								<div id="all_media_container" class="panel-body list-group list-group-contacts scroll" style="max-height: 500px; overflow: scroll; overflow-x: hidden; overflow-y: hidden; height: auto;">
									<div class="alert alert-danger" role="alert" id="all-media_no_data_available" style="display: none;">
										<button type="button" class="close" data-dismiss="alert"></button>
										<strong>Oops!</strong> No data available for this tab.
									</div>

									<div id="all_media_row" class="row">			
										
									</div>

									<div id="all_media-expiring_in_n_days_row" class="row" style="display: none;">			
										
									</div>
								</div>
							</div>
						</div>

						<div class="tab-pane" id="available_media">
							<h2>Available Media</h2>

							<div class="panel-body posts">
								<div class="panel-body list-group list-group-contacts scroll" style="max-height: 500px; overflow: scroll; overflow-x: hidden; overflow-y: hidden; height: auto;">
									<div id="available_media_row" class="row">
										<div class="alert alert-danger" role="alert" id="available-media_no_data_available" style="display: none;">
											<button type="button" class="close" data-dismiss="alert"></button>
											<strong>Oops!</strong> No data available for this tab.
										</div>
									</div>
								</div>
							</div>
						</div>

						<div class="tab-pane" id="changelog">
							<div class="panel panel-default">
								<div class="panel-heading">
									<c:choose>
										<c:when test="${user.user_role == 'IT_ADMIN'}">
											<button
													type="button"
													class="btn btn-info btn-lg btn-rounded"
													style="padding: 10px; font-size: 15px; background-color:#1caf9a; color: white;"
													data-toggle="modal"
													data-target="#add_changelog-modal">
												<span class="fa fa-plus-circle"></span> <strong>Add New Changelog</strong>
											</button>
										</c:when>
									</c:choose>

									<ul class="panel-controls">
										<li>
											<a href="#" class="panel-collapse">
												<span class="fa fa-angle-down"></span>
											</a>
										</li>
	
										<li>
											<a href="#" class="panel-refresh">
												<span class="fa fa-refresh"></span>
											</a>
										</li>
									</ul>
								</div>
	
								<div class="panel-body" style="font-size: 15px;">
									<!-- <h3 class="push-down-0">General Questions</h3> -->
									<table id="tblChangelog" class="display" style="width:100%;">
										<thead style="background-color: #1caf9a; color: white;">
											<tr>
												<th style="text-align: center;">Details</th>
												<th style="text-align: center;">Version</th>
											</tr>
										</thead>
	
										<tfoot style="background-color: #1caf9a; color: white;">
											<tr>
												<th style="text-align: center;">Details</th>
												<th style="text-align: center;">Version</th>
											</tr>
										</tfoot>
									</table>
								</div>
							</div>

							<!--
							<div class="panel-body posts">
								<div class="panel-body list-group list-group-contacts scroll" style="max-height: 400px; overflow: scroll; overflow-x: hidden; overflow-y: hidden; height: auto;">
									<div class="col-md-12">
										
										
										<div class="panel panel-default">
											<div class="panel-body">
												<h3 class="push-down-0">Changelog</h3>
											</div>

											<div class="panel-body faq">
												<div id="changelog_row" class="row">
													<div class="alert alert-danger" role="alert" id="changelog_no_data_available" style="display: none;">
														<button type="button" class="close" data-dismiss="alert"></button>
														<strong>Oops!</strong> No data available for this tab.
													</div>
												
												<div class="faq-item">
													<div class="faq-title">
														<span class="fa fa-angle-down"></span>How to aliquam at ipsum
														sapien?
													</div>
													<div class="faq-text">
														<h5>Aliquam at ipsum sapien</h5>
														<p>Class aptent taciti sociosqu ad litora torquent per
															conubia nostra, per inceptos himenaeos. Donec adipiscing
															vehicula tortor dapibus adipiscing.</p>
														<p>Nullam quis quam massa. Donec vitae metus tortor.
															Vestibulum vel diam orci. Etiam sollicitudin venenatis justo
															ut posuere. Etiam facilisis est ut ligula ornare accumsan.
															Class aptent taciti sociosqu ad litora torquent per conubia
															nostra, per inceptos himenaeos.</p>
													</div>
												</div>
													
												</div>				
											</div>
										</div>
									</div>
								</div>
							</div-->
						</div>
					</div>
				</div>
				<!-- END TABS -->
			</div>

			<div class="col-md-4">
				<!-- START PROJECTS BLOCK -->
				<div class="panel panel-default">
					<div class="panel-heading">
						<div class="panel-title-box">
							<h3>Statistics</h3>
						</div>                                    
						<ul class="panel-controls" style="margin-top: 2px;">
							<li><a href="#" class="panel-refresh"><span class="fa fa-refresh"></span></a></li>
							<li class="dropdown">
								<a href="#" class="dropdown-toggle" data-toggle="dropdown"><span class="fa fa-cog"></span></a>                                        
								<ul class="dropdown-menu">
									<li><a href="#" class="panel-collapse"><span class="fa fa-angle-down"></span> Collapse</a></li>
								</ul>                                        
							</li>                                        
						</ul>
					</div>
					<div class="panel-body panel-body-table">							
						<div class="table-responsive">
							<table class="table table-bordered table-striped">
								<thead>
									<tr>
										<th width="70%">Description</th>
										<th width="30%" style="text-align: center;">Count</th>
									</tr>
								</thead>
								<tbody>
									<tr>
										<td><strong>Total Number of Billboards</strong></td>
										<td style="text-align: center;"><strong><span id="all_media_count"></span></strong></td>
									</tr>

									<tr>
										<td><strong>Available Sites</strong></td>
										<td style="text-align: center;"><strong><span id="available_media_count"></span></strong></td>
									</tr>

									<tr>
										<td><strong>Live Contracts</strong></td>
										<td style="text-align: center;"><strong><span id="live_contract_count"></span></strong></td>
									</tr>
									
									<tr>
										<td><strong>Soon to Expire (<span id="soon_to_expire_count"></span>)</strong></td>
										<td style="text-align: center;"><strong><button id="btnSoonToExpire_showDetails" class="btn btn-info" data-toggle="modal" data-target="#show_all_soon_to_expire-modal">Show Details</button></strong></td>
									</tr>

									<tr>
										<td><strong>Needs Update (<span id="needs_update_count"></span>)</strong></td>
										<td style="text-align: center;"><button id="btnNeedsUpdate_showDetails" class="btn btn-info" data-toggle="modal" data-target="#show_all_needs_update-modal">Show Details</button></td>
									</tr>
								</tbody>
							</table>
						</div>
					</div>
				</div>
				<!-- END PROJECTS BLOCK -->
			</div>
		</div>

		<div class="row">
			<div class="common-modal modal fade" id="common-Modal1"
				tabindex="-1" role="dialog" aria-hidden="true">
				<div class="modal-content">
					<ul class="list-inline item-details">
						<li>
							<a href="http://themifycloud.com/downloads/janux-premium-responsive-bootstrap-admin-dashboard-template/">Admin templates</a>
						</li>

						<li>
							<a href="http://themescloud.org">Bootstrap themes</a>
						</li>
					</ul>
				</div>
			</div>
		</div>

		<!-- START DASHBOARD CHART -->
		<div class="chart-holder" id="dashboard-area-1" style="height: 200px;"></div>
		<div class="block-full-width"></div>
		<!-- END DASHBOARD CHART -->

	</div>
	<!-- END PAGE CONTENT WRAPPER -->
</div>
<!-- END PAGE CONTENT -->

<tiles:insertAttribute name="add_changelog_modal" />
<tiles:insertAttribute name="view_edit_changelog_modal" />
<tiles:insertAttribute name="show_all_needs_update_modal" />
<tiles:insertAttribute name="show_all_soon_to_expire_modal" />

<script type="text/javascript" src="/resources/js/soon_to_expire-and-needs_update_media.js"></script>
<script type="text/javascript" src="/resources/js/bulletin_board.js"></script>
<script type="text/javascript" src="/resources/js/Changelog.js"></script>