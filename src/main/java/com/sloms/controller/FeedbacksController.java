package com.sloms.controller;

import java.security.Principal;
import java.util.Date;
import java.util.List;

import com.sloms.service.UsersService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.web.bind.annotation.*;

import com.google.gson.JsonObject;
import com.sloms.entity.Feedbacks;
import com.sloms.entity.Notifications;
import com.sloms.entity.Users;
import com.sloms.service.FeedbacksService;

@RestController
public class FeedbacksController {

	@Autowired private FeedbacksService feedbacksService;
	@Autowired private UsersService usersService;
	@Autowired private SimpMessagingTemplate template;
	private JsonObject status = new JsonObject();

	@PostMapping("/add-feedback")
	@ResponseBody
	public String addFeedback(
			@ModelAttribute("feedbacks") Feedbacks feedbacks, 
			@ModelAttribute("notifications") Notifications notifications,
			Principal principal) {

		Users loggedInUser = usersService.findUserByUsername(principal.getName());
		
		final Date dateAndTimeToday = new Date();
		
		feedbacks.setFeedback_created_by(loggedInUser);
		feedbacks.setFeedback_dateAndTimeCreated(dateAndTimeToday);
		feedbacksService.saveFeedback(feedbacks,notifications);
		
		status.addProperty("status", true);
		
		template.convertAndSend("/sloms/all_feedbacks", "all_feedbacks");
		template.convertAndSend("/sloms/all_feedbacks_of_logged_in_user", "all_feedbacks_of_logged_in_user");
		
		return status.toString();
	}

	@GetMapping("/feedbacks")
	public List<Feedbacks> getAllFeedbacks(){
		return feedbacksService.getAllFeedbacks();
	}

	@GetMapping("/my_feedback")
	public List<Feedbacks> getAllFeedbacksOfLoggedInUser(Principal principal){
		return feedbacksService.getAllFeedbacksOfLoggedInUser(principal);
	}
}
