package com.sloms.controller;

import java.security.Principal;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpSession;

import com.sloms.service.UsersService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.google.gson.JsonObject;
import com.sloms.entity.Media;
import com.sloms.entity.Notes;
import com.sloms.entity.Notifications;
import com.sloms.entity.Users;
import com.sloms.repository.MediaRepository;
import com.sloms.repository.NotesRepository;
import com.sloms.repository.UsersRepository;
import com.sloms.service.NotesService;

@RestController
public class NotesController {
	@Autowired private NotesRepository notesRepository;
	@Autowired private NotesService notesService;
	@Autowired private UsersService usersService;
	@Autowired private MediaRepository mediaRepository;
	@Autowired private SimpMessagingTemplate template;
	private JsonObject status = new JsonObject();

	@RequestMapping(path="/notes", method=RequestMethod.GET)
	public List<Notes> getAllNotes(){
		return notesService.getAllNotes();
	}
	
	@RequestMapping(path="/notes/media_id/{media_id}", method=RequestMethod.GET)
	public List<Notes> getAllNotesByNotesBillboardMediaId(@PathVariable("media_id") Integer media_id){
		return notesRepository.getAllNotesByNotesBillboardMediaId(media_id);
	}
	
	@RequestMapping(path="/activated_notes", method=RequestMethod.GET)
	public List<Notes> getAllActivatedNotes(){
		return notesService.getAllActivatedNotes();
	}
	
	@RequestMapping(value = "/notes/{notesID}", method = RequestMethod.GET)
	public Notes getNotesById(@PathVariable("notesID") Integer notesID) {
		return notesService.getNotesById(notesID);
	}
	
	@RequestMapping(value="/add-notes",method=RequestMethod.POST)
	@ResponseBody
	public String addNotes(
			@RequestParam Integer notes_billboard,
			@ModelAttribute("notes") Notes notes,
			@ModelAttribute("notifications") Notifications notifications,
			Principal principal) {
		Users loggedInUser = usersService.findUserByUsername(principal.getName());
		Media mediaID = mediaRepository.findMediaByMediaId(notes_billboard);
		
		final Date dateAndTimeCreated = new Date();
		
		notes.setNotes_billboard(mediaID);
		notes.setNotes_created_by(loggedInUser);
		notes.setNotes_dateAndTimeCreated(dateAndTimeCreated);
		notes.setNotes_status("Activated");
		
		notesService.saveNotes(notes, notifications);
		
		status.addProperty("isAddingNotesSuccess", true);
		
		template.convertAndSend("/sloms/bulletin_board","bulletin_board");
		template.convertAndSend("/sloms/all_notes","all_notes");
		template.convertAndSend("/sloms/all_activated_notes","all_activated_notes");
		
		return status.toString();
	}
	
	@RequestMapping(value="/update-notes",method=RequestMethod.POST)
	@ResponseBody
	public String updateNotes(@ModelAttribute("notes") Notes notes) {
		notesService.updateNotes(notes);
		status.addProperty("isUpdateNotesSuccess", true);
		
		template.convertAndSend("/sloms/bulletin_board","bulletin_board");
		template.convertAndSend("/sloms/all_notes","all_notes");
		template.convertAndSend("/sloms/all_activated_notes","all_activated_notes");
		
		return status.toString();
	}
	
	@RequestMapping(value="/frmDeleteNotes",method=RequestMethod.POST)
	@ResponseBody
	public String deleteNotes(@ModelAttribute("notes") Notes notes) {
		Notes notesIdExists = notesRepository.findNotesByNotesId(notes.getNotes_id());
		
		if(notesIdExists != null) {
			notesRepository.delete(notesIdExists);
			status.addProperty("status", true);
		}
		
		return status.toString();
	}
}
