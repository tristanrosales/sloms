<div class="modal" id="view_edit_thread-modal" tabindex="-1" role="dialog" aria-labelledby="largeModalHead" aria-hidden="true">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">
					<span aria-hidden="true">&times;</span><span class="sr-only">Close</span>
				</button>
				<h3 class="modal-title" id="largeModalHead"><span id="lblThread_name"></span></h3>
			</div>
			<div class="modal-body">
				<form id="frmUpdateDeleteThread" class="form-horizontal" role="form" method="POST" action="/update-thread">
					<input type="hidden" id="frmUpdateDeleteThread_lblThread_id" name="thread_id">

					<div class="form-group">
						<label class="col-md-2 control-label">Type</label>
						<div class="col-md-10">
							<select 
								class="form-control select" 
								data-live-search="true"
								name="thread_notes_id" 
								id="viewEdit_cmbNotes_type" 
								required="required">

								<option disabled="disabled" selected="selected">---SELECT TYPE---</option>

							</select> 
							
							<span class="help-block">Required type</span>
						</div>
					</div>

					<div class="form-group">
						<label class="col-md-2 control-label">Name</label>
						<div class="col-md-10">
							<input 
								type="text" 
								class="form-control"
								placeholder="Enter thread caption..." 
								required="required"
								name="thread_caption" 
								id="txtThread_caption" /> 
								
							<span class="help-block">Required thread name</span>
						</div>
					</div>

					<div class="form-group">
						<label class="col-md-2 control-label">Status</label>
						<div class="col-md-10">
							<select 
								class="form-control select" 
								data-live-search="true"
								name="thread_status" 
								id="cmbThread_status" 
								required="required">

								<option disabled="disabled" selected="selected">---SELECT STATUS---</option>
								<option value="Open">Open</option>
								<option value="Closed">Closed</option>

							</select> 
							
							<span class="help-block">Required status</span> <br><br>
							
							<div style="float: right;">
								<button type="submit" id="btnUpdateThread" class="btn btn-info btn-lg btn-rounded">Update</button>
							</div>
						</div>
					</div>
				</form>
			</div>

			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
			</div>
		</div>
	</div>
</div>