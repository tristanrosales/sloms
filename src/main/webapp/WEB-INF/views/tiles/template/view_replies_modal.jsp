<div class="modal" id="view_replies-modal" tabindex="-1" role="dialog"
	aria-labelledby="largeModalHead" aria-hidden="true"
	data-backdrop="static" data-keyboard="false"
	xmlns:th="http://www.thymeleaf.org">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" id="btnCloseRepliesModal" class="close" data-dismiss="modal">
					<span aria-hidden="true">&times;</span><span class="sr-only">Close</span>
				</button>
				<h3 class="modal-title" id="largeModalHead">Comment & Replies</h3>
			</div>
								
			<div id="emptyReply" class="alert alert-danger" role="alert"
				style="display: none; font-size: 15px;">
				<strong>Oops! We're sorry!</strong> No reply available!
			</div>

			<div class="modal-body" style="max-height: calc(100vh - 200px); overflow-y: auto;">
				<ul class="media-list" id="replies">
					<!-- 
					<div class="media">
						<a class="pull-left" href="#"> <img
							class="media-object img-text"
							src="/resources/assets/images/users/user6.jpg" alt="Darh Vader"
							width="64">
						</a>
						<div class="media-body">
							<h4 class="media-heading">Mark Zuckerberg</h4>
							<p>What? What did you say? It's not even a language...</p>
							<p class="text-muted">February 22, 2018, 2:05PM</p>
						</div>
					</div>
					-->
				</ul>
			</div>
			<div class="modal-footer">
				<button type="button" id="btnCloseRepliesModal" class="btn btn-default" data-dismiss="modal">Close</button>
			</div>
		</div>
	</div>
</div>