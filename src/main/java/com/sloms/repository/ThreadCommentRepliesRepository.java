package com.sloms.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.sloms.entity.ThreadCommentReplies;

@Repository
public interface ThreadCommentRepliesRepository extends JpaRepository<ThreadCommentReplies,Integer>{
	@Query("SELECT tcr FROM ThreadCommentReplies tcr WHERE tcr.thread_comment_id.thread_comment_id=:thread_comment_id ORDER BY tcr.thread_reply_id DESC")
	List<ThreadCommentReplies> getAllThreadCommentRepliesByThreadCommentId(@Param("thread_comment_id") Integer thread_comment_id);
	
	@Query("SELECT tcr FROM ThreadCommentReplies tcr WHERE tcr.thread_reply_id=:thread_reply_id")
	ThreadCommentReplies findCommentRepliesByThreadReplyId(@Param("thread_reply_id") Integer thread_reply_id);
}
