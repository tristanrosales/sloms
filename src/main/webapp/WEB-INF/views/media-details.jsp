<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<!-- PAGE CONTENT -->
<div class="page-content">

	<tiles:insertAttribute name="x_navigationVertical" />

	<!-- START BREADCRUMB -->
	<ul class="breadcrumb">
		<li><a href="/home">Home</a></li>
		<li><a href="/media_page">Media</a></li>
		<li class="active">Media Details</li>
	</ul>
	<!-- END BREADCRUMB -->

	<!-- PAGE TITLE -->
	<div class="page-title">
		<h2>
			<span class="fa fa-info-circle"></span> Media Details
		</h2>
	</div>
	<!-- END PAGE TITLE -->

	<!-- PAGE CONTENT WRAPPER -->
	<div class="page-content-wrap">

		<div class="row">
			<div class="col-md-12">
				<div class="panel panel-default">
					<div class="panel-body posts">

						<div class="post-item">
							<div class="post-title">
								<span id="lblMedia_name"></span>
							</div>
							<div class="post-date">
								<span class="fa fa-calendar"></span> <span id="lblMedia_dateAndTimeCreated"></span> / by 
								<a href="#"><span id="lblMedia_createdBy"></span></a>
							</div>

							<div class="col-md-6">
								<img id="imgMediaPreviewImage" class="img-responsive img-text" style='max-height: 220px; height: 220px; max-width: 270px; width: 270px;'/>
								
								<br>
								<div class="post-text">
									<!--CLIENT CONTRACT DETAILS-->
									<h3>CLIENT CONTRACT DETAILS</h3>

									<p style="color: #1caf9a; font-weight: bold;">
										CLIENT: <span id="lblMedia_client" style="color: black;"></span>
									</p>

									<p style="color: #1caf9a; font-weight: bold;">
										CONTRACT START: <span id="lblMedia_contract_start"
											style="color: black;"></span>
									</p>

									<p style="color: #1caf9a; font-weight: bold;">
										EXPIRATION: <span id="lblMedia_expiration"
											style="color: black;"></span>
									</p>

									<p style="color: #1caf9a; font-weight: bold;">
										CONTRACTED RATE: <span id="lblMedia_contracted_rate"
											style="color: black;"></span>
									</p>

									<!--
									<div class="embed-responsive embed-responsive-16by9">
										<iframe
											id="iframe_media-location"
											class="embed-responsive-item"
											width="400"
											height="200"
											frameborder="0"
											style="border:0"
											allowfullscreen>
										</iframe>
									</div>
									-->
								</div>
							</div>

							<div class="col-md-6">
								<div class="post-text">
									<h3>MEDIA DETAILS</h3>

									<p style="color: #1caf9a; font-weight: bold;">
										BILLBOARD NAME: 
										<span
											id="lblMedia_billboardName"
											style="color: black;"></span>
									</p>

									<p style="color: #1caf9a; font-weight: bold;">
										MEDIA CODE: 
										<span
											id="lblMedia_mediaCode"
											style="color: black;"></span>
									</p>

									<p style="color: #1caf9a; font-weight: bold;">
										LOCATION: <span id="lblMedia_location" style="color: black;"></span>&nbsp;&nbsp; <a href="">View Map <b>(UNDER MAINTENANCE)</b></a>
									</p>

									<p style="color: #1caf9a; font-weight: bold; display: none;">
										LATITUDE / LONGITUDE: <span id="lblMedia_latlong"
											style="color: black;"></span>
									</p>

									<p style="color: #1caf9a; font-weight: bold;">
										REGION: <span id="lblMedia_region" style="color: black;"></span>
									</p>

									<p style="color: #1caf9a; font-weight: bold;">
										CITY: <span id="lblMedia_city" style="color: black;"></span>
									</p>

									<p style="color: #1caf9a; font-weight: bold;">
										DIMENSION (HXW): <span id="lblMedia_dimension"
											style="color: black;"></span>
									</p>

									<p style="color: #1caf9a; font-weight: bold;">
										PRICE/MO: <span id="lblMedia_price" style="color: black;"></span>
									</p>

									<p style="color: #1caf9a; font-weight: bold;">
										ROAD POSITION: <span id="lblMedia_road_position"
											style="color: black;"></span>
									</p>

									<p style="color: #1caf9a; font-weight: bold;">
										PANEL ORIENTATION: <span id="lblMedia_panel_orientation"
											style="color: black;"></span>
									</p>

									<p style="color: #1caf9a; font-weight: bold;">
										ILLUMINATION: <span id="lblMedia_illumination"
											style="color: black;"></span>
									</p>

									<p style="color: #1caf9a; font-weight: bold;">
										STRUCTURE TYPE: <span id="lblMedia_structure_type"
											style="color: black;"></span>
									</p>

									<p style="color: #1caf9a; font-weight: bold;">
										TRAFFIC DETAILS: <span id="lblMedia_traffic_details"
											style="color: black;"></span>
									</p>

									<p style="color: #1caf9a; font-weight: bold;">
										CREATED: <span id="lblMedia_created" style="color: black;"></span>
									</p>

									<br><hr width="100%">

									<h3>Photos</h3>
									<div id="mediaPhotosDetector" class="alert alert-info" role="alert" style="font-size: 15px; display: none;">
										<button type="button" class="close" data-dismiss="alert"></button>
										<strong>Oops! We're sorry!</strong><br><br>
										<span>No photos available.</span>
									</div>

									<div class="gallery" id="links_media-images">            
									</div>

								</div>	
							</div>									
						</div>

						<c:choose>
							<c:when test="${fn:length(notes) <= 0}">
								<div class="alert alert-info" role="alert">
									<button type="button" class="close" data-dismiss="alert"></button>
									<strong>Reminder!</strong> You can't add thread unless there's a note added!
								</div>
							</c:when>
						</c:choose>

						<button
								type="button"
								class="btn btn-warning btn-rounded btn-lg btn-rounded"
								style="margin-left: 10px; padding: 10px; font-size: 15px; background-color:orange; color: white;"
								data-toggle="modal"
								data-target="#show_all_notes-modal">
							<span class="fa fa-eye"></span> Show All Notes
						</button>


						<c:choose>
							<c:when test="${user.user_role == 'IT_ADMIN' || user.user_role == 'SYS_ADMIN' || user.user_role == 'SYS_SPECIALIST'}">
								<c:if test="${user.user_role == 'IT_ADMIN' || user.user_role == 'SYS_ADMIN'}">
									<button
											type="button"
											class="btn btn-info btn-rounded btn-lg btn-rounded"
											style="margin-left: 10px; padding: 10px; font-size: 15px; background-color:#1caf9a; color: white;"
											data-toggle="modal"
											data-target="#add_new_note-modal">
										<span class="fa fa-plus-circle"></span> Add New Note
									</button>
								</c:if>

								<c:if test="${fn:length(notes) > 0}">
									<button
											type="button"
											class="btn btn-rounded btn-lg btn-rounded"
											style="margin-left: 10px; padding: 10px; font-size: 15px; background-color:red; color: white;"
											data-toggle="modal"
											data-target="#add_new_thread-modal">
										<span class="fa fa-plus-circle"></span> Add New Thread
									</button>
								</c:if>
							</c:when>
						</c:choose>
						
						<br><br>

						<div class="alert alert-info" role="alert" style="font-size: 15px;">
							<button type="button" class="close" data-dismiss="alert"></button>
							<strong>Reminder!</strong><br>
							<ol>
								<li>You can't add a comment or reply if the thread is <b>CLOSED</b>!</li>
								<li>You can't add a thread if notes is <b>EMPTY</b>!</li>
							</ol> 
						</div>

						<div class="col-md-12">									
							<div id="panel_tabs" class="panel panel-default tabs">
								<ul id='notes_tabs' class='nav nav-tabs' role='tablist'>
									<li class="active"><a href='#all_discussions' role='tab' data-toggle='tab'>ALL DISCUSSIONS</a></li>
								</ul>
							</div>
							
							<div id='notes_content' class='panel-body tab-content'>

								<div class='tab-pane active' id='all_discussions' style="font-size: 15px;">

									<div class="alert alert-danger" role="alert" id="no-data-available-for-this-selection" style="display: none;">
										<button type="button" class="close" data-dismiss="alert"></button>
										<strong>Oops!</strong> No data available for this selection.
									</div>

									<table id="tblAllDiscussions" class="display" style="width:100%">
										<thead style="background-color: #1caf9a; color: white;">
											<tr>
												<th style="text-align: center;">Details</th>
												<th style="text-align: center;">Thread Name</th>
											</tr>
										</thead>
	
										<tfoot style="background-color: #1caf9a; color: white;">
											<tr>
												<th style="text-align: center;">Details</th>
												<th style="text-align: center;">Thread Name</th>
											</tr>
										</tfoot>
									</table>

								</div>

							</div>
						</div>
						
						<br><br><br><br><br><br><br><br>

					</div>
				</div>
			</div>
		</div>

	</div>
	<!-- END PAGE CONTENT WRAPPER -->

</div>
<!-- END PAGE CONTENT -->

<tiles:insertAttribute name="add_new_thread_modal" />
<tiles:insertAttribute name="view_comments_modal" />
<tiles:insertAttribute name="view_replies_modal" />
<tiles:insertAttribute name="add_notes_modal" />
<tiles:insertAttribute name="add_comment_modal" />
<tiles:insertAttribute name="add_reply_modal" />
<tiles:insertAttribute name="show_all_notes_modal" />
<tiles:insertAttribute name="view_edit_notes_modal" />
<tiles:insertAttribute name="edit_comment_or_reply_modal" />

<tiles:insertAttribute name="update_thread_preview_image_modal" />

<script type="text/javascript" src="/resources/js/MediaDetails.js"></script>
<script type="text/javascript" src="/resources/js/Notes.js"></script>
<script type="text/javascript" src="/resources/js/NotesAndThreads.js"></script>
<script type="text/javascript" src="/resources/js/CommentsAndReplies.js"></script>