package com.sloms.entity;

import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.*;

@Getter
@Setter
@ToString
@EqualsAndHashCode
@NoArgsConstructor
@Entity
public class Threads{
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Integer thread_id;
	
	@ManyToOne(fetch=FetchType.LAZY)
    @JoinColumn(name = "thread_notes_id")
	@JsonIgnoreProperties(
			{
				"hibernateLazyInitializer",
				"handler",
				"notes_created_by",
				"threads"
			})
	private Notes thread_notes_id;
	
	@Column(unique=true,length=2000)
	@NotNull
	private String thread_caption;
	
	private String thread_preview_image;
	
	@ManyToOne(fetch=FetchType.LAZY)
    @JoinColumn(name = "thread_billboard")
	@JsonIgnoreProperties(
			{
				"hibernateLazyInitializer",
				"handler",
				"medias",
				"media_created_by",
				"mediaDetails",
				"mediaClientDetails",
				"threads"
			})
	private Media thread_billboard;

	@NotNull
	private String thread_status;
	
	@Temporal(TemporalType.TIMESTAMP)
	private Date thread_dateAndTimeCreated;
	
	@ManyToOne(fetch=FetchType.LAZY)
    @JoinColumn(name = "thread_created_by")
	@JsonIgnoreProperties(
			{
				"hibernateLazyInitializer",
				"handler",
				"threads",
				"user_password",
				"user_role",
				"user_status",
				"user_sms_notif",
				"user_email_notif",
				"user_dateAndTimeCreated"
			})
	private Users thread_created_by;
	
	@Temporal(TemporalType.TIMESTAMP)
	private Date thread_modified;
	
	@OneToMany(cascade = CascadeType.ALL,
            fetch = FetchType.LAZY,
            mappedBy = "thread_id")
	@JsonIgnoreProperties("thread_id")
	private List<ThreadComments> threadComments;
	
	@OneToMany(
			cascade = CascadeType.ALL,
			fetch = FetchType.LAZY,
			mappedBy = "threads")
	@JsonIgnore
	private List<Notifications> notifications;

}
