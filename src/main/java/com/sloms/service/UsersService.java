package com.sloms.service;

import java.util.List;

import com.sloms.classes.UsersDao;
import com.sloms.entity.Users;
import org.springframework.ui.Model;

public interface UsersService {
	void saveUser(Users usersDAO, Model model);
	Users findUserById(Integer user_id);
	Users findUserByEmail(String email);
	Users findUserByUsername(String username);
	List<UsersDao> getAllUsers();
	String approveOrDecline(Integer user_id, String user_status);
	void updateUsername( String username, Integer user_id);
	void updateUserPassword(String user_password, String username);
	void updateProfileInformation(
			String user_firstname,
			String user_lastname,
			String user_email_address,
			String user_phone_number,
			String user_gender,
			String user_sms_notif,
			String user_email_notif,
			Integer user_id);
}
