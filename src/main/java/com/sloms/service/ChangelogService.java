package com.sloms.service;

import com.sloms.entity.Changelog;
import com.sloms.entity.Notifications;

public interface ChangelogService {
	void saveChangelog(Changelog changelogDAO, Notifications notificationsDAO);
	void updateChangelog(
			String changelog_version,
			String changelog_details,
			Integer changelog_id);
}
