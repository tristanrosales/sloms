<div class="modal" id="show_all_soon_to_expire-modal" tabindex="-1" role="dialog" aria-labelledby="largeModalHead" aria-hidden="true" data-backdrop="static" data-keyboard="false">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">
                        <span aria-hidden="true">&times;</span><span class="sr-only">Close</span>
                    </button>
                    <h3 class="modal-title" id="largeModalHead"><span class="fa fa-calendar"></span> Soon To Expire</h3>
                </div>
                <div class="modal-body" style="max-height: calc(100vh - 200px); overflow-y: auto;">
                    <!-- PAGE CONTENT WRAPPER -->
                    <div class="page-content-wrap">
                        <div class="row">
                            <div class="col-md-12">
                                <!-- START ACCORDION -->        
                                <div class="panel-group accordion" id="soon_to_expire_panel-accordion">
                                </div>
                                <!-- END ACCORDION -->
                            </div>
                        </div>
    
                    </div>
                    <!-- PAGE CONTENT WRAPPER -->
                </div>
                <div class="modal-footer">
                    <button 
						type="button" 
						class="btn btn-warning btn-rounded" 
						style="padding: 10px; font-size: 15px; background-color: orange; color: white;" 
						data-dismiss="modal">
						<strong>
							<span class="glyphicon glyphicon-remove-sign"></span> Close
						</strong>
					</button>
                </div>
            </div>
        </div>
    </div>