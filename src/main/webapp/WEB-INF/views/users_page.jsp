<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<!-- PAGE CONTENT -->
<div class="page-content">

    <tiles:insertAttribute name="x_navigationVertical" />

    <!-- START BREADCRUMB -->
    <ul class="breadcrumb">
        <li><a href="${pageContext.request.contextPath}/home">Home</a></li>
        <li class="active">Users</li>
    </ul>
    <!-- END BREADCRUMB -->

    <!-- PAGE TITLE -->
    <div class="page-title">
        <h2><span class="fa fa-users"></span> Users</h2>
    </div>
    <!-- END PAGE TITLE -->

    <!-- PAGE CONTENT WRAPPER -->
    <div class="page-content-wrap">

        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <ul class="panel-controls">
                            <li>
                                <a href="#" class="panel-collapse">
                                    <span class="fa fa-angle-down"></span>
                                </a>
                            </li>

                            <li>
                                <a href="#" class="panel-refresh">
                                    <span class="fa fa-refresh"></span>
                                </a>
                            </li>
                        </ul>
                    </div>
                    <div class="panel-body" style="font-size: 15px;">
                        <table id="tblUsers" class="display" style="width:100%">
                            <thead style="background-color: #1caf9a; color: white;">
                                <tr>
                                    <th style="text-align: center;">Details</th>
                                    <th style="text-align: center;">Name</th>
                                </tr>
                            </thead>

                            <tfoot style="background-color: #1caf9a; color: white;">
                                <tr>
                                    <th style="text-align: center;">Details</th>
                                    <th style="text-align: center;">Name</th>
                                </tr>
                            </tfoot>
                        </table>
                    </div>
                </div>

            </div>
        </div>

    </div>
    <!-- PAGE CONTENT WRAPPER -->
</div>
<!-- END PAGE CONTENT -->

<tiles:insertAttribute name="add_new_user_modal" />
<tiles:insertAttribute name="view_edit_user_modal" />

<script type="text/javascript" src="${pageContext.request.contextPath}/resources/js/Users.js"></script>