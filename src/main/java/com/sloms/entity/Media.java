package com.sloms.entity;

import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.OrderBy;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.*;

@Getter
@Setter
@ToString
@EqualsAndHashCode
@NoArgsConstructor
@Entity
public class Media{
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Integer media_id;

	@NotNull
	@Column(unique=true)
	private String media_name;

	@NotNull
	@Column(unique=true)
	private String media_code;
	private String media_preview_image;
	
	@ElementCollection(fetch=FetchType.EAGER)
	private List<String> media_images;

	@NotNull
	private String media_status;
	
	@Temporal(TemporalType.TIMESTAMP)
	private Date media_dateAndTime_created;

	@NotNull
	@ManyToOne(fetch=FetchType.LAZY)
    @JoinColumn(name = "media_created_by")
	@JsonIgnoreProperties(
			{
				"hibernateLazyInitializer",
				"handler",
				"medias",
				"user_password",
				"user_role",
				"user_status",
				"user_sms_notif",
				"user_email_notif",
				"user_dateAndTimeCreated"
			})
	private Users media_created_by;
	
	@Temporal(TemporalType.TIMESTAMP)
	private Date media_modified;
	
	@OneToOne(fetch = FetchType.LAZY,
            cascade =  CascadeType.ALL,
            mappedBy = "media")
	@JsonIgnoreProperties("media")
	private MediaDetails mediaDetails;
	
	@OneToOne(fetch = FetchType.LAZY,
            cascade =  CascadeType.ALL,
            mappedBy = "media")
	@JsonIgnoreProperties("media")
	private MediaClientDetails mediaClientDetails;
	
	@OneToMany(cascade = CascadeType.ALL,
            fetch = FetchType.LAZY,
            mappedBy = "thread_billboard")
	@JsonIgnoreProperties(
			{
				"thread_billboard",
				"threadComments"
			})
	@OrderBy("thread_id DESC")
	private List<Threads> threads;
	
	@OneToMany(
			cascade = CascadeType.ALL,
			fetch = FetchType.LAZY,
			mappedBy = "media")
	@JsonIgnore
	private List<Notifications> notifications;
	
	@OneToMany(
			cascade = CascadeType.ALL,
            fetch = FetchType.LAZY,
            mappedBy = "notes_billboard")
	@JsonIgnoreProperties(
			{
				"notes_billboard",
				"medias"
			}
	)
	@JsonIgnore
	private List<Notes> notes;

}
