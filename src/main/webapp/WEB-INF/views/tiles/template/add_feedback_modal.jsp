<div class="modal" id="add_feedback-modal" tabindex="-1" role="dialog" aria-labelledby="largeModalHead" aria-hidden="true" data-backdrop="static" data-keyboard="false">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">

			<form id="frmAddFeedback" class="form-horizontal" role="form" action="/add-feedback" method="POST">

				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">
						<span aria-hidden="true">&times;</span><span class="sr-only">Close</span>
					</button>
					<h3 class="modal-title" id="largeModalHead">Add Feedback</h3>
				</div>

				<div class="modal-body" style="max-height: calc(100vh - 200px); overflow-y: auto;">
	                <div class="form-group">
	                    <label class="col-md-2 control-label">Feedback Type</label>
	                    <div class="col-md-10">
	                        <select class="form-control select" data-live-search="true" name="feedback_type" required="required"> 
	                            <option disabled="disabled" selected="selected" value="">---SELECT FEEDBACK TYPE---</option>
	                            <option value="Bug_Report">Bug Report</option>
	                            <option value="Comments">Comments</option>
	                            <option value="Suggestions">Suggestions</option>
	                        </select>
	                    </div>
	                </div>

					<div class="form-group">
						<label class="col-md-2 control-label">Feedback</label>
						<div class="col-md-10">

							<textarea 
								class="form-control" 
								rows="5"
								placeholder="Enter feedback..." 
								required="required"
								name="feedback"
								style="resize: none; background-color:white !important;"></textarea>

							<span class="help-block">Required feedback</span>
						</div>
					</div>
					
					<script>
						$('#add_feedback-modal').on('hidden.bs.modal', function () {
							$('#frmAddFeedback')[0].reset();
						});
					</script>
				</div>

				<div class="modal-footer">
					<button 
						type="submit" 
						class="btn btn-info btn-lg btn-rounded" 
						style="padding: 10px; font-size: 15px; background-color:#1caf9a; color: white;">
						<strong>
							<span class="fa fa-hand-o-right"></span> Submit Feedback
						</strong>
					</button>

					<button 
	                    type="button" 
	                    class="btn btn-warning btn-rounded" 
	                    style="padding: 10px; font-size: 15px; background-color: orange; color: white;" 
	                    data-dismiss="modal">
	                    <strong>
	                        <span class="glyphicon glyphicon-remove-sign"></span> Close
	                    </strong>
	                </button>
				</div>

			</form>
		</div>
	</div>
</div>