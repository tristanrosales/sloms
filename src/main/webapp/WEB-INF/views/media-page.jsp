<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<!-- PAGE CONTENT -->
<div class="page-content">

	<tiles:insertAttribute name="x_navigationVertical" />

	<!-- START BREADCRUMB -->
	<ul class="breadcrumb">
		<li><a href="${pageContext.request.contextPath}/home">Home</a></li>
		<li class="active">Media</li>
	</ul>
	<!-- END BREADCRUMB -->

	<!-- PAGE TITLE -->
	<div class="page-title">
		<h2>
			<span class="fa fa-picture-o"></span> Media
		</h2>
	</div>
	<!-- END PAGE TITLE -->

	<!-- PAGE CONTENT WRAPPER -->
	<div class="page-content-wrap">
		<div class="row">
			<c:choose>
				<c:when test="${fn:length(groups) <= 0}">
					<div class="alert alert-info" role="alert">
						<button type="button" class="close" data-dismiss="alert"></button>
						<strong>Reminder!</strong> You can't add a media or billboard unless there is a group added.
						To add a group, click this <a href="/groups_page" style="color: black; font-weight: bold;">link</a>.
					</div>
				</c:when>
			</c:choose>

			<div class="col-md-12">
				<div class="panel panel-default">
					<div class="panel-heading">
						<c:choose>
							<c:when test="${user.user_role == 'SYS_ADMIN' || user.user_role == 'IT_ADMIN' || fn:length(groups) > 0}">
								<button
										type="button"
										class="btn btn-info btn-lg btn-rounded"
										style="padding: 10px; font-size: 15px; background-color:#1caf9a; color: white;"
										data-toggle="modal"
										data-target="#add_new_media-modal">
									<span class="fa fa-plus-circle"></span> <strong>Add New Media</strong>
								</button>
							</c:when>
						</c:choose>

						<ul class="panel-controls">
							<li>
								<a href="#" class="panel-collapse">
									<span class="fa fa-angle-down"></span>
								</a>
							</li>
							<li>
								<a href="#" class="panel-refresh">
									<span class="fa fa-refresh"></span>
								</a>
							</li>
						</ul>
					</div>
					<div class="panel-body" style="font-size: 15px;">
						<table id="tblMedia" class="display" style="width:100%;">
							<thead style="background-color: #1caf9a; color: white;">
							<tr>
								<th style="text-align: center;">Details</th>
								<th style="text-align: center;">Media Name</th>
							</tr>
							</thead>

							<tfoot style="background-color: #1caf9a; color: white;">
							<tr>
								<th style="text-align: center;">ID</th>
								<th style="text-align: center;">Media Name</th>
							</tr>
							</tfoot>
						</table>
					</div>
				</div>

			</div>
		</div>
	</div>
	<!-- PAGE CONTENT WRAPPER -->
</div>
<!-- END PAGE CONTENT -->

<tiles:insertAttribute name="add_media_modal" />
<tiles:insertAttribute name="view_edit_media_modal" />
<tiles:insertAttribute name="update_media_preview_image_modal" />

<script type="text/javascript" src="${pageContext.request.contextPath}/resources/js/MediaPage.js"></script>






