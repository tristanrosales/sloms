var stompClient = null;

function connectWebSocket(){
    var socket = new SockJS('/sloms_web_socket');
    stompClient = Stomp.over(socket);

    stompClient.connect({}, function(frame) {
		stompClient.subscribe("/sloms/errors", function(message) {
            console.log("Web Socket Error Message : " + message.body);
            connectWebSocket();
		});

		stompClient.subscribe("/sloms/all_feedbacks", function(message) {
            $('#tblFeedbacks').DataTable().ajax.reload();
		});

		stompClient.subscribe("/sloms/all_feedbacks_of_logged_in_user", function(message) {
			
		});
	},function(error) {
        console.log("Web Socket Error Message : " + error);
    });
}

connectWebSocket();


String.prototype.replaceAll = function (searchText, replacementText) {
	return this.split(searchText).join(replacementText);
};

function addFeedback(){
	$('#frmAddFeedback').submit(function(event) {
		event.preventDefault();
		var formEl = $(this);
	
		swal({
			title: "Do you still want to continue?",
			icon: "warning",
			buttons: ["No!", "Yes!"],
			dangerMode: true
		})
		.then((willAdd) => {
			if (willAdd) {
				$.ajax({
					type: 'POST',
					dataType : 'JSON',
					url: formEl.prop('action'),
					data: formEl.serialize(),
					success: function(data) {
						if(data.status){
							iziToast.success({
                                title: 'Congratulations!',
                                theme: 'light', // dark
                                color: 'green', // blue, red, green, yellow
                                position: 'bottomRight', // bottomRight, bottomLeft, topRight, topLeft, topCenter, bottomCenter, center
                                message: "Feedback has been added successfully!"
                            });
    
                            $("#add_feedback-modal").modal('toggle');
							$('#tblFeedbacks').DataTable().ajax.reload();
						}
					},
					error: function(e){
						console.log(e);
                        iziToast.error({
                            title: "Oops! We're sorry!",
                            theme: 'light',
                            color: 'red',
                            position: 'bottomRight',
                            message: "Failed to add feedback. Please try again!"
                        });
					}
				});
			}
		});
	});
}

function showFeedbackDetails(d) {
	var body;

	body = '<p style="text-align: left;"><pre>' + d.feedback + '</pre></p>';
	body += '<p><span class="fa fa-clock-o"></span> ' + timeago().format(d.feedback_dateAndTimeCreated) + '</p>';

	//$('#frmUpdateChangelog_lblID').val(d.changelog_id);
	//$('#frmUpdateChangelog_txtVersion').val(d.changelog_version);
	//$('#frmUpdateChangelog_txtDetails').val(d.changelog_details);

	return body;
}

function showAllFeedback(){
	$("#tblFeedbacks tbody").empty();

	/* DataTables instantiation. */
	var table = $("#tblFeedbacks").DataTable({
		destroy: true,
		"ajax": {
			"url": "/feedbacks",
			"dataSrc": ""
		},
		"columns": [
			{
				"className": 'details-control',
				"orderable": false,
				"data": null,
				"defaultContent": ''
			},
			{ 
				"data": null,
				render: function (data,type,row) {
					var respondent_imageSrc = '';

					if(data.feedback_created_by.user_profile_picture == "" || data.feedback_created_by.user_profile_picture == null){
						respondent_imageSrc = 'resources/assets/images/users/no-image.jpg';
					}else{
						respondent_imageSrc = 'SLOMS_server' + data.feedback_created_by.user_profile_picture;
					}

					return '<img src="' + respondent_imageSrc + '" style="width: 40px; height: 40px; border-radius: 50%;" />&nbsp;&nbsp;' + data.feedback_created_by.user_firstname + ' ' + data.feedback_created_by.user_lastname;
				}
			},
			{ 
				"data": null,
				render: function (data,type,row) {
					return '<center><b>' + data.feedback_type.replaceAll("_"," ") + '</b></center>';
				}
			}
		],
		"iDisplayLength": 5,
		"aLengthMenu": [[5, 10, 25, 50, 100, -1], [5, 10, 25, 50, 100, "All"]],
		"pagingType": "full_numbers"
	});

	// Add event listener for opening and closing details
    $('#tblFeedbacks tbody').on('click', 'td.details-control', function () {
        var tr = $(this).closest('tr');
        var row = table.row(tr);
 
        if (row.child.isShown()) {
            // This row is already open - close it
            row.child.hide();
            tr.removeClass('shown');
        }
        else {
            // Open this row
            row.child(showFeedbackDetails(row.data())).show();
            tr.addClass('shown');
        }
    });
}

addFeedback();
showAllFeedback();
