<div class="modal" id="add_new_reply-modal" tabindex="-1" role="dialog" aria-labelledby="largeModalHead" aria-hidden="true" data-backdrop="static" data-keyboard="false">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<form id="frmAddReply" class="form-horizontal" role="form" action="/add-reply" method="POST">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">
						<span aria-hidden="true">&times;</span><span class="sr-only">Close</span>
					</button>
					<h3 class="modal-title" id="largeModalHead">Add Reply to <span id="lblReply_to"></span></h3>
				</div>

				<div class="modal-body" style="max-height: calc(100vh - 200px); overflow-y: auto;">			
					<input type="hidden" name="thread_comment_id" id="frmAddReply_lblThread_comment_id">
					
					<div class="form-group">
						<label class="col-md-2 control-label">Reply</label>
						<div class="col-md-10">

							<textarea 
								class="form-control" 
								rows="5"
								placeholder="Write your reply..." 
								required="required"
								name="thread_reply" 
								id="frmAddReply_thread_reply" 
								style="resize: none; background-color:white !important;" 
								maxlength="5000"></textarea>

							<span class="help-block">Required reply</span>
						</div>
					</div>

					<div class="form-group">
						<div class="col-md-12">
							<div class="panel panel-default">
								<div class="panel-body">
									<h3>
										<span class="fa fa-download"></span> Reply Images
									</h3>
									
									<div id="thread-comment-reply-images-holder"></div>
								</div>
							</div>
						</div>
					</div>

					<script>
						var threadCommentReplyImagesMultiFileUploader = new qq.FineUploader({
							element: document.getElementById('thread-comment-reply-images-holder'),
							template: 'qq-template-manual-trigger',
							validation: {
								allowedExtensions: ["jpeg","jpg","png"],
								itemLimit: 10
							},
							request: {
								endpoint: '/add-reply/add-thread-comment-reply-images',
								method: "POST"
							},
							deleteFile: {
								enabled: true,
								forceConfirm: true,
								endpoint: '/add-reply/delete-thread-comment-reply-images',
								method: "DELETE"
							},
							thumbnails: {
								placeholders: {
									waitingPath: '/resources/js/plugins/fine-uploader/placeholders/waiting-generic.png',
									notAvailablePath: '/resources/js/plugins/fine-uploader/placeholders/not_available-generic.png'
								}
							},
							autoUpload: false,
							debug: true,
							callbacks: {
								onComplete: function (id,name,responseJSON,maybeXhr){
									console.log(responseJSON);
									$('input#lblAddReplyImage_' + id).val(responseJSON.threadCommentReplyImageLink);
								},
								onCancel: function(id,name){
									console.log("cancel");
									$('#lblAddReplyImage_' + id).remove();
								},
								onError: function(id,name,reason,maybeXhrOrXdr){
									//logic here
								},
								onAllComplete: function(successful,failed){
									if(failed.length <= 0){
										//logic here
									}else{
										//logic here
									}
								},
								onSubmitted: function(id,name){
									$(".qq-uploader-selector").append("<input type='hidden' id='lblAddReplyImage_" + id + "' name='thread_comment_reply_images' value='' />");
								},
								onUpload (id,name){
									console.log(name + " before uploading...");
								},

								onSubmitDelete: function(id){
									this.setDeleteFileParams({filename: this.getName(id)}, id);
								},
								onDeleteComplete: function(id){
									$('#lblAddReplyImage_' + id).remove();
								}
							}
						});
						
						qq(document.querySelector("#thread-comment-reply-images-holder #trigger-upload")).attach("click", function () {
							threadCommentReplyImagesMultiFileUploader.uploadStoredFiles();
						});

						$('#add_new_reply-modal').on('hidden.bs.modal', function () {
							$('#frmAddReply')[0].reset();
							threadCommentReplyImagesMultiFileUploader.cancelAll();
							threadCommentReplyImagesMultiFileUploader.clearStoredFiles();
						});

						function addMaxLengthTo(input){
							$(input).maxlength({
								alwaysShow: true,
								threshold: 10,
								warningClass: "label label-success",
								limitReachedClass: "label label-danger",
								separator: ' of ',
								preText: 'You have ',
								postText: ' chars remaining.',
								validate: true
							});
						}

						function implementBootstrapMaxLength(){
							addMaxLengthTo('textarea#frmAddReply_thread_reply');
						}

						implementBootstrapMaxLength();
					</script>	
				</div>

				<div class="modal-footer">
					<button 
						type="submit" 
						class="btn btn-info btn-lg btn-rounded" 
						style="padding: 10px; font-size: 15px; background-color:#1caf9a; color: white;">
						<strong>
							<span class="fa fa-hand-o-right"></span> Add Reply
						</strong>
					</button>

					<button 
						type="button" 
						class="btn btn-warning btn-rounded" 
						style="padding: 10px; font-size: 15px; background-color: orange; color: white;" 
						data-dismiss="modal">
						<strong>
							<span class="glyphicon glyphicon-remove-sign"></span> Close
						</strong>
					</button>
				</div>
			</form>
		</div>
	</div>
</div>