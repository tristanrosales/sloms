package com.sloms.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.sloms.entity.ThreadComments;

@Repository
public interface ThreadCommentsRepository extends JpaRepository<ThreadComments,Integer>{
	@Query("SELECT tc FROM ThreadComments tc WHERE tc.thread_comment_id=:thread_comment_id")
	ThreadComments findThreadCommentsById(@Param("thread_comment_id") Integer thread_comment_id);
	
	@Query("SELECT tc FROM ThreadComments tc WHERE tc.thread_id.thread_id=:thread_id ORDER BY tc.thread_comment_id DESC")
	List<ThreadComments> getAllThreadCommentsByThreadId(@Param("thread_id") Integer thread_id);

	@Query("SELECT tc FROM ThreadComments tc WHERE tc.thread_comment_id=:thread_comment_id")
	ThreadComments getThreadCommentsByThreadCommentId(@Param("thread_comment_id") Integer thread_comment_id);
	
	@Query("SELECT tc FROM ThreadComments tc ORDER BY tc.thread_id DESC")
	List<ThreadComments> getAllThreadComments();
}
