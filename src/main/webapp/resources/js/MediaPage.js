var stompClient = null;

var userID = document.getElementById("loggedInUserId").innerText;
var userRole = document.getElementById("loggedInUserRole").innerText;

var money = value => currency(value, { formatWithSymbol: true });

String.prototype.replaceAll = function (searchText, replacementText) {
    return this.split(searchText).join(replacementText);
};

function connectWebSocket(){
    var socket = new SockJS('/sloms_web_socket');
    stompClient = Stomp.over(socket);

    stompClient.connect({}, function(frame) {
		stompClient.subscribe("/sloms/errors", function(message) {
            console.log("Web Socket Error Message : " + message.body);
            connectWebSocket();
		});

		stompClient.subscribe("/sloms/all_media_table", function(message) {
			$('#tblMedia').DataTable().ajax.reload();
		});
	}, function(error) {
        console.log("Web Socket Error Message : " + error);
    });
}

function populateDropdowns(){
    $.getJSON("/media", function(response) {
        $.each(response, function(key,data) {
            $('#cmbThread_billboard').append("<option value='" + data.media_id + "'>" + data.media_name + "</option>");
            $('#viewEdit_cmbThread_billboard').append("<option value='" + data.media_id + "'>" + data.media_name + "</option>");
        });

        $('#cmbThread_billboard').selectpicker('refresh');
        $('#cmbThread_billboard').selectpicker('render');

        $('#viewEdit_cmbThread_billboard').selectpicker('refresh');
        $('#viewEdit_cmbThread_billboard').selectpicker('render');
    });

    $.getJSON("/groups", function(response) {
        $.each(response, function(key,data) {
            $('#frmUpdateDeleteMedia_cmbMediaGroup').append("<option value='" + data.group_id + "'>" + data.group_name + "</option>");
        });

        $('#frmUpdateDeleteMedia_cmbMediaGroup').selectpicker('refresh');
        $('#frmUpdateDeleteMedia_cmbMediaGroup').selectpicker('render');
    });

    $.getJSON("/available_groups", function(response) {
        $.each(response, function(key,data) {
            $('#cmbMedia_group').append("<option value='" + data.group_id + "'>" + data.group_name + "</option>");
        });

        $('#cmbMedia_group').selectpicker('refresh');
        $('#cmbMedia_group').selectpicker('render');
    });


    $.getJSON("/resources/js/plugins/philippines-master/regions.json", function(response) {
        $.each(response, function(key,data) {
            $('#frmUpdateDeleteMedia_cmbRegion').append("<option value='" + data.key + "'>" + data.long + " (" + data.name + ")</option>");
        });

        $('#frmUpdateDeleteMedia_cmbRegion').selectpicker('refresh');
        $('#frmUpdateDeleteMedia_cmbRegion').selectpicker('render');
    });

    $.getJSON("/resources/js/plugins/philippines-master/provinces.json", function(response) {
        $.each(response, function(key,data) {
            if(data.region == $('#frmUpdateDeleteMedia_cmbRegion option:selected').val()){
                $('#frmUpdateDeleteMedia_cmbProvince').append("<option value='" + data.key + "'>" + data.name + "</option>");
            }
        });

        $('#frmUpdateDeleteMedia_cmbProvince').selectpicker('refresh');
        $('#frmUpdateDeleteMedia_cmbProvince').selectpicker('render');
    });

    $.getJSON("/resources/js/plugins/philippines-master/cities.json", function(response) {
        $.each(response, function(key,data) {
            if(data.province == $('#frmUpdateDeleteMedia_cmbProvince option:selected').val() && data.city == true){
                $('#frmUpdateDeleteMedia_cmbCity').append("<option value='" + data.name + "'>" + data.name + "</option>");
            }
        });

        $('#frmUpdateDeleteMedia_cmbCity').selectpicker('refresh');
        $('#frmUpdateDeleteMedia_cmbCity').selectpicker('render');
    });

    $("#frmUpdateDeleteMedia_cmbRegion").change(function(){
        $('#frmUpdateDeleteMedia_cmbProvince').empty();
        $.getJSON("/resources/js/plugins/philippines-master/provinces.json", function(response) {
            $.each(response, function(key,data) {
                if(data.region == $('#frmUpdateDeleteMedia_cmbRegion option:selected').val()){
                    $('#frmUpdateDeleteMedia_cmbProvince').append("<option value='" + data.key + "'>" + data.name + "</option>");
                }
            });

            $('#frmUpdateDeleteMedia_cmbProvince').selectpicker('refresh');
            $('#frmUpdateDeleteMedia_cmbProvince').selectpicker('render');
        });
    });

    $("#frmUpdateDeleteMedia_cmbProvince").change(function(){
        $('#frmUpdateDeleteMedia_cmbCity').empty();
        $.getJSON("/resources/js/plugins/philippines-master/cities.json", function(response) {
            $.each(response, function(key,data) {
                if(data.province == $('#frmUpdateDeleteMedia_cmbProvince option:selected').val() && data.city == true){
                    $('#frmUpdateDeleteMedia_cmbCity').append("<option value='" + data.name + "'>" + data.name + "</option>");
                }
            });

            $('#frmUpdateDeleteMedia_cmbCity').selectpicker('refresh');
            $('#frmUpdateDeleteMedia_cmbCity').selectpicker('render');
        });
    });
}

function showMediaDetails(d) {
	var table,access,creatorProfilePic;

    if(d.media_created_by.user_profile_picture == null || d.media_created_by.user_profile_picture == ""){
        creatorProfilePic = '/resources/img/no-image-available.jpg';
    }else{
        creatorProfilePic = '/SLOMS_server' + d.media_created_by.user_profile_picture;
    }

	table = '<table cellpadding="5" cellspacing="0" border="0" style="padding-left:50px;" class="table table-bordered table-striped display">'+
				'<tr>'+
					'<td><strong>LOCATION</strong></td>'+
					'<td>'+ d.mediaDetails.media_location +'</td>'+
				'</tr>'+
				'<tr>'+
					'<td><strong>DIMENSION</strong></td>'+
					'<td>'+ d.mediaDetails.media_size_h + 'x' + d.mediaDetails.media_size_w + ' ft.</td>'+
				'</tr>'+
				'<tr>'+
					'<td><strong>REGION</strong></td>'+
					'<td>'+ d.mediaDetails.media_region +'</td>'+
				'</tr>'+
				'<tr>'+
					'<td><strong>CITY</strong></td>'+
					'<td>'+ d.mediaDetails.media_city +'</td>'+
				'</tr>'+
				'<tr>'+
					'<td><strong>PRICE</strong></td>'+
					'<td>₱' + money(d.mediaDetails.media_price).format(false) + '</td>'+
				'</tr>'+
				'<tr>'+
					'<td><strong>CREATED</strong></td>'+
					'<td>'+ moment(new Date(d.media_dateAndTime_created)).format('LLLL') +'</td>'+
				'</tr>'+
				'<tr>'+
					'<td><strong>CREATOR</strong></td>'+
					'<td><img src="' + creatorProfilePic + '" style="width: 30px; height: 30px; border-radius: 50%;" />&nbsp;&nbsp;'+ d.media_created_by.user_firstname + " " + d.media_created_by.user_lastname + '</td>'+
				'</tr>'+
				'<tr>'+
					'<td><strong>STATUS</strong></td>'+
					'<td>'+ d.media_status +'</td>'+
				'</tr>'+
				'<tr>';

    			if(userRole == "SYS_ADMIN" || userRole == "IT_ADMIN"){
					if(userID == d.media_created_by.user_id){
						access = '<button type="button" class="btn btn-info btn-lg btn-rounded" style="padding: 10px; font-size: 15px; background-color:#1caf9a; color: white;" data-toggle="modal" data-target="#view_edit_media-modal"><strong><span class="fa fa-edit"></span> Edit This Media</strong></button>&nbsp;' +
								'<button type="button" class="btn btn-warning btn-lg btn-rounded" style="padding: 10px; font-size: 15px; background-color:#FF8000; color: white;" data-toggle="modal" data-target="#update_media_preview_image-modal"><strong><span class="fa fa-picture-o"></span> Change Preview Image</strong></button>';
					}else{
						access = "<span class='fa fa-ban' style='font-size: 18px;'><br>Access Denied</span>";
					}
				}else{
					access = "<span class='fa fa-ban' style='font-size: 18px;'><br>Access Denied</span>";
				}

		table += '<td colspan="2" style="text-align: center;">' + access + '</td>'+
				'</tr>'+
			'</table>';

			//label
			$('#frmUpdateDeleteMedia_lblMediaName').text(d.media_name);
			
			$('#frmUpdateDeleteMedia_lblMedia_id').val(d.media_id);

			//textboxes,textarea
			$('#frmUpdateDeleteMedia_txtMediaName').val(d.media_name);
			$('#frmUpdateDeleteMedia_txtMediaCode').val(d.media_code);
			$('#frmUpdateDeleteMedia_txtMediaDescription').val(d.mediaDetails.media_description);
			$('#frmUpdateDeleteMedia_txtMediaPrice').val(d.mediaDetails.media_price);
			$('#frmUpdateDeleteMedia_txtMediaLocation').val(d.mediaDetails.media_location);
			//$('#frmUpdateDeleteMedia_txtMediaRegion').val(d.mediaDetails.media_region);
			//$('#frmUpdateDeleteMedia_txtMediaCity').val(d.mediaDetails.media_city);
			$('#frmUpdateDeleteMedia_txtMediaSize_h').val(d.mediaDetails.media_size_h);
			$('#frmUpdateDeleteMedia_txtMediaSize_w').val(d.mediaDetails.media_size_w);
			$('#frmUpdateDeleteMedia_txtMediaClient').val(d.mediaClientDetails.media_client);

			$('#frmUpdateDeleteMedia_txtMediaContractedRate').val(d.mediaClientDetails.media_contracted_rate);
			$('#frmUpdateDeleteMedia_txtMediaContractStart').val(d.mediaClientDetails.media_contract_start);
			$('#frmUpdateDeleteMedia_txtMediaExpiration').val(d.mediaClientDetails.media_expiration);

			//select
			$('#frmUpdateDeleteMedia_cmbRegion')
				.removeAttr('selected')
				.find('option[value="' + d.mediaDetails.media_region + '"]')
				.attr("selected",true)
				.change();

			$('#frmUpdateDeleteMedia_cmbProvince')
				.removeAttr('selected')
				.find('option[value="' + d.mediaDetails.media_province + '"]')
				.attr("selected",true)
				.change();

			$('#frmUpdateDeleteMedia_cmbCity')
				.removeAttr('selected')
				.find('option[value="' + d.mediaDetails.media_city + '"]')
				.attr("selected",true)
				.change();


			$('#frmUpdateDeleteUser_cmbMedia_Status')
				.removeAttr('selected')
				.find('option[value="' + d.media_status + '"]')
				.attr("selected",true)
				.change();
			
			$('#frmUpdateDeleteMedia_cmbMediaGroup')
				.removeAttr('selected')
				.find('option[value="' + d.mediaDetails.media_group + '"]')
				.attr("selected",true)
				.change();
			
			$('#frmUpdateDeleteMedia_cmbMediaRoadPosition')
				.removeAttr('selected')
				.find("option[value='" + d.mediaDetails.media_road_position.replaceAll(" ","_") + "']")
				.attr("selected",true)
				.change();
			
			$('#frmUpdateDeleteMedia_cmbMediaPanelOrientation')
				.removeAttr('selected')
				.find('option[value="' + d.mediaDetails.media_panel_orientation.replaceAll(" ","_") + '"]')
				.attr("selected",true)
				.change();
			
			$('#frmUpdateDeleteMedia_cmbMediaIllumination')
				.removeAttr('selected')
				.find('option[value="' + d.mediaDetails.media_illumination + '"]')
				.attr("selected",true)
				.change();
			
			$('#frmUpdateDeleteMedia_cmbMediaStructureType')
				.removeAttr('selected')
				.find('option[value="' + d.mediaDetails.media_structure_type.replaceAll(" ","_") + '"]')
				.attr("selected",true)
				.change();
			
			$('#frmUpdateDeleteMedia_cmbMediaTrafficDetails')
				.removeAttr('selected')
				.find('option[value="' + d.mediaDetails.media_traffic_details.replaceAll(" ","_") + '"]')
				.attr("selected",true)
				.change();

			$('#frmUpdateMediaPreviewImage_mediaId').val(d.media_id);

	return table;
}

function showAllMedia(){
	$("#tblMedia tbody").empty();

	var table = $("#tblMedia").DataTable({
		"ajax": {
			"url": "/media",
			"dataSrc": ""
		},
		"columns": [
			{
				"className": 'details-control',
				"orderable": false,
				"data": null,
				"defaultContent": ''
			},
			{ 
				"data": null,
				render: function (data,type,row) {
                    var mediaPreviewImage;

                    if(data.media_preview_image == null || data.media_preview_image == ""){
                        mediaPreviewImage = '/resources/img/no-image-available.jpg';
                    }else{
                        mediaPreviewImage = '/SLOMS_server' + data.media_preview_image;
                    }

					return '<img src="' + mediaPreviewImage + '" style="width: 30px; height: 30px; border-radius: 50%;" />&nbsp;&nbsp;<a href="/media_details?media_id=' + data.media_id + '" target="_blank">' + data.media_name + ' <b>(' + data.media_code + ')</b></a>';
				}
			}
		],
		"iDisplayLength": 5,
		"aLengthMenu": [[5, 10, 25, 50, 100, -1], [5, 10, 25, 50, 100, "All"]],
		"pagingType": "full_numbers"
	});

    $('#tblMedia tbody').on('click', 'td.details-control', function () {
        var tr = $(this).closest('tr');
        var row = table.row(tr);
 
        if (row.child.isShown()) {
            row.child.hide();
            tr.removeClass('shown');
        }
        else {
            row.child(showMediaDetails(row.data())).show();
            tr.addClass('shown');
        }
	});
}

function validateMedia(){
    $("#frmAddMedia").validate({
        ignore : [],
        rules : {
            media_name: {
                required: true,
                maxlength: 50
            },
            media_group: {
                required: true
            },
            media_description: {
                required: true,
                maxlength: 5000
            },
            media_price: {
                required: true
            },
            media_code: {
                required: true,
                maxlength: 20
            },
            media_location: {
                required: true,
                maxlength: 500
            },
            media_region: {
                required: true
            },
            media_province: {
                required: true
            },
            media_city: {
                required: true
            },
            media_size_h: {
                required: true
            },
            media_size_w: {
                required: true
            },
            media_road_position: {
                required: true
            },
            media_panel_orientation: {
                required: true
            },
            media_illumination: {
                required: true
            },
            media_structure_type: {
                required: true
            },
            media_traffic_details: {
                required: true
            },
            media_client: {
                required: true,
                maxlength: 100
            },
            media_contracted_rate: {
                required: true
            },
            media_contract_start: {
                required: true
            },
            media_expiration: {
                required: true
            }
        },
        submitHandler: function(form) {
            addMedia(form);
        }
    });
}

function addMedia(form){
	var formEl = $(form);
	
	swal({
		title: "Do you still want to continue?",
		icon: "warning",
		buttons: ["No!", "Yes!"],
		dangerMode: true
	})
	.then((willAdd) => {
		if (willAdd) {
			$('body').loadingModal({
				position: 'auto',
				text: 'Please wait while creating this group...',
				color: '#fff',
				opacity: '0.7',
				backgroundColor: 'rgb(0,0,0)',
				animation: 'doubleBounce'
			});

			$.ajax({
				type: 'POST',
				dataType : 'JSON',
				url: formEl.prop('action'),
				data: formEl.serialize(),
				success: function(data) {
                    $('body').loadingModal('destroy');

					if(data.status == "mediaNameExists"){
                        iziToast.error({
                            title: "Oops! We're sorry!",
                            theme: 'light',
                            color: 'red',
                            position: 'bottomRight',
                            message: "There is already a media registered with the media name provided. Please try again!"
                        });
					}else if(data.status == "mediaCodeExists"){
                        iziToast.error({
                            title: "Oops! We're sorry!",
                            theme: 'light',
                            color: 'red',
                            position: 'bottomRight',
                            message: "There is already a media registered with the media code provided. Please try again!"
                        });
					}else if(data.status){
						iziToast.success({
							title: 'Congratulations!',
							theme: 'light', // dark
							color: 'green', // blue, red, green, yellow
							position: 'bottomRight', // bottomRight, bottomLeft, topRight, topLeft, topCenter, bottomCenter, center
							message: "Media has been added successfully!"
						});

						$("#add_new_media-modal").modal('toggle');
					}
				},
				error: function(e){
					console.log(e);
                    $('body').loadingModal('destroy');
                    iziToast.error({
                        title: "Oops! We're sorry!",
                        theme: 'light',
                        color: 'red',
                        position: 'bottomRight',
                        message: "Failed to create a media. Please try again!"
                    });
				}
			});
		}
	});
}

function updateMedia(){
	$('.frmUpdateDeleteMedia').submit(function(event) {
		event.preventDefault();
		var formEl = $(this);
		
		swal({
			  title: "Are you sure?",
			  text: "Once updated, you will not be able to undo this action!",
			  icon: "warning",
			  buttons: ["No, not sure yet!", "Yes, I'm Sure!"],
			  dangerMode: true
		})
		.then((willUpdate) => {
			if (willUpdate) {
				$.ajax({
					type: 'POST',
					dataType : 'JSON',
					url: "/update-media",
					data: formEl.serialize(),
					success: function(data) {
						 if(data.isUpdateMediaSuccess){
							iziToast.success({
								title: 'Congratulations!',
								theme: 'light', // dark
								color: 'green', // blue, red, green, yellow
								position: 'bottomRight', // bottomRight, bottomLeft, topRight, topLeft, topCenter, bottomCenter, center
								message: "Media has been updated successfully!"
							});
	
							$("#view_edit_media-modal").modal('toggle');
							$('#tblMedia').DataTable().ajax.reload();
						 }
					},
					error: function(e){
						console.log(e);
					}
				});
			}
		});
	});
}

function updateMediaPreviewImage(){
	$('#frmUpdateMediaPreviewImage').submit(function(event) {
		event.preventDefault();
		var formEl = $(this);
		
		swal({
			  title: "Are you sure?",
			  text: "Once updated, you will not be able to undo this action!",
			  icon: "warning",
			  buttons: ["No, not sure yet!", "Yes, I'm Sure!"],
			  dangerMode: true
		})
		.then((willUpdate) => {
			if (willUpdate) {
				$.ajax({
					type: 'POST',
					dataType : 'JSON',
					url: formEl.prop('action'),
					data: formEl.serialize(),
					// if received a response from the server
					success: function(data) {
						if(data.isUpdatingMediaPreviewImage){
							iziToast.success({
								title: 'Congratulations!',
								theme: 'light', // dark
								color: 'green', // blue, red, green, yellow
								position: 'bottomRight', // bottomRight, bottomLeft, topRight, topLeft, topCenter, bottomCenter, center
								message: "Media preview image has been updated successfully!"
							});
	
							$("#update_media_preview_image-modal").modal('toggle');
							$('#tblMedia').DataTable().ajax.reload();
						}
					},
					error: function(e){
						console.log(e);
					}
				});
			}
		});
	});
}

function deleteMedia(){
	swal({
		  title: "Are you sure?",
		  text: "Once deleted, you will not be able to undo this action!",
		  icon: "warning",
		  buttons: ["No, not sure yet!", "Yes, I'm Sure!"],
		  dangerMode: true,
	})
	.then((willDelete) => {
		if (willDelete) {
			$.ajax({
	            type: 'POST',
	            dataType : 'JSON',
	            url: "/frmDeleteMedia",
	            data: $('.frmUpdateDeleteMedia').serialize(),
	            // if received a response from the server
	            success: function( data, textStatus, jqXHR) {
						 if(data.status){
							 swal("Media has been deleted successfully!", {
			          		    icon: "success"
			          		 }).then((willDelete) => {
			          			 if(willDelete){
			          				location.reload(); 
			          			 }
			          		 });
						 }
	            },
	            error: function(jqXHR, textStatus, errorThrown){
	                alert('Error: ' + textStatus + ' - ' + errorThrown);
	            }
			});
		}
	});
}

connectWebSocket();
showAllMedia();
populateDropdowns();
validateMedia();
updateMedia();
updateMediaPreviewImage();