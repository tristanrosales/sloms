package com.sloms.service.implementation;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;

import com.sloms.service.EmailService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Service;

import com.sloms.classes.Mail;

@Service
public class EmailServiceImpl implements EmailService {
	
	@Autowired
	private JavaMailSender javaMailSender;
	
	@Override
	public void sendSimpleMessage(Mail mail) {
		try {
			MimeMessage mimeMessage = javaMailSender.createMimeMessage();
			MimeMessageHelper helper = new MimeMessageHelper(mimeMessage,true);
			
			//helper.addAttachment("logo.png", new ClassPathResource("SLOMS-logo.png"));
			
			helper.setSubject(mail.getSubject().trim());
			helper.setText(mail.getContent().trim(), true);
			helper.setTo(mail.getTo().trim());
			helper.setFrom(mail.getFrom().trim());
			
			javaMailSender.send(mimeMessage);
		} catch (MessagingException e) {
			e.printStackTrace();
		}
		
		/*
		SimpleMailMessage message = new SimpleMailMessage();
        message.setSubject(mail.getSubject());
        message.setText(mail.getContent());
        message.setTo(mail.getTo());
        message.setFrom(mail.getFrom());

        javaMailSender.send(message);
        */
	}

}
