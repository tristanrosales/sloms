package com.sloms.repository;

import java.util.Date;
import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.sloms.entity.Groups;
import com.sloms.entity.Users;

@Repository
public interface GroupsRepository extends JpaRepository<Groups,Integer>{
	@Query("SELECT g FROM Groups g ORDER BY g.group_id DESC")
	List<Groups> getAllGroups();

	@Query("SELECT g FROM Groups g WHERE g.group_name=:groupName ORDER BY g.group_id DESC")
	Groups getGroupByGroupName(@Param("groupName") String groupName);
	
	@Query("SELECT g FROM Groups g WHERE g.group_created_by=g.group_created_by AND g.group_status='Activated' ORDER BY g.group_id DESC")
	List<Groups> getAllGroupsThatIsActivated();
	
	@Query("SELECT g FROM Groups g WHERE g.group_created_by=:userID ORDER BY g.group_id DESC")
	List<Groups> getAllGroupsOfLoggedInUser(@Param("userID") Users userID);
	
	@Transactional
	@Modifying
	@Query("UPDATE Groups g SET "
			+ "g.group_name=:group_name,"
			+ "g.group_description=:group_description,"
			+ "g.group_status=:group_status,"
			+ "g.group_modified=:group_modified "
			+ "WHERE g.group_id=:group_id")
	void updateGroupInformation(
			@Param("group_name") String group_name,
			@Param("group_description") String group_description,
			@Param("group_status") String group_status,
			@Param("group_modified") Date group_modified,
			@Param("group_id") Integer group_id);
	
	@Transactional
	@Modifying
	@Query("UPDATE Groups g SET "
			+ "g.group_preview_image=:group_preview_image "
			+ "WHERE g.group_id=:group_id")
	void updateGroupPreviewImage(
			@Param("group_preview_image") String group_preview_image,
			@Param("group_id") Integer group_id);
}
