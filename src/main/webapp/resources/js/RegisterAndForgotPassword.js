String.prototype.replaceAll = function (searchText, replacementText) {
	return this.split(searchText).join(replacementText);
};

function validateChangePasswordForm(){
    $("#frmForgotPassword_changePassword").validate({
        ignore : [],
        rules : {
            user_password : {
                required : true,
                minlength : 8,
                maxlength : 16
            },
            're-password' : {
                required : true,
                minlength : 8,
                maxlength : 16,
                equalTo : "#txtNewPassword"
            }
        }
    });
}

function forgotPassword_getSecurityCode(){
    if($('#forgotPassword_username').val() == ""){
        iziToast.error({
            title: "We're sorry!",
            theme: 'light', // dark
            color: 'red', // blue, red, green, yellow
            position: 'bottomRight', // bottomRight, bottomLeft, topRight, topLeft, topCenter, bottomCenter, center
            message: "Username cannot be null or empty. Please try again!"
        });

        $('#forgotPassword_username').focus();
    }else{
        $('body').loadingModal({
            position: 'auto',
            text: 'Please wait while verification code is sending to your email!',
            color: '#fff',
            opacity: '0.7',
            backgroundColor: 'rgb(0,0,0)',
            animation: 'doubleBounce'
        });

        $.ajax({
            type: 'POST',
            dataType : 'JSON',
            url: '/forgot-password-get-security-code',
            data: $('#frmForgotPassword_checkUsername').serialize(),
            // if received a response from the server
            success: function(response) {
                $('body').loadingModal('hide');
                
                if(response.isUsernameExists == "usernameExists"){
                    $('#lblEnterYourSecurityCode_username').text($('#forgotPassword_username').val());

                    $('#enter_your_security-code-modal').modal('show');
                }else if(response.isUsernameExists == "usernameNotExists"){
                    iziToast.error({
                        title: "We're sorry!",
                        theme: 'light', // dark
                        color: 'red', // blue, red, green, yellow
                        position: 'bottomRight', // bottomRight, bottomLeft, topRight, topLeft, topCenter, bottomCenter, center
                        message: "Username doesn't exist yet in the datatabase. Please try again!"
                    });
                }
            },
            error: function(e){
                console.log(e);
                $('body').loadingModal('hide');
            }
        });
    }
}

function forgotPassword_getUserInfoByUsername(){
    $.getJSON("/forgot_password/info/" + $('#forgotPassword_username').val(), function(response) {
        $('#forgotPassword_lblUsername').text(response.username);
        $('#forgotPassword_lblUser_securityQuestion').text(response.user_security_question.replaceAll("_"," "));
    });
}

function forgotPassword_checkUsername(){
    $('#frmForgotPassword_checkUsername').submit(function(event) {
        event.preventDefault();
        var formEl = $(this);
        
        $('body').loadingModal({
            position: 'auto',
            text: 'Checking your username... Please wait!',
            color: '#fff',
            opacity: '0.7',
            backgroundColor: 'rgb(0,0,0)',
            animation: 'doubleBounce'
        });

        $.ajax({
            type: 'POST',
            dataType : 'JSON',
            url: formEl.prop('action'),
            data: formEl.serialize(),
            // if received a response from the server
            success: function(data) {
                $('body').loadingModal('hide');

                if(data.status == "usernameExists"){
                    forgotPassword_getUserInfoByUsername();
                    $("#forgot-password_check-username").modal('toggle');
                    $('#enter_your_secret_answer-modal').modal('show');
                    $('#forgotPassword_lblUser_securityQuestion').html($('#forgotPassword_lblUser_securityQuestion').text().replaceAll("_"," "));
                }else if(data.status == "emptySecurityQuestion"){
                    iziToast.error({
                        title: "We're sorry!",
                        theme: 'light', // dark
                        color: 'red', // blue, red, green, yellow
                        position: 'bottomRight', // bottomRight, bottomLeft, topRight, topLeft, topCenter, bottomCenter, center
                        message: "It seems that this username doesn't have any security question with secret answer. Please try again!"
                    });
                }else if(data.status == "usernameNotExists"){
                    iziToast.error({
                        title: "We're sorry!",
                        theme: 'light', // dark
                        color: 'red', // blue, red, green, yellow
                        position: 'bottomRight', // bottomRight, bottomLeft, topRight, topLeft, topCenter, bottomCenter, center
                        message: "Username doesn't exist yet in the database. Please try again!"
                    });
                }
            },
            error: function(e){
                console.log(e);
                $('body').loadingModal('hide');
            }
        });
    });
}

function forgotPassword_enterSecretAnswer(){
    $('#frmForgotPassword_enterSecretAnswer').submit(function(event) {
        event.preventDefault();
        var formEl = $(this);
        
        $('body').loadingModal({
            position: 'auto',
            text: 'Checking your answer... Please wait!',
            color: '#fff',
            opacity: '0.7',
            backgroundColor: 'rgb(0,0,0)',
            animation: 'doubleBounce'
        });

        $.ajax({
            type: 'POST',
            dataType : 'JSON',
            url: formEl.prop('action'),
            data: formEl.serialize(),
            // if received a response from the server
            success: function(data) {
                $('body').loadingModal('hide');

                if(data.isSecretAnswerCorrect){
                    $('#enter_your_secret_answer-modal').modal('toggle');
                    $('#change_password-modal').modal('show');
                }else{
                    swal("We're sorry!", "Secret answer doesn't matched in the database. Please try again!", "error");
                }
            },
            error: function(e){
                console.log(e);
                $('body').loadingModal('hide');
            }
        });
    });
}

function forgotPassword_changePassword(){
    $('#frmForgotPassword_changePassword').submit(function(event) {
        event.preventDefault();
        var formEl = $(this);
        
        swal({
            title: "Are you sure?",
            text: "Once added, you will not be able to undo this action!",
            icon: "warning",
            buttons: ["No, not sure yet!", "Yes, I'm Sure!"],
            dangerMode: true
        })
        .then((willUpdate) => {
            if (willUpdate) {
                $('body').loadingModal({
                    position: 'auto',
                    text: 'Updating your password... Please wait!',
                    color: '#fff',
                    opacity: '0.7',
                    backgroundColor: 'rgb(0,0,0)',
                    animation: 'doubleBounce'
                });

                $.ajax({
                    type: 'POST',
                    dataType : 'JSON',
                    url: formEl.prop('action'),
                    data: {
                        user_password: $('#txtNewPassword').val(),
                        username: $('#forgotPassword_username').val()
                    },
                    // if received a response from the server
                    success: function(data) {
                        $('body').loadingModal('hide');

                        if(data.isChangingPasswordSuccess){
                            swal("Password has been changed successfully!", {
                                icon: "success"
                            }).then((willUpdate) => {
                                if(willUpdate){
                                    location.reload();
                                }
                            });
                        }
                    },
                    error: function(e){
                        console.log(e);
                        $('body').loadingModal('hide');
                    }
                });
            }
        });
    });
}

function forgotPassword_enterSecurityCode(){
    $('#frmForgotPassword_enterSecurityCode').submit(function(event) {
        event.preventDefault();
        var formEl = $(this);
        
        $('body').loadingModal({
            position: 'auto',
            text: 'Checking the verification code... Please wait!',
            color: '#fff',
            opacity: '0.7',
            backgroundColor: 'rgb(0,0,0)',
            animation: 'doubleBounce'
        });

        $.ajax({
            type: 'POST',
            dataType : 'JSON',
            url: formEl.prop('action'),
            data: formEl.serialize(),
            // if received a response from the server
            success: function(data) {
                if(data.isSecurityCodeCorrect){
                    $('body').loadingModal('hide');

                    $("#forgot-password_check-username").modal('toggle');
                    $('#change_password-modal').modal('show');
                }else{
                    swal("We're sorry!", "Security code doesn't matched to the code sended to your email. Please try again!", "error");
                }
            },
            error: function(e){
                console.log(e);
                $('body').loadingModal('hide');
            }
        });
    });
}

function validateUser(){
    var formEl = $('#frmRegisterAccount');

    $('body').loadingModal({
        position: 'auto',
        text: 'Please wait while validating your account!',
        color: '#fff',
        opacity: '0.7',
        backgroundColor: 'rgb(0,0,0)',
        animation: 'doubleBounce'
    });

    $.ajax({
        type: 'POST',
        dataType : 'JSON',
        url: formEl.prop('action'),
        data: formEl.serialize(),
        success: function(data) {
            $('body').loadingModal('hide');

            if(data.status == "SUCCESS"){
                swal("User has been added successfully!", {
                    icon: "success"
                }).then((willUpdate) => {
                    if(willUpdate){
                        location.replace("/");
                    }
                });
            }else{
                var errorInfo = "";

                for(i = 0; i < data.result.length; i++){
                    errorInfo += "<br>" + (i + 1) + ". " + data.result[i].code;
                }

                $('#lblErrorMessages').html("<br><hr width='100%' />Please correct following errors: <br>" + errorInfo);
            }
        },
        error: function(e){
            console.log(e);
            $('body').loadingModal('hide');
        }
    });
}

//call all methods
validateChangePasswordForm();
forgotPassword_checkUsername();
forgotPassword_enterSecretAnswer();
forgotPassword_changePassword();
forgotPassword_enterSecurityCode();