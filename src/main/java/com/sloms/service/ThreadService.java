package com.sloms.service;

import com.sloms.entity.*;

import java.util.List;

public interface ThreadService{
	List<Threads> getAllThreads();
	List<Threads> getAllThreadsOfLoggedInUser(Users loggedInUser);
	void saveThread(Threads threadsDAO, Notifications notificationsDAO);
	void saveThreadComments(ThreadComments threadCommentsDAO, Notifications notificationsDAO);
	void saveThreadCommentReplies(ThreadCommentReplies threadCommentRepliesDAO, Notifications notificationsDAO);
	Threads getThreadById(Integer threadID);
	Threads findThreadByThreadCaption(String thread_caption);
	Threads findThreadByThreadId(Integer thread_id);
	void updateThread(Notes thread_notes_id, String thread_caption, String thread_status, Integer thread_id);
	void openOrCloseSelectedThread(String thread_status, Integer thread_id);
	void updateThreadPreviewImage(String thread_preview_image,Integer thread_id);
	void deleteThread(Threads selectedThread);
}
