package com.sloms.repository;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.sloms.entity.Media;
import com.sloms.entity.MediaDetails;

@Repository
public interface MediaDetailsRepository extends JpaRepository<MediaDetails,Integer>{
	@Query("SELECT md FROM MediaDetails md WHERE md.media.media_id=:media_id")
	MediaDetails findMediaDetailsByMediaId(@Param("media_id") Integer media_id);
	
	@Transactional
	@Modifying
	@Query("UPDATE MediaDetails md SET "
			+ "md.media_group=:media_group,"
			+ "md.media_description=:media_description,"
			+ "md.media_price=:media_price,"
			+ "md.media_location=:media_location,"
			+ "md.media_region=:media_region,"
			+ "md.media_city=:media_city,"
			+ "md.media_size_h=:media_size_h,"
			+ "md.media_size_w=:media_size_w,"
			+ "md.media_road_position=:media_road_position,"
			+ "md.media_panel_orientation=:media_panel_orientation,"
			+ "md.media_illumination=:media_illumination,"
			+ "md.media_structure_type=:media_structure_type,"
			+ "md.media_traffic_details=:media_traffic_details "
			+ "WHERE md.media=:media_id")
	void updateMediaDetailsInformation(
			@Param("media_group") String media_group,
			@Param("media_description") String media_description,
			@Param("media_price") String media_price,
			@Param("media_location") String media_location,
			@Param("media_region") String media_region,
			@Param("media_city") String media_city,
			@Param("media_size_h") String media_size_h,
			@Param("media_size_w") String media_size_w,
			@Param("media_road_position") String media_road_position,
			@Param("media_panel_orientation") String media_panel_orientation,
			@Param("media_illumination") String media_illumination,
			@Param("media_structure_type") String media_structure_type,
			@Param("media_traffic_details") String media_traffic_details,
			@Param("media_id") Media media_id);
}
