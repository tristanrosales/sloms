package com.sloms.classes;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class JsonResponse {
    private String status = null;
    private Object result = null;
}
