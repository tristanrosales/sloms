package com.sloms.controller;

import java.security.Principal;
import java.util.Date;
import java.util.List;

import com.sloms.classes.RoleConstants;
import com.sloms.service.UsersService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.web.bind.annotation.*;

import com.google.gson.JsonObject;
import com.sloms.entity.Changelog;
import com.sloms.entity.Notifications;
import com.sloms.entity.Users;
import com.sloms.repository.ChangelogRepository;
import com.sloms.service.ChangelogService;

@RestController
public class ChangelogController {
	@Autowired private ChangelogService changelogService;
	@Autowired private ChangelogRepository changelogRepository;
	@Autowired private UsersService usersService;
	@Autowired private SimpMessagingTemplate template;
	private JsonObject status = new JsonObject();
	
	@GetMapping("/changelog")
	public List<Changelog> getAllChangelog(){
		return changelogRepository.getAllChangelog();
	}
	
	@PostMapping("/add-changelog")
	@ResponseBody
	public String addChangelog(
			@ModelAttribute("changelog") Changelog changelog, 
			@ModelAttribute("notifications") Notifications notifications,
			Principal principal) {

		Users loggedInUser = usersService.findUserByUsername(principal.getName());
		final Date dateAndTimeToday = new Date();

		if(RoleConstants.IT_ADMIN.equals(loggedInUser.getUser_role())){
			changelog.setChangelog_added_by(loggedInUser);
			changelog.setChangelog_dateAndTimeAdded(dateAndTimeToday);
			changelogService.saveChangelog(changelog,notifications);

			status.addProperty("status", true);
			template.convertAndSend("/sloms/all_changelog", "all_changelog");
		}else{
			status.addProperty("status", "accessDenied");
		}
		
		return status.toString();
	}

	@PostMapping("/update-changelog")
	@ResponseBody
	public String updateChangelog(
			@RequestParam String changelog_version, 
			@RequestParam String changelog_details, 
			@RequestParam Integer changelog_id,
			Principal principal) {

		Users loggedInUser = usersService.findUserByUsername(principal.getName());

		if(RoleConstants.IT_ADMIN.equals(loggedInUser.getUser_role())) {
			changelogService.updateChangelog(changelog_version.trim(), changelog_details.trim(), changelog_id);

			status.addProperty("isUpdatingChangelogSuccess", true);
			template.convertAndSend("/sloms/all_changelog", "all_changelog");
		}else{
			status.addProperty("isUpdatingChangelogSuccess", "accessDenied");
		}
		
		return status.toString();
	}	
}
