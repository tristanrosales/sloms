<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ page session="true"%>
<!DOCTYPE html>
<html lang="en" xmlns:th="http://www.thymeleaf.org">
<head>
<!-- META SECTION -->
<title>Soon To Expire | SLOMS</title>
<tiles:insertAttribute name="headInit" />
</head>
<body>
	<!-- START PAGE CONTAINER -->
	<div class="page-container">

		<tiles:insertAttribute name="sidebar" />

		<!-- PAGE CONTENT -->
		<div class="page-content">

			<tiles:insertAttribute name="x_navigationVertical" />

			<!-- START BREADCRUMB -->
			<ul class="breadcrumb">
				<li><a href="${pageContext.request.contextPath}/home">Home</a></li>
				<li class="active">Soon To Expire</li>
			</ul>
			<!-- END BREADCRUMB -->

			<!-- PAGE TITLE -->
			<div class="page-title">
				<h2>
					<span class="fa fa-calendar"></span> Soon To Expire
				</h2>
			</div>
			<!-- END PAGE TITLE -->

			<!-- PAGE CONTENT WRAPPER -->
			<div class="page-content-wrap">
				<div class="row">
                    <div class="col-md-12">
                        <!-- START ACCORDION -->        
                        <div class="panel-group accordion">
                            <div class="panel panel-info">
                                <div class="panel-heading">
                                    <h4 class="panel-title">
                                        <a href="#expiring_in_15days">
                                            Expiring in <b>15 days</b>
                                        </a>
                                    </h4>
                                </div>         
                                <div class="panel-body" id="expiring_in_15days" class="row">
                                    <div class="alert alert-danger" role="alert" id="expiring_in_15days_no_data_available">
                                        <button type="button" class="close" data-dismiss="alert"></button>
                                        <strong>Oops!</strong> No data available!
                                    </div>

                                    <ol id="orderedList_expiring_15days" style="font-size: 15px;"></ol>
                                </div>                                
                            </div>
                            <div class="panel panel-info">
                                <div class="panel-heading">
                                    <h4 class="panel-title">
                                        <a href="#expiring_in_30days">
                                            Expiring in <b>30 days</b>
                                        </a>
                                    </h4>
                                </div>                                
                                <div class="panel-body" id="expiring_in_30days">
                                    <div class="alert alert-danger" role="alert" id="expiring_in_30days_no_data_available">
                                        <button type="button" class="close" data-dismiss="alert"></button>
                                        <strong>Oops!</strong> No data available!
                                    </div>

                                    <ol id="orderedList_expiring_30days" style="font-size: 15px;"></ol>
                                </div>
                            </div>
                            <div class="panel panel-warning">
                                <div class="panel-heading">
                                    <h4 class="panel-title">
                                        <a href="#expiring_in_45days">
                                            Expiring in <b>45 days</b>
                                        </a>
                                    </h4>
                                </div>                          
                                <div class="panel-body" id="expiring_in_45days">
                                    <div class="alert alert-danger" role="alert" id="expiring_in_45days_no_data_available">
                                        <button type="button" class="close" data-dismiss="alert"></button>
                                        <strong>Oops!</strong> No data available!
                                    </div>

                                    <ol id="orderedList_expiring_45days" style="font-size: 15px;"></ol>
                                </div>
                            </div>

                            <div class="panel panel-warning">
                                <div class="panel-heading">
                                    <h4 class="panel-title">
                                        <a href="#expiring_in_60days">
                                            Expiring in <b>60 days</b>
                                        </a>
                                    </h4>
                                </div>                                
                                <div class="panel-body" id="expiring_in_60days">
                                    <div class="alert alert-danger" role="alert" id="expiring_in_60days_no_data_available">
                                        <button type="button" class="close" data-dismiss="alert"></button>
                                        <strong>Oops!</strong> No data available!
                                    </div>

                                    <ol id="orderedList_expiring_60days" style="font-size: 15px;"></ol>
                                </div>
                            </div>

                            <div class="panel panel-warning">
                                <div class="panel-heading">
                                    <h4 class="panel-title">
                                        <a href="#expiring_in_75days">
                                            Expiring in <b>75 days</b>
                                        </a>
                                    </h4>
                                </div>                                
                                <div class="panel-body" id="expiring_in_75days">
                                    <div class="alert alert-danger" role="alert" id="expiring_in_75days_no_data_available">
                                        <button type="button" class="close" data-dismiss="alert"></button>
                                        <strong>Oops!</strong> No data available!
                                    </div>

                                    <ol id="orderedList_expiring_75days" style="font-size: 15px;"></ol>
                                </div>
                            </div>

                            <div class="panel panel-warning">
                                <div class="panel-heading">
                                    <h4 class="panel-title">
                                        <a href="#expiring_in_90days">
                                            Expiring in <b>90 days</b>
                                        </a>
                                    </h4>
                                </div>         
                                <div class="panel-body" id="expiring_in_90days">
                                    <div class="alert alert-danger" role="alert" id="expiring_in_90days_no_data_available">
                                        <button type="button" class="close" data-dismiss="alert"></button>
                                        <strong>Oops!</strong> No data available!
                                    </div>

                                    <ol id="orderedList_expiring_90days" style="font-size: 15px;"></ol>
                                </div>
                            </div>
                        </div>
                        <!-- END ACCORDION -->

                    </div>
				</div>

			</div>
			<!-- PAGE CONTENT WRAPPER -->
		</div>
		<!-- END PAGE CONTENT -->
	</div>
	<!-- END PAGE CONTAINER -->

    <tiles:insertAttribute name="signOutModal_and_scripts" />
</body>
</html>