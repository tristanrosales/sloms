package com.sloms.classes;

import java.io.File;
import java.io.IOException;
import java.nio.file.FileSystems;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.Map;

import org.apache.commons.codec.digest.DigestUtils;
import org.apache.commons.lang.StringUtils;
import org.springframework.web.multipart.MultipartFile;

public class UploadImage {
	public static String uploadImage(String hashValue, MultipartFile multipartFile,String filePath,String imageFolder) {
		try {
			byte[] imageBytes = multipartFile.getBytes();
			
			Map<String, String> imageTypes = new HashMap<String,String>(){
				/**
				 * 
				 */
				private static final long serialVersionUID = 1L;

			{
				put("image/jpeg", ".jpg");
				put("image/jpg", ".jpg");
				put("image/png", ".png");
			}};
			
			String imageLink = StringUtils.EMPTY;
			
			String imageFileName = DigestUtils.md5Hex(hashValue) + imageTypes.get(multipartFile.getContentType());
			String imageFilePath = FileSystems.getDefault().getPath(filePath) + File.separator + imageFileName;
			Path path = Paths.get(imageFilePath);
			Files.write(path, imageBytes);
			
			imageLink = imageFolder + imageFileName;
			
			return imageLink;
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return null;
	}
}
