<div class="alert alert-info" role="alert" style="font-size: 15px;">
    <button type="button" class="close" data-dismiss="alert"></button>
    <h3 style="text-align: center;"><strong>Registration Page</strong></h3><hr width="100%" />
    <strong>Reminder!</strong><br>
    -Please remember your password because it's auto-generated!<br>
    -After you've registered your account, you are tagged as <b>"Unauthenticated"</b> because you're not yet approved by the management.<br>
    -You can't login your account if your status is  <b>"Unauthenticated"</b>

    <span id="lblErrorMessages"></span>
</div>

<a href="${pageContext.request.contextPath}/" style="cursor: pointer; float: left;">
    <button class="forgot-password100-form-btn" type="button">
        <span class="fa fa-arrow-left"></span>&nbsp;&nbsp;<strong>Go Back</strong>
    </button>
</a>

<form id="frmRegisterAccount" class="login100-form validate-form" action="${pageContext.request.contextPath}/registerAccount" method="POST" onsubmit="return false">
    <div class="wrap-input100 validate-input m-b-26" data-validate="Username is required">
        <span class="label-input100" style="color: black; font-weight: bold;">Username</span>
        <input class="input100" type="text" name="username" placeholder="Enter username" autocomplete="off">
        <span class="focus-input100"></span>
    </div>

    <div class="wrap-input100 validate-input m-b-26" data-validate="Firstname is required">
        <span class="label-input100" style="color: black; font-weight: bold;">Firstname</span>
        <input class="input100" type="text" name="user_firstname" placeholder="Enter firstname" autocomplete="off">
        <span class="focus-input100"></span>
    </div>

    <div class="wrap-input100 validate-input m-b-26" data-validate="Lastname is required">
        <span class="label-input100" style="color: black; font-weight: bold;">Lastname</span>
        <input class="input100" type="text" name="user_lastname" placeholder="Enter lastname" autocomplete="off">
        <span class="focus-input100"></span>
    </div>

    <div class="wrap-input100 validate-input m-b-26" data-validate="Email is required">
        <span class="label-input100" style="color: black; font-weight: bold;">Email</span>
        <input class="input100" type="email" name="user_email_address" placeholder="Enter email" autocomplete="off">
        <span class="focus-input100"></span>
    </div>

    <div class="wrap-input100 validate-input m-b-26" data-validate="Gender is required">
        <span class="label-input100" style="color: black; font-weight: bold;">Gender</span>

        <select class="form-control input100" name="user_gender">
            <option value="Male">Male</option>
            <option value="Female">Female</option>
        </select>
    </div>

    <div class="wrap-input100 validate-input m-b-26" data-validate="Role is required">
        <span class="label-input100" style="color: black; font-weight: bold;">Role</span>

        <select class="form-control input100" name="user_role">
            <option value="SYS_ADMIN">System Administrator</option>
            <option value="SYS_SPECIALIST">System Specialist</option>
        </select>
    </div>

    <div class="wrap-input100 validate-input m-b-18" data-validate="Password is required">
        <span class="label-input100" style="color: black; font-weight: bold;">Password</span>
        <input class="input100" id="frmLogin_txtPassword" type="text" name="user_password" placeholder="Enter password" readonly value="${randomPassword}">
        <span class="focus-input100"></span>
    </div>

    <div class="container-login100-form-btn">
        <button class="login100-form-btn" type="submit" onclick="validateUser()">
            <span class="fa fa-save"></span>&nbsp;&nbsp;<strong>Register</strong>
        </button>
    </div>
</form>