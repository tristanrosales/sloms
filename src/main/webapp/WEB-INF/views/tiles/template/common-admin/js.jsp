<script src="${pageContext.request.contextPath}/resources/Login_v15/vendor/jquery/jquery-3.2.1.min.js"></script>
<script src="${pageContext.request.contextPath}/resources/Login_v15/vendor/animsition/js/animsition.min.js"></script>
<script src="${pageContext.request.contextPath}/resources/Login_v15/vendor/bootstrap/js/popper.js"></script>
<script src="${pageContext.request.contextPath}/resources/Login_v15/vendor/bootstrap/js/bootstrap.min.js"></script>
<script src="${pageContext.request.contextPath}/resources/Login_v15/vendor/select2/select2.min.js"></script>
<script src="${pageContext.request.contextPath}/resources/Login_v15/vendor/daterangepicker/moment.min.js"></script>
<script src="${pageContext.request.contextPath}/resources/Login_v15/vendor/daterangepicker/daterangepicker.js"></script>
<script src="${pageContext.request.contextPath}/resources/Login_v15/vendor/countdowntime/countdowntime.js"></script>
<script src="${pageContext.request.contextPath}/resources/Login_v15/js/main.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/resources/js/plugins/Fullscreen-Loading-Modal-Indicator/js/jquery.loadingModal.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/resources/js/plugins/sweetalert.min.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/resources/js/plugins/iziToast/js/iziToast.min.js"></script>
<script type='text/javascript' src='${pageContext.request.contextPath}/resources/js/plugins/validationengine/languages/jquery.validationEngine-en.js'></script>
<script type='text/javascript' src='${pageContext.request.contextPath}/resources/js/plugins/validationengine/jquery.validationEngine.js'></script>
<script type='text/javascript' src='${pageContext.request.contextPath}/resources/js/plugins/jquery-validation/jquery.validate.js'></script>