package com.sloms.controller;

import java.io.IOException;
import java.security.Principal;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.validation.Valid;

import com.sloms.classes.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.scheduling.annotation.Async;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.ValidationUtils;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.thymeleaf.context.Context;
import org.thymeleaf.spring4.SpringTemplateEngine;

import com.clockworksms.ClockWorkSmsService;
import com.clockworksms.ClockworkException;
import com.clockworksms.ClockworkSmsResult;
import com.clockworksms.SMS;
import com.google.common.base.CharMatcher;
import com.google.gson.JsonObject;
import com.sloms.entity.Users;
import com.sloms.repository.UsersRepository;
import com.sloms.service.EmailService;
import com.sloms.service.UsersService;

@RestController
public class UsersController {
	@Autowired private UsersRepository usersRepository;
	@Autowired private UsersService usersService;
	@Autowired private BCryptPasswordEncoder bCryptPasswordEncoder;
	@Autowired private EmailService emailService;
	@Autowired private SpringTemplateEngine templateEngine;
	@Autowired private SimpMessagingTemplate template;
	private JsonObject status = new JsonObject();

	@PostMapping("/forgot-password-get-security-code")
	@ResponseBody
	public String forgotPassword_getSecurityCode(@RequestParam String username,HttpSession httpSession,HttpServletRequest request) {		
		Users usernameExists = usersRepository.getAllUserInfoByUsername(username.trim());
		
		if(usernameExists != null) {
			status.addProperty("isUsernameExists", "usernameExists");
			
			httpSession.setAttribute("forgotPassword_fullname", usernameExists.getUser_firstname() + " " + usernameExists.getUser_lastname());
			
			sendSecurityCodeToEmailAddress(usernameExists.getUser_email_address(),httpSession,request);
		}else if(usernameExists == null) {
			status.addProperty("isUsernameExists", "usernameNotExists");
		}
		
		return status.toString();
	}

	@PostMapping("/forgot-password-enter-security-code")
	@ResponseBody
	public String forgotPassword_enterSecurityCode(@RequestParam Integer security_code,HttpSession httpSession) {
		if(httpSession.getAttribute("randomSecurityCode").toString().equalsIgnoreCase(security_code.toString())) {
			status.addProperty("isSecurityCodeCorrect", true);
		}else {
			status.addProperty("isSecurityCodeCorrect", false);
		}
		
		return status.toString();
	}

	@PostMapping("/forgot-password-change-password")
	@ResponseBody
	public String forgotPassword_changePassword(@RequestParam String user_password,@RequestParam String username) {
		usersRepository.updateUserPassword(bCryptPasswordEncoder.encode(user_password), username);
		
		status.addProperty("isChangingPasswordSuccess", true);
		
		return status.toString();
	}

	@PostMapping("/registerAccount")
    @ResponseBody
	public JsonResponse validateUserThenRegister(
			@ModelAttribute("users") @Valid Users users,
			Model model,
			BindingResult result){

        JsonResponse response = new JsonResponse();
		Users usernameExists = usersRepository.getAllUserInfoByUsername(users.getUsername());
		Users emailExists = usersRepository.findUserByEmail(users.getUser_email_address());

	    ValidationUtils.rejectIfEmptyOrWhitespace(result, "username", "Username cannot be empty.");
        ValidationUtils.rejectIfEmptyOrWhitespace(result, "user_password", "Password cannot be empty.");
        ValidationUtils.rejectIfEmptyOrWhitespace(result, "user_firstname", "Firstname cannot be empty.");
        ValidationUtils.rejectIfEmptyOrWhitespace(result, "user_lastname", "Lastname cannot be empty.");
        ValidationUtils.rejectIfEmptyOrWhitespace(result, "user_email_address", "Email address cannot be empty.");
        ValidationUtils.rejectIfEmptyOrWhitespace(result, "user_gender", "Gender cannot be empty.");
        ValidationUtils.rejectIfEmptyOrWhitespace(result, "user_role", "Role cannot be empty.");

		if(usernameExists != null) {
			result.rejectValue("username", "Username must be unique.");
		}

		if(emailExists != null) {
			result.rejectValue("user_email_address", "Email address must be unique.");
		}

        if(result.hasErrors()){
            response.setStatus("FAIL");
            response.setResult(result.getAllErrors());
		}else{
			usersService.saveUser(users,model);
			response.setStatus("SUCCESS");
			sendEmailConfirmationAfterAddingUser(users);
			template.convertAndSend("/sloms/bulletin_board", "bulletin_board");
        }

        return response;
	}

	@PostMapping("/add-user")
	@ResponseBody
	public String addUser(
			@ModelAttribute("users") Users users,
			Model model) {
		
		Users usernameExists = usersRepository.getAllUserInfoByUsername(users.getUsername());
		Users emailExists = usersRepository.findUserByEmail(users.getUser_email_address());
		
		final Date dateAndTimeToday = new Date();
		
		if(usernameExists != null) {
			status.addProperty("isAddingUserSuccess", "usernameExists");
		}else if(emailExists != null) {
			status.addProperty("isAddingUserSuccess", "emailExists");
		}else {
			users.setUser_status("Activated");
			users.setUser_sms_notif("Disabled");
			users.setUser_email_notif("Disabled");
			users.setUser_dateAndTimeCreated(dateAndTimeToday);
			
			usersService.saveUser(users,model);
			
			status.addProperty("isAddingUserSuccess", "successCreate");
			template.convertAndSend("/sloms/bulletin_board", "bulletin_board");
			
			//sendEmailConfirmationAfterAddingUser(users,httpSession);
			//sendSMSConfirmationAfterAddingUser(users);
		}
		
		return status.toString();
	}
	
	private String getUserDetails(Users users) {
		String message = "Hi, ";
		
		if(users.getUser_gender().equalsIgnoreCase("Male")) {
			message += "Mr. ";
		}else if(users.getUser_gender().equalsIgnoreCase("Female")) {
			message += "Ms. ";
		}
		
		message += users.getUser_firstname() + " " + users.getUser_lastname() + "!\n\n";
		message += "Below is your account information:\n\n";
		message += "Email: " + users.getUser_email_address() + "\n";
		message += "Username" + users.getUsername() + "\n";
		message += "Role: " + users.getUser_role() + "\n";
		message += "Temporary password: " + users.getUser_password() + "\n";
		
		message += "You can change it anytime!\nWelcome to SLOMS, Inc.\n\n " +
				"Note: You're currently tagged as 'Unauthenticated' because you are not yet approved by the management. \nPlease check your email from time to time!";
		
		return message;
	}
	
	@Async("processExecuter")
	void sendSMSConfirmationAfterAddingUser(Users users) {
		try {
			ClockWorkSmsService clockWorkSmsService = new ClockWorkSmsService("9c6adc7f85d45aed33f7b57477d93ebdbe6e563c");
			SMS sms = new SMS(users.getUser_phone_number(), getUserDetails(users));
			ClockworkSmsResult result = clockWorkSmsService.send(sms);
			
			if(result.isSuccess()) {
				System.out.println("Sent with ID: " + result.getId());
			}else {
				System.out.println("Error: " + result.getErrorMessage());
			}
			
		} catch (ClockworkException e) {
			e.printStackTrace();
		}
	}
	
	@Async("processExecuter")
	public void sendEmailConfirmationAfterAddingUser(Users users) {
		Mail mail = new Mail();
        mail.setFrom("sloms.inc03@gmail.com");
        mail.setTo(users.getUser_email_address());
        mail.setSubject("SLOMS, Inc. | Create Account");
        
        Map<String,String> model = new HashMap<String,String>();
        model.put("user_fullname", users.getUser_firstname() + " " + users.getUser_lastname());
        model.put("user_email", users.getUser_email_address());
        model.put("username", users.getUsername());
        model.put("user_password", users.getUser_password());
        model.put("user_role", users.getUser_role());
        
        Context context = new Context();
        context.setVariables(model);
        String html = templateEngine.process("create-account-email-template", context);
        
        mail.setContent(html);
        
        emailService.sendSimpleMessage(mail);
	}

	@Async("processExecuter")
	public void sendEmailConfirmationAfterApproveOrDeclineUser(Users loggedInUser, Users selectedUser){
		Mail mail = new Mail();
		mail.setFrom("sloms.inc03@gmail.com");
		mail.setTo(selectedUser.getUser_email_address());

		String subject,action,selectedUserFullname,selectedUserRole,approvedOrDeclinedBy;

		selectedUserFullname = selectedUser.getUser_firstname() + " " + selectedUser.getUser_lastname();
		selectedUserRole = selectedUser.getUser_role();
		approvedOrDeclinedBy = loggedInUser.getUser_firstname() + " " + loggedInUser.getUser_lastname() + " (" + loggedInUser.getUsername() + ")";

		if(!selectedUser.getUser_status().equals("Authenticated")){
			subject = "SLOMS, Inc. | You have been approved as a " + selectedUserRole + "!";
			action = "approved";
		}else{
			subject = "SLOMS, Inc. | You have been declined as a " + selectedUserRole + "!";
			action = "declined";
		}

		mail.setSubject(subject);

		Map<String,String> model = new HashMap<>();
		model.put("user_fullname", selectedUserFullname);
		model.put("action", action);
		model.put("approvedOrDeclinedBy", approvedOrDeclinedBy);
		model.put("emailAt", loggedInUser.getUser_email_address());

		Context context = new Context();
		context.setVariables(model);
		String html = templateEngine.process("approve-decline-email-template", context);

		mail.setContent(html);

		emailService.sendSimpleMessage(mail);
	}

	public void sendSecurityCodeToEmailAddress(String user_email_address, HttpSession httpSession,HttpServletRequest request) {
		Mail mail = new Mail();
        mail.setFrom("sloms.inc03@gmail.com");
        mail.setTo(user_email_address);
        mail.setSubject("SLOMS, Inc. | Forgot Password: Security Code");
        
        httpSession = request.getSession(true);
        
        Map<String,String> model = new HashMap<String,String>();
        model.put("user_fullname", httpSession.getAttribute("forgotPassword_fullname").toString());
       
		int randomSecurityCode = (int)(Math.random()*9000)+1000;
		
		System.out.println("Random Security Code: " + randomSecurityCode);
		
		httpSession.setAttribute("randomSecurityCode", randomSecurityCode);
		
		model.put("user_randomSecurityCode", httpSession.getAttribute("randomSecurityCode").toString());
		
        Context context = new Context();
        context.setVariables(model);
        String html = templateEngine.process("send-security-code-template", context);
        
        mail.setContent(html);
        
        emailService.sendSimpleMessage(mail);
	}

	@PostMapping("/add-user/add-user-profile-picture")
	@ResponseBody
	public void addUserProfilePicture(
			@RequestParam("qqfile") MultipartFile profilePicture,
			@RequestParam("qquuid") String uuid,
			@RequestParam("qqfilename") String fileName,
			HttpServletResponse response) throws IOException {
		
		String uploadedFilename = UploadImage.uploadImage(uuid, profilePicture, "src\\main\\webapp\\SLOMS_server\\user_profile_picture", "/user_profile_picture/");
		
		System.out.println("Uploading " + profilePicture.getOriginalFilename());
		System.out.println("UUID=" + uuid);
		
		JsonObject jsonObject = new JsonObject();
		
		jsonObject.addProperty("success", true);
		jsonObject.addProperty("profilePictureLink", uploadedFilename);
		
		response.setCharacterEncoding("UTF-8");
		response.setContentType("text/plain");
		response.getWriter().print(jsonObject);
		response.flushBuffer();	
	}

	@DeleteMapping("/add-user/delete-user-profile-picture/{qquuid}")
	public void deleteUserProfilePicture(
			HttpServletResponse response,
			HttpServletRequest request,
			@PathVariable("qquuid") String qquuid,
			@RequestParam("filename") String filename) throws IOException {
		
		System.out.println("uploadDelete() called " + filename);
		
		DeleteImage.deleteImage(qquuid, "src\\main\\webapp\\SLOMS_server\\user_profile_picture\\");
		
		response.setStatus(200);
		response.flushBuffer();
	}

	@PostMapping("/super-admin/update-user-role")
	@ResponseBody
	public String updateUserRole(
			@RequestParam String user_email_address,
			@RequestParam String user_role,
			@RequestParam String user_status,
			@RequestParam Integer user_id) {
		
		Users userIdExists = usersRepository.findUserByUserId(user_id);
		
		if(userIdExists != null) {
			usersRepository.updateUserEmail_Role_Status(user_email_address, user_role, user_status, user_id);
			
			status.addProperty("isUpdateUser_Email_Role_Status_Success", true);
		}else {
			status.addProperty("isUpdateUser_Email_Role_Status_Success", false);
		}
		
		return status.toString();
	}

	@PostMapping("/update-user-profile-picture")
	public String updateProfilePicture(
			@RequestParam Integer user_id,
			@RequestParam String username,
			@RequestParam("user_profile_picture") String user_profile_picture) {

		Users userIdExists = usersRepository.findUserByUserId(user_id);
		
		if(userIdExists != null) {
			if(userIdExists.getUser_profile_picture() != null || userIdExists.getUser_profile_picture() != ""){
				DeleteImage.deleteImageInFolder("src\\main\\webapp\\SLOMS_server\\" + userIdExists.getUser_profile_picture());
			}
			
			usersRepository.updateUserProfilePicture(user_profile_picture.replaceAll(",", ""),user_id);
			status.addProperty("isUpdateUserProfilePictureSuccess", true);
		}else {
			status.addProperty("isUpdateUserProfilePictureSuccess", false);
		}
		
		return status.toString();
	}

	@PostMapping("/profile/update-username")
	public String updateUsername(@RequestParam String username, Principal principal) {
		Users loggedInUser = usersService.findUserByUsername(principal.getName());
		usersService.updateUsername(username.trim(),loggedInUser.getUser_id());
		
		status.addProperty("isUpdateUsernameSuccess", true);
		
		return status.toString();
	}

	@PostMapping("/profile/update-password")
	public String updateUserPassword(@RequestParam String user_password, Principal principal) {
		Users loggedInUser = usersService.findUserByUsername(principal.getName());
		usersService.updateUserPassword(bCryptPasswordEncoder.encode(user_password.trim()),loggedInUser.getUsername());

		status.addProperty("isUpdatePasswordSuccess", true);
		
		return status.toString();
	}

	@PostMapping("/profile/update-profile-information")
	public String updateProfileInformation(
			@RequestParam String user_firstname,
			@RequestParam String user_lastname,
			@RequestParam String user_email_address,
			@RequestParam String user_phone_number,
			@RequestParam String user_gender,
			@RequestParam String user_sms_notif,
			@RequestParam String user_email_notif,
			Principal principal) {

		Users loggedInUser = usersService.findUserByUsername(principal.getName());

		usersService.updateProfileInformation(
				user_firstname.trim(),
				user_lastname.trim(),
				user_email_address.trim(), CharMatcher.anyOf("()- ").removeFrom(user_phone_number.trim()),
				user_gender.trim(),
				user_sms_notif.trim(),
				user_email_notif.trim(),
				loggedInUser.getUser_id());
		
		status.addProperty("isUpdateProfileInformationSuccess", true);
		
		return status.toString();
	}

	@GetMapping("/sloms_users")
	public List<UsersDao> getAllUsers(){
		return usersService.getAllUsers();
	}

	@PostMapping("/sloms_users/approveOrDecline")
	@ResponseBody
	public String approveOrDecline(@RequestParam Integer user_id, @RequestParam String user_status, Principal principal){
		Users loggedInUser = usersService.findUserByUsername(principal.getName());
		Users selectedUser = usersService.findUserById(user_id);

		if(selectedUser.getUser_role().equals(RoleConstants.SYS_ADMIN) && loggedInUser.getUser_role().equals(RoleConstants.IT_ADMIN) || selectedUser.getUser_role().equals(RoleConstants.SYS_SPECIALIST) && loggedInUser.getUser_role().equals(RoleConstants.SYS_ADMIN)){
			if(selectedUser.getUser_id().equals(loggedInUser.getUser_id()) && selectedUser.getUser_status().equals("Authenticated") && !user_status.equals("Unauthenticated") || selectedUser.getUser_status().equals("Unauthenticated") && !user_status.equals("Authenticated")){
				status.addProperty("status", "INVALID");
			}else{
				String result = usersService.approveOrDecline(user_id,user_status);

				if(result.equals("GOOD")){
					sendEmailConfirmationAfterApproveOrDeclineUser(loggedInUser,selectedUser);
					template.convertAndSend("/sloms/bulletin_board", "bulletin_board");
					template.convertAndSend("/sloms/users", "users");
					status.addProperty("status","SUCCESS");
				}else{
					status.addProperty("status", "FAILED");
				}
			}
		}else{
			status.addProperty("status","ACCESS_DENIED");
		}

		return status.toString();
	}
}
