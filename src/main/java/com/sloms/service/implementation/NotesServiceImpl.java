package com.sloms.service.implementation;

import java.util.Date;
import java.util.List;

import com.sloms.service.NotesService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;

import com.sloms.entity.Notes;
import com.sloms.entity.Notifications;
import com.sloms.repository.NotesRepository;
import com.sloms.repository.NotificationsRepository;

@Service
@Transactional(readOnly=true)
public class NotesServiceImpl implements NotesService {
	
	@Autowired
	private NotesRepository notesRepository;
	
	@Autowired
	private NotificationsRepository notificationsRepository;
	
	@Override
	@Transactional
	public void saveNotes(Notes notesDAO, Notifications notificationsDAO) {
		Notes notes = new Notes();
		Notifications notifications = new Notifications();
		
		//notes
		notes.setNotes_type(StringUtils.capitalize(notesDAO.getNotes_type().trim()) + " Notes");
		notes.setNotes_description(notesDAO.getNotes_description().trim());
		notes.setNotes_billboard(notesDAO.getNotes_billboard());
		notes.setNotes_created_by(notesDAO.getNotes_created_by());
		notes.setNotes_status(notesDAO.getNotes_status().trim());
		notes.setNotes_dateAndTimeCreated(notesDAO.getNotes_dateAndTimeCreated());
		notes.setNotes_dateAndTimeModified(notesDAO.getNotes_dateAndTimeModified());
		
		//notification
		notifications.setNotes(notes);
		notifications.setNotif_type("add_notes");
		
		notesRepository.save(notes);
		notificationsRepository.save(notifications);
	}
	
	@Override
	@Transactional
	public void updateNotes(Notes notesDAO) {
		final Date dateAndTimeModified = new Date();
		
		//update notes
		notesRepository.updateNotes(
				notesDAO.getNotes_type(), 
				notesDAO.getNotes_description(), 
				notesDAO.getNotes_status(), 
				dateAndTimeModified, 
				notesDAO.getNotes_id());
	}

	@Override
	public Notes getNotesById(Integer notesID) {
		return notesRepository.findOne(notesID);
	}

	@Override
	public List<Notes> getAllNotes() {
		return notesRepository.getAllNotes();
	}
	
	@Override
	public List<Notes> getAllActivatedNotes() {
		return notesRepository.getAllActivatedNotes();
	}
}
