package com.sloms.service;

import com.sloms.classes.Mail;

public interface EmailService{
	void sendSimpleMessage(final Mail mail);
}
