package com.sloms.entity;

import lombok.*;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.validation.constraints.NotNull;

@Getter
@Setter
@ToString
@EqualsAndHashCode
@NoArgsConstructor
@Entity
public class MediaClientDetails{
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Integer id;

	@NotNull
	@OneToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="media_id",nullable = false)
	private Media media;
	
	@NotNull
	private String media_client;

	@NotNull
	private String media_contract_start;
	
	@NotNull
	private String media_expiration;
	
	@NotNull
	private String media_contracted_rate;

}
