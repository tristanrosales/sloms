<div class="modal" id="add_new_comment-modal" tabindex="-1" role="dialog" aria-labelledby="largeModalHead" aria-hidden="true" data-backdrop="static" data-keyboard="false">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<form id="frmAddComment" class="form-horizontal" role="form" action="/add-comment" method="POST">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">
						<span aria-hidden="true">&times;</span><span class="sr-only">Close</span>
					</button>
					<h3 class="modal-title" id="largeModalHead">Add Comment</h3>
				</div>

				<div class="modal-body" style="max-height: calc(100vh - 200px); overflow-y: auto;">		
					<input type="hidden" name="thread_id" id="frmAddComment_lblThread_id">
					
					<div class="form-group">
						<label class="col-md-2 control-label">Comment</label>
						<div class="col-md-10">

							<textarea 
								class="form-control" 
								rows="5"
								placeholder="Write your comment..." 
								required="required"
								name="thread_comment" 
								id="frmAddComment_thread_comment" 
								style="resize: none; background-color:white !important;" 
								maxlength="5000"></textarea>

							<span class="help-block">Required comment</span>
						</div>
					</div>

					<div class="form-group">
						<div class="col-md-12">
							<div class="panel panel-default">
								<div class="panel-body">
									<h3>
										<span class="fa fa-download"></span> Comment Images
									</h3>
									
									<div id="thread-comment-images-holder"></div>
								</div>
							</div>
						</div>
					</div>
					
					<script>
						var threadCommentImagesMultiFileUploader = new qq.FineUploader({
							element: document.getElementById('thread-comment-images-holder'),
							template: 'qq-template-manual-trigger',
							validation: {
								allowedExtensions: ["jpeg","jpg","png"],
								itemLimit: 10
							},
							request: {
								endpoint: '/add-comment/add-thread-comment-images',
								method: "POST"
							},
							deleteFile: {
								enabled: true,
								forceConfirm: true,
								endpoint: '/add-comment/delete-thread-comment-images',
								method: "DELETE"
							},
							thumbnails: {
								placeholders: {
									waitingPath: '/resources/js/plugins/fine-uploader/placeholders/waiting-generic.png',
									notAvailablePath: '/resources/js/plugins/fine-uploader/placeholders/not_available-generic.png'
								}
							},
							autoUpload: false,
							debug: true,
							callbacks: {
								onComplete: function (id,name,responseJSON,maybeXhr){
									console.log(responseJSON);
									$('#lblAddComment_' + id).val(responseJSON.threadCommentImageLink);
								},
								onCancel: function(id,name){
									console.log("cancel");
									$('#lblAddComment_' + id).remove();
								},
								onError: function(id,name,reason,maybeXhrOrXdr){
									//logic here
								},
								onAllComplete: function(successful,failed){
									if(failed.length <= 0){
										//logic here
									}else{
										//logic here
									}
								},
								onSubmitted: function(id,name){
									$(".qq-uploader-selector").append("<input type='hidden' id='lblAddComment_" + id + "' name='thread_comment_images' value='' />");
								},
								onUpload (id,name){
									console.log(name + " before uploading...");
								},

								onSubmitDelete: function(id){
									this.setDeleteFileParams({filename: this.getName(id)}, id);
								},
								onDeleteComplete: function(id){
									$('#lblAddComment_' + id).remove();
								}
							}
						});
						
						qq(document.querySelector("#thread-comment-images-holder #trigger-upload")).attach("click", function () {
							threadCommentImagesMultiFileUploader.uploadStoredFiles();
						});

						$('#add_new_comment-modal').on('hidden.bs.modal', function () {
							$('#frmAddComment')[0].reset();
							threadCommentImagesMultiFileUploader.cancelAll();
							threadCommentImagesMultiFileUploader.clearStoredFiles();
						});

						function addMaxLengthTo(input){
							$(input).maxlength({
								alwaysShow: true,
								threshold: 10,
								warningClass: "label label-success",
								limitReachedClass: "label label-danger",
								separator: ' of ',
								preText: 'You have ',
								postText: ' chars remaining.',
								validate: true
							});
						}

						function implementBootstrapMaxLength(){
							addMaxLengthTo('textarea#frmAddComment_thread_comment');
						}

						implementBootstrapMaxLength();
					</script>
				</div>

				<div class="modal-footer">
					<button 
						type="submit" 
						class="btn btn-info btn-lg btn-rounded" 
						style="padding: 10px; font-size: 15px; background-color:#1caf9a; color: white;">
						<strong>
							<span class="fa fa-hand-o-right"></span> Add Comment
						</strong>
					</button>

					<button 
						type="button" 
						class="btn btn-warning btn-rounded" 
						style="padding: 10px; font-size: 15px; background-color: orange; color: white;" 
						data-dismiss="modal">
						<strong>
							<span class="glyphicon glyphicon-remove-sign"></span> Close
						</strong>
					</button>
				</div>
			</form>
		</div>
	</div>
</div>