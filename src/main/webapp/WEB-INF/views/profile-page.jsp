<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!-- PAGE CONTENT -->
<div class="page-content">

	<tiles:insertAttribute name="x_navigationVertical" />

	<!-- START BREADCRUMB -->
	<ul class="breadcrumb">
		<li><a href="/home">Home</a></li>
		<li class="active">Profile</li>
	</ul>
	<!-- END BREADCRUMB -->

	<!-- PAGE TITLE -->
	<div class="page-title">
		<h2>
			<span class="fa fa-user"></span> Profile<br>
		</h2>
	</div>
	<!-- END PAGE TITLE -->
	
	<!-- PAGE CONTENT WRAPPER -->
	<div class="page-content-wrap">
		<div class="row">
			<div class="col-md-12">
				<div class="panel panel-default tabs">
					<ul class="nav nav-tabs" role="tablist">
						<li class="active">
							<a href="#my_account" role="tab" data-toggle="tab">My Account</a>
						</li>
						<c:choose>
							<c:when test="${user.user_role == 'IT_ADMIN' || user.user_role == 'GUEST'}">
								<li><a href="#my_feedback" role="tab" data-toggle="tab">My Feedback</a></li>
							</c:when>
							<c:otherwise>
								<li><a href="#my_media" role="tab" data-toggle="tab">My Media</a></li>
								<li><a href="#my_group" role="tab" data-toggle="tab">My Group</a></li>
								<li><a href="#my_thread" role="tab" data-toggle="tab">My Thread</a></li>
							</c:otherwise>
						</c:choose>
					</ul>

					<div class="panel-body tab-content">
						<style>
							ol.orderedList_my{
								font-size: 15px;
							}
						
							ol.orderedList_my li:not(:last-child) {
								margin-bottom: 10px;
							}
						</style>

						<div class="tab-pane active" id="my_account">							
							<div class="panel-body">
								<div style="float: right; margin-right: 10px; margin-bottom: 10px;">
									<button 
										type="button" 
										id="profile_btnChangePassword" 
										class="btn btn-warning btn-lg btn-rounded" 
										data-toggle='modal' 
										data-target='#change_password-modal' 
										style="padding: 10px; font-size: 15px; background-color:orange; color: white;">

										<span class="fa fa-key"></span> Change Password

									</button>
									
									<button 
										type="button" 
										id="profile_btnEditAccount" 
										class="btn btn-info btn-lg btn-rounded" 
										data-toggle='modal' 
										data-target='#edit_profile_information-modal' 
										style="padding: 10px; font-size: 15px; background-color:#1caf9a; color: white;">
										
										<span class="fa fa-edit"></span> Edit Information
									</button>

									<button 
										type="button" 
										id="profile_btnChangeProfilePicture" 
										class="btn btn-danger btn-lg btn-rounded" 
										data-toggle='modal' 
										data-target='#edit_profile_picture-modal' 
										style="padding: 10px; font-size: 15px; background-color:red; color: white;">
										
										<span class="fa fa-camera"></span> Change profile picture
									</button>
								</div>

								<div class="contact-info">
									<p style="font-size: 18px;"><small>Fullname</small><br/>${user.user_firstname} ${user.user_lastname}</p>
									<p style="font-size: 18px;"><small>Role</small><br/>${user.user_role}</p>
									<p style="font-size: 18px;"><small>Username</small><br/>${user.username}
										&nbsp;&nbsp;&nbsp;
										<button 
											type="button" 
											id="profile_btnChangeUsername" 
											class="btn btn-info btn-lg btn-rounded" 
											data-toggle='modal' 
											data-target='#change_username-modal' 
											style="padding: 10px; font-size: 15px; background-color:#1caf9a; color: white;">

											<span class="fa fa-edit"></span> Change username

										</button>
									</p>

									<p style="font-size: 18px;"><small>Email</small><br/>${user.user_email_address}</p>
									<p style="font-size: 18px;"><small>Phone Number</small><br/>${user.user_phone_number}</p>
									<p style="font-size: 18px;"><small>Gender</small><br/>${user.user_gender}</p>

									<hr width="100%">
									<div class="alert alert-danger" role="alert" id="profile_emptySecurityQuestionAndSecretAnswer_detector" style="display: none; font-size: 15px;">
										<button type="button" class="close" data-dismiss="alert"></button>
										<strong style="font-size: 15px;">Oops!</strong> It seems that you don't have any security question with secret answer yet. It will be helpful when you forgot your password.
										
										<br><br>
										
										<button 
											type="button" 
											style="font-size: 15px; white-space: normal !important; word-wrap: break-word;" 
											id="profile_btnAddSecurityQuestionWithSecretAnswer" 
											class="btn btn-warning" 
											data-toggle='modal' 
											data-target='#add_security_question_with_secret_answer-modal'>
											Set-up security question with secret answer now!
										</button>
									</div>
								</div>
							</div> 
						</div>

						<c:choose>
							<c:when test="${user.user_role != 'IT_ADMIN' || user.user_role != 'GUEST'}">
								<div class="tab-pane" id="my_media" style="font-size: 15px;">
									<div class="alert alert-info" role="alert" id="reminder_adding-media" style="display: none;">
										<button type="button" class="close" data-dismiss="alert"></button>
										<strong>Reminder!</strong> You can't add a media or billboard unless there is a group added.
										To add a group, click this <a style="color: black; font-weight: bold;" data-toggle="modal" data-target="#add_new_group-modal">link</a>.
									</div>

									<!--
                                    <button
                                        type="button"
                                        id="media-page_btnAddNewMedia"
                                        class="btn btn-info btn-lg"
                                        style="display: none;"
                                        data-toggle="modal"
                                        data-target="#add_new_media-modal">
                                        <span class="glyphicon glyphicon-plus-sign"></span> Add New Media
                                    </button><br><br>
                                    -->

									<table id="tblMyMedia" class="display" style="width:100%">
										<thead style="background-color: #1caf9a; color: white;">
										<tr>
											<th style="text-align: center;">Details</th>
											<th style="text-align: center;">Name</th>
										</tr>
										</thead>

										<tfoot style="background-color: #1caf9a; color: white;">
										<tr>
											<th style="text-align: center;">ID</th>
											<th style="text-align: center;">Name</th>
										</tr>
										</tfoot>
									</table>
								</div>

								<div class="tab-pane" id="my_group" style="font-size: 15px;">
									<!--
                                    <button
                                        type="button"
                                        id="groups-page_btnAddNewGroup"
                                        class="btn btn-info btn-lg"
                                        style="display: none;"
                                        data-toggle="modal"
                                        data-target="#add_new_group-modal">
                                        <span class="glyphicon glyphicon-plus-sign"></span> Add New Group
                                    </button><br><br>
                                    -->

									<table id="tblMyGroup" class="display" style="width:100%">
										<thead style="background-color: #1caf9a; color: white;">
										<tr>
											<th style="text-align: center;">Details</th>
											<th style="text-align: center;">Name</th>
										</tr>
										</thead>

										<tfoot style="background-color: #1caf9a; color: white;">
										<tr>
											<th style="text-align: center;">Details</th>
											<th style="text-align: center;">Name</th>
										</tr>
										</tfoot>
									</table>
								</div>

								<div class="tab-pane" id="my_thread" style="font-size: 15px;">
									<table id="tblMyThread" class="display" style="width:100%">
										<thead style="background-color: #1caf9a; color: white;">
										<tr>
											<th style="text-align: center;">Details</th>
											<th style="text-align: center;">Thread Name</th>
										</tr>
										</thead>

										<tfoot style="background-color: #1caf9a; color: white;">
										<tr>
											<th style="text-align: center;">Details</th>
											<th style="text-align: center;">Thread Name</th>
										</tr>
										</tfoot>
									</table>
								</div>
							</c:when>

							<c:otherwise>
								<div class="tab-pane" id="my_feedback">
									<ol id="orderedList_myFeedback" class="orderedList_my"></ol>
								</div>
							</c:otherwise>
						</c:choose>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!-- END PAGE CONTENT WRAPPER -->
</div>
<!-- END PAGE CONTENT -->

<tiles:insertAttribute name="view_edit_user_modal" />
	
<tiles:insertAttribute name="edit_profile_picture_modal" />
<tiles:insertAttribute name="change_username_modal" />
<tiles:insertAttribute name="change_password_modal" />


<tiles:insertAttribute name="view_edit_thread_modal" />

<tiles:insertAttribute name="edit_profile_information_modal" />


<tiles:insertAttribute name="add_media_modal" />
<tiles:insertAttribute name="view_edit_media_modal" />

<tiles:insertAttribute name="add_new_group_modal" />
<tiles:insertAttribute name="view_edit_group_modal" />

<script type="text/javascript" src="/resources/js/profile.js"></script>