package com.sloms.entity;

import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.*;

@Getter
@Setter
@ToString
@EqualsAndHashCode
@NoArgsConstructor
@Entity
public class Changelog{	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Integer changelog_id;

	@NotNull
	@Column(length=1000)
	private String changelog_version;

	@NotNull
	@Column(length=5000)
	private String changelog_details;
	
	@Temporal(TemporalType.TIMESTAMP)
	private Date changelog_dateAndTimeAdded;
	
	@Temporal(TemporalType.TIMESTAMP)
	private Date changelog_modified;
	
	@ManyToOne(fetch=FetchType.LAZY)
    @JoinColumn(name = "changelog_added_by")
	@JsonIgnoreProperties(
			{
				"hibernateLazyInitializer",
				"handler",
				"medias",
				"user_password",
				"user_phone_number",
				"user_status",
				"user_sms_notif",
				"user_email_notif",
				"user_security_question",
				"user_secret_answer",
				"user_dateAndTimeCreated"
			})
	private Users changelog_added_by;
	
	@OneToMany(
			cascade = CascadeType.ALL,
			fetch = FetchType.LAZY,
			mappedBy = "changelog")
	@JsonIgnore
	private List<Notifications> notifications;

}
