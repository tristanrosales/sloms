<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!-- START PAGE SIDEBAR -->
<div class="page-sidebar">
	<!-- START X-NAVIGATION -->
	<ul class="x-navigation">
		<li class="xn-logo">
			<a href="/home" style="text-align: center;">
				SLOMS, Inc.
			</a> 
			<a href="#" class="x-navigation-control"></a>
		</li>
		<li class="xn-profile">
			<a href="/profile" class="profile-mini">
				<c:choose>
					<c:when test="${not empty user.user_profile_picture}">
						<img src="/SLOMS_server${user.user_profile_picture}" id="navigation-profile-mini-user_profile_picture" style="cursor: pointer;"/>
					</c:when>
					<c:otherwise>
						<img src="resources/assets/images/users/no-image.jpg" id="navigation-profile-mini-user_profile_picture" style="cursor: pointer;"/>
					</c:otherwise>
				</c:choose>
			</a>
			<div class="profile">
				<div class="profile-image">
					<a href="/profile">
						<c:choose>
							<c:when test="${not empty user.user_profile_picture}">
								<img src="/SLOMS_server${user.user_profile_picture}" id="navigation-profile-image-user_profile_picture" style="cursor: pointer;"/>
							</c:when>
							<c:otherwise>
								<img src="resources/assets/images/users/no-image.jpg" id="navigation-profile-image-user_profile_picture" style="cursor: pointer;"/>
							</c:otherwise>
						</c:choose>
					</a>
				</div>
				<div class="profile-data">
					<div class="profile-data-name">${user.user_firstname} ${user.user_lastname}</div>
					<div class="profile-data-title"><span id="loggedInUserRole">${user.user_role}</span></div>

					<span id="loggedInUserId" style="display: none;">${user.user_id}</span>
				</div>
			</div>
		</li>

		<li>
			<a href="/profile"> 
				<span class="fa fa-user"></span>
				<span class="xn-text">My Account</span>
			</a>
		</li>

		<li>
			<a href="#" class="mb-control" data-box="#mb-signout">	
				<span class="fa fa-sign-out"></span>
				<span class="xn-text">Logout</span>
			</a>
		</li>

		<li class="xn-title">Navigation</li>
		<li>
			<a href="/home"> 
				<span class="fa fa-dashboard"></span> 
				<span class="xn-text">Dashboard</span>
			</a>
		</li>

		<c:choose>
			<c:when test="${user.user_role != 'GUEST'}">
				<li>
					<a href="/media_page">
						<span class="fa fa-picture-o"></span>
						<span class="xn-text">Media</span>
					</a>
				</li>

				<li>
					<a href="/users_page">
						<span class="fa fa-users"></span>
						<span class="xn-text">Users</span>
					</a>
				</li>
			</c:when>
		</c:choose>

		<li>
			<a href="/groups_page">
				<span class="fa fa-globe"></span>
				<span class="xn-text">Groups</span>
			</a>
		</li>

		<li>
			<a href="/faqs_page"> 
				<span class="fa fa-question-circle"></span>
				<span class="xn-text">FAQs</span>
			</a>
		</li>

		<li>
			<a href="/feedbacks_and_help"> 
				<span class="fa fa-thumbs-o-up"></span>
				<span class="xn-text">Feedbacks & Help</span>
			</a>
		</li>

		<li>
			<a href="/about"> 
				<span class="fa fa-info-circle"></span>
				<span class="xn-text">About</span>
			</a>
		</li>
	</ul>
	<!-- END X-NAVIGATION -->
</div>
<!-- END PAGE SIDEBAR -->