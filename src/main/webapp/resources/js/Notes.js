var stompClient = null;
var userID = document.getElementById("loggedInUserId").innerText;
var userRole = document.getElementById("loggedInUserRole").innerText;

var IT_ADMIN = "IT_ADMIN";
var SYS_ADMIN = "SYS_ADMIN";
var SYS_SPECIALIST = "SYS_SPECIALIST";

function connectWebSocket(){
    var socket = new SockJS('/sloms_web_socket');
    stompClient = Stomp.over(socket);

    stompClient.connect({}, function(frame) {
		stompClient.subscribe("/sloms/errors", function(message) {
            console.log("Web Socket Error Message : " + message.body);
            connectWebSocket();
        });
	},function(error) {
        console.log("Web Socket Error Message : " + error);
    });
}

function populateNotesTypeInViewEdit(){
    $.getJSON("/activated_notes", function(response) {
        $.each(response, function(key,data) {
            $('#viewEdit_cmbNotes_type').append("<option value='" + data.notes_id + "'>" + data.notes_type + "</option>");
        });

        $('#viewEdit_cmbNotes_type').selectpicker('refresh');
        $('#viewEdit_cmbNotes_type').selectpicker('render');
    });
}

function showNotesDetails(d) {
	var table,access;

	table = '<table cellpadding="5" cellspacing="0" border="0" style="padding-left:50px;" class="table table-bordered table-striped display">'+
				'<tr>'+
					'<td>Description:</td>'+
					'<td>'+d.notes_description+'</td>'+
				'</tr>'+
				'<tr>'+
					'<td>Creator:</td>'+
					'<td>'+d.notes_created_by.user_firstname + " " + d.notes_created_by.user_lastname +'</td>'+
				'</tr>'+
				'<tr>'+
					'<td>Created:</td>'+
					'<td>'+ moment(new Date(d.notes_dateAndTimeCreated)).format('LLLL') +'</td>'+
				'</tr>'+
				'<tr>'+
					'<td>Modified:</td>' +
					'<td>'+ ((d.notes_dateAndTimeModified == null) ? "None" : moment(new Date(d.notes_dateAndTimeModified)).format('LLLL')) +'</td>'+
				'</tr>'+
				'<tr>'+
					'<td>Status:</td>'+
					'<td>'+ d.notes_status +'</td>'+
				'</tr>'+
				'<tr>';

				if(userRole == IT_ADMIN || userRole == SYS_ADMIN){
					access = '<button type="button" class="btn btn-info btn-lg btn-rounded" style="padding: 10px; font-size: 15px; background-color:#1caf9a; color: white;" data-toggle="modal" data-target="#view_edit_notes-modal"><strong><span class="fa fa-edit"></span> Edit This Note</strong></button>';
				}else{
					access = "<span class='fa fa-ban' style='font-size: 18px;'><br>Access Denied</span>";
				}

		table += '<td colspan="2" style="text-align: center;">' + access + '</td>'+
				'</tr>'+
			'</table>';

			$('#frmUpdateDeleteNotes_lblNotes_id').val(d.notes_id);
			$('#frmUpdateDeleteNotes_lblNotes_createdBy').val(d.notes_created_by.user_id);
			$('#frmUpdateDeleteNotes_lblNotes_dateAndTimeCreated').val(moment(new Date(d.notes_dateAndTimeCreated)).format('LLLL'));
			
			$('#frmUpdateDeleteNotes_lblNotes_type').text("Edit " + d.notes_type);
			
			$('#frmUpdateDeleteNotes_txtNotes_type').val(d.notes_type);
			$('#frmUpdateDeleteNotes_txtNotes_description').val(d.notes_description);
			
			$('#frmUpdateDeleteNotes_cmbNotes_status')
				.removeAttr('selected')
				.find('option[value="' + d.notes_status + '"]')
				.attr("selected",true)
				.change();

	return table;
}

function showAllNotes(){
	$("#tblNotes tbody").empty();

	var table = $("#tblNotes").DataTable({
		"ajax": {
			"url": "/notes",
			"dataSrc": ""
		},
		"columns": [
			{
				"className": 'details-control',
				"orderable": false,
				"data": null,
				"defaultContent": ''
			},
			{ "data": "notes_type" }
		],
		"iDisplayLength": 5,
		"aLengthMenu": [[5, 10, 25, 50, 100, -1], [5, 10, 25, 50, 100, "All"]],
		"pagingType": "full_numbers"
	});

    $('#tblNotes tbody').on('click', 'td.details-control', function () {
        var tr = $(this).closest('tr');
        var row = table.row(tr);
 
        if (row.child.isShown()) {
            row.child.hide();
            tr.removeClass('shown');
        }
        else {
            row.child(showNotesDetails(row.data())).show();
            tr.addClass('shown');
        }
	});
}

function validateNotes(){
    $("#frmAddNotes").validate({
        ignore : [],
        rules : {
            notes_type: {
                required : true,
                minlength : 5,
                maxlength : 30
            },
            notes_description: {
                required : true,
                minlength : 20,
                maxlength : 5000
            }
        },
        submitHandler: function(form) {
            addNotes(form);
        }
    });
}

function addNotes(form){
	var formEl = $(form);
	
	swal({
		title: "Do you still want to continue?",
		icon: "warning",
		buttons: ["No!", "Yes!"],
		dangerMode: true
	})
	.then((willAdd) => {
		if (willAdd) {
			$('body').loadingModal({
				position: 'auto',
				text: 'Please wait while creating this note...',
				color: '#fff',
				opacity: '0.7',
				backgroundColor: 'rgb(0,0,0)',
				animation: 'doubleBounce'
			});

			$.ajax({
				type: 'POST',
				dataType : 'JSON',
				url: formEl.prop('action'),
				data: formEl.serialize(),
				success: function(data) {
					$('body').loadingModal('destroy');

					if(data.isAddingNotesSuccess){
						iziToast.success({
							title: 'Congratulations!',
							theme: 'light', // dark
							color: 'green', // blue, red, green, yellow
							position: 'bottomRight', // bottomRight, bottomLeft, topRight, topLeft, topCenter, bottomCenter, center
							message: $('#frmAddNotes_type').val() + " Notes has been added successfully!"
						});

						$("#add_new_note-modal").modal('toggle');
					}
				},
				error: function(e){
					console.log(e);
					$('body').loadingModal('destroy');
                    iziToast.error({
                        title: 'Oops! There is an error!',
                        theme: 'light',
                        color: 'red',
                        position: 'bottomRight',
                        message: "Failed to create a note. Please try again!"
                    });
				}
			});
		}
	});
}

function updateNotes(){
	$('#frmUpdateDeleteNotes').submit(function(event) {
		event.preventDefault();
		var formEl = $(this);
	
		swal({
			  title: "Are you sure?",
			  text: "Once updated, you will not be able to undo this action!",
			  icon: "warning",
			  buttons: ["No, not sure yet!", "Yes, I'm Sure!"],
			  dangerMode: true
		})
		.then((willUpdate) => {
			if (willUpdate) {
				$.ajax({
					type: 'POST',
					dataType : 'JSON',
					url: formEl.prop('action'),
					data: formEl.serialize(),
					// if received a response from the server
					success: function( data, textStatus, jqXHR) {
						if(data.isUpdateNotesSuccess){
							iziToast.success({
								title: 'Congratulations!',
								theme: 'light', // dark
								color: 'green', // blue, red, green, yellow
								position: 'bottomRight', // bottomRight, bottomLeft, topRight, topLeft, topCenter, bottomCenter, center
								message: "Notes has been updated successfully!"
							});
	
							$("#view_edit_notes-modal").modal('toggle');
							$('#tblNotes').DataTable().ajax.reload();
						}
					},
					error: function(e){
						console.log(e);
					}
				});
			}
		});
	});
}

function deleteNotes(){
	$('body').on('click', '#btnDeleteNotes',function(){	
		swal({
			  title: "Are you sure?",
			  text: "Once deleted, you will not be able to recover this group!",
			  icon: "warning",
			  buttons: true,
			  dangerMode: true,
		})
		.then((willDelete) => {
			if (willDelete) {
				$.ajax({
	              	type: 'POST',
	              	dataType : 'JSON',
	              	url: "/frmDeleteNotes",
	              	data: $('#frmUpdateDeleteNotes').serialize(),
	              	// if received a response from the server
					success: function( data, textStatus, jqXHR) {
						if(data.status){
							swal("Notes has been deleted successfully!", {
								icon: "success"
							}).then((willDelete) => {
								if(willDelete){
									location.reload(); 
								}
							});
						}
					},
					error: function(e){
						console.log(e);
					}
	          });
			}
		});
	});
}

//call all methods
connectWebSocket();
populateNotesTypeInViewEdit();
showAllNotes();
validateNotes();
updateNotes();