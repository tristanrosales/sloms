<div class="modal" id="add_new_note-modal" tabindex="-1" role="dialog" aria-labelledby="largeModalHead" aria-hidden="true" data-backdrop="static" data-keyboard="false">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<form id="frmAddNotes" class="form-horizontal" role="form" action="/add-notes" method="POST">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">
						<span aria-hidden="true">&times;</span><span class="sr-only">Close</span>
					</button>
					<h3 class="modal-title" id="largeModalHead">Add Notes</h3>
				</div>

				<div class="modal-body" style="max-height: calc(100vh - 200px); overflow-y: auto;">
					<div class="alert alert-info" role="alert" style="font-size: 15px;">
						<button type="button" class="close" data-dismiss="alert"></button>
						<strong>Reminder!</strong> For adding notes, you can write the note type without writing the word "notes" after the note type.<br>
						Example: 
						<ul>
							<li>Client</li>
							<li>Finance</li>
						</ul>
					</div>

					<input type="hidden" id="frmAddNotes_billboard" name="notes_billboard">

					<div class="form-group">
						<label class="col-md-2 control-label">Type</label>
						<div class="col-md-10">
							<input 
								type="text" 
								class="form-control"
								placeholder="Enter notes type..." 
								required="required"
								name="notes_type" 
								id="frmAddNotes_type" 
								style="text-transform: capitalize; background-color:white !important;" 
								maxlength="30"/> 
								
							<span class="help-block">Required notes type</span>
						</div>
					</div>

					<div class="form-group">
						<label class="col-md-2 control-label">Description</label>
						<div class="col-md-10">

							<textarea 
								class="form-control" 
								rows="5"
								placeholder="Enter description of notes..." 
								required="required"
								name="notes_description" 
								id="frmAddNotes_description"
								style="resize: none; background-color:white !important;" 
								maxlength="5000"></textarea>

							<span class="help-block">Required notes description</span>
						</div>
					</div>

					<script>
						function addMaxLengthTo(input){
							$(input).maxlength({
								alwaysShow: true,
								threshold: 10,
								warningClass: "label label-success",
								limitReachedClass: "label label-danger",
								separator: ' of ',
								preText: 'You have ',
								postText: ' chars remaining.',
								validate: true
							});
						}

						function implementBootstrapMaxLength(){
							addMaxLengthTo('input#frmAddNotes_type');
							addMaxLengthTo('textarea#frmAddNotes_description');
						}

						implementBootstrapMaxLength();

						$('#add_new_note-modal').on('hidden.bs.modal', function () {
							$('#frmAddNotes')[0].reset();
						});
					</script>
				</div>

				<div class="modal-footer">
					<button 
						type="submit" 
						class="btn btn-info btn-lg btn-rounded" 
						style="padding: 10px; font-size: 15px; background-color:#1caf9a; color: white;">
						<strong>
							<span class="fa fa-hand-o-right"></span> Add Note
						</strong>
					</button>

					<button 
						type="button" 
						class="btn btn-warning btn-rounded" 
						style="padding: 10px; font-size: 15px; background-color: orange; color: white;" 
						data-dismiss="modal">
						<strong>
							<span class="glyphicon glyphicon-remove-sign"></span> Close
						</strong>
					</button>
				</div>
			</form>
		</div>
	</div>
</div>