package com.sloms.service.implementation;

import com.sloms.entity.Users;
import com.sloms.service.FeedbacksService;
import com.sloms.service.UsersService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.sloms.entity.Feedbacks;
import com.sloms.entity.Notifications;
import com.sloms.repository.FeedbacksRepository;
import com.sloms.repository.NotificationsRepository;

import java.security.Principal;
import java.util.List;

@Service
@Transactional(readOnly=true)
public class FeedbacksServiceImpl implements FeedbacksService {
	@Autowired private FeedbacksRepository feedbacksRepository;
	@Autowired private NotificationsRepository notificationsRepository;
	@Autowired private UsersService usersService;

	@Override
	@Transactional
	public void saveFeedback(Feedbacks feedbacksDAO, Notifications notificationsDAO) {
		Feedbacks feedbacks = new Feedbacks();
		Notifications notifications = new Notifications();
		
		//feedbacks
		feedbacks.setFeedback_type(feedbacksDAO.getFeedback_type().trim());
		feedbacks.setFeedback(feedbacksDAO.getFeedback().trim());
		feedbacks.setFeedback_created_by(feedbacksDAO.getFeedback_created_by());
		feedbacks.setFeedback_dateAndTimeCreated(feedbacksDAO.getFeedback_dateAndTimeCreated());
		
		//notification
		notifications.setFeedbacks(feedbacks);
		notifications.setNotif_type("add_feedback");
		
		feedbacksRepository.save(feedbacks);
		notificationsRepository.save(notifications);
	}

	@Override
	public List<Feedbacks> getAllFeedbacksOfLoggedInUser(Principal principal) {
		Users loggedInUser = usersService.findUserByUsername(principal.getName());
		return feedbacksRepository.getAllFeedbacksOfLoggedInUser(loggedInUser);
	}

	@Override
	public List<Feedbacks> getAllFeedbacks() {
		return feedbacksRepository.getAllFeedbacks();
	}
}
