package com.sloms.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.sloms.entity.Notifications;

@Repository
public interface NotificationsRepository extends JpaRepository<Notifications, Integer>{
	@Query("SELECT n FROM Notifications n ORDER BY n.id DESC")
	List<Notifications> getAllNotifications();
	
	@Query("SELECT n FROM Notifications n WHERE n.id=:notif_id ORDER BY n.id DESC")
	Notifications findNotificationByNotifId(@Param("notif_id") Integer notif_id);
}
