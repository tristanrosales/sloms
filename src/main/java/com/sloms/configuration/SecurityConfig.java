package com.sloms.configuration;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;

@EnableWebSecurity
@Configuration
public class SecurityConfig extends WebSecurityConfigurerAdapter{
	@Autowired
	private BCryptPasswordEncoder bCryptPasswordEncoder;

	@Autowired
	private DataSource dataSource;
	
	@Override
	protected void configure(AuthenticationManagerBuilder auth)
			throws Exception {
		auth.
			jdbcAuthentication()
				.usersByUsernameQuery("SELECT username,user_password,true FROM Users WHERE username=? AND user_status='Authenticated'")
				.authoritiesByUsernameQuery("SELECT username,user_role FROM Users WHERE username=? AND user_status='Authenticated'")
				.dataSource(dataSource)
				.passwordEncoder(bCryptPasswordEncoder);
	}

	@Override
	protected void configure(HttpSecurity http) throws Exception {
	    String[] roles =
                {
                        "IT_ADMIN",
                        "SYS_ADMIN",
                        "SYS_SPECIALIST"
                };

		http.
			authorizeRequests()
				.antMatchers("/").permitAll()
				.antMatchers("/login").permitAll()
				.antMatchers("/forgot_password_page").permitAll()
				.antMatchers("/registration_page").permitAll()
				.antMatchers("/registerAccount").permitAll()

				.antMatchers("/forgot-password-check-username").permitAll()
				.antMatchers("/forgot-password-enter-secret-answer").permitAll()
				.antMatchers("/forgot-password-change-password").permitAll()
				.antMatchers("/forgot-password-get-security-code").permitAll()
				.antMatchers("/forgot-password-enter-security-code").permitAll()
				.antMatchers("/forgot_password/info/{username}").permitAll()
				
				/*
				.antMatchers("/users_page").permitAll()
				.antMatchers("/add-user").permitAll()
				.antMatchers("/users").permitAll()
				*/
				
				.antMatchers("/home/**").hasAnyAuthority(roles).anyRequest()
				.authenticated().and().csrf().disable().formLogin()
				.loginPage("/").failureUrl("/?error")
				.loginProcessingUrl("/")
				.defaultSuccessUrl("/home")
				.usernameParameter("username")
				.passwordParameter("password")
				.and().logout()
				.logoutRequestMatcher(new AntPathRequestMatcher("/logout"))
				.logoutSuccessUrl("/?logout").and().exceptionHandling()
				.accessDeniedPage("/access-denied");
	}
	
	@Override
	public void configure(WebSecurity web) throws Exception {
	    web
	       .ignoring()
	       .antMatchers("/resources/**", "/static/**", "/css/**", "/js/**", "/images/**");
	}
}
