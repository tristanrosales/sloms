package com.sloms.controller;

import java.io.IOException;
import java.security.Principal;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.sloms.service.UsersService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.google.gson.JsonObject;
import com.sloms.classes.DeleteImage;
import com.sloms.classes.UploadImage;
import com.sloms.entity.Notifications;
import com.sloms.entity.ThreadCommentReplies;
import com.sloms.entity.Users;
import com.sloms.repository.ThreadCommentRepliesRepository;
import com.sloms.service.ThreadService;

@RestController
public class ThreadCommentRepliesController {
	@Autowired private ThreadCommentRepliesRepository threadCommentRepliesRepository;
	@Autowired private ThreadService threadService;
	@Autowired private UsersService usersService;
	@Autowired private SimpMessagingTemplate template;
	private JsonObject status = new JsonObject();

	@RequestMapping(value="/add-reply",method=RequestMethod.POST)
	@ResponseBody
	public String addReply(
			@RequestParam(value="thread_comment_reply_images",required=false) List<String> threadCommentReplyImageLink,
			@ModelAttribute("threadCommentReplies") ThreadCommentReplies threadCommentReplies,
			@ModelAttribute("notifications") Notifications notifications,
			Principal principal) {

		Users loggedInUser = usersService.findUserByUsername(principal.getName());
		
		threadCommentReplies.setThread_comment_reply_images(threadCommentReplyImageLink);
		threadCommentReplies.setThread_reply_by(loggedInUser);
		
		threadService.saveThreadCommentReplies(threadCommentReplies,notifications);
		
		status.addProperty("isAddingReplySuccess", true);
		
		template.convertAndSend("/sloms/bulletin_board", "bulletin_board");
		template.convertAndSend("/sloms/comment","comment");
		template.convertAndSend("/sloms/reply","reply");
		
		return status.toString();
	}
	
	@RequestMapping(value="/add-reply/add-thread-comment-reply-images",method=RequestMethod.POST)
	@ResponseBody
	public void addThreadCommentReplyImages(
			@RequestParam("qqfile") MultipartFile threadCommentReplyImage,
			@RequestParam("qquuid") String uuid,
			@RequestParam("qqfilename") String fileName,
			HttpServletResponse response) throws IOException {
		
		String uploadedFilename = UploadImage.uploadImage(uuid, threadCommentReplyImage, "src\\main\\webapp\\SLOMS_server\\reply", "/reply/");
		
		System.out.println("Uploading " + threadCommentReplyImage.getOriginalFilename());
		System.out.println("UUID=" + uuid);
		
		JsonObject jsonObject = new JsonObject();
		
		jsonObject.addProperty("success", true);
		jsonObject.addProperty("threadCommentReplyImageLink", uploadedFilename);
		
		response.setCharacterEncoding("UTF-8");
		response.setContentType("text/plain");
		response.getWriter().print(jsonObject);
		response.flushBuffer();
	}
	
	@RequestMapping(value="/add-reply/delete-thread-comment-reply-images/{qquuid}",method={RequestMethod.DELETE})
	public void deleteThreadCommentReplyImages(
			HttpServletResponse response,
			HttpServletRequest request,
			@PathVariable("qquuid") String qquuid,
			@RequestParam("filename") String filename) throws IOException {
			
		System.out.println("uploadDelete() called " + filename);
		
		DeleteImage.deleteImage(qquuid, "src\\main\\webapp\\SLOMS_server\\reply\\");
		
		response.setStatus(200);
		response.flushBuffer();
	}
	
	@RequestMapping(value="/frmEditReply",method=RequestMethod.POST)
	@ResponseBody
	public String editReply(@ModelAttribute("threadCommentReplies") ThreadCommentReplies threadCommentReplies) {
		ThreadCommentReplies threadCommentReplies_threadReplyIdExists = threadCommentRepliesRepository.findCommentRepliesByThreadReplyId(threadCommentReplies.getThread_reply_id());
		
		final Date dateAndTimeToday = new Date();
		
		if(threadCommentReplies_threadReplyIdExists != null) {
			threadCommentReplies.setThread_reply_modified(dateAndTimeToday);
			threadCommentRepliesRepository.save(threadCommentReplies);
			
			status.addProperty("status", true);
			
			template.convertAndSend("/sloms/bulletin_board", "bulletin_board");
			template.convertAndSend("/sloms/reply","reply");
		}
		
		return status.toString();
	}
	
	@RequestMapping(value="/frmDeleteReply",method=RequestMethod.POST)
	@ResponseBody
	public String deleteReply(@ModelAttribute("threadCommentReplies") ThreadCommentReplies threadCommentReplies) {
		ThreadCommentReplies threadCommentReplies_threadReplyIdExists = threadCommentRepliesRepository.findCommentRepliesByThreadReplyId(threadCommentReplies.getThread_reply_id());
		
		if(threadCommentReplies_threadReplyIdExists != null) {
			threadCommentRepliesRepository.delete(threadCommentReplies_threadReplyIdExists);
			status.addProperty("status", true);
			
			template.convertAndSend("/sloms/bulletin_board", "bulletin_board");
			template.convertAndSend("/sloms/reply","reply");
		}
				
		return status.toString();
	}
	
	@RequestMapping(path="/thread_comment_replies/{thread_comment_id}", method=RequestMethod.GET)
	@ResponseBody
	public List<ThreadCommentReplies> getAllThreadCommentRepliesByThreadCommentId(@PathVariable("thread_comment_id") Integer thread_comment_id){
		return threadCommentRepliesRepository.getAllThreadCommentRepliesByThreadCommentId(thread_comment_id);
	}
	
	@RequestMapping(path="/thread_comment_reply/{thread_reply_id}", method=RequestMethod.GET)
	@ResponseBody
	public ThreadCommentReplies findCommentRepliesByThreadReplyId(@PathVariable("thread_reply_id") Integer thread_reply_id) {
		return threadCommentRepliesRepository.findCommentRepliesByThreadReplyId(thread_reply_id);
	}
}

