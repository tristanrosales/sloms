package com.sloms.entity;

import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.*;

@Getter
@Setter
@ToString
@EqualsAndHashCode
@NoArgsConstructor
@Entity
public class Notes{
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Integer notes_id;

	@NotNull
	private String notes_type;

	@NotNull
	@Column(length=5000)
	private String notes_description;
	
	@ManyToOne(fetch=FetchType.LAZY)
    @JoinColumn(name = "notes_billboard")
	@JsonIgnoreProperties(
			{
				"hibernateLazyInitializer",
				"handler",
				"notes",
				"medias",
				"media_created_by",
				"mediaDetails",
				"mediaClientDetails",
				"media_images",
				"media_status",
				"media_dateAndTime_created",
				"media_modified",
				"threads"
			}
	)
	private Media notes_billboard;
	
	@ManyToOne(fetch=FetchType.LAZY)
    @JoinColumn(name = "notes_created_by")
	@JsonIgnoreProperties(
			{
				"hibernateLazyInitializer",
				"handler",
				"notes",
				"threads",
				"groups",
				"notes",
				"threads",
				"threadComments",
				"threadCommentReplies",
				"medias",
				"user_password",
				"user_role",
				"user_status",
				"user_sms_notif",
				"user_email_notif",
				"user_email_address",
				"user_phone_number",
				"user_security_question",
				"user_secret_answer",
				"user_dateAndTimeCreated"
			})
	private Users notes_created_by;

	@NotNull
	private String notes_status;
	
	@Temporal(TemporalType.TIMESTAMP)
	private Date notes_dateAndTimeCreated;
	
	@Temporal(TemporalType.TIMESTAMP)
	private Date notes_dateAndTimeModified;
	
	@OneToMany(cascade = CascadeType.ALL,
            fetch = FetchType.LAZY,
            mappedBy = "thread_notes_id")
	@JsonIgnoreProperties("thread_notes_id")
	private List<Threads> threads;
	
	@OneToMany(
			cascade = CascadeType.ALL,
			fetch = FetchType.LAZY,
			mappedBy = "notes")
	@JsonIgnore
	private List<Notifications> notifications;

}
