//show notes
String.prototype.replaceAll = function (searchText, replacementText) {
	return this.split(searchText).join(replacementText);
};

//show threads
$.ajax({
	url: '/threads',
	dataType: 'json',
	mimeType: 'json',
	success: function(data) {
		$.each(data, function(i, data) {
			var body = "<tr>";
			body += "<td>" + data.thread_id + "</td>";
			body += "<td>" + data.thread_notes_id.notes_type + "</td>";
			body += "<td>" + data.thread_caption + "</td>";
			body += "<td>" + data.thread_billboard.media_name + "</td>";
			body += "<td>" + data.thread_created_by.user_firstname + " " + data.thread_created_by.user_lastname + "</td>";
			body += "<td>" + data.thread_status + "</td>";
			body += "<td>" + moment(new Date(data.thread_dateAndTimeCreated)).format('LLLL') + "</td>";
			
			if(data.thread_modified == null){
				body += "<td>None</td>";
			}else{
				body += "<td>" + moment(new Date(data.thread_modified)).format('LLLL') + "</td>";
			}
			
			body += "<td style='text-align: center;'>" +
						"<span " +
							"class='fa fa-edit btnViewEditThread' " +
							"style='font-size: 18px; cursor: pointer; display: none;' " +
							"data-toggle='modal' " +
							"data-target='#view_edit_thread-modal' " +
							"data-id='" + data.thread_id + "'> " +
							"Edit" +
						"</span>" +
						"" +
						"<span " +
							"class='fa fa-ban tblThreads_accessDenied' " +
							"style='font-size: 14px; display: none;'><br>Access Denied" +
						"</span>" +
					"</td>";
			body += "</tr>";
			
			$("#tblThreads tbody").append(body);
		});
		
		/* DataTables instantiation. */
		$("#tblThreads").DataTable();
	},
	error: function(e){
		console.log(e);
	}
});

//show feedbacks
$.ajax({
	url: '/feedbacks',
	dataType: 'json',
	mimeType: 'json',
	success: function(data) {
		$.each(data, function(i, data) {	
			var body = "<tr>";
			body += "<td style='text-align: center;'>" + data.feedback_id + "</td>";
			body += "<td>" + data.feedback_type.replaceAll("_"," ") + "</td>";
			body += "<td>" + data.feedback + "</td>";
			body += "<td>" + data.feedback_created_by.user_firstname + " " + data.feedback_created_by.user_lastname + "</td>";
			body += "<td>" + moment(new Date(data.feedback_dateAndTimeCreated)).format('LLLL') + "</td>";
			body += "</tr>";
			
			$("#tblFeedbacks tbody").append(body);
		});

		/* DataTables instantiation. */
		$("#tblFeedbacks").DataTable();
	},
	error: function(e){
		console.log(e.responseText);
	}
});


