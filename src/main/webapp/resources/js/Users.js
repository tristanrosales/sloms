var userID = document.getElementById("loggedInUserId").innerText;
var userRole = document.getElementById("loggedInUserRole").innerText;

var IT_ADMIN = "IT_ADMIN";
var SYS_ADMIN = "SYS_ADMIN";
var SYS_SPECIALIST = "SYS_SPECIALIST";

var AUTHENTICATED = "Authenticated";
var UNAUTHENTICATED = "Unauthenticated";

function connectWebSocket(){
    var socket = new SockJS('/sloms_web_socket');
    stompClient = Stomp.over(socket);

    stompClient.connect({}, function(frame) {
        stompClient.subscribe("/sloms/errors", function(message) {
            console.log("Web Socket Error Message : " + message.body);
            connectWebSocket();
        });

        stompClient.subscribe("/sloms/bulletin_board", function(message) {
            $('#tblUsers').DataTable().ajax.reload();
        });

        stompClient.subscribe("/sloms/users", function(message) {
            $('#tblUsers').DataTable().ajax.reload();
        });
    }, function(error) {
        console.log("Web Socket Error Message : " + error);
    });
}

function validateUserForm(){
    $("#frmAddUser").validate({
        ignore : [],
        rules : {
            username: {
                required: true,
                minlength: 8,
                maxlength: 30
            },
            user_firstname: {
                required: true
            },
            user_lastname: {
                required: true
            },
            user_email_address : {
                required : true,
                email : true
            },
            user_phone_number: {
                required: true
            },
            user_gender: {
                required: true
            },
            user_role: {
                required: true
            }
        },
        submitHandler: function(form) {
            addUser(form);
        }
    });
}

function showUserDetails(d) {
	var table,access;

	table = '<table cellpadding="5" cellspacing="0" border="0" style="padding-left:50px;" class="table table-bordered table-striped display">' +
				'<tr>'+
					'<td><strong>FULLNAME</strong></td>'+
					'<td>' + d.user_firstname + " " + d.user_lastname + '</td>'+
				'</tr>'+
				'<tr>'+
					'<td><strong>EMAIL ADDRESS</strong></td>'+
					'<td>'+ d.user_email_address +'</td>'+
				'</tr>'+
				'<tr>'+
					'<td><strong>ROLE</strong></td>'+
					'<td>'+ d.user_role +'</td>'+
				'</tr>'+
				'<tr>'+
					'<td><strong>CREATED</strong></td>'+
					'<td>'+ moment(new Date(d.user_dateAndTimeCreated)).format('LLLL') +'</td>'+
				'</tr>'+
				'<tr>'+
					'<td><strong>STATUS</strong></td>'+
					'<td>'+ d.user_status +'</td>'+
				'</tr>'+
				'<tr>';

				if(d.user_role == SYS_ADMIN && userRole == IT_ADMIN || d.user_role == SYS_SPECIALIST && userRole == SYS_ADMIN){
                    if(d.user_id != userID && d.user_status == 'Authenticated'){
                        access = '<button type="button" class="btn btn-lg btn-rounded" style="padding: 10px; font-size: 15px; background-color:#FF8000; color: white;" onclick="approveOrDeclineUser(' + d.user_id + ', 0)"><strong><span class="fa fa-thumbs-o-down"></span> Decline</strong></button>';
                    }else{
                        access = '<button type="button" class="btn btn-lg btn-rounded" style="padding: 10px; font-size: 15px; background-color:#1caf9a; color: white;" onclick="approveOrDeclineUser(' + d.user_id + ', 1)"><strong><span class="fa fa-thumbs-o-up"></span> Approve</strong></button>';
                    }
				}else{
                    access = "<span class='fa fa-ban' style='font-size: 18px;'><br>Access Denied</span>";
                }

				/*
				if($('#lblUser_role').text() == "Super Administrator"){
					access = '<button type="button" class="btn btn-info btn-lg btn-rounded" style="padding: 10px; font-size: 15px; background-color:#1caf9a; color: white;" data-toggle="modal" data-target="#view_edit_user-modal"><strong><span class="fa fa-edit"></span> Edit Information</strong></button>';
				}else{
					access = "<span class='fa fa-ban' style='font-size: 18px;'><br>Access Denied</span>";
				}
				*/

		table += '<td colspan="2" style="text-align: center;">' + access + '</td>'+
				'</tr>'+
			'</table>';

			$('#frmUpdateUser_Email_Role_Status_lblUser_fullname').text("Edit " + d.user_firstname + " " + d.user_lastname + "?");

			$('#frmUpdateUser_Email_Role_Status_lblUser_id').val(d.user_id);
			$('#frmUpdateUser_Email_Role_Status_lblUser_emailAddress').val(d.user_email_address);
			
			$('#frmUpdateUser_Email_Role_Status_cmbUser_role')
				.removeAttr('selected')
				.find('option[value="' + d.user_role.replaceAll(" ","_") + '"]')
				.attr("selected",true)
				.change();

			$('#frmUpdateUser_Email_Role_Status_cmbUser_status')
				.removeAttr('selected')
				.find('option[value="' + d.user_status.replaceAll(" ","_") + '"]')
				.attr("selected",true)
				.change();

	return table;
}

function showAllUsers(){
	$("#tblUsers tbody").empty();

	var table = $("#tblUsers").DataTable({
		"ajax": {
			"url": "/sloms_users",
			"dataSrc": ""
		},
		"columns": [
			{
				"className": 'details-control',
				"orderable": false,
				"data": null,
				"defaultContent": ''
			},
			{ 
				"data": null,
				render: function (data,type,row) {
					var imageSrc = '';

					if(data.user_profile_picture == "" || data.user_profile_picture == null){
						imageSrc = 'resources/assets/images/users/no-image.jpg';
					}else{
						imageSrc = 'SLOMS_server' + data.user_profile_picture;
					}

					return '<img src="' + imageSrc + '" style="width: 40px; height: 40px; border-radius: 50%;" />&nbsp;&nbsp;' + data.user_firstname + ' ' +  data.user_lastname + ' <b>(' + data.username + ')</b>';
				}
			}
		],
		"iDisplayLength": 5,
		"aLengthMenu": [[5, 10, 25, 50, 100, -1], [5, 10, 25, 50, 100, "All"]],
		"pagingType": "full_numbers"
	});

    $('#tblUsers tbody').on('click', 'td.details-control', function () {
        var tr = $(this).closest('tr');
        var row = table.row(tr);
 
        if (row.child.isShown()) {
            row.child.hide();
            tr.removeClass('shown');
        }
        else {
            row.child(showUserDetails(row.data())).show();
            tr.addClass('shown');
        }
	});
}

function addUser(form){
	var formEl = $(form);
	
	swal({
		title: "Are you sure?",
				text: "Once added, you will not be able to undo this action!",
				icon: "warning",
				buttons: ["No, not sure yet!", "Yes, I'm Sure!"],
				dangerMode: true
	})
	.then((willAdd) => {
		if (willAdd) {
			$('body').loadingModal({
				position: 'auto',
				text: 'Please wait while creating this account :)',
				color: '#fff',
				opacity: '0.7',
				backgroundColor: 'rgb(0,0,0)',
				animation: 'doubleBounce'
			});

			$.ajax({
				type: 'POST',
				dataType : 'JSON',
				url: formEl.prop('action'),
				data: formEl.serialize(),
				// if received a response from the server
				success: function(data) {
					$('body').loadingModal('hide');

					if(data.isAddingUserSuccess == "usernameExists"){
						swal("We're sorry!", "There is already a user registered with the username provided!", "error");
					}else if(data.isAddingUserSuccess == "emailExists"){
						swal("We're sorry!", "There is already a user registered with the email provided!", "error");
					}else if(data.isAddingUserSuccess == "successCreate"){
						iziToast.success({
							title: 'Congratulations!',
							theme: 'light', // dark
							color: 'green', // blue, red, green, yellow
							position: 'bottomRight', // bottomRight, bottomLeft, topRight, topLeft, topCenter, bottomCenter, center
							message: "User has been added successfully! Please check your email!"
						});

						$("#add_new_user-modal").modal('toggle');
						$('#tblUsers').DataTable().ajax.reload();
					}
				},
				error: function(e){
					console.log(e);
					$('body').loadingModal('hide');
					swal("We're sorry!", "Cannot create an account. Please try again!", "error");
				}
			});
		}
	});
}

function updateUser_Email_Role_Status(){
	$('#frmUpdateUser_Email_Role_Status').submit(function(event) {
		event.preventDefault();
		var formEl = $(this);
		
		swal({
			  title: "Are you sure?",
			  text: "Once updated, you will not be able to undo this action!",
			  icon: "warning",
			  buttons: ["No, not sure yet!", "Yes, I'm Sure!"],
			  dangerMode: true
		})
		.then((willUpdate) => {
			if (willUpdate) {
				$('body').loadingModal({
					position: 'auto',
					text: 'Please wait while updating this account!',
					color: '#fff',
					opacity: '0.7',
					backgroundColor: 'rgb(0,0,0)',
					animation: 'doubleBounce'
				});

				$.ajax({
					type: 'POST',
					dataType : 'JSON',
					url: formEl.prop('action'),
					data: formEl.serialize(),
					// if received a response from the server
					success: function(data) {
						$('body').loadingModal('hide');

						if(data.isUpdateUser_Email_Role_Status_Success){
							iziToast.success({
								title: 'Congratulations!',
								theme: 'light', // dark
								color: 'green', // blue, red, green, yellow
								position: 'bottomRight', // bottomRight, bottomLeft, topRight, topLeft, topCenter, bottomCenter, center
								message: "User has been updated successfully!"
							});

							$("#view_edit_user-modal").modal('toggle');
							$('#tblUsers').DataTable().ajax.reload();
						}else{
							iziToast.error({
								title: 'Oops!',
								theme: 'light', // dark
								color: 'red', // blue, red, green, yellow
								position: 'bottomRight', // bottomRight, bottomLeft, topRight, topLeft, topCenter, bottomCenter, center
								message: 'User has been updated unsuccessfully. Please try again!'
							});
						}
					},
					error: function(e){
						console.log(e);
						$('body').loadingModal('hide');
						swal("We're sorry!", "Failed to update this account. Please try again!", "error");
					}
				});
			}
		});
	});
}

function deleteUser(){
	swal({
		  title: "Are you sure?",
		  text: "Once deleted, you will not be able to undo this action!",
		  icon: "warning",
		  buttons: ["No, not sure yet!", "Yes, I'm Sure!"],
		  dangerMode: true,
	})
	.then((willDelete) => {
		if (willDelete) {
			$.ajax({
	            type: 'POST',
	            dataType : 'JSON',
	            url: "/frmDeleteUser",
	            data: $('#frmUpdateDeleteUser').serialize(),
	            // if received a response from the server
	            success: function( data, textStatus, jqXHR) {
						 if(data.status){
							 swal("User has been deleted successfully!", {
			          		    icon: "success"
			          		 }).then((willDelete) => {
			          			 if(willDelete){
			          				location.reload(); 
			          			 }
			          		 });
						 }
	            },
	            error: function(e){
					console.log(e);
				}
			});
		}
	});
}

function approveOrDeclineUser(user_id,user_status){
	var status,dialogMessage;

	if(user_status === 0){
		status = UNAUTHENTICATED;
        dialogMessage = "A user has been declined successfully!";
	}else{
		status = AUTHENTICATED;
        dialogMessage = "A user has been approved successfully!";
	}

    var values = {
        'user_id'		: 	user_id,
		'user_status' 	:	status
    };

    swal({
        title: "Do you still want to continue?",
        icon: "warning",
        buttons: ["No!", "Yes!"],
        dangerMode: true
    })
        .then((willUpdate) => {
        if (willUpdate) {
            $('body').loadingModal({
                position: 'auto',
                text: 'Validating record... Please wait!',
                color: '#fff',
                opacity: '0.7',
                backgroundColor: 'rgb(0,0,0)',
                animation: 'doubleBounce'
            });

            $.ajax({
                url: "/sloms_users/approveOrDecline",
                type: "POST",
                dataType : 'JSON',
                data: values,
                success: function(data) {
                    $('body').loadingModal('destroy');

                    if(data.status == "SUCCESS"){
                        iziToast.success({
                            title: 'Congratulations!',
                            theme: 'light', // dark
                            color: 'green', // blue, red, green, yellow
                            position: 'bottomRight', // bottomRight, bottomLeft, topRight, topLeft, topCenter, bottomCenter, center
                            message: dialogMessage
                        });
                    }else if(data.status == "FAILED"){
                    	iziToast.error({
                            title: 'Oops! There is an error!',
                            theme: 'light',
                            color: 'red',
                            position: 'bottomRight',
                            message: "Failed to approve or decline a user. Please try again!"
						});
					}else{
                        iziToast.error({
                            title: "Oops! We're sorry!",
                            theme: 'light',
                            color: 'red',
                            position: 'bottomRight',
                            message: "Access Denied. Please try again!"
                        });
					}
                },
                error: function(e){
                    $('body').loadingModal('destroy');
                    console.log(e);
                }
            });
        }
    });
}

//call all methods
connectWebSocket();
validateUserForm();
showAllUsers();
updateUser_Email_Role_Status();