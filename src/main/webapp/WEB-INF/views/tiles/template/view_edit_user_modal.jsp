<div class="modal" id="view_edit_user-modal" tabindex="-1" role="dialog" aria-labelledby="largeModalHead" aria-hidden="true" data-backdrop="static" data-keyboard="false">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<form id="frmUpdateUser_Email_Role_Status" class="form-horizontal" role="form" method="POST" action="/super-admin/update-user-role">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">
						<span aria-hidden="true">&times;</span><span class="sr-only">Close</span>
					</button>
					<h3 class="modal-title" id="largeModalHead"><span id="frmUpdateUser_Email_Role_Status_lblUser_fullname"></span></h3>
				</div>

				<div class="modal-body" style="max-height: calc(100vh - 200px); overflow-y: auto;">
					<input type="hidden" id="frmUpdateUser_Email_Role_Status_lblUser_id" name="user_id">
					
					<div class="form-group">
						<label class="col-md-2 control-label">E-mail:</label>
						<div class="col-md-10">
							<input 
								type="email" 
								id="frmUpdateUser_Email_Role_Status_lblUser_emailAddress" 
								name="user_email_address" 
								class="form-control"
								placeholder="Enter email address..." 
								style="background-color:white !important;" 
								maxlength="40" /> 
								
							<span class="help-block">Required email address</span>
						</div>
					</div>

					<div class="form-group">
						<label class="col-md-2 control-label">Role</label>
						<div class="col-md-10">
							<select class="form-control select" data-live-search="true" required="required" name="user_role" id="frmUpdateUser_Email_Role_Status_cmbUser_role">
								<option disabled="disabled" selected="selected">---SELECT ROLE---</option>
								<option value="Super_Administrator">Super Administrator</option>
								<option value="Administrator">Administrator</option>
								<option value="Member_or_Editor">Member or Editor</option>
							</select> 
							
							<span class="help-block">Required role</span>
						</div>
					</div>

					<div class="form-group">
						<label class="col-md-2 control-label">Status</label>
						<div class="col-md-10">
							<select class="form-control select" data-live-search="true" required="required" name="user_status" id="frmUpdateUser_Email_Role_Status_cmbUser_status">
								<option disabled="disabled" selected="selected">---SELECT STATUS---</option>
								<option value="Activated">Activated</option>
								<option value="Deactivated">Deactivated</option>
							</select>
							
							<span class="help-block">Required status</span>
						</div>
					</div>

					<script>
						function addMaxLengthTo(input){
							$(input).maxlength({
								alwaysShow: true,
								threshold: 10,
								warningClass: "label label-success",
								limitReachedClass: "label label-danger",
								separator: ' of ',
								preText: 'You have ',
								postText: ' chars remaining.',
								validate: true
							});
						}

						function implementBootstrapMaxLength(){
							addMaxLengthTo('input#frmUpdateUser_Email_Role_Status_lblUser_emailAddress');
						}

						implementBootstrapMaxLength();
					</script>
				</div>

				<div class="modal-footer">
					<button 
						type="submit" 
						class="btn btn-info btn-lg btn-rounded" 
						style="padding: 10px; font-size: 15px; background-color:#1caf9a; color: white;">
						<strong>
							<span class="fa fa-hand-o-right"></span> Update User
						</strong>
					</button>
					
					<button 
						type="button" 
						class="btn btn-warning btn-rounded" 
						style="padding: 10px; font-size: 15px; background-color: orange; color: white;" 
						data-dismiss="modal">
						<strong>
							<span class="glyphicon glyphicon-remove-sign"></span> Close
						</strong>
					</button>
				</div>
			</form>

		</div>
	</div>
</div>