<div class="modal" id="edit_profile_picture-modal" tabindex="-1" role="dialog" aria-labelledby="largeModalHead" aria-hidden="true" data-backdrop="static" data-keyboard="false">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<form id="frmUpdateProfilePicture" class="form-horizontal" role="form" action="/update-user-profile-picture" method="POST">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">
						<span aria-hidden="true">&times;</span><span class="sr-only">Close</span>
					</button>
					<h3 class="modal-title" id="largeModalHead">Change Profile Picture</h3>
				</div>

				<div class="modal-body" style="max-height: calc(100vh - 200px); overflow-y: auto;">
					<div class="alert alert-info" role="alert" style="font-size: 15px;">
						<button type="button" class="close" data-dismiss="alert"></button>
						<strong>Reminder!</strong><br>
						<ul>
							<li>Maximum file size: <b>50kb</b></li>
							<li>Allowed file extension: <b>jpeg, jpg, and png</b></li>
						</ul> 
					</div>
		
					<input type="hidden" name="user_id" id="frmUpdateProfilePicture_user_id">
					<input type="hidden" name="username" id="frmUpdateProfilePicture_username">
					
					<div class="form-group">
						<label class="col-md-2 control-label">Profile Picture</label>
						<div class="col-md-10">
							<div class="panel panel-default">
								<div class="panel-body">
									<h3>
										<span class="fa fa-download"></span> Profile Picture
									</h3>
									
									<div id="profile-picture-holder"></div>

									<script>
										var profilePictureUploader = new qq.FineUploader({
											element: document.getElementById('profile-picture-holder'),
											template: 'qq-template-manual-trigger_single-upload',
											validation: {
												allowedExtensions: ["jpeg","jpg","png"],
												itemLimit: 1,
												sizeLimit: 51200 // 50 kB = 50 * 1024 bytes
											},
											request: {
												endpoint: '/add-user/add-user-profile-picture',
												method: "POST"
											},
											deleteFile: {
												enabled: true,
												forceConfirm: true,
												endpoint: '/add-user/delete-user-profile-picture',
												method: "DELETE"
											},
											thumbnails: {
												placeholders: {
													waitingPath: '/resources/js/plugins/fine-uploader/placeholders/waiting-generic.png',
													notAvailablePath: '/resources/js/plugins/fine-uploader/placeholders/not_available-generic.png'
												}
											},
											autoUpload: false,
											debug: true,
											callbacks: {
												onComplete: function (id,name,responseJSON,maybeXhr){
													console.log(responseJSON);
													$('#lblUserProfilePicture_' + id).val(responseJSON.profilePictureLink);
												},
												onCancel: function(id,name){
													console.log("cancel");
													$('#lblUserProfilePicture_' + id).remove();
												},
												onError: function(id,name,reason,maybeXhrOrXdr){
													//logic here
												},
												onAllComplete: function(successful,failed){
													if(failed.length <= 0){
														//logic here
													}else{
														//logic here
													}
												},
												onSubmitted: function(id,name){
													$(".qq-uploader-selector").append("<input type='hidden' id='lblUserProfilePicture_" + id + "' name='user_profile_picture' value='' />");
												},
												onUpload (id,name){
													console.log(name + " before uploading...");
												},
					
												onSubmitDelete: function(id){
													this.setDeleteFileParams({filename: this.getName(id)}, id);
												},
												onDeleteComplete: function(id){
													$('#lblUserProfilePicture_' + id).remove();
												}
											}
										});
					
										qq(document.querySelector("#profile-picture-holder #trigger-upload")).attach("click", function () {
											profilePictureUploader.uploadStoredFiles();
										});
									</script>
								</div>
							</div>
						</div>
					</div>
				</div>

				<div class="modal-footer">
					<button
						type="submit" 
						class="btn btn-info btn-lg btn-rounded" 
						style="padding: 10px; font-size: 15px; background-color:#1caf9a; color: white;">
						<strong>
							<span class="glyphicon glyphicon-hand-right"></span> Update Profile Picture
						</strong>
					</button>

					<button 
						type="button" 
						class="btn btn-warning btn-rounded" 
						style="padding: 10px; font-size: 15px; background-color: orange; color: white;" 
						data-dismiss="modal">
						<strong>
							<span class="glyphicon glyphicon-remove-sign"></span> Close
						</strong>
					</button>
				</div>
			</form>
		</div>
	</div>
</div>