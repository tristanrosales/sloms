//show media details by parameter media_id in url
var getUrlParameter = function getUrlParameter(sParam) {
    var sPageURL = decodeURIComponent(window.location.search.substring(1)),
        sURLVariables = sPageURL.split('&'),
        sParameterName,
        i;

    for (i = 0; i < sURLVariables.length; i++) {
        sParameterName = sURLVariables[i].split('=');

        if (sParameterName[0] === sParam) {
            return sParameterName[1] === undefined ? true : sParameterName[1];
        }
    }
};

String.prototype.replaceAll = function (searchText, replacementText) {
	return this.split(searchText).join(replacementText);
};

var media_id = getUrlParameter('media_id');
var media_name = "";
var money = value => currency(value, { formatWithSymbol: true });

$.ajax({
	url: '/media/' + media_id,
	dataType: 'json',
	mimeType: 'json',
	success: function(data) {
		$('#lblMedia_name').text(data.media_name);

		$('#addNewThread_lblThreadBillboard').val(data.media_id);
		$('#frmAddNotes_billboard').val(data.media_id);

		if(data.media_preview_image === "" || data.media_preview_image === null){
			$('#imgMediaPreviewImage').attr('src',"/resources/img/no-image-available.jpg");	
		}else{
			$('#imgMediaPreviewImage').attr('src',"/SLOMS_server" + data.media_preview_image);
		}

		$('#lblMedia_dateAndTimeCreated').text(moment(new Date(data.media_dateAndTime_created)).format('LLLL'));
		$('#lblMedia_createdBy').text(data.media_created_by.user_firstname + " " + data.media_created_by.user_lastname);
		
		//media details
		$('#lblMedia_billboardName').text(data.media_name);
		$('#lblMedia_mediaCode').text(data.media_code);
		
		$('#lblMedia_location').text(data.mediaDetails.media_location);

		$('#iframe_media-location').prop("src","https://www.google.com/maps/embed/v1/place?key=AIzaSyCk3AJ31f6ZVPlfpDnyvpCdJt27io0k-fA&q=" + data.mediaDetails.media_location.replaceAll(" ","+"));
		
		$('#lblMedia_latlong').text(data.mediaDetails.media_latlong);
		$('#lblMedia_region').text(data.mediaDetails.media_region);
		$('#lblMedia_city').text(data.mediaDetails.media_city);

		var media_unit = "";
		
		if(data.mediaDetails.media_unit == "foot"){
			media_unit = "ft."
		}

		$('#lblMedia_dimension').text(data.mediaDetails.media_size_h + "x" + data.mediaDetails.media_size_w + " " + media_unit);
		$('#lblMedia_price').text("₱" + money(data.mediaDetails.media_price).format(false));
		$('#lblMedia_road_position').text(data.mediaDetails.media_road_position.replaceAll("_"," "));
		$('#lblMedia_panel_orientation').text(data.mediaDetails.media_panel_orientation.replaceAll("_"," "));
		$('#lblMedia_illumination').text(data.mediaDetails.media_illumination);
		$('#lblMedia_structure_type').text(data.mediaDetails.media_structure_type.replaceAll("_"," "));
		$('#lblMedia_traffic_details').text(data.mediaDetails.media_traffic_details);
		$('#lblMedia_created').text(moment(new Date(data.media_dateAndTime_created)).format('LLLL'));

		var gallery = $('#links_media-images');
		var images = "";

		if(data.media_images.length < 1){
           $("#mediaPhotosDetector").css("display", "inline");
		}else{
            $("#mediaPhotosDetector").css("display", "none");

            for (var i = 0; i < data.media_images.length; i++) {
                if(data.media_images[i] != ""){
                    console.log(data.media_images[i]);

                    var image = document.createElement("img");
                    image.setAttribute("src", "/SLOMS_server" + data.media_images[i]);

                    images += "<a href='/SLOMS_server" + data.media_images[i] + "' class='gallery-item' data-gallery><div class='image'><img src='/SLOMS_server" + data.media_images[i] + "' style='max-height: 70px; height: 70px; max-width: 80px; width: 80px;'/></div></a>";
                }
            }

            gallery.append(images);
		}

		//client contract details
		$('#lblMedia_client').text(data.mediaClientDetails.media_client);
		$('#lblMedia_contract_start').text(data.mediaClientDetails.media_contract_start);
		$('#lblMedia_expiration').text(data.mediaClientDetails.media_expiration);
		$('#lblMedia_contracted_rate').text("₱" + money(data.mediaClientDetails.media_contracted_rate).format(false));
	},
	error: function(e){
		console.log(e.responseText);
		/*
		swal("Oops! We're sorry!","The media you've been searching was not yet exist. Please try again!", {
			icon: "error",
			closeOnClickOutside: false,
		}).then((willLoad) => {
			if(willLoad){
				//location.replace("/home");
			}
		});
		*/
	}
});

document.getElementById('links_media-images').onclick = function (event) {
	event = event || window.event;
	var target = event.target || event.srcElement;
	var link = target.src ? target.parentNode : target;
	var options = {index: link, event: event,onclosed: function(){
			setTimeout(function(){
				$("body").css("overflow","");
			},200);                        
		}};
	var links = this.getElementsByTagName('a');
	blueimp.Gallery(links, options);
};