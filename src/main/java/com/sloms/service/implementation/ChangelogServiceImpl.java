package com.sloms.service.implementation;

import java.util.Date;

import com.sloms.service.ChangelogService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.sloms.entity.Changelog;
import com.sloms.entity.Notifications;
import com.sloms.repository.ChangelogRepository;
import com.sloms.repository.NotificationsRepository;

@Service
@Transactional(readOnly=true)
public class ChangelogServiceImpl implements ChangelogService {

	@Autowired
	private ChangelogRepository changelogRepository;
	
	@Autowired
	private NotificationsRepository notificationsRepository;
	
	@Override
	@Transactional
	public void saveChangelog(Changelog changelogDAO, Notifications notificationsDAO) {
		Changelog changelog = new Changelog();
		Notifications notifications = new Notifications();
		
		//changelog
		changelog.setChangelog_version(changelogDAO.getChangelog_version().trim());
		changelog.setChangelog_details(changelogDAO.getChangelog_details().trim());
		changelog.setChangelog_dateAndTimeAdded(changelogDAO.getChangelog_dateAndTimeAdded());
		changelog.setChangelog_modified(changelogDAO.getChangelog_modified());
		changelog.setChangelog_added_by(changelogDAO.getChangelog_added_by());
		
		//notification
		notifications.setChangelog(changelog);
		notifications.setNotif_type("add_changelog");
		
		changelogRepository.save(changelog);
		notificationsRepository.save(notifications);
	}

	@Override
	@Transactional
	public void updateChangelog(
			String changelog_version, 
			String changelog_details, 
			Integer changelog_id) {
		
		final Date dateAndTimeModified = new Date();
		
		changelogRepository.updateChangelogInformation(
				changelog_version,
				changelog_details,
				dateAndTimeModified,
				changelog_id);
	}
	
}
