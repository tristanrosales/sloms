package com.sloms.repository;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.sloms.entity.Media;
import com.sloms.entity.MediaClientDetails;

@Repository
public interface MediaClientDetailsRepository extends JpaRepository<MediaClientDetails,Integer>{
	@Query("SELECT mcd FROM MediaClientDetails mcd WHERE mcd.media.media_id=:media_id")
	MediaClientDetails findMediaClientDetailsByMediaId(@Param("media_id") Integer media_id);
	
	@Transactional
	@Modifying
	@Query("UPDATE MediaClientDetails mcd SET "
			+ "mcd.media_client=:media_client,"
			+ "mcd.media_contracted_rate=:media_contracted_rate,"
			+ "mcd.media_contract_start=:media_contract_start,"
			+ "mcd.media_expiration=:media_expiration "
			+ "WHERE mcd.media=:media_id")
	void updateMediaClientDetailsInformation(
			@Param("media_client") String media_client,
			@Param("media_contracted_rate") String media_contracted_rate,
			@Param("media_contract_start") String media_contract_start,
			@Param("media_expiration") String media_expiration,
			@Param("media_id") Media media_id);
}
