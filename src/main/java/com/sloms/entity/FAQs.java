package com.sloms.entity;

import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.*;

@Getter
@Setter
@ToString
@EqualsAndHashCode
@NoArgsConstructor
@Entity
public class FAQs{	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Integer faqs_id;

	@NotNull
	@Column(length=5000,unique=true)
	private String faqs_question;

	@NotNull
	@Column(length=5000)
	private String faqs_answer;
	
	@ManyToOne(fetch=FetchType.LAZY)
    @JoinColumn(name = "faqs_created_by")
	@JsonIgnoreProperties(
			{
				"hibernateLazyInitializer",
				"handler",
				"notes",
				"threads",
				"groups",
				"notes",
				"threads",
				"threadComments",
				"threadCommentReplies",
				"medias",
				"user_password",
				"user_phone_number",
				"user_status",
				"user_sms_notif",
				"user_email_notif",
				"user_security_question",
				"user_secret_answer",
				"user_dateAndTimeCreated"
			})
	private Users faqs_created_by;

	@NotNull
	private String faqs_status;
	
	@Temporal(TemporalType.TIMESTAMP)
	private Date faqs_dateAndTimeCreated;
	
	@Temporal(TemporalType.TIMESTAMP)
	private Date faqs_dateAndTimeModified;
	
	@OneToMany(
			cascade = CascadeType.ALL,
			fetch = FetchType.LAZY,
			mappedBy = "faQs")
	@JsonIgnore
	private List<Notifications> notifications;

}
