<h5 style="text-align: center;"><strong>${msg}</strong></h5>

<form id="frmLoginAccount" class="login100-form validate-form" action="${pageContext.request.contextPath}/" method="POST">
	<div class="wrap-input100 validate-input m-b-26" data-validate="Username is required">
		<span class="label-input100" style="color: black; font-weight: bold;">Username</span>
		<input class="input100" type="text" name="username" placeholder="Enter username" autocomplete="off">
		<span class="focus-input100"></span>
	</div>

	<div class="wrap-input100 validate-input m-b-18" data-validate = "Password is required">
		<span class="label-input100" style="color: black; font-weight: bold;">Password</span>
		<input class="input100" id="frmLogin_txtPassword" type="password" name="password" placeholder="Enter password">
		<span class="focus-input100"></span>
	</div>

	<div class="flex-sb-m w-full p-b-30">
		<div class="contact100-form-checkbox">
			<input class="input-checkbox100" id="ckb1" type="checkbox" onclick="showOrHidePassword()">
			<label class="label-checkbox100" for="ckb1" style="color: black; font-weight: bold;">
				Show Password
			</label>
		</div>

		<script type="text/javascript">
            function showOrHidePassword() {
                var x = document.getElementById("frmLogin_txtPassword");
                if (x.type === "password") {
                    x.type = "text";
                } else {
                    x.type = "password";
                }
            }
		</script>

		<div>
			<a class="txt1" href="${pageContext.request.contextPath}/forgot_password_page" style="cursor: pointer; color: black; font-weight: bold;">
				<strong>Forgot Password?</strong>
			</a>
		</div>

	</div>

	<div class="container-login100-form-btn">
		<button class="login100-form-btn" type="submit">
			<span class="fa fa-sign-in"></span>&nbsp;&nbsp;<strong>Login</strong>
		</button>
	</div>

	<h3 style="text-align: center;">
		<strong style="font-size: 15px;">Not a member? <a href="${pageContext.request.contextPath}/registration_page">Join now</a></strong>
	</h3>

</form>