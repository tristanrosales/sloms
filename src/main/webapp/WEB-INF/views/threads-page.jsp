<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ page session="true"%>
<!DOCTYPE html>
<html lang="en">
<head>
<!-- META SECTION -->
<title>Threads | SLOMS</title>
<tiles:insertAttribute name="headInit" />
</head>
<body>
	<!-- START PAGE CONTAINER -->
	<div class="page-container">

		<tiles:insertAttribute name="sidebar" />

		<!-- PAGE CONTENT -->
		<div class="page-content">

			<tiles:insertAttribute name="x_navigationVertical" />

			<!-- START BREADCRUMB -->
			<ul class="breadcrumb">
				<li><a href="${pageContext.request.contextPath}/home">Home</a></li>
				<li class="active">Threads</li>
			</ul>
			<!-- END BREADCRUMB -->

			<!-- PAGE TITLE -->
			<div class="page-title">
				<h2><span class="fa fa-comments"></span> Threads</h2>
			</div>
			<!-- END PAGE TITLE -->

			<!-- PAGE CONTENT WRAPPER -->
			<div class="page-content-wrap">

				<div class="row">
					<div class="col-md-12">
						<!-- START SIMPLE DATATABLE -->
						<div class="panel panel-default">
							<div class="panel-heading">
								<button type="button" id="threads-page_btnAddNewThread"
									class="btn btn-info btn-lg"
									style="display: none;" 
									data-toggle="modal"
									data-target="#add_new_thread-modal">
									Add New Thread
								</button>
								
								<ul class="panel-controls">
									<li><a href="#" class="panel-collapse"><span
											class="fa fa-angle-down"></span></a></li>
									<li><a href="#" class="panel-refresh"><span
											class="fa fa-refresh"></span></a></li>
								</ul>
							</div>
							<div class="panel-body">
								<div class="table-responsive">
									<table id="tblThreads"
										class="table table-bordered table-striped">
										<thead>
											<tr>
												<th>ID</th>
												<th>Type</th>
												<th>Thread Name</th>
												<th>Media Name</th>
												<th>Created By</th>
												<th>Status</th>
												<th>Created</th>
												<th>Modified</th>
												<th>Actions</th>
											</tr>
										</thead>

										<tbody></tbody>

										<tfoot>
											<tr>
												<th>ID</th>
												<th>Type</th>
												<th>Name</th>
												<th>Media</th>
												<th>User</th>
												<th>Status</th>
												<th>Created</th>
												<th>Modified</th>
												<th>Actions</th>
											</tr>
										</tfoot>
									</table>
								</div>
							</div>
						</div>
						<!-- END SIMPLE DATATABLE -->

					</div>
				</div>

			</div>
			<!-- PAGE CONTENT WRAPPER -->
		</div>
		<!-- END PAGE CONTENT -->
	</div>
	<!-- END PAGE CONTAINER -->

	<tiles:insertAttribute name="add_new_thread_modal" />
	<tiles:insertAttribute name="view_edit_thread_modal" />
	<tiles:insertAttribute name="signOutModal_and_scripts" />

</body>
</html>