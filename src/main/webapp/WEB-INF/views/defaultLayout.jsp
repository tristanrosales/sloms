<%@ page contentType="text/html; charset=utf-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<!DOCTYPE html>
<html>
    <head>
        <title><tiles:getAsString name="title" /></title>
        <tiles:insertAttribute name="headInit" />
    </head>
    
    <body>
        <!-- START PAGE CONTAINER -->
	    <div class="page-container">
            <tiles:insertAttribute name="sidebar" />
            <tiles:insertAttribute name="signOutModal_and_scripts" />

            <tiles:insertAttribute name="body" />
        </div>
    </body>
</html>