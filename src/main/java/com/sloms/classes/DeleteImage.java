package com.sloms.classes;

import java.io.File;
import java.nio.file.FileSystems;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.codec.digest.DigestUtils;

public class DeleteImage {
	public static void deleteImage(String filename,String filePath) {
		List<String> fileTypes = new ArrayList<String>() {/**
			 * 
			 */
			private static final long serialVersionUID = 1L;

		{
			add(".png");
			add(".jpg");
		}};
		
		File deletedFile = null;
		
		for (String fileType: fileTypes) {
			deletedFile = FileSystems.getDefault().getPath(filePath + DigestUtils.md5Hex(filename) + fileType).toFile();
			System.out.println(deletedFile.getAbsolutePath());
			
			if (deletedFile.exists()) {
				deletedFile.delete();
				break;
			}
		}
	}
	
	public static void deleteImageInFolder(String filePath) {		
		File deletedFile = null;
		
		deletedFile = FileSystems.getDefault().getPath(filePath).toFile();
		System.out.println(deletedFile.getAbsolutePath());
		
		if (deletedFile.exists()) {
			deletedFile.delete();
		}
	}
}
