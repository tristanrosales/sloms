for(var a=15; a<=90; a++){
    if(a % 15 == 0){
        addPanelInfoForNeedsUpdate(a);
        addPanelInfoForSoonToExpire(a);
    }
}

$('#needs_update_panel-accordion').accordion('destroy').accordion();
$('#soon_to_expire_panel-accordion').accordion('destroy').accordion();

function addPanelInfoForNeedsUpdate(days){
    var orderedList = '<ol id="orderedList_needsUpdate_' + days + 'days" style="font-size: 15px;"></ol>';

    var body = '<div class="panel panel-info">' +
        '<div class="panel-heading">' + 
            '<h4 class="panel-title">' + 
                '<a href="#needs_update_in_' + days + 'days">' +
                    'Needs update in <b>' + days + 'days</b>' +
                '</a>' +
            '</h4>' +
        '</div>' + 
        '<div class="panel-body posts" id="needs_update_in_' + days + 'days" class="row">';

    /*
        body += '<button type="button" id="email_btn_needs_update_in_' + days + 'days" class="btn btn-info btn-rounded" onclick="sendEmailNotificationToNeedsUpdateInDays(' + days + ')" style="cursor: pointer; margin-bottom: 10px; display: none; padding: 10px; font-size: 15px; background-color: #1caf9a; color: white;">' +
                    '<strong>' +
                        '<span class="glyphicon glyphicon-send"></span> Send Email Notification To All Users' +
                    '</strong>' +
                '</button>';


                '<button type="button" id="sms_btn_needs_update_in_' + days + 'days" class="btn btn-warning btn-rounded" onclick="sendSMSNotificationToNeedsUpdateInDays(' + days + ')" style="white-space: normal !important; word-wrap: break-word; cursor: pointer; margin-bottom: 10px; display: none; padding: 10px; font-size: 15px; background-color: orange; color: white;">' +
                    '<strong>' +
                        '<span class="glyphicon glyphicon-send"></span> Send SMS Notification To All Users' +
                    '</strong>' +
                '</button>';
    */

    body += '<div class="alert alert-danger" role="alert" id="needs_update_in_' + days + 'days_no_data_available" style="font-size: 15px;">' +
                '<button type="button" class="close" data-dismiss="alert"></button>' +
                '<strong>Oops!</strong> No data available!' +
            '</div>';

    body += '</div></div>';
    
    $('#needs_update_panel-accordion').append(body);
    $('#needs_update_in_' + days + 'days').append(orderedList);
}

function addPanelInfoForSoonToExpire(days){
    var soon_to_expire_orderedList = '<ol id="orderedList_expiring_' + days + 'days" style="font-size: 15px;"></ol>';

    var soon_to_expire_body = '<div class="panel panel-info">' +
        '<div class="panel-heading">' + 
            '<h4 class="panel-title">' + 
                '<a href="#expiring_in_' + days + 'days">' +
                    'Expiring in <b>' + days + 'days</b>' +
                '</a>' +
            '</h4>' +
        '</div>' + 
        '<div class="panel-body" id="expiring_in_' + days + 'days" class="row">';

    /*
        soon_to_expire_body += '<button type="button" id="email_btn_expiring_in_' + days + 'days" class="btn btn-info btn-rounded" onclick="sendEmailNotificationToExpiringMediaInDays(' + days + ')" style="white-space: normal !important; word-wrap: break-word; cursor: pointer; margin-bottom: 10px; display: none; padding: 10px; font-size: 15px; background-color: #1caf9a; color: white;">' +
                                    '<strong>' +
                                        '<span class="glyphicon glyphicon-send"></span> Send Email Notification To All Users' +
                                    '</strong>' +
                                '</button>';


                                '<button type="button" id="sms_btn_expiring_in_' + days + 'days" class="btn btn-warning btn-rounded" onclick="sendSMSNotificationToExpiringMediaInDays(' + days + ')" style="white-space: normal !important; word-wrap: break-word; cursor: pointer; margin-bottom: 10px; display: none; padding: 10px; font-size: 15px; background-color: orange; color: white;">' +
                                    '<strong>' +
                                        '<span class="glyphicon glyphicon-send"></span> Send SMS Notification To All Users' +
                                    '</strong>' +
                                '</button>';
    */
                                
    soon_to_expire_body += '<div class="alert alert-danger" role="alert" id="expiring_in_' + days + 'days_no_data_available" style="font-size: 15px;">' +
                '<button type="button" class="close" data-dismiss="alert"></button>' +
                '<strong>Oops!</strong> No data available!' +
            '</div>';

    soon_to_expire_body += '</div></div>';
    
    $('#soon_to_expire_panel-accordion').append(soon_to_expire_body);
    $('#expiring_in_' + days + 'days').append(soon_to_expire_orderedList);
}