package com.sloms.service;

import java.util.List;

import com.sloms.entity.Notes;
import com.sloms.entity.Notifications;

public interface NotesService {
	Notes getNotesById(Integer notesID);
	void saveNotes(Notes notesDAO, Notifications notificationsDAO);
	void updateNotes(Notes notesDAO);
	List<Notes> getAllNotes();
	List<Notes> getAllActivatedNotes();
}
