package com.sloms.controller;

import java.security.Principal;
import java.util.List;

import com.sloms.classes.RoleConstants;
import com.sloms.entity.Users;
import com.sloms.service.UsersService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.web.bind.annotation.*;

import com.google.gson.JsonObject;
import com.sloms.entity.FAQs;
import com.sloms.entity.Notifications;
import com.sloms.service.FAQsService;

@RestController
public class FAQsController {
	@Autowired private FAQsService faQsService;
	@Autowired private UsersService usersService;
	@Autowired private SimpMessagingTemplate template;
	private JsonObject status = new JsonObject();

	@PostMapping("/add-faqs")
	@ResponseBody
	public String addFAQs(
			@ModelAttribute("faQs") FAQs faQs,
			@ModelAttribute("notifications") Notifications notifications,
			Principal principal) {

		Users loggedInUser = usersService.findUserByUsername(principal.getName());

		if(RoleConstants.IT_ADMIN.equals(loggedInUser.getUser_role())){
			FAQs faqsQuestionExists = faQsService.findFAQsByFAQsQuestion(faQs.getFaqs_question());

			if(faqsQuestionExists != null) {
				status.addProperty("isAddingFAQsSuccess", "faqsQuestionExists");
			}else{
				faQsService.saveFAQ(faQs, notifications, principal);
				status.addProperty("isAddingFAQsSuccess", true);

				template.convertAndSend("/sloms/bulletin_board", "bulletin_board");
				template.convertAndSend("/sloms/all_faqs", "all_faqs");
			}
		}else{
			status.addProperty("isAddingFAQsSuccess", "accessDenied");
		}

		return status.toString();
	}
	
	@PostMapping("/update-faqs")
	@ResponseBody
	public String updateFAQs(
			@RequestParam String faqs_question, 
			@RequestParam String faqs_answer,
			@RequestParam String faqs_status,
			@RequestParam Integer faqs_id,
			Principal principal) {

		Users loggedInUser = usersService.findUserByUsername(principal.getName());

		if(RoleConstants.IT_ADMIN.equals(loggedInUser.getUser_role())){
			faQsService.updateFAQs(faqs_question.trim(), faqs_answer.trim(),faqs_status.trim(), faqs_id);
			status.addProperty("isUpdatingFAQsSuccess", true);

			template.convertAndSend("/sloms/bulletin_board", "bulletin_board");
			template.convertAndSend("/sloms/all_faqs", "all_faqs");
		}else{
			status.addProperty("status", "accessDenied");
		}
		
		return status.toString();
	}
	
	@GetMapping("/all_faqs")
	public List<FAQs> getAllFAQs(){
		return faQsService.getAllFAQs();
	}
}
