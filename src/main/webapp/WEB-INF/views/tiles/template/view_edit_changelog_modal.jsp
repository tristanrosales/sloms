<div class="modal" id="view_edit_changelog-modal" tabindex="-1" role="dialog" aria-labelledby="largeModalHead" aria-hidden="true" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <form id="frmUpdateChangelog" class="form-horizontal" role="form" action="/update-changelog" method="POST">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">
                        <span aria-hidden="true">&times;</span><span class="sr-only">Close</span>
                    </button>
                    <h3 class="modal-title" id="largeModalHead">Edit Changelog</h3>
                </div>

                <div class="modal-body" style="max-height: calc(100vh - 200px); overflow-y: auto;">
                    <input type="hidden" id="frmUpdateChangelog_lblID" name="changelog_id">

                    <div class="form-group">
                        <label class="col-md-2 control-label">Version</label>
                        <div class="col-md-10">
                            <input 
                                type="text" 
                                id="frmUpdateChangelog_txtVersion" 
                                class="form-control" 
                                placeholder="Enter version..." 
                                required="required" 
                                name="changelog_version" 
                                style="background-color:white !important;"/>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-md-2 control-label">Changelog Details</label>
                        <div class="col-md-10">

                            <textarea 
                                id="frmUpdateChangelog_txtDetails"
                                class="form-control" 
                                rows="5"
                                placeholder="Enter changelog details..." 
                                required="required"
                                name="changelog_details" 
                                style="resize: none; background-color:white !important;"></textarea>

                            <span class="help-block">Required changelog details</span>
                        </div>
                    </div>    
                </div>

                <div class="modal-footer">
                    <button
                        type="submit" 
                        class="btn btn-info btn-lg btn-rounded" 
                        style="padding: 10px; font-size: 15px; background-color:#1caf9a; color: white;">
                        <strong>
                            <span class="glyphicon glyphicon-hand-right"></span> Update Changelog
                        </strong>
                    </button>

                    <button 
                        type="button" 
                        class="btn btn-warning btn-rounded" 
                        style="padding: 10px; font-size: 15px; background-color: orange; color: white;" 
                        data-dismiss="modal">
                        <strong>
                            <span class="glyphicon glyphicon-remove-sign"></span> Close
                        </strong>
                    </button>
                </div>
            </form>
        </div>
    </div>
</div>