package com.sloms.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.handler.annotation.MessageExceptionHandler;
import org.springframework.messaging.handler.annotation.SendTo;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.sloms.entity.Notifications;
import com.sloms.repository.NotificationsRepository;

@RestController
public class NotificationsController {
	
	@Autowired
	private NotificationsRepository notificationsRepository;
	
	@RequestMapping(path="/notifications", method=RequestMethod.GET)
	public List<Notifications> getAllNotifications(){
		return notificationsRepository.getAllNotifications();
	}
	
	@MessageExceptionHandler
    @SendTo("/sloms/errors")
    public String handleException(Throwable exception) {
        return exception.getMessage();
    }
}
