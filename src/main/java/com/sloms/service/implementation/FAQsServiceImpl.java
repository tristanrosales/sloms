package com.sloms.service.implementation;

import java.security.Principal;
import java.util.Date;
import java.util.List;

import com.sloms.service.FAQsService;
import com.sloms.service.UsersService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.sloms.entity.FAQs;
import com.sloms.entity.Notifications;
import com.sloms.entity.Users;
import com.sloms.repository.FAQsRepository;
import com.sloms.repository.NotificationsRepository;

@Service
@Transactional(readOnly=true)
public class FAQsServiceImpl implements FAQsService {
	
	@Autowired private FAQsRepository faQsRepository;
	@Autowired private NotificationsRepository notificationsRepository;
	@Autowired private UsersService usersService;
	
	@Override
	public List<FAQs> getAllFAQs() {
		return faQsRepository.getAllFAQs();
	}
	
	@Override
	@Transactional
	public void saveFAQ(FAQs faQsDAO, Notifications notificationsDAO,  Principal principal) {
		FAQs faQs = new FAQs();
		Notifications notifications = new Notifications();
		Users loggedInUser = usersService.findUserByUsername(principal.getName());
		final Date dateAndTimeToday = new Date();
		
		//faqs
		faQs.setFaqs_question(faQsDAO.getFaqs_question().trim());
		faQs.setFaqs_answer(faQsDAO.getFaqs_answer().trim());
		faQs.setFaqs_dateAndTimeCreated(dateAndTimeToday);
		faQs.setFaqs_created_by(loggedInUser);
		faQs.setFaqs_status("Enabled");
		
		//notification
		notifications.setFaQs(faQs);
		notifications.setNotif_type("add_faqs");
		
		faQsRepository.save(faQs);
		notificationsRepository.save(notifications);
	}

	@Override
	@Transactional
	public void updateFAQs(String faqs_question, String faqs_answer, String faqs_status, Integer faqs_id) {
		final Date dateAndTimeModified = new Date();
		faQsRepository.updateFAQsInformation(faqs_question, faqs_answer, faqs_status, dateAndTimeModified, faqs_id);
	}

	@Override
	public FAQs findFAQsByFAQsQuestion(String faqs_question) {
		return faQsRepository.findFAQsByFAQsQuestion(faqs_question);
	}
}
