package com.sloms.repository;

import java.util.Date;
import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.sloms.entity.Changelog;

@Repository
public interface ChangelogRepository extends JpaRepository<Changelog, Integer>{
	@Query("SELECT c FROM Changelog c ORDER BY c.changelog_id DESC")
	List<Changelog> getAllChangelog();
	
	@Transactional
	@Modifying
	@Query("UPDATE Changelog c SET "
			+ "c.changelog_version=:changelog_version,"
			+ "c.changelog_details=:changelog_details,"
			+ "c.changelog_modified=:changelog_modified "
			+ "WHERE c.changelog_id=:changelog_id")
	void updateChangelogInformation(
			@Param("changelog_version") String changelog_version,
			@Param("changelog_details") String changelog_details,
			@Param("changelog_modified") Date changelog_modified,
			@Param("changelog_id") Integer changelog_id);
}
