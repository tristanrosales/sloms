package com.sloms.classes;

public class RoleConstants {
    public static final String IT_ADMIN = "IT_ADMIN";
    public static final String SYS_ADMIN = "SYS_ADMIN";
    public static final String SYS_SPECIALIST = "SYS_SPECIALIST";
    public static final String GUEST = "GUEST";
}
