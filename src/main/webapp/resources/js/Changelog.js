var stompClient = null;
var userID = document.getElementById("loggedInUserId").innerText;
var userRole = document.getElementById("loggedInUserRole").innerText;

var IT_ADMIN = "IT_ADMIN";

function connectWebSocket(){
    var socket = new SockJS('/sloms_web_socket');
    stompClient = Stomp.over(socket);

    stompClient.connect({}, function(frame) {
		stompClient.subscribe("/sloms/errors", function(message) {
            console.log("Web Socket Error Message : " + message.body);
            connectWebSocket();
		});

		stompClient.subscribe("/sloms/all_changelog", function(message) {
            $('#tblChangelog').DataTable().ajax.reload();
		});
	},function(error) {
        console.log("Web Socket Error Message : " + error);
    });
}

function addChangelog(){
	$('#frmAddChangelog').submit(function(event) {
		event.preventDefault();
		var formEl = $(this);
	
		swal({
			title: "Do you still want to continue?",
			icon: "warning",
			buttons: ["No!", "Yes!"],
			dangerMode: true
		})
		.then((willAdd) => {
			if (willAdd) {
				$.ajax({
					type: 'POST',
					dataType : 'JSON',
					url: formEl.prop('action'),
					data: formEl.serialize(),
					success: function(data) {
						if(data.status){
							iziToast.success({
                                title: 'Congratulations!',
                                theme: 'light', // dark
                                color: 'green', // blue, red, green, yellow
                                position: 'bottomRight', // bottomRight, bottomLeft, topRight, topLeft, topCenter, bottomCenter, center
                                message: "Changelog has been added successfully!"
                            });
    
                            $("#add_changelog-modal").modal('toggle');
							$('#tblChangelog').DataTable().ajax.reload();
						}else if (data.status == "accessDenied"){
                            iziToast.error({
                                title: "Oops! We're sorry!",
                                theme: 'light',
                                color: 'red',
                                position: 'bottomRight',
                                message: "Permission denied. Please try again!"
                            });
						}
					},
					error: function(e){
						console.log(e);
                        iziToast.error({
                            title: "Oops! We're sorry!",
                            theme: 'light',
                            color: 'red',
                            position: 'bottomRight',
                            message: "Failed to add changelog. Please try again!"
                        });
					}
				});
			}
		});
	});
}

function updateChangelog(){
	$('#frmUpdateChangelog').submit(function(event) {
		event.preventDefault();
		var formEl = $(this);
		
		swal({
			  title: "Do you still want to continue?",
			  icon: "warning",
			  buttons: ["No!", "Yes!"],
			  dangerMode: true
		})
		.then((willUpdate) => {
			if (willUpdate) {
				$.ajax({
					type: 'POST',
					dataType : 'JSON',
					url: formEl.prop('action'),
					data: formEl.serialize(),
					success: function( data, textStatus, jqXHR) {
						if(data.isUpdatingChangelogSuccess){
							iziToast.success({
								title: 'Congratulations!',
								theme: 'light', // dark
								color: 'green', // blue, red, green, yellow
								position: 'bottomRight', // bottomRight, bottomLeft, topRight, topLeft, topCenter, bottomCenter, center
								message: "Changelog has been updated successfully!"
							});
	
							$("#view_edit_changelog-modal").modal('toggle');
							$('#tblChangelog').DataTable().ajax.reload();
						}else if(data.isUpdatingChangelogSuccess == "accessDenied"){
                            iziToast.error({
                                title: "Oops! We're sorry!",
                                theme: 'light',
                                color: 'red',
                                position: 'bottomRight',
                                message: "Permission denied. Please try again!"
                            });
						}
					},
					error: function(e){
						console.log(e);
                        iziToast.error({
                            title: "Oops! We're sorry!",
                            theme: 'light',
                            color: 'red',
                            position: 'bottomRight',
                            message: "Failed to update changelog. Please try again!"
                        });
					}
				});
			}
		});
	});
}

function showChangelogDetails(d) {
	var body,access;

	body = '<h3>What&#39;s new in Version ' + d.changelog_version + '?&nbsp;&nbsp;';

	if(userRole == IT_ADMIN){
		access = '<button type="button" id="btnViewOrEditChangelog" class="btn btn-info" data-toggle="modal" data-target="#view_edit_changelog-modal" data-id="' + d.changelog_id + '">Edit Changelog</button>';
	}else{
		access = '';
	}

	body += access + "</h3>";

	body += '<p style="text-align: left;"><pre>' + d.changelog_details + '</pre></p>';
	body += '<p><span class="fa fa-clock-o"></span> ' + timeago().format(d.changelog_dateAndTimeAdded) + '</p><br>';

	$('#frmUpdateChangelog_lblID').val(d.changelog_id);
	$('#frmUpdateChangelog_txtVersion').val(d.changelog_version);
	$('#frmUpdateChangelog_txtDetails').val(d.changelog_details);

	return body;
}

function showAllChangelog(){
	$("#tblChangelog tbody").empty();

	/* DataTables instantiation. */
	var table = $("#tblChangelog").DataTable({
		"ajax": {
			"url": "/changelog",
			"dataSrc": ""
		},
		"columns": [
			{
				"className": 'details-control',
				"orderable": false,
				"data": null,
				"defaultContent": ''
			},
			{ 
				"data": null,
				render: function (data,type,row) {
					return '<center><span style="font-weight: bold;">' + data.changelog_version + '</span></center>';
				}
			}
		],
		"iDisplayLength": 5,
		"aLengthMenu": [[5, 10, 25, 50, 100, -1], [5, 10, 25, 50, 100, "All"]],
		"pagingType": "full_numbers"
	});

    $('#tblChangelog tbody').on('click', 'td.details-control', function () {
        var tr = $(this).closest('tr');
        var row = table.row(tr);
 
        if (row.child.isShown()) {
            row.child.hide();
            tr.removeClass('shown');
        }
        else {
            row.child(showChangelogDetails(row.data())).show();
            tr.addClass('shown');
        }
    });
}

connectWebSocket();
showAllChangelog();
addChangelog();
updateChangelog();
