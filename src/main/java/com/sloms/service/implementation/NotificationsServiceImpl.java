package com.sloms.service.implementation;

import com.sloms.service.NotificationsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.sloms.entity.Notifications;
import com.sloms.repository.NotificationsRepository;

@Service
@Transactional(readOnly=true)
public class NotificationsServiceImpl implements NotificationsService {
	
	@Autowired
	private NotificationsRepository notificationsRepository;
	
	@Override
	@Transactional
	public void saveNotification(Notifications notificationsDAO) {
		Notifications notifications = new Notifications();
		
		notifications.setNotif_type(notificationsDAO.getNotif_type().trim());
		
		notificationsRepository.save(notifications);
	}
}
