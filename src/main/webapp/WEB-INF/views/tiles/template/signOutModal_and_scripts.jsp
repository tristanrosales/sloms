<!-- BLUEIMP GALLERY -->
<div id="blueimp-gallery" class="blueimp-gallery blueimp-gallery-controls">
	<div class="slides"></div>
	<h3 class="title"></h3>
	<a class="prev"><span class="fa fa-chevron-circle-left"></span></a>
	<a class="next"><span class="fa fa-chevron-circle-right"></span></a>
	<a class="close"><span class="fa fa-times-circle"></span></a>
	<a class="play-pause"></a>
	<ol class="indicator"></ol>
</div>      
<!-- END BLUEIMP GALLERY -->

<!-- MESSAGE BOX-->
<div class="message-box animated fadeIn" data-sound="alert" id="mb-signout">
	<div class="mb-container">
		<div class="mb-middle">
			<div class="mb-title">
				<span class="fa fa-sign-out"></span> Log <strong>Out</strong> ?
			</div>
			<div class="mb-content">
				<p>Are you sure you want to log out?</p>
				<p>Press No if you want to continue work. Press Yes to logout current user.</p>
			</div>
			<div class="mb-footer">
				<div class="pull-right">
					<a href="/logout" class="btn btn-success btn-lg">Yes</a>
					<button class="btn btn-default btn-lg mb-control-close">No</button>
				</div>
			</div>
		</div>
	</div>
</div>
<!-- END MESSAGE BOX-->

<!-- START PRELOADS -->
<audio id="audio-alert" src="${pageContext.request.contextPath}/resources/audio/alert.mp3" preload="auto"></audio>
<audio id="audio-fail" src="${pageContext.request.contextPath}/resources/audio/fail.mp3" preload="auto"></audio>
<!-- END PRELOADS -->

<!-- START SCRIPTS -->
<!-- START PLUGINS -->
<script type="text/javascript" src="${pageContext.request.contextPath}/resources/js/plugins/jquery/jquery.min.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/resources/js/plugins/jquery/jquery-ui.min.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/resources/js/plugins/bootstrap/bootstrap.min.js"></script>
<!-- END PLUGINS -->

<!-- START THIS PAGE PLUGINS-->
<script type="text/javascript" src="${pageContext.request.contextPath}/resources/js/plugins/icheck/icheck.min.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/resources/js/plugins/blueimp/jquery.blueimp-gallery.min.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/resources/js/plugins/mcustomscrollbar/jquery.mCustomScrollbar.min.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/resources/js/plugins/scrolltotop/scrolltopcontrol.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/resources/js/plugins/highlight/jquery.highlight-4.js"></script>

<script type="text/javascript" src="${pageContext.request.contextPath}/resources/js/plugins/bootstrap/bootstrap-datepicker.js"></script>

<script type="text/javascript" src="${pageContext.request.contextPath}/resources/js/plugins/moment.min.js"></script>

<script type="text/javascript" src="${pageContext.request.contextPath}/resources/js/plugins/datatables/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/resources/js/plugins/datatables/dataTables.responsive.min.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/resources/js/plugins/datatables/dataTables.rowReorder.min.js"></script>

<script type="text/javascript" src="${pageContext.request.contextPath}/resources/js/plugins/bootstrap/bootstrap-select.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/resources/js/plugins/validationengine/languages/jquery.validationEngine-en.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/resources/js/plugins/validationengine/jquery.validationEngine.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/resources/js/plugins/jquery-validation/jquery.validate.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/resources/js/plugins/maskedinput/jquery.maskedinput.min.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/resources/js/plugins/moment.min.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/resources/js/plugins/moment-with-locales.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/resources/js/plugins/sweetalert.min.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/resources/js/plugins/timeago/src/timeago.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/resources/js/plugins/currency.min.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/resources/js/plugins/smtp-js/smtp.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/resources/js/plugins/Datejs/build/date.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/resources/js/plugins/easyPaginate/jquery.easyPaginate.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/resources/js/plugins/jquery-searchable/jquery.searchable.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/resources/js/plugins/mimo84-bootstrap-maxlength/bootstrap-maxlength.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/resources/js/plugins/web-socket/sockjs.min.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/resources/js/plugins/web-socket/stomp.min.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/resources/js/plugins/Fullscreen-Loading-Modal-Indicator/js/jquery.loadingModal.js"></script>
<!-- END THIS PAGE PLUGINS-->


<!-- START TEMPLATE -->
<script type="text/javascript" src="${pageContext.request.contextPath}/resources/js/plugins.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/resources/js/actions.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/resources/js/faq.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/resources/js/datatable.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/resources/js/MediaHome.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/resources/js/validation.js"></script>
<!-- END TEMPLATE -->

<script>	
	var jvalidate = $("#jvalidate").validate({
		ignore : [],
		rules : {
			login : {
				required : true,
				maxlength : 8
			},
			user_password : {
				required : true,
				maxlength : 16
			},
			're-password' : {
				required : true,
				maxlength : 16,
				equalTo : "#password2"
			},
			age : {
				required : true,
				min : 18,
				max : 100
			},
			user_email_address : {
				required : true,
				email : true
			},
			date : {
				required : true,
				date : true
			},
			credit : {
				required : true,
				creditcard : true
			},
			site : {
				required : true,
				url : true
			}
		}
	});
</script>
<!-- END SCRIPTS -->