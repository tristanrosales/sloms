var stompClient = null;
var userID = document.getElementById("loggedInUserId").innerText;
var userRole = document.getElementById("loggedInUserRole").innerText;

var IT_ADMIN = "IT_ADMIN";

function connectWebSocket(){
    var socket = new SockJS('/sloms_web_socket');
    stompClient = Stomp.over(socket);

    stompClient.connect({}, function(frame) {
		stompClient.subscribe("/sloms/errors", function(message) {
            console.log("Web Socket Error Message : " + message.body);
            connectWebSocket();
		});

		stompClient.subscribe("/sloms/all_faqs", function(message) {
            $('#tblFAQs').DataTable().ajax.reload();
		});
	},function(error) {
        console.log("Web Socket Error Message : " + error);
    });
}

function showFAQsDetails(d) {
	var table,access,creatorProfilePic;

    if(d.faqs_created_by.user_profile_picture == null || d.faqs_created_by.user_profile_picture == ""){
        creatorProfilePic = '/resources/img/no-image-available.jpg';
    }else{
        creatorProfilePic = '/SLOMS_server' + d.faqs_created_by.user_profile_picture;
    }

	table = '<table cellpadding="5" cellspacing="0" border="0" style="padding-left:50px;" class="table table-bordered table-striped display">'+
				'<tr>'+
					'<td><strong>ANSWER</strong></td>'+
					'<td>'+d.faqs_answer+'</td>'+
				'</tr>'+
				'<tr>'+
					'<td><strong>CREATOR</strong></td>'+
					'<td><img src="' + creatorProfilePic + '" style="width: 50px; height: 50px; border-radius: 50%;" />&nbsp;&nbsp;&nbsp;' + d.faqs_created_by.user_firstname + " " + d.faqs_created_by.user_lastname + ' <b>(' + d.faqs_created_by.user_role + ')</b></td>'+
				'</tr>'+
				'<tr>'+
					'<td><strong>CREATED</strong></td>'+
					'<td>'+ moment(new Date(d.faqs_dateAndTimeCreated)).format('LLLL') + ' | <b>' + timeago().format(d.faqs_dateAndTimeCreated) + '</b></td>'+
				'</tr>' +
				'<tr>'+
					'<td><strong>MODIFIED</strong></td>' +
					'<td>'+ ((d.faqs_dateAndTimeModified == null) ? "None" : moment(new Date(d.faqs_dateAndTimeModified)).format('LLLL') + ' | <b>' + timeago().format(d.faqs_dateAndTimeModified) + '</b>') + '</td>'+
				'</tr>'+
				'<tr>';

				if(userRole == IT_ADMIN){
					if(userID == d.faqs_created_by.user_id){
						access = '<button type="button" id="btnViewOrEditFAQs" class="btn btn-info btn-lg" data-toggle="modal" data-target="#view_edit_faqs-modal" data-id="' + d.faqs_id + '">Edit FAQ</button>';
					}
				}else{
					access = "<span class='fa fa-ban' style='font-size: 18px;'><br>Access Denied</span>";
				}

		table += '<td colspan="2" style="text-align: center;">' + access + '</td>'+
				'</tr>'+
			'</table>';

	$('#frmUpdateFAQs_lblID').val(d.faqs_id);
	$('#frmUpdateFAQs_txtQuestion').val(d.faqs_question);
	$('#frmUpdateFAQs_answer').val(d.faqs_answer);

	$('#frmUpdateFAQs_cmbStatus')
		.removeAttr('selected')
		.find('option[value="' + d.faqs_status + '"]')
		.attr("selected",true)
		.change();

	return table;
}

function showAllFAQs(){
	$("#tblFAQs tbody").empty();

	var table = $("#tblFAQs").DataTable({
		"ajax": {
			"url": "/all_faqs",
			"dataSrc": ""
		},
		"columns": [
			{
				"className": 'details-control',
				"orderable": false,
				"data": null,
				"defaultContent": ''
			},
			{ "data": "faqs_question" }
		],
		"iDisplayLength": 5,
		"aLengthMenu": [[5, 10, 25, 50, 100, -1], [5, 10, 25, 50, 100, "All"]],
		"pagingType": "full_numbers"
	});

    $('#tblFAQs tbody').on('click', 'td.details-control', function () {
        var tr = $(this).closest('tr');
        var row = table.row(tr);
 
        if (row.child.isShown()) {
            row.child.hide();
            tr.removeClass('shown');
        }
        else {
            row.child(showFAQsDetails(row.data())).show();
            tr.addClass('shown');
        }
    });
}

function validateFAQs(){
    $("#tblFAQs").validate({
		ignore : [],
		rules : {
			faqs_question : {
				required : true,
				maxlength : 1000
			},
			faqs_answer : {
				required : true,
				maxlength : 100
			}
		}
	});
}

function addFAQs(){
    $('#frmAddFAQs').submit(function(event) {
        event.preventDefault();
        var formEl = $(this);
    
        swal({
            title: "Do you still want to continue?",
            icon: "warning",
            buttons: ["No!", "Yes!"],
            dangerMode: true
        })
        .then((willAdd) => {
            if (willAdd) {
                $.ajax({
                    type: 'POST',
                    dataType : 'JSON',
                    url: formEl.prop('action'),
                    data: formEl.serialize(),
                    success: function( data, textStatus, jqXHR) {
                        if(data.isAddingFAQsSuccess == "faqsQuestionExists"){
                            iziToast.error({
                                title: "Oops! We're sorry!",
                                theme: 'light',
                                color: 'red',
                                position: 'bottomRight',
                                message: "FAQs question exists. Please try again!"
                            });
                        }else if(data.isAddingFAQsSuccess == "accessDenied"){
                            iziToast.error({
                                title: "Oops! We're sorry!",
                                theme: 'light',
                                color: 'red',
                                position: 'bottomRight',
                                message: "Permission denied. Please try again!"
                            });
						}else if(data.isAddingFAQsSuccess){
                            iziToast.success({
                                title: 'Congratulations!',
                                theme: 'light', // dark
                                color: 'green', // blue, red, green, yellow
                                position: 'bottomRight', // bottomRight, bottomLeft, topRight, topLeft, topCenter, bottomCenter, center
                                message: "FAQs has been added successfully!"
                            });
    
                            $("#add_faqs-modal").modal('toggle');
                            $('#tblFAQs').DataTable().ajax.reload();
                        }
                    },
                    error: function(e){
                        console.log(e);
                        iziToast.error({
                            title: "Oops! We're sorry!",
                            theme: 'light',
                            color: 'red',
                            position: 'bottomRight',
                            message: "Failed to add FAQs. Please try again!"
                        });
                    }
                });
            }
        });
    });
}

function updateFAQs(){
	$('#frmUpdateFAQs').submit(function(event) {
		event.preventDefault();
		var formEl = $(this);
		
		swal({
			  title: "Are you sure?",
			  text: "Once updated, you will not be able to undo this action!",
			  icon: "warning",
			  buttons: ["No, not sure yet!", "Yes, I'm Sure!"],
			  dangerMode: true
		})
		.then((willUpdate) => {
			if (willUpdate) {
				$.ajax({
					type: 'POST',
					dataType : 'JSON',
					url: formEl.prop('action'),
					data: formEl.serialize(),
					// if received a response from the server
					success: function( data, textStatus, jqXHR) {
						if(data.isUpdatingFAQsSuccess){
							iziToast.success({
								title: 'Congratulations!',
								theme: 'light', // dark
								color: 'green', // blue, red, green, yellow
								position: 'bottomRight', // bottomRight, bottomLeft, topRight, topLeft, topCenter, bottomCenter, center
								message: "FAQs has been updated successfully!"
							});
	
							$("#view_edit_faqs-modal").modal('toggle');
							$('#tblFAQs').DataTable().ajax.reload();
						}
					},
					error: function(e){
						console.log(e);
					}
				});
			}
		});
	});
}

connectWebSocket();
showAllFAQs();
validateFAQs();
addFAQs();
updateFAQs();