<!-- edit comment modal -->
<div class="modal" id="edit_comment-modal" tabindex="-1" role="dialog"
	aria-labelledby="largeModalHead" aria-hidden="true">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">
					<span aria-hidden="true">&times;</span><span class="sr-only">Close</span>
				</button>
				<h3 class="modal-title" id="largeModalHead">Edit Comment</h3>
			</div>
			<div class="modal-body">
				<form id="jvalidate" class="form-horizontal frmEditDeleteComment"
					role="form" method="POST">
					
					<input 
						type="hidden" 
						name="thread_comment_id"
						id="frmEditDeleteComment_lblThread_comment_id">
					<input 
						type="hidden" 
						name="thread_id"
						id="frmEditDeleteComment_lblThread_id"> 
					<input 
						type="hidden"
						name="thread_comment_by" 
						id="frmEditDeleteComment_lblThread_commentBy">
					<input 
						type="hidden"
						name="thread_dateAndTime_posted" 
						id="frmEditDeleteComment_lblThread_dateAndTime_posted">
					
					<div class="form-group">
						<label class="col-md-2 control-label">Comment</label>
						<div class="col-md-10">

							<textarea class="form-control" rows="5"
								placeholder="Write your comment..." required="required"
								name="thread_comment" id="frmEditDeleteComment_txtThread_comment"></textarea>

							<span class="help-block">Required comment</span>
						</div>
					</div>

					<div class="form-group">
						<div class="col-md-10" style="display: none;">
							<div class="panel panel-default">
								<div class="panel-body">
									<h3>
										<span class="fa fa-download"></span> Dropzone
									</h3>
									<p>
										Add form with class
										<code>dropzone</code>
										to get dropzone box
									</p>
									<!-- <form action="#" class="dropzone"></form> -->
								</div>
							</div>
						</div>

						<label class="col-md-2 control-label">Images (optional)</label>
						<div class="col-md-10">
							<div class="panel panel-default">
								<div class="panel-body">
									<h3>
										<span class="fa fa-download"></span> Comment Image
									</h3>
									
									<input 
										type="file" 
										class="dropzone dropzone-mini" 
										name="thread_comment_images">
								</div>
							</div>
							
							<div style="float: right;">
								<button type="submit" class="btn btn-info btn-lg btn-rounded">Update</button>
								<button type="button" id="btnDeleteComment"
									class="btn btn-warning btn-lg btn-rounded"
									onclick="deleteComment()">Delete</button>
							</div>
						</div>
					</div>
				</form>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
			</div>
		</div>
	</div>
</div>


<!-- edit reply modal -->
<div class="modal" id="edit_reply-modal" tabindex="-1" role="dialog"
	aria-labelledby="largeModalHead" aria-hidden="true">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">
					<span aria-hidden="true">&times;</span><span class="sr-only">Close</span>
				</button>
				<h3 class="modal-title" id="largeModalHead">Edit Reply</h3>
			</div>
			<div class="modal-body">
				<form id="jvalidate" class="form-horizontal frmEditDeleteReply" role="form" method="POST">
					<input 
						type="hidden" 
						name="thread_reply_id"
						id="frmEditDeleteReply_lblThread_reply_id">
					<input 
						type="hidden" 
						name="thread_comment_id"
						id="frmEditDeleteReply_lblThread_comment_id"> 
					<input
						type="hidden" 
						name="thread_reply_by"
						id="frmEditDeleteReply_lblThread_reply_by">
					<input
						type="hidden" 
						name="thread_dateAndTime_replied"
						id="frmEditDeleteReply_lblThread_dateAndTime_replied">

					<div class="form-group">
						<label class="col-md-2 control-label">Reply</label>
						<div class="col-md-10">

							<textarea 
								class="form-control" 
								rows="5"
								placeholder="Write your reply..." 
								required="required"
								name="thread_reply"
								id="frmEditDeleteReply_txtThread_reply">
							</textarea>

							<span class="help-block">Required reply</span>
						</div>
					</div>

					<div class="form-group">
						<label class="col-md-2 control-label">Images (optional)</label>
						<div class="col-md-10">
							<div class="panel panel-default">
								<div class="panel-body">
									<h3>
										<span class="fa fa-download"></span> Comment Image
									</h3>
									
									<input 
										type="file" 
										class="dropzone dropzone-mini"
										name="thread_reply_images">
								</div>
							</div>

							<div style="float: right;">
								<button type="submit" class="btn btn-info btn-lg btn-rounded">Update</button>
								<button type="button" id="btnDeleteReply"
									class="btn btn-warning btn-lg btn-rounded"
									onclick="deleteReply()">Delete</button>
							</div>
						</div>
					</div>
				</form>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
			</div>
		</div>
	</div>
</div>