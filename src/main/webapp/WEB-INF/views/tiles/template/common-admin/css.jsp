<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta http-equiv="X-UA-Compatible" content="IE=edge" />
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">

<link rel="icon" href="${pageContext.request.contextPath}/resources/sloms.ico" type="image/x-icon" />

<link rel="stylesheet" type="text/css" id="theme" href="${pageContext.request.contextPath}/resources/css/theme-blue.css" />
<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/resources/Login_v15/vendor/bootstrap/css/bootstrap.min.css">
<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/resources/Login_v15/fonts/font-awesome-4.7.0/css/font-awesome.min.css">
<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/resources/Login_v15/fonts/Linearicons-Free-v1.0.0/icon-font.min.css">
<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/resources/Login_v15/vendor/animate/animate.css">
<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/resources/Login_v15/vendor/css-hamburgers/hamburgers.min.css">
<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/resources/Login_v15/vendor/animsition/css/animsition.min.css">
<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/resources/Login_v15/vendor/select2/select2.min.css">
<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/resources/Login_v15/vendor/daterangepicker/daterangepicker.css">
<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/resources/Login_v15/css/util.css">
<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/resources/Login_v15/css/main.css">
<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/js/plugins/iziToast/css/iziToast.min.css">
<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/js/plugins/Fullscreen-Loading-Modal-Indicator/css/jquery.loadingModal.css">
