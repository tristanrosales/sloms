<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<!DOCTYPE html>
<html lang="en" class="body-full-height">
<head>

	<title>Login Page | SLOMS</title>
	<tiles:insertAttribute name="headInit" />

	<script type="text/javascript" src="/resources/js/plugins/jquery/jquery.min.js"></script>
	<script type="text/javascript" src="/resources/js/plugins/jquery/jquery-ui.min.js"></script>
	<script type="text/javascript" src="/resources/js/plugins/bootstrap/bootstrap.min.js"></script>

	<script type="text/javascript" src="/resources/js/plugins/sweetalert.min.js"></script>

	<link rel="stylesheet" href="/resources/js/plugins/iziToast-master/dist/css/iziToast.min.css">
	<script src="/resources/js/plugins/iziToast-master/dist/js/iziToast.min.js" type="text/javascript"></script>

</head>

<body>
	<div class="login-container">

		<div class="login-box animated fadeInDown">
			
			<h1 style="text-align: center; color: white;">
				<img src="/resources/img/sloms.png" style="width: 50px; height: 50px; vertical-align: middle;"> SLOMS, Inc.
			</h1>

			<!--<div class="login-logo"></div>-->
			<div class="login-body">
				<div class="login-title">
					<strong>Log In</strong> to your account
					<br><br>
					<span>${error}</span>
				</div>		
				
				<form id="frmLoginAccount" action="/login" class="form-horizontal" method="POST">
					<div class="form-group">
						<div class="col-md-12">
							<input type="text" class="form-control" placeholder="Username" name="username" autocomplete="off"/>
						</div>
					</div>
					<div class="form-group">
						<div class="col-md-12">
							<input type="password" class="form-control" placeholder="Password" name="password"/>
						</div>
					</div>
					<div class="form-group">
						<div class="col-md-6">
							<a href="#" class="btn btn-link btn-block" data-toggle="modal" data-target="#forgot-password_check-username">Forgot your password?</a>
						</div>
						<div class="col-md-6">
							<button class="btn btn-info btn-block" type="submit">Log In</button>
						</div>
					</div>
					
					<input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}" />
				</form>	
			</div>
			<div class="login-footer">
				<div class="pull-left">&copy; 2018 SLOMS, Inc.</div>
				<div class="pull-right">
					<a href="#">About</a> | <a href="#">Privacy</a> | <a href="#">Contact
						Us</a>
				</div>
			</div>
		</div>

		<!--modal -->
		<div class="modal" id="forgot-password_check-username" tabindex="-1" role="dialog" aria-labelledby="defModalHead" aria-hidden="true" data-backdrop="static" data-keyboard="false">
			<div class="modal-dialog">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
						<h4 class="modal-title" id="defModalHead">Forgot Password?</h4>
					</div>
					<div class="modal-body">
						<form id="frmForgotPassword_checkUsername" class="form-horizontal" role="form" action="/forgot-password-check-username" method="POST">

							<div class="form-group">
								<label class="col-md-2 control-label">Username</label>
								<div class="col-md-10">
									<input 
										type="text" 
										class="form-control" 
										placeholder="Enter your username..." 
										required="required" 
										id="forgotPassword_username" 
										name="username" 
										autocomplete="off"/>

									<span class="help-block">Required username</span>

									<br>

									<div style="float: right;">
										<button 
											type="submit" 
											class="btn btn-info btn-lg">

											<span class="fa fa-arrow-circle-right"></span>
											Answer Security Question
										
										</button>
										
										<button 
											type="button" 
											class="btn btn-warning btn-lg" 
											onclick="forgotPassword_getSecurityCode()">
											
											<span class="glyphicon glyphicon-send"></span>
											Get Security Code
										
										</button>
									</div>
								</div>		
							</div>

							<div class="alert alert-info" role="alert">
								<button type="button" class="close" data-dismiss="alert"></button>
								<strong>Reminder!</strong><br>
								Click <b>Answer Security Question</b> button if you have security question with answer set on your account.<br>
								Click <b>Get Security Code</b> button if you don't have any security question with answer set in your account.
							</div>
						</form>

					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
					</div>
				</div>
			</div>
		</div>

		<div class="modal" id="enter_your_secret_answer-modal" tabindex="-1" role="dialog" aria-labelledby="largeModalHead" aria-hidden="true" data-backdrop="static" data-keyboard="false">
			<div class="modal-dialog modal-lg">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal">
							<span aria-hidden="true">&times;</span><span class="sr-only">Close</span>
						</button>
						<h3 class="modal-title" id="largeModalHead">Hi, <span id="forgotPassword_lblUsername"></span>!</h3>
					</div>
					<div class="modal-body">
						<h3><span id="forgotPassword_lblUser_securityQuestion"></span></h3>

						<br>

						<form id="frmForgotPassword_enterSecretAnswer" action="/forgot-password-enter-secret-answer" method="POST" class="form-horizontal"
							role="form">

							<div class="form-group">
								<label class="col-md-2 control-label">Secret answer</label>
								<div class="col-md-10">
									<input type="password" class="form-control" placeholder="Enter your secret answer..." required="required" name="user_secret_answer" />
									<span class="help-block">Required secret answer</span>

									<br>
									<button type="submit" class="btn btn-info btn-lg" style="float: right;">Check Answer</button>
								</div>
							</div>
						</form>

					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
					</div>
				</div>
			</div>
		</div>

		<div class="modal" id="enter_your_security-code-modal" tabindex="-1" role="dialog" aria-labelledby="largeModalHead" aria-hidden="true" data-backdrop="static" data-keyboard="false">
			<div class="modal-dialog modal-lg">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal">
							<span aria-hidden="true">&times;</span><span class="sr-only">Close</span>
						</button>
						<h3 class="modal-title" id="largeModalHead">Hi, <span><%= session.getAttribute("forgotPassword_fullname") %>!</span></h3>
					</div>
					<div class="modal-body">
						<form id="frmForgotPassword_enterSecurityCode" action="/forgot-password-enter-security-code" method="POST" class="form-horizontal" role="form">

							<div class="form-group">
								<label class="col-md-2 control-label">Security Code</label>
								<div class="col-md-10">
									<input type="number" class="form-control" placeholder="Enter security code..." required="required" name="security_code" maxlength="4"/>
									<span class="help-block">Required security code</span>

									<br>

									<button type="submit" class="btn btn-info btn-lg" style="float: right;">
										<span class="glyphicon glyphicon-check"></span> 
										Check Code
									</button>
								</div>
							</div>
						</form>

					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
					</div>
				</div>
			</div>
		</div>

		<div class="modal" id="change_password-modal" tabindex="-1" role="dialog" aria-labelledby="largeModalHead" aria-hidden="true" data-backdrop="static" data-keyboard="false">
			<div class="modal-dialog modal-lg">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal">
							<span aria-hidden="true">&times;</span><span class="sr-only">Close</span>
						</button>
						<h3 class="modal-title" id="largeModalHead">You can now change your password</h3>
					</div>
					<div class="modal-body">
						<form id="frmForgotPassword_changePassword" class="form-horizontal" role="form" action="/forgot-password-change-password" method="POST">

							<div class="form-group">
								<label class="col-md-2 control-label">New Password:</label>
								<div class="col-md-10">
									<input type="password" class="form-control" name="user_password" id="txtNewPassword" placeholder="Enter new password..." /> 
									<span class="help-block">min size = 8, max size = 16</span>
								</div>
							</div>
		
							<div class="form-group">
								<label class="col-md-2 control-label">Confirm Password:</label>
								<div class="col-md-10">
									<input type="password" class="form-control" name="re-password" placeholder="Enter password again..." /> 
									<span class="help-block">Required same value as Password</span>

									<br>
									<button type="submit" class="btn btn-info btn-lg" style="float: right;">Change Password</button>
								</div>
							</div>

						</form>

					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
					</div>
				</div>
			</div>
		</div>

	</div>

	<script type='text/javascript' src='/resources/js/plugins/validationengine/languages/jquery.validationEngine-en.js'></script>
	<script type='text/javascript' src='/resources/js/plugins/validationengine/jquery.validationEngine.js'></script>
	<script type='text/javascript' src='/resources/js/plugins/jquery-validation/jquery.validate.js'></script>

	<script type="text/javascript" src="/resources/js/RegisterAndForgotPassword.js"></script>

	<script type="text/javascript">
		$("#frmLoginAccount").validate({
			ignore : [],
			rules : {
				username : {
					required : true
				},
				password : {
					required : true
				}
			}
		});
	</script>
</body>
</html>