<div class="alert alert-info" role="alert" style="font-size: 15px;">
    <button type="button" class="close" data-dismiss="alert"></button>
    <h3 style="text-align: left;"><strong>Forgot Password</strong></h3><br>
    Your Email must be the same as the one you used to register yourself at SLOMS.<br><br>

    <strong>Note:</strong> This is not yet functional.<br><br>

    <a href="${pageContext.request.contextPath}/" style="cursor: pointer; float: left;">
        <button class="forgot-password100-form-btn" type="button">
            <span class="fa fa-arrow-left"></span>&nbsp;&nbsp;<strong>Go Back</strong>
        </button>
    </a>

    <br><br>
    <hr width="100%">

    <span>${msg}</span>
</div>

<form id="frmForgotPassword" class="login100-form validate-form" method="POST">
    <div class="wrap-input100 validate-input m-b-18" data-validate = "Email is required">
        <span class="label-input100" style="color: black; font-weight: bold;">Email Address</span>
        <input class="input100" type="email" name="user_email_address" placeholder="Enter email address" autocomplete="off">
        <span class="focus-input100"></span>
    </div>

    <div class="container-login100-form-btn">
        <button class="login100-form-btn" type="submit">
            <span class="fa fa-mail-forward"></span>&nbsp;&nbsp;<strong>Send reset password email</strong>
        </button>
    </div>
</form>