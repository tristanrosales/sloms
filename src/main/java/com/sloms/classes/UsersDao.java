package com.sloms.classes;

import java.util.Date;

public interface UsersDao {

    Integer getUser_id();

    String getUsername();

    String getUser_firstname();

    String getUser_lastname();

    String getUser_email_address();

    String getUser_phone_number();

    String getUser_gender();

    String getUser_profile_picture();

    String getUser_role();

    String getUser_status();

    String getUser_sms_notif();

    String getUser_email_notif();

    Date getUser_dateAndTimeApproved();

    Date getUser_dateAndTimeCreated();
}
