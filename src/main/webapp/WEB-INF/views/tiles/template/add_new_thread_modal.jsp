<div class="modal" id="add_new_thread-modal" tabindex="-1" role="dialog" aria-labelledby="largeModalHead" aria-hidden="true" data-backdrop="static" data-keyboard="false">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<form id="frmAddThread" class="form-horizontal" role="form" method="POST" action="/add-thread">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">
						<span aria-hidden="true">&times;</span><span class="sr-only">Close</span>
					</button>
					<h3 class="modal-title" id="largeModalHead">Add Thread</h3>
				</div>

				<div class="modal-body" style="max-height: calc(100vh - 200px); overflow-y: auto;">
					<div class="alert alert-info" role="alert" style="font-size: 15px;">
						<button type="button" class="close" data-dismiss="alert"></button>
						<strong>Reminder!</strong><br>
						<ul>
							<li>Maximum file size: <b>10mb</b></li>
							<li>Allowed file extension: <b>jpeg, jpg, and png</b></li>
						</ul> 
					</div>
					
					<div class="form-group">
						<label class="col-md-2 control-label">Thread Preview Image</label>
						<div class="col-md-10">
							<div class="panel panel-default">
								<div class="panel-body">
									<h3>
										<span class="fa fa-download"></span> Thread Preview Image
									</h3>
									
									<div id="thread-preview-image-holder"></div>
								</div>
							</div>
						</div>
					</div>

					<div class="form-group">
						<label class="col-md-2 control-label">Type</label>
						<div class="col-md-10">
							<select 
								class="form-control select" 
								data-live-search="true" 
								name="thread_notes_id" 
								id="cmbNotes_type" 
								required="required">

								<option disabled="disabled" selected="selected" value="">---SELECT TYPE---</option>
							</select> 
							
							<span class="help-block">Required type</span>
						</div>
					</div>

					<input type="hidden" id="addNewThread_lblThreadBillboard" name="thread_billboard">

					<div class="form-group">
						<label class="col-md-2 control-label">Name</label>
						<div class="col-md-10">
							<input 
								type="text" 
								class="form-control"
								placeholder="Enter thread caption..." 
								required="required"
								name="thread_caption" 
								id="frmAddThread_caption"
								style="text-transform: capitalize; background-color:white !important;" 
								maxlength="50"/> 
							
								<span class="help-block">Required thread name</span>
						</div>
					</div>

					<script>
						function implementFineUploader(){
							var threadPreviewImageUploader = new qq.FineUploader({
								element: document.getElementById('thread-preview-image-holder'),
								template: 'qq-template-manual-trigger',
								validation: {
									allowedExtensions: ["jpeg","jpg","png"],
									itemLimit: 1,
                                    sizeLimit: 10000 * 1024 // 10000 kB = 10000 * 1024 bytes | 10mb
								},
								request: {
									endpoint: '/add-thread/add-thread-preview-image',
									method: "POST"
								},
								deleteFile: {
									enabled: true,
									forceConfirm: true,
									endpoint: '/add-thread/delete-thread-preview-image',
									method: "DELETE"
								},
								thumbnails: {
									placeholders: {
										waitingPath: '/resources/js/plugins/fine-uploader/placeholders/waiting-generic.png',
										notAvailablePath: '/resources/js/plugins/fine-uploader/placeholders/not_available-generic.png'
									}
								},
								autoUpload: false,
								debug: true,
								callbacks: {
									onComplete: function (id,name,responseJSON,maybeXhr){
										console.log(responseJSON);
										$('#lblThreadPreviewImage_' + id).val(responseJSON.threadPreviewImageLink);
									},
									onCancel: function(id,name){
										console.log("cancel");
										$('#lblThreadPreviewImage_' + id).remove();
									},
									onError: function(id,name,reason,maybeXhrOrXdr){
										//logic here
									},
									onAllComplete: function(successful,failed){
										if(failed.length <= 0){
											//logic here
										}else{
											//logic here
										}
									},
									onSubmitted: function(id,name){
										$(".qq-uploader-selector").append("<input type='hidden' id='lblThreadPreviewImage_" + id + "' name='thread_preview_image' value='' />");
									},
									onUpload (id,name){
										console.log(name + " before uploading...");
									},

									onSubmitDelete: function(id){
										this.setDeleteFileParams({filename: this.getName(id)}, id);
									},
									onDeleteComplete: function(id){
										$('#lblThreadPreviewImage_' + id).remove();
									}
								}
							});

							qq(document.querySelector("#thread-preview-image-holder #trigger-upload")).attach("click", function () {
								threadPreviewImageUploader.uploadStoredFiles();
							});
							
							function resetDropdown(dropdownID,value){
								$(dropdownID).val(value);
								$(dropdownID).selectpicker('refresh');
								$(dropdownID).selectpicker('render');
							}

							$('#add_new_thread-modal').on('hidden.bs.modal', function () {
								$('#frmAddThread')[0].reset();
								resetDropdown('#cmbNotes_type','---SELECT TYPE---');
								threadPreviewImageUploader.cancelAll();
								threadPreviewImageUploader.clearStoredFiles();
							});
						}

						function addMaxLengthTo(input){
							$(input).maxlength({
								alwaysShow: true,
								threshold: 10,
								warningClass: "label label-success",
								limitReachedClass: "label label-danger",
								separator: ' of ',
								preText: 'You have ',
								postText: ' chars remaining.',
								validate: true
							});
						}

						function implementBootstrapMaxLength(){
							addMaxLengthTo('input#frmAddThread_caption');
						}

						implementBootstrapMaxLength();
						implementFineUploader();
					</script>
				</div>

				<div class="modal-footer">
					<button 
						type="submit" 
						class="btn btn-info btn-lg btn-rounded" 
						style="padding: 10px; font-size: 15px; background-color:#1caf9a; color: white;">
						<strong>
							<span class="fa fa-hand-o-right"></span> Add Thread
						</strong>
					</button>

					<button 
						type="button" 
						class="btn btn-warning btn-rounded" 
						style="padding: 10px; font-size: 15px; background-color: orange; color: white;" 
						data-dismiss="modal">
						<strong>
							<span class="glyphicon glyphicon-remove-sign"></span> Close
						</strong>
					</button>
				</div>
			</form>
		</div>
	</div>
</div>