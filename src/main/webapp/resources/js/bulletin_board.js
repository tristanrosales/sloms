var stompClient = null;
var notification_count = 0;

function connectWebSocket(){
    var socket = new SockJS('/sloms_web_socket');
    stompClient = Stomp.over(socket);

    stompClient.connect({}, function(frame) {
		stompClient.subscribe("/sloms/errors", function(message) {
            iziToast.error({
                title: 'Oops!',
                theme: 'light', // dark
                color: 'blue', // blue, red, green, yellow
                position: 'bottomRight', // bottomRight, bottomLeft, topRight, topLeft, topCenter, bottomCenter, center
                message: 'Error: ' + message.body
            });
            connectWebSocket();
		});

		stompClient.subscribe("/sloms/bulletin_board", function(message) {
            //alert(JSON.parse(message.body));
            refreshBulletinBoard();
		});
	}, function(error) {
        iziToast.error({
            title: 'Oops!',
            theme: 'light', // dark
            color: 'blue', // blue, red, green, yellow
            position: 'bottomRight', // bottomRight, bottomLeft, topRight, topLeft, topCenter, bottomCenter, center
            message: 'STOMP error: ' + error
        });
    });
}

connectWebSocket();

String.prototype.replaceAll = function (searchText, replacementText) {
    return this.split(searchText).join(replacementText);
};

function getAllNotification(){
    $.ajax({
        url: '/notifications',
        dataType: 'json',
        mimeType: 'json',
        success: function(response) {
            $.each(response, function(i, data) {
                if(data.notif_type == "add_user"){
                    notif_users(data);
                }

                if(data.notif_type == "add_group"){
                    notif_group(data);
                }
                
                if(data.notif_type == "add_media"){
                    notif_media(data);
                }

                if(data.notif_type == "add_notes"){
                    notif_notes(data);
                }

                if(data.notif_type == "add_thread"){
                    notif_threads(data);
                }

                if(data.notif_type == "add_thread_comment"){
                    notif_thread_comments(data);
                }

                if(data.notif_type == "add_thread_comment_reply"){
                    notif_thread_comment_replies(data);
                }

                if(data.notif_type == "add_faqs"){
                    notif_faqs(data);
                }
            });
        },
        error: function(e){
            console.log(e);
        }
    });
}

getAllNotification();

function notif_users(data){
    var bulletin_board = "";
    var indefinite_article = "";
    var bulletin_board_image_source = "";

    bulletin_board += "" +
                "<a href='#' class='list-group-item'>";

                if(data.users.user_profile_picture == null){
                    bulletin_board_image_source = "resources/assets/images/users/no-image.jpg";
                }else{
                    bulletin_board_image_source = "/SLOMS_server" + data.users.user_profile_picture;
                }

    bulletin_board += "<img src='" + bulletin_board_image_source + "' class='pull-left'/>";

                if(data.users.user_role.replaceAll("_"," ") == "Super Administrator" || data.users.user_role.replaceAll("_"," ") == "Member or Editor"){
                    indefinite_article = "a";
                }else{
                    indefinite_article = "an";
                }

    bulletin_board += "<p><b>" + data.users.user_firstname + " " + data.users.user_lastname + "</b> (" + data.users.username + ") has been added to the website as " + indefinite_article + " <b>" + data.users.user_role.replaceAll("_"," ") + "</b>.</p>" + 
                "<span class='contacts-title'>" + timeago().format(data.users.user_dateAndTimeCreated) + "</span>" + 
            "</a>";

    $('#bulletin_board_row').append(bulletin_board);
}

function notif_group(data){
    var bulletin_board = "";

    var notif_style = '';

    bulletin_board += "<a href='#' class='list-group-item' onclick='seen(" + data.id + ")''>";   

    bulletin_board += notif_style;

    if(data.groups.group_created_by.user_profile_picture == null){
        bulletin_board_image_source = "resources/assets/images/users/no-image.jpg";
    }else{
        bulletin_board_image_source = "/SLOMS_server" + data.groups.group_created_by.user_profile_picture;
    }

    bulletin_board += "<img src='" + bulletin_board_image_source + "' class='pull-left'/>" +
                "<p><b>" + data.groups.group_created_by.user_firstname + " " + data.groups.group_created_by.user_lastname + "</b> (" + data.groups.group_created_by.username + ") added a new group (<b>" + data.groups.group_name + "</b>).</p>" + 
                "<span class='contacts-title'>" + timeago().format(data.groups.group_dateAndTime_created) + "</span>" +
            "</a>";

    $('#bulletin_board_row').append(bulletin_board);
}

function notif_media(data){
    var bulletin_board = "";

    bulletin_board += "" +
                "<a href='#' class='list-group-item'>";

                if(data.media.media_created_by.user_profile_picture == null){
                    bulletin_board_image_source = "resources/assets/images/users/no-image.jpg";
                }else{
                    bulletin_board_image_source = "/SLOMS_server" + data.media.media_created_by.user_profile_picture;
                }

    bulletin_board += "<img src='" + bulletin_board_image_source + "' class='pull-left'/>" +
                    "<p><b>" + data.media.media_created_by.user_firstname + " " + data.media.media_created_by.user_lastname + "</b> (" + data.media.media_created_by.username + ") added a new media or billboard named <b>" + data.media.media_name + " (" + data.media.media_code + ")</b>.</p>" + 
                    "<span class='contacts-title'>" + timeago().format(data.media.media_dateAndTime_created) + "</span>" + 
                "</a>";

    $('#bulletin_board_row').append(bulletin_board);
}

function notif_notes(data){
    var bulletin_board = "";

    bulletin_board += "" +
                "<a href='#' class='list-group-item'>";

                if(data.notes.notes_created_by.user_profile_picture == null){
                    bulletin_board_image_source = "resources/assets/images/users/no-image.jpg";
                }else{
                    bulletin_board_image_source = "/SLOMS_server" + data.notes.notes_created_by.user_profile_picture;
                }

    bulletin_board += "<img src='" + bulletin_board_image_source  + "' class='pull-left'/>" +
                    "<p class='media-heading'><b>" + data.notes.notes_created_by.user_firstname + " " + data.notes.notes_created_by.user_lastname + "</b> (" + data.notes.notes_created_by.username + ") added a new note (<b>" + data.notes.notes_type + "</b>) on a media (<b>" + data.notes.notes_billboard.media_name + "</b>).</p>" + 
                    "<span class='contacts-title'>" + timeago().format(data.notes.notes_dateAndTimeCreated) + "</span>" + 
                "</a>";

    $('#bulletin_board_row').append(bulletin_board);
}

function notif_threads(data){
    var bulletin_board = "";

    bulletin_board += "" +
                "<a href='#' class='list-group-item'>";

                if(data.threads.thread_created_by.user_profile_picture == null){
                    bulletin_board_image_source = "resources/assets/images/users/no-image.jpg";
                }else{
                    bulletin_board_image_source = "/SLOMS_server" + data.threads.thread_created_by.user_profile_picture;
                }

    bulletin_board += "<img src='" + bulletin_board_image_source + "' class='pull-left'/>" +
                    "<p><b>" + data.threads.thread_created_by.user_firstname + " " + data.threads.thread_created_by.user_lastname + "</b> (" + data.threads.thread_created_by.username + ") added a new thread (<b>" + data.threads.thread_caption + "</b>) to a billboard or media (<b>" + data.threads.thread_billboard.media_name + "</b>).</p>" + 
                    "<span class='contacts-title'>" + timeago().format(data.threads.thread_dateAndTimeCreated) + "</span>" + 
                "</a>";

    $('#bulletin_board_row').append(bulletin_board);
}

function notif_thread_comments(data){
    var bulletin_board = "";
    var thread_comment_by = "";
    

    if($('#lblUser_id').text() == data.threadComments.thread_comment_by.user_id){
        thread_comment_by = "<b>You</b>";
    }else{
        thread_comment_by = "<b>" + data.threadComments.thread_comment_by.user_firstname + " " + data.threadComments.thread_comment_by.user_lastname + "</b> (" + data.threadComments.thread_comment_by.username + ")";
    }

    bulletin_board += "" +
            "<a href='#' class='list-group-item'>";

            if(data.threadComments.thread_comment_by.user_profile_picture == null){
                bulletin_board_image_source = "resources/assets/images/users/no-image.jpg";
            }else{
                bulletin_board_image_source = "/SLOMS_server" + data.threadComments.thread_comment_by.user_profile_picture;
            }

    bulletin_board += "<img src='" + bulletin_board_image_source + "' class='pull-left'/>" +
                "<p>" + thread_comment_by + " commented on a media (<b>" + data.threadComments.thread_id.thread_billboard.media_name + "</b>) with the thread (<b>" + data.threadComments.thread_id.thread_caption + "</b>).</p>" + 
                "<span class='contacts-title'>" + timeago().format(data.threadComments.thread_dateAndTime_posted) + "</span>" + 
            "</a>";

    $('#bulletin_board_row').append(bulletin_board);
}

function notif_thread_comment_replies(data){
    var bulletin_board = "";
    var thread_reply_by = "";

    if($('#lblUser_id').text() == data.threadCommentReplies.thread_reply_by.user_id){
        thread_reply_by = "<b>You</b>";
    }else{
        thread_reply_by = "<b>" + data.threadCommentReplies.thread_reply_by.user_firstname + " " + data.threadCommentReplies.thread_reply_by.user_lastname + "</b> (" + data.threadCommentReplies.thread_reply_by.username + ")";
    }
    
    //thread comment replies
    bulletin_board += "" +
        "<a href='#' class='list-group-item'>";

            if(data.threadCommentReplies.thread_reply_by.user_profile_picture == null){
                bulletin_board_image_source = "resources/assets/images/users/no-image.jpg";
            }else{
                bulletin_board_image_source = "/SLOMS_server" + data.threadCommentReplies.thread_reply_by.user_profile_picture;
            }

     bulletin_board += "<img src='" + bulletin_board_image_source + "' class='pull-left'/>" +
            "<p>" + thread_reply_by + " replied on the comment of <b>" + data.threadCommentReplies.thread_comment_id.thread_comment_by.user_firstname + " " + data.threadCommentReplies.thread_comment_id.thread_comment_by.user_lastname + "</b> on a media (<b>" + data.threadCommentReplies.thread_comment_id.thread_id.thread_billboard.media_name + "</b>) with the thread (<b>" + data.threadCommentReplies.thread_comment_id.thread_id.thread_caption + "</b>).</p>" + 
            "<span class='contacts-title'>" + timeago().format(data.threadCommentReplies.thread_dateAndTime_replied) + "</span>" + 
        "</a>";

    $('#bulletin_board_row').append(bulletin_board);
}

function notif_faqs(data){
    var bulletin_board = "";

    bulletin_board += "" +
                "<a href='#' class='list-group-item'>";

                if(data.faQs.faqs_created_by.user_profile_picture == null){
                    bulletin_board_image_source = "resources/assets/images/users/no-image.jpg";
                }else{
                    bulletin_board_image_source = "/SLOMS_server" + data.faQs.faqs_created_by.user_profile_picture;
                }

    bulletin_board += "<img src='" + bulletin_board_image_source + "' class='pull-left'/>" +
                    "<p>" + data.faQs.faqs_created_by.user_firstname + " " + data.faQs.faqs_created_by.user_lastname + " <b>(" + data.faQs.faqs_created_by.username + ")</b> added a new FAQs (<b>" + data.faQs.faqs_question + "</b>).</p>" + 
                    "<span class='contacts-title'>" + timeago().format(data.faQs.faqs_dateAndTimeCreated) + "</span>" + 
                "</a>";

    $('#bulletin_board_row').append(bulletin_board);
}

function seen(notif_id){
    var values = {
		'notif_id': notif_id
	};

	$.ajax({
        url: "/seen-notif",
        type: "POST",
        dataType : 'JSON',
        data: values,
        success: function(data) {
            if(data.isSeenNotificationSuccess){
                iziToast.success({
                    title: 'Congratulations!',
                    theme: 'light', // dark
                    color: 'green', // blue, red, green, yellow
                    position: 'bottomRight', // bottomRight, bottomLeft, topRight, topLeft, topCenter, bottomCenter, center
                    message: "Group has been seen successfully!"
                });

                $('#bulletin_board_row').html('');
                getAllNotification();
            }
        },
        error: function(e){
            console.log(e);
        }
    });
}

function refreshBulletinBoard(){
    $('#bulletin_board_row').html('');
    getAllNotification();

    notification_count++;

    $('#lblNotificationCount').text(notification_count);
}