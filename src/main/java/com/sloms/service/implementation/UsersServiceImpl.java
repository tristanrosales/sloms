package com.sloms.service.implementation;

import java.util.Date;
import java.util.List;

import com.sloms.classes.RoleConstants;
import com.sloms.classes.UsersDao;
import com.sloms.service.UsersService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.google.common.base.CharMatcher;
import com.sloms.entity.Notifications;
import com.sloms.entity.Users;
import com.sloms.repository.NotificationsRepository;
import com.sloms.repository.UsersRepository;
import org.springframework.ui.Model;

@Service
@Transactional(readOnly=true)
public class UsersServiceImpl implements UsersService {
	
	@Autowired private UsersRepository usersRepository;
	@Autowired private NotificationsRepository notificationsRepository;
	@Autowired private BCryptPasswordEncoder bCryptPasswordEncoder;

	@Override
	@Transactional
	public void saveUser(Users usersDAO, Model model) {
		try {
			Users users = new Users();
			Notifications notifications = new Notifications();
			final Date dateAndTimeToday = new Date();

			//users
			users.setUsername(usersDAO.getUsername().trim());
			users.setUser_password(bCryptPasswordEncoder.encode(usersDAO.getUser_password().trim()));
			users.setUser_firstname(usersDAO.getUser_firstname().trim());
			users.setUser_lastname(usersDAO.getUser_lastname().trim());
			users.setUser_email_address(usersDAO.getUser_email_address().trim());
			//users.setUser_phone_number(CharMatcher.anyOf("()- ").removeFrom(usersDAO.getUser_phone_number().trim()));
			users.setUser_gender(usersDAO.getUser_gender());
			users.setUser_role(usersDAO.getUser_role().trim());

			if(RoleConstants.GUEST.equals(usersDAO.getUser_role().trim())){
				users.setUser_status("Authenticated");
			}else{
				users.setUser_status("Unauthenticated");
			}

			users.setUser_sms_notif("Disabled");
			users.setUser_email_notif("Disabled");
			users.setUser_dateAndTimeCreated(dateAndTimeToday);
			
			//notifications
			notifications.setUsers(users);
			notifications.setNotif_type("add_user");

			model.addAttribute("user_password", usersDAO.getUser_password().trim());
			
			usersRepository.save(users);
			notificationsRepository.save(notifications);
		} catch (Exception e) {
			System.out.println("saveUser Exception : " + e.getMessage());
		}
	}

	@Override
	public Users findUserById(Integer user_id) {
		return usersRepository.findUserByUserId(user_id);
	}

	@Override
	public Users findUserByEmail(String email) {
		return usersRepository.findUserByEmail(email);
	}

	@Override
	public Users findUserByUsername(String username) {
		return usersRepository.getAllUserInfoByUsername(username);
	}

	@Override
	public List<UsersDao> getAllUsers() {
		return usersRepository.getAllUsers();
	}

	@Override
	@Transactional
	public String approveOrDecline(Integer user_id, String user_status) {
		String status;
		final Date dateAndTimeToday = new Date();

		try{
			usersRepository.approveOrDecline(user_id,user_status,dateAndTimeToday);
			status = "GOOD";
		}catch(Exception ex){
			System.out.println("approveOrDecline exception : " + ex.toString());
			status = "BAD";
		}

		return status;
	}

	@Override
	@Transactional
	public void updateUsername(String username, Integer user_id) {
		usersRepository.updateUsername(username.trim(),user_id);
	}

	@Override
	@Transactional
	public void updateUserPassword(String user_password, String username) {
		usersRepository.updateUserPassword(user_password,username);
	}

	@Override
	@Transactional
	public void updateProfileInformation(String user_firstname, String user_lastname, String user_email_address, String user_phone_number, String user_gender, String user_sms_notif, String user_email_notif, Integer user_id) {
		usersRepository.updateProfileInformation(
				user_firstname.trim(),
				user_lastname.trim(),
				user_email_address.trim(), CharMatcher.anyOf("()- ").removeFrom(user_phone_number.trim()),
				user_gender.trim(),
				user_sms_notif.trim(),
				user_email_notif.trim(),
				user_id);
	}
}
