<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<div class="modal" id="edit_profile_information-modal" tabindex="-1" role="dialog" aria-labelledby="largeModalHead" aria-hidden="true" data-backdrop="static" data-keyboard="false">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<form id="frmUpdateProfileInformation" class="form-horizontal" role="form" method="POST" action="/profile/update-profile-information">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">
						<span aria-hidden="true">&times;</span><span class="sr-only">Close</span>
					</button>
					<h3 class="modal-title" id="largeModalHead">Edit Profile</h3>
				</div>

				<div class="modal-body" style="max-height: calc(100vh - 200px); overflow-y: auto;">					
					<div class="form-group">
						<label class="col-md-2 control-label">First Name</label>
						<div class="col-md-10">
							<input 
								type="text"
								id="frmUpdateProfileInformation_txtUser_firstname"
								class="form-control"
								placeholder="Enter first name..." 
								required="required"
								name="user_firstname"
								style="text-transform: capitalize; background-color:white !important;" 
								maxlength="20"
								value="${user.user_firstname}"/>
								
							<span class="help-block">Required first name</span>
						</div>
					</div>

					<div class="form-group">
						<label class="col-md-2 control-label">Last Name</label>
						<div class="col-md-10">
							<input 
								type="text"
								id="frmUpdateProfileInformation_txtUser_lastname"
								class="form-control"
								placeholder="Enter last name..." 
								required="required"
								name="user_lastname"
								style="text-transform: capitalize; background-color:white !important;" 
								maxlength="20"
								value="${user.user_lastname}"/>
								
							<span class="help-block">Required last name</span>
						</div>
					</div>

					<div class="form-group">
						<label class="col-md-2 control-label">E-mail:</label>
						<div class="col-md-10">
							<input 
								type="text" 
								name="user_email_address"
								id="frmUpdateProfileInformation_txtUser_emailAddress"
								class="form-control"
								placeholder="Enter email..." 
								style="background-color:white !important;" 
								maxlength="40"
								value="${user.user_email_address}" />
								
							<span class="help-block">Required email</span>
						</div>
					</div>
					
					<div class="form-group">
						<label class="col-md-2 control-label">Phone:</label>    
						<div class="col-md-10">
							<input 
								type="text"
								name="user_phone_number" 
								class="mask_phone form-control" 
								style="background-color:white !important;" value="${user.user_phone_number}"/>

							<span class="help-block">Required phone number. Example: 98 (765) 432-10-98</span>
						</div>
					</div>
					
					<div class="form-group">
						<label class="col-md-2 control-label">Gender</label>
						<div class="col-md-10">
							<select 
								class="form-control select" 
								data-live-search="true" 
								required="required" 
								name="user_gender">

								<c:choose>
									<c:when test="${user.user_gender == 'Male'}">
										<option value="Male" selected>Male</option>
										<option value="Female">Female</option>
									</c:when>
									<c:otherwise>
										<option value="Male">Male</option>
										<option value="Female" selected>Female</option>
									</c:otherwise>
								</c:choose>

							</select> 
							
							<span class="help-block">Required gender</span>
						</div>
					</div>
					
					<div id="div_sms_notif" class="form-group">
						<label class="col-md-2 control-label">Short Message Service (SMS) Notification</label>
						<div class="col-md-10">
							<select 
								class="form-control select" 
								data-live-search="true"
								required="required" 
								name="user_sms_notif">

								<c:choose>
									<c:when test="${user.user_sms_notif == 'Enabled'}">
										<option value="Enabled" selected>Enabled</option>
										<option value="Disabled">Disabled</option>
									</c:when>
									<c:otherwise>
										<option value="Enabled">Enabled</option>
										<option value="Disabled" selected>Disabled</option>
									</c:otherwise>
								</c:choose>

							</select> 
							
							<span class="help-block">Required SMS notification status</span>
						</div>
					</div>
					
					<div id="div_email_notif" class="form-group">
						<label class="col-md-2 control-label">Email Notification</label>
						<div class="col-md-10">
							<select 
								class="form-control select" 
								data-live-search="true"
								required="required" 
								name="user_email_notif">

								<c:choose>
									<c:when test="${user.user_email_notif == 'Enabled'}">
										<option value="Enabled" selected>Enabled</option>
										<option value="Disabled">Disabled</option>
									</c:when>
									<c:otherwise>
										<option value="Enabled">Enabled</option>
										<option value="Disabled" selected>Disabled</option>
									</c:otherwise>
								</c:choose>

							</select> 
							
							<span class="help-block">Required email notification status</span>
						</div>
					</div>
					
					<script>
						function addMaxLengthTo(input){
							$(input).maxlength({
								alwaysShow: true,
								threshold: 10,
								warningClass: "label label-success",
								limitReachedClass: "label label-danger",
								separator: ' of ',
								preText: 'You have ',
								postText: ' chars remaining.',
								validate: true
							});
						}

						function implementBootstrapMaxLength(){
							addMaxLengthTo('input#frmUpdateProfileInformation_txtUser_firstname');
							addMaxLengthTo('input#frmUpdateProfileInformation_txtUser_lastname');
							addMaxLengthTo('input#frmUpdateProfileInformation_txtUser_emailAddress');
						}

						implementBootstrapMaxLength();
					</script>				
				</div>

				<div class="modal-footer">
					<button
						type="submit" 
						class="btn btn-info btn-lg btn-rounded" 
						style="padding: 10px; font-size: 15px; background-color:#1caf9a; color: white;">
						<strong>
							<span class="glyphicon glyphicon-hand-right"></span> Update Information
						</strong>
					</button>

					<button 
						type="button" 
						class="btn btn-warning btn-rounded" 
						style="padding: 10px; font-size: 15px; background-color: orange; color: white;" 
						data-dismiss="modal">
						<strong>
							<span class="glyphicon glyphicon-remove-sign"></span> Close
						</strong>
					</button>
				</div>
			</form>
		</div>
	</div>
</div>