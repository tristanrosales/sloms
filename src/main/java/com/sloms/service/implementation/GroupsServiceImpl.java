package com.sloms.service.implementation;

import java.util.Date;
import java.util.List;

import com.sloms.entity.Users;
import com.sloms.service.GroupsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.sloms.entity.Groups;
import com.sloms.entity.Notifications;
import com.sloms.repository.GroupsRepository;
import com.sloms.repository.NotificationsRepository;

@Service
@Transactional(readOnly=true)
public class GroupsServiceImpl implements GroupsService {
	@Autowired private GroupsRepository groupsRepository;
	@Autowired private NotificationsRepository notificationsRepository;
	
	@Override
	@Transactional
	public void saveGroup(Groups groupsDAO, Notifications notificationsDAO) {
		Groups groups = new Groups();
		Notifications notifications = new Notifications();
		
		//groups
		groups.setGroup_name(groupsDAO.getGroup_name().trim());
		groups.setGroup_description(groupsDAO.getGroup_description().trim());
		groups.setGroup_preview_image(groupsDAO.getGroup_preview_image());
		groups.setGroup_dateAndTime_created(groupsDAO.getGroup_dateAndTime_created());
		groups.setGroup_created_by(groupsDAO.getGroup_created_by());
		groups.setGroup_modified(groupsDAO.getGroup_modified());
		groups.setGroup_status(groupsDAO.getGroup_status().trim());
		
		//notification
		notifications.setGroups(groups);
		notifications.setNotif_type("add_group");
		
		groupsRepository.save(groups);
		notificationsRepository.save(notifications);
	}
	
	@Override
	@Transactional
	public void updateGroup(Groups groupsDAO, Notifications notificationsDAO) {
		Notifications notifications = new Notifications();
		final Date dateAndTimeModified = new Date();
		
		//update group
		groupsRepository.updateGroupInformation(
				groupsDAO.getGroup_name().trim(), 
				groupsDAO.getGroup_description().trim(), 
				groupsDAO.getGroup_status().trim(),
				dateAndTimeModified,
				groupsDAO.getGroup_id());

		//notification
		notifications.setGroups(groupsDAO);
		notifications.setNotif_type("edit_group");
		
		notificationsRepository.save(notifications);
	}

	@Override
	public void updateGroupPreviewImage(String group_preview_image, Integer group_id) {
		groupsRepository.updateGroupPreviewImage(group_preview_image, group_id);
	}

	@Override
	public void deleteGroup(Groups selectedGroup) {
		groupsRepository.delete(selectedGroup);
	}

	@Override
	public List<Groups> getAllGroups() {
		return groupsRepository.getAllGroups();
	}
	
	@Override
	public List<Groups> getAllGroupsThatIsActivated() {
		return groupsRepository.getAllGroupsThatIsActivated();
	}

	@Override
	public List<Groups> getAllGroupsOfLoggedInUser(Users loggedInUser) {
		return groupsRepository.getAllGroupsOfLoggedInUser(loggedInUser);
	}

	@Override
	public Groups getGroupById(Integer groupID) {
		return groupsRepository.findOne(groupID);
	}

	@Override
	public Groups getGroupByGroupName(String groupName) {
		return groupsRepository.getGroupByGroupName(groupName);
	}
}
