package com.sloms.repository;

import java.util.Date;
import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.sloms.entity.Notes;
import com.sloms.entity.Threads;
import com.sloms.entity.Users;

@Repository
public interface ThreadsRepository extends JpaRepository<Threads,Integer>{
	@Query("SELECT t FROM Threads t ORDER BY t.thread_id DESC")
	List<Threads> getAllThreads();
	
	@Query("SELECT t FROM Threads t WHERE t.thread_created_by=:userID ORDER BY t.thread_id DESC")
	List<Threads> getAllThreadsOfLoggedInUser(@Param("userID") Users userID);
	
	@Query("SELECT t FROM Threads t WHERE t.thread_caption=:thread_caption ORDER BY t.thread_id DESC")
	Threads findThreadByThreadCaption(@Param("thread_caption") String thread_caption);
	
	@Query("SELECT t FROM Threads t WHERE t.thread_id=:thread_id ORDER BY t.thread_id DESC")
	Threads findThreadByThreadId(@Param("thread_id") Integer thread_id);
	
	@Transactional
	@Modifying
	@Query("UPDATE Threads t SET "
			+ "t.thread_notes_id=:thread_notes_id,"
			+ "t.thread_caption=:thread_caption,"
			+ "t.thread_status=:thread_status,"
			+ "t.thread_modified=:thread_modified "
			+ "WHERE t.thread_id=:thread_id")
	void updateThread(
			@Param("thread_notes_id") Notes thread_notes_id,
			@Param("thread_caption") String thread_caption,
			@Param("thread_status") String thread_status,
			@Param("thread_modified") Date thread_modified,
			@Param("thread_id") Integer thread_id);
	
	@Transactional
	@Modifying
	@Query("UPDATE Threads t SET "
			+ "t.thread_status=:thread_status,"
			+ "t.thread_modified=:thread_modified "
			+ "WHERE t.thread_id=:thread_id")
	void openOrCloseSelectedThread(
			@Param("thread_status") String thread_status,
			@Param("thread_modified") Date thread_modified,
			@Param("thread_id") Integer thread_id);
	
	@Transactional
	@Modifying
	@Query("UPDATE Threads t SET "
			+ "t.thread_preview_image=:thread_preview_image "
			+ "WHERE t.thread_id=:thread_id")
	void updateThreadPreviewImage(
			@Param("thread_preview_image") String thread_preview_image,
			@Param("thread_id") Integer thread_id);
	
}
