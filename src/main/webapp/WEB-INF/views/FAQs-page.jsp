<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!-- PAGE CONTENT -->
<div class="page-content">

	<tiles:insertAttribute name="x_navigationVertical" />

	<!-- START BREADCRUMB -->
	<ul class="breadcrumb">
		<li><a href="${pageContext.request.contextPath}/home">Home</a></li>
		<li class="active">FAQs</li>
	</ul>
	<!-- END BREADCRUMB -->

	<!-- PAGE TITLE -->
	<div class="page-title">
		<h2>
			<span class="fa fa-question-circle"></span> Frequently Asked Questions
		</h2>
	</div>
	<!-- END PAGE TITLE -->

	<!-- PAGE CONTENT WRAPPER -->
	<div class="page-content-wrap">
		<div class="row">
			<div class="col-md-12">
				<div class="panel panel-default">
					<div class="panel-heading">
						<c:choose>
							<c:when test="${user.user_role == 'IT_ADMIN'}">
								<button
										type="button"
										class="btn btn-info btn-lg btn-rounded"
										style="padding: 10px; font-size: 15px; background-color:#1caf9a; color: white;"
										data-toggle="modal"
										data-target="#add_faqs-modal">
									<span class="fa fa-plus-circle"></span> <strong>Add New FAQs</strong>
								</button>
							</c:when>
						</c:choose>

						<ul class="panel-controls">
							<li>
								<a href="#" class="panel-collapse">
									<span class="fa fa-angle-down"></span>
								</a>
							</li>

							<li>
								<a href="#" class="panel-refresh">
									<span class="fa fa-refresh"></span>
								</a>
							</li>
						</ul>
					</div>

					<div class="panel-body" style="font-size: 15px;">
						<!-- <h3 class="push-down-0">General Questions</h3> -->
						<table id="tblFAQs" class="display" style="width:100%">
							<thead style="background-color: #1caf9a; color: white;">
								<tr>
									<th style="text-align: center;">Details</th>
									<th style="text-align: center;">Question</th>
								</tr>
							</thead>

							<tfoot style="background-color: #1caf9a; color: white;">
								<tr>
									<th style="text-align: center;">Details</th>
									<th style="text-align: center;">Question</th>
								</tr>
							</tfoot>
						</table>
					</div>
				</div>

			</div>
		</div>

	</div>
	<!-- END PAGE CONTENT WRAPPER -->

</div>
<!-- END PAGE CONTENT -->

<tiles:insertAttribute name="add_faqs_modal" />
<tiles:insertAttribute name="view_edit_faqs_modal" />

<script type="text/javascript" src="${pageContext.request.contextPath}/resources/js/FAQs.js"></script>