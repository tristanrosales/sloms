package com.sloms.controller;

import java.io.IOException;
import java.security.Principal;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.sloms.classes.RoleConstants;
import com.sloms.service.UsersService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import com.google.gson.JsonObject;
import com.sloms.classes.DeleteImage;
import com.sloms.classes.UploadImage;
import com.sloms.entity.Groups;
import com.sloms.entity.Notifications;
import com.sloms.entity.Users;
import com.sloms.service.GroupsService;

@RestController
public class GroupController {
	@Autowired private GroupsService groupsService;
	@Autowired private UsersService usersService;
	@Autowired private SimpMessagingTemplate template;

	private JsonObject status = new JsonObject();
	
	private void sendDataToClient() {
		template.convertAndSend("/sloms/bulletin_board", "bulletin_board");
		template.convertAndSend("/sloms/all_groups", "all_groups");
		template.convertAndSend("/sloms/all_groups_of_logged_in_user", "all_groups_of_logged_in_user");
		template.convertAndSend("/sloms/all_activated_groups", "all_activated_groups");
	}

	@PostMapping("/add-group")
	@ResponseBody
	public String addGroup(
			@ModelAttribute("groups") Groups groups,
			@RequestParam(value="group_preview_image",required=false) String group_preview_image,
			@ModelAttribute("notifications") Notifications notifications,
			Principal principal) {

		Users loggedInUser = usersService.findUserByUsername(principal.getName());
		final Date dateAndTimeToday = new Date();

		if(RoleConstants.SYS_SPECIALIST.equals(loggedInUser.getUser_role())){
			status.addProperty("status", "accessDenied");
		}else{
			Groups groupNameExists = groupsService.getGroupByGroupName(groups.getGroup_name());

			if(groupNameExists != null){
				status.addProperty("status", "groupNameExists");
			}else{
				if(group_preview_image != null) {
					groups.setGroup_preview_image(group_preview_image.replaceAll(",", ""));
				}

				groups.setGroup_created_by(loggedInUser);
				groups.setGroup_dateAndTime_created(dateAndTimeToday);
				groups.setGroup_status("Activated");

				groupsService.saveGroup(groups, notifications);
				status.addProperty("status", true);

				sendDataToClient();
			}
		}

		return status.toString();
	}

	@PostMapping("/add-group/add-group-preview-image")
	@ResponseBody
	public void addGroupPreviewImage(
			@RequestParam("qqfile") MultipartFile groupPreviewImage,
			@RequestParam("qquuid") String uuid,
			@RequestParam("qqfilename") String fileName,
			HttpServletResponse response) throws IOException {
		
		String uploadedFilename = UploadImage.uploadImage(uuid, groupPreviewImage, "src\\main\\webapp\\SLOMS_server\\group", "/group/");
		
		System.out.println("Uploading " + groupPreviewImage.getOriginalFilename());
		System.out.println("UUID=" + uuid);
		
		JsonObject jsonObject = new JsonObject();
		
		jsonObject.addProperty("success", true);
		jsonObject.addProperty("groupPreviewImageLink", uploadedFilename);
		
		response.setCharacterEncoding("UTF-8");
		response.setContentType("text/plain");
		response.getWriter().print(jsonObject);
		response.flushBuffer();
	}

	@DeleteMapping("/add-group/delete-group-preview-image/{qquuid}")
	public void deleteGroupPreviewImage(
			HttpServletResponse response,
			HttpServletRequest request,
			@PathVariable("qquuid") String qquuid,
			@RequestParam("filename") String filename) throws IOException {
			
		System.out.println("uploadDelete() called " + filename);
		
		DeleteImage.deleteImage(qquuid, "src\\main\\webapp\\SLOMS_server\\group\\");
		
		response.setStatus(200);
		response.flushBuffer();
	}

	@PostMapping("/update-group")
	@ResponseBody
	public String updateGroup(
			@ModelAttribute("groups") Groups groups, 
			@ModelAttribute("notifications") Notifications notifications) {
		
		groupsService.updateGroup(groups, notifications);
		status.addProperty("isUpdateGroupSuccess", true);
		
		sendDataToClient();
		
		return status.toString();
	}

	@PostMapping("/update-group-preview-image")
	@ResponseBody
	public String updateGroupPreviewImage(
			@RequestParam Integer group_id,
			@RequestParam(value="group_preview_image",required=false) String group_preview_image){
		
		Groups groups = groupsService.getGroupById(group_id);
		
		if(groups.getGroup_preview_image() != null || groups.getGroup_preview_image() != "") {
			DeleteImage.deleteImageInFolder("src\\main\\webapp\\SLOMS_server\\" + groups.getGroup_preview_image());
		}

		groupsService.updateGroupPreviewImage(group_preview_image, group_id);
		status.addProperty("isUpdatingGroupPreviewImage", true);
		
		return status.toString();
	}

	@DeleteMapping("/delete-group")
	@ResponseBody
	public String deleteGroup(@ModelAttribute("groups") Groups groups) {
		Groups groupIDExists = groupsService.getGroupById(groups.getGroup_id());

		if(groupIDExists != null) {
			groupsService.deleteGroup(groupIDExists);
			status.addProperty("status", true);
		}
		
		return status.toString();
	}

	@GetMapping("/groups")
	public List<Groups> getAllGroups(){
		return  groupsService.getAllGroups();
	}

	@GetMapping("/available_groups")
	public List<Groups> getAllGroupsThatIsActivated(){
		return groupsService.getAllGroupsThatIsActivated();
	}

	@GetMapping("/groups/{groupID}")
	public Groups getGroupById(@PathVariable("groupID") Integer groupID) {
		return groupsService.getGroupById(groupID);
	}

	@GetMapping("/my_group")
	public List<Groups> getAllGroupsOfLoggedInUser(Principal principal){
		Users loggedInUser = usersService.findUserByUsername(principal.getName());
		return groupsService.getAllGroupsOfLoggedInUser(loggedInUser);
	}
}
