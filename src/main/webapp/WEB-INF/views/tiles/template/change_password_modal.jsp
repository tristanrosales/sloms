<div class="modal" id="change_password-modal" tabindex="-1" role="dialog" aria-labelledby="largeModalHead" aria-hidden="true" data-backdrop="static" data-keyboard="false">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<form id="frmChangePassword" class="form-horizontal" role="form" method="POST" action="/profile/update-password">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">
						<span aria-hidden="true">&times;</span><span class="sr-only">Close</span>
					</button>
					<h3 class="modal-title" id="largeModalHead">Change Password</h3>
				</div>

				<div class="modal-body" style="max-height: calc(100vh - 200px); overflow-y: auto;">	
					<div class="form-group">
						<label class="col-md-3 control-label">New Password:</label>
						<div class="col-md-9">
							<input 
								type="password" 
								class="form-control" 
								name="user_password" 
								id="txtPassword" 
								maxlength="16" 
								placeholder="Enter new password..." 
								style="background-color:white !important;"/>                 
							<span class="help-block">min size = 8, max size = 16</span>
						</div>
					</div>
					
					<div class="form-group">
						<label class="col-md-3 control-label">Confirm Password:</label>
						<div class="col-md-9">
							<input type="password" class="form-control" name="re-password" maxlength="16" placeholder="Re-enter new password..." style="background-color:white !important;"/>
							<span class="help-block">required same value as password</span>
						</div>
					</div>
				</div>

				<div class="modal-footer">
					<button
						type="submit" 
						class="btn btn-info btn-lg btn-rounded" 
						style="padding: 10px; font-size: 15px; background-color:#1caf9a; color: white;">
						<strong>
							<span class="glyphicon glyphicon-hand-right"></span> Update Password
						</strong>
					</button>

					<button 
						type="button" 
						class="btn btn-warning btn-rounded" 
						style="padding: 10px; font-size: 15px; background-color: orange; color: white;" 
						data-dismiss="modal">
						<strong>
							<span class="glyphicon glyphicon-remove-sign"></span> Close
						</strong>
					</button>
				</div>
			</form>
		</div>
	</div>
</div>