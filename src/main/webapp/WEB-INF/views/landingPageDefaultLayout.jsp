<%@ page contentType="text/html; charset=utf-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<!DOCTYPE html>
<html>
<head>
    <title><tiles:getAsString name="title" /></title>
    <tiles:insertAttribute name="css" />
</head>
<body>
    <div class="limiter">
        <div class="container-login100" style="background-image: url(/resources/img/backgrounds/wall_1.jpg); background-size: 100%;">
            <div class="wrap-login100">
                <div class="login100-form-title" style="background-image: url(/resources/Login_v15/images/edsa_forbes_3.jpg);">
                    <h3 style="text-align: center;">
                            <span style="color: white; font-weight: bold; letter-spacing: 3px; text-shadow: -2px -2px 0 #000,2px -2px 0 #000,-2px 2px 0 #000,2px 2px 0 #000;">
                                <strong style="color: #1caf9a; font-size: 50px; text-shadow: 0 0 10px #fff, 0 0 20px #fff, 0 0 30px #fff, 0 0 40px #ff00de, 0 0 70px #ff00de, 0 0 80px #ff00de, 0 0 100px #ff00de, 0 0 150px #ff00de;">S</strong>ign
                                <strong style="color: #1caf9a; font-size: 50px; text-shadow: 0 0 10px #fff, 0 0 20px #fff, 0 0 30px #fff, 0 0 40px #ff00de, 0 0 70px #ff00de, 0 0 80px #ff00de, 0 0 100px #ff00de, 0 0 150px #ff00de;">L</strong>anguage
                                <strong style="color: #1caf9a; font-size: 50px; text-shadow: 0 0 10px #fff, 0 0 20px #fff, 0 0 30px #fff, 0 0 40px #ff00de, 0 0 70px #ff00de, 0 0 80px #ff00de, 0 0 100px #ff00de, 0 0 150px #ff00de;">O</strong>utdoor
                                <strong style="color: #1caf9a; font-size: 50px; text-shadow: 0 0 10px #fff, 0 0 20px #fff, 0 0 30px #fff, 0 0 40px #ff00de, 0 0 70px #ff00de, 0 0 80px #ff00de, 0 0 100px #ff00de, 0 0 150px #ff00de;">M</strong>edia
                                <strong style="color: #1caf9a; font-size: 50px; text-shadow: 0 0 10px #fff, 0 0 20px #fff, 0 0 30px #fff, 0 0 40px #ff00de, 0 0 70px #ff00de, 0 0 80px #ff00de, 0 0 100px #ff00de, 0 0 150px #ff00de;">S</strong>ystem</span> <br>

                        <strong style="color: #1caf9a; font-size: 50px; text-shadow: 0 0 10px #fff, 0 0 20px #fff, 0 0 30px #fff, 0 0 40px #ff00de, 0 0 70px #ff00de, 0 0 80px #ff00de, 0 0 100px #ff00de, 0 0 150px #ff00de;"> Inc.</strong></span>
                    </h3>
                </div>

                <br>

                <tiles:insertAttribute name="body" />

            </div>
        </div>

        <!--modal -->
        <div class="modal" id="forgot-password_check-username" tabindex="-1" role="dialog" aria-labelledby="largeModalHead" aria-hidden="true" data-backdrop="static" data-keyboard="false">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <form id="frmForgotPassword_checkUsername" class="form-horizontal" role="form" action="/forgot-password-check-username" method="POST">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                            <h4 class="modal-title" id="defModalHead" style="text-align: left;">Forgot Password?</h4>
                        </div>

                        <div class="modal-body" style="max-height: calc(100vh - 200px); overflow-y: auto;">
                            <div class="form-group">
                                <label class="col-md-2 control-label">Username</label>
                                <div class="col-md-10">
                                    <input
                                            type="text"
                                            class="form-control"
                                            placeholder="Enter your username..."
                                            required="required"
                                            id="forgotPassword_username"
                                            name="username"
                                            autocomplete="off"
                                            maxlength="30"/>

                                    <span class="help-block">Required username</span>
                                </div>
                            </div>
                        </div>

                        <div class="modal-footer">
                            <button
                                    type="button"
                                    class="btn btn-success btn-lg"
                                    onclick="forgotPassword_getSecurityCode()"
                                    style="white-space: normal !important; word-wrap: break-word;">

                                <span class="glyphicon glyphicon-send"></span>
                                Get Security Code

                            </button>

                            <button
                                    type="button"
                                    class="btn btn-danger btn-lg"
                                    data-dismiss="modal"
                                    style="color: white; white-space: normal !important; word-wrap: break-word;">

                                <span class="glyphicon glyphicon-remove"></span>
                                Close

                            </button>
                        </div>
                    </form>
                </div>
            </div>
        </div>

        <div class="modal" id="enter_your_security-code-modal" tabindex="-1" role="dialog" aria-labelledby="largeModalHead" aria-hidden="true" data-backdrop="static" data-keyboard="false">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <form id="frmForgotPassword_enterSecurityCode" action="/forgot-password-enter-security-code" method="POST" class="form-horizontal" role="form">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal">
                                <span aria-hidden="true">&times;</span><span class="sr-only">Close</span>
                            </button>
                            <h3 class="modal-title" id="largeModalHead">Hi, <span id="lblEnterYourSecurityCode_username"></span>!</h3>
                        </div>

                        <div class="modal-body" style="max-height: calc(100vh - 200px); overflow-y: auto;">
                            <div class="alert alert-info" role="alert" style="font-size: 15px;">
                                <button type="button" class="close" data-dismiss="alert"></button>
                                <strong>Reminder!</strong><br>
                                Please check your email address to see the security or verification code!
                            </div>

                            <div class="form-group">
                                <label class="col-md-2 control-label">Security Code</label>
                                <div class="col-md-10">
                                    <input type="number" class="form-control" placeholder="Enter security code..." required="required" name="security_code" maxlength="4" max="4"/>
                                    <span class="help-block">Required security code</span>
                                </div>
                            </div>
                        </div>

                        <div class="modal-footer">
                            <button type="submit" class="btn btn-info btn-lg">
                                <span class="glyphicon glyphicon-check"></span>
                                Check Code
                            </button>

                            <button
                                    type="button"
                                    class="btn btn-warning btn-lg"
                                    data-dismiss="modal"
                                    style="color: white;">

                                <span class="glyphicon glyphicon-remove"></span>
                                Close

                            </button>
                        </div>
                    </form>
                </div>
            </div>
        </div>

        <div class="modal" id="change_password-modal" tabindex="-1" role="dialog" aria-labelledby="largeModalHead" aria-hidden="true" data-backdrop="static" data-keyboard="false">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <form id="frmForgotPassword_changePassword" class="form-horizontal" role="form" action="/forgot-password-change-password" method="POST">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal">
                                <span aria-hidden="true">&times;</span><span class="sr-only">Close</span>
                            </button>
                            <h3 class="modal-title" id="largeModalHead">You can now change your password</h3>
                        </div>

                        <div class="modal-body" style="max-height: calc(100vh - 200px); overflow-y: auto;">
                            <div class="form-group">
                                <label class="col-md-2 control-label">New Password:</label>
                                <div class="col-md-10">
                                    <input type="password" class="form-control" name="user_password" id="txtNewPassword" placeholder="Enter new password..." />
                                    <span class="help-block">min size = 8, max size = 16</span>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-md-2 control-label">Confirm Password:</label>
                                <div class="col-md-10">
                                    <input type="password" id="txtConfirmPassword" class="form-control" name="re-password" placeholder="Enter password again..." />
                                    <span class="help-block">Required same value as Password</span>
                                </div>
                            </div>

                            <div class="flex-sb-m w-full p-b-30">
                                <div class="contact100-form-checkbox">
                                    <input class="input-checkbox100" id="cmbShowNewPassword" type="checkbox" onclick="showOrHideNewPassword()">
                                    <label class="label-checkbox100" for="cmbShowNewPassword">
                                        <strong>Show password</strong>
                                    </label>
                                </div>

                                <script type="text/javascript">
                                    function showOrHideNewPassword(){
                                        var x = document.getElementById("txtNewPassword");
                                        var y = document.getElementById("txtConfirmPassword");

                                        if (x.type === "password") {
                                            x.type = "text";
                                        } else {
                                            x.type = "password";
                                        }

                                        if (y.type === "password") {
                                            y.type = "text";
                                        } else {
                                            y.type = "password";
                                        }
                                    }
                                </script>
                            </div>
                        </div>

                        <div class="modal-footer">
                            <button type="submit" class="btn btn-info btn-lg">Change Password</button>

                            <button
                                    type="button"
                                    class="btn btn-danger btn-lg"
                                    data-dismiss="modal"
                                    style="color: white;">

                                <span class="glyphicon glyphicon-remove"></span>
                                Close

                            </button>
                        </div>
                    </form>
                </div>
            </div>
        </div>

    </div>

    <tiles:insertAttribute name="js" />

    <script type="text/javascript" src="/resources/js/RegisterAndForgotPassword.js"></script>
</body>
</html>