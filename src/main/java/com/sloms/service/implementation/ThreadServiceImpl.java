package com.sloms.service.implementation;

import java.util.Date;
import java.util.List;

import com.sloms.entity.*;
import com.sloms.service.ThreadService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.sloms.repository.NotificationsRepository;
import com.sloms.repository.ThreadCommentRepliesRepository;
import com.sloms.repository.ThreadCommentsRepository;
import com.sloms.repository.ThreadsRepository;

@Service
@Transactional(readOnly=true)
public class ThreadServiceImpl implements ThreadService {

	@Autowired
	private ThreadsRepository threadsRepository;
	
	@Autowired
	private ThreadCommentsRepository threadCommentsRepository;
	
	@Autowired
	private ThreadCommentRepliesRepository threadCommentRepliesRepository;
	
	@Autowired
	private NotificationsRepository notificationsRepository;

	@Override
	public List<Threads> getAllThreads() {
		return threadsRepository.getAllThreads();
	}

	@Override
	public List<Threads> getAllThreadsOfLoggedInUser(Users loggedInUser) {
		return threadsRepository.getAllThreadsOfLoggedInUser(loggedInUser);
	}

	@Override
	@Transactional
	public void saveThread(Threads threadsDAO, Notifications notificationsDAO) {
		Threads threads = new Threads();
		Notifications notifications = new Notifications();
		
		//threads
		threads.setThread_notes_id(threadsDAO.getThread_notes_id());
		threads.setThread_caption(threadsDAO.getThread_caption().trim());
		threads.setThread_preview_image(threadsDAO.getThread_preview_image());
		threads.setThread_billboard(threadsDAO.getThread_billboard());
		threads.setThread_status("Open");
		threads.setThread_dateAndTimeCreated(threadsDAO.getThread_dateAndTimeCreated());
		threads.setThread_modified(threadsDAO.getThread_modified());
		threads.setThread_created_by(threadsDAO.getThread_created_by());
		
		//notification
		notifications.setThreads(threads);
		notifications.setNotif_type("add_thread");
		
		threadsRepository.save(threads);
		notificationsRepository.save(notifications);
	}
	
	@Override
	@Transactional
	public void saveThreadComments(ThreadComments threadCommentsDAO, Notifications notificationsDAO) {
		ThreadComments threadComments = new ThreadComments();
		Notifications notifications = new Notifications();
		final Date dateAndTimeToday = new Date();
		
		//thread comments
		threadComments.setThread_id(threadCommentsDAO.getThread_id());
		threadComments.setThread_comment(threadCommentsDAO.getThread_comment().trim());
		threadComments.setThread_comment_images(threadCommentsDAO.getThread_comment_images());
		threadComments.setThread_comment_by(threadCommentsDAO.getThread_comment_by());
		threadComments.setThread_dateAndTime_posted(dateAndTimeToday);
		threadComments.setThread_comment_modified(threadCommentsDAO.getThread_comment_modified());
		
		//notification
		notifications.setThreadComments(threadComments);
		notifications.setNotif_type("add_thread_comment");
				
		threadCommentsRepository.save(threadComments);
		notificationsRepository.save(notifications);
	}

	@Override
	@Transactional
	public void saveThreadCommentReplies(ThreadCommentReplies threadCommentRepliesDAO, Notifications notificationsDAO) {
		ThreadCommentReplies threadCommentReplies = new ThreadCommentReplies();
		Notifications notifications = new Notifications();
		final Date dateAndTimeToday = new Date();
		
		//thread comment replies
		threadCommentReplies.setThread_comment_id(threadCommentRepliesDAO.getThread_comment_id());
		threadCommentReplies.setThread_reply(threadCommentRepliesDAO.getThread_reply().trim());
		threadCommentReplies.setThread_comment_reply_images(threadCommentRepliesDAO.getThread_comment_reply_images());
		threadCommentReplies.setThread_reply_by(threadCommentRepliesDAO.getThread_reply_by());
		threadCommentReplies.setThread_dateAndTime_replied(dateAndTimeToday);
		threadCommentReplies.setThread_reply_modified(threadCommentRepliesDAO.getThread_reply_modified());
		
		//notification
		notifications.setThreadCommentReplies(threadCommentReplies);
		notifications.setNotif_type("add_thread_comment_reply");

		threadCommentRepliesRepository.save(threadCommentReplies);
		notificationsRepository.save(notifications);
	}

	@Override
	public Threads getThreadById(Integer threadID) {
		return threadsRepository.findOne(threadID);
	}

	@Override
	public Threads findThreadByThreadCaption(String thread_caption) {
		return threadsRepository.findThreadByThreadCaption(thread_caption);
	}

	@Override
	public Threads findThreadByThreadId(Integer thread_id) {
		return threadsRepository.findThreadByThreadId(thread_id);
	}

	@Override
	@Transactional
	public void updateThread(
			Notes thread_notes_id, 
			String thread_caption, 
			String thread_status,
			Integer thread_id) {
		
		final Date dateAndTimeModified = new Date();
		
		threadsRepository.updateThread(
				thread_notes_id, 
				thread_caption.trim(), 
				thread_status.trim(), 
				dateAndTimeModified, 
				thread_id);
	}

	@Override
	@Transactional
	public void openOrCloseSelectedThread(String thread_status, Integer thread_id) {
		final Date dateAndTimeModified = new Date();
		
		threadsRepository.openOrCloseSelectedThread(thread_status, dateAndTimeModified, thread_id);
	}

	@Override
	public void updateThreadPreviewImage(String thread_preview_image, Integer thread_id) {
		threadsRepository.updateThreadPreviewImage(thread_preview_image, thread_id);
	}

	@Override
	public void deleteThread(Threads selectedThread) {
		threadsRepository.delete(selectedThread);
	}

}
