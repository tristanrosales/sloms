<div class="modal" id="add_new_user-modal" tabindex="-1" role="dialog" aria-labelledby="largeModalHead" aria-hidden="true" data-backdrop="static" data-keyboard="false" xmlns:th="http://www.thymeleaf.org">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<form id="frmAddUser" class="form-horizontal" role="form" action="/add-user" method="POST">

				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">
						<span aria-hidden="true">&times;</span><span class="sr-only">Close</span>
					</button>
					<h3 class="modal-title" id="largeModalHead">Add User</h3>
				</div>

				<div class="modal-body" style="max-height: calc(100vh - 200px); overflow-y: auto;">
					<div class="alert alert-info" role="alert" style="font-size: 15px;">
						<button type="button" class="close" data-dismiss="alert"></button>
						<strong>Reminder!</strong> 
						<ul>
							<li><strong>Email Notification</strong> is automatically <strong>enabled</strong> once you add a user.</li>
							<li><strong>All fields are required!</strong></li>
						</ul>

						
					</div>

					<div class="form-group">
						<label class="col-md-2 control-label">Username</label>
						<div class="col-md-10">
							<input 
								type="text" 
								class="form-control" 
								placeholder="Enter username..." 
								required="required" 
								name="username" 
								autocomplete="off" 
								id="frmAddUser_txtUsername" 
								maxlength="30" 
								style="background-color:white !important;"/> 
								
							<span class="help-block">Required username</span>
						</div>
					</div>

					<div class="form-group">
						<label class="col-md-2 control-label">First Name</label>
						<div class="col-md-10">
							<input 
								type="text" 
								class="form-control"
								placeholder="Enter first name..." 
								required="required"
								name="user_firstname" 
								style="text-transform: capitalize; background-color:white !important;" 
								id="frmAddUser_txtUser_firstname" 
								maxlength="20"/>

							<span class="help-block">Required first name</span>
						</div>
					</div>

					<div class="form-group">
						<label class="col-md-2 control-label">Last Name</label>
						<div class="col-md-10">
							<input 
								type="text" 
								class="form-control"
								placeholder="Enter last name..." 
								required="required"
								name="user_lastname" 
								style="text-transform: capitalize; background-color:white !important;" 
								id="frmAddUser_txtUser_lastname" 
								maxlength="20"/> 
								
							<span class="help-block">Required last name</span>
						</div>
					</div>

					<div class="form-group">
						<label class="col-md-2 control-label">E-mail:</label>
						<div class="col-md-10">
							<input 
								type="email" 
								name="user_email_address" 
								class="form-control"
								placeholder="Enter email..." 
								id="frmAddUser_txtUser_emailAddress" 
								maxlength="40" 
								style="background-color:white !important;"/> 
								
							<span class="help-block">Required email</span>
						</div>
					</div>

					<div class="form-group">
						<label class="col-md-2 control-label">Phone:</label>    
						<div class="col-md-10">
							<input type="text" name="user_phone_number" class="mask_phone form-control" value="" style="background-color:white !important;" required/>
							<span class="help-block">Required phone number. Example: 98 (765) 432-10-98</span>
						</div>
					</div>

					<div class="form-group">
						<label class="col-md-2 control-label">Gender</label>
						<div class="col-md-10">
							<select id="frmAddUser_cmbGender" class="form-control select" data-live-search="true" required="required" name="user_gender">
								<option disabled="disabled" selected="selected" value="">---SELECT GENDER---</option>
								<option value="Male">Male</option>
								<option value="Female">Female</option>
							</select> 
							
							<span class="help-block">Required gender</span>
						</div>
					</div>

					<div class="form-group">
						<label class="col-md-2 control-label">Role</label>
						<div class="col-md-10">
							<select id="frmAddUser_cmbRole" class="form-control select" data-live-search="true" required="required" name="user_role">
								<option disabled="disabled" selected="selected" value="">---SELECT ROLE---</option>
								<option value="IT_ADMIN">IT Administrator</option>
								<option value="SYS_ADMIN">System Administrator</option>
								<option value="SYS_SPECIALIST">System Specialist</option>
								<option value="GUEST">Guest</option>
							</select>
							
							<span class="help-block">Required role</span>
						</div>
					</div>

					<script>
						function addMaxLengthTo(input){
							$(input).maxlength({
								alwaysShow: true,
								threshold: 10,
								warningClass: "label label-success",
								limitReachedClass: "label label-danger",
								separator: ' of ',
								preText: 'You have ',
								postText: ' chars remaining.',
								validate: true
							});
						}

						function implementBootstrapMaxLength(){
							addMaxLengthTo('input#frmAddUser_txtUsername');
							addMaxLengthTo('input#frmAddUser_txtUser_firstname');
							addMaxLengthTo('input#frmAddUser_txtUser_lastname');
							addMaxLengthTo('input#frmAddUser_txtUser_emailAddress');
						}

						implementBootstrapMaxLength();

						function resetDropdown(dropdownID,value){
							$(dropdownID).val(value);
							$(dropdownID).selectpicker('refresh');
							$(dropdownID).selectpicker('render');
						}

						$('#add_new_user-modal').on('hidden.bs.modal', function () {
							$('#frmAddUser')[0].reset();
							resetDropdown('#frmAddUser_cmbGender','---SELECT GENDER---');
							resetDropdown('#frmAddUser_cmbRole','---SELECT ROLE---');
						});

						function validateEntry(){
							$('#frmAddUser_txtUser_firstname').keypress(function (e) {
								var regex = new RegExp("^[a-zA-Z\s ]+$");
								var str = String.fromCharCode(!e.charCode ? e.which : e.charCode);
								if (regex.test(str)) {
									return true;
								}else{
									e.preventDefault();
									return false;
								}
							});

							$('#frmAddUser_txtUser_lastname').keypress(function (e) {
								var regex = new RegExp("^[a-zA-Z\s ]+$");
								var str = String.fromCharCode(!e.charCode ? e.which : e.charCode);
								if (regex.test(str)) {
									return true;
								}else{
									e.preventDefault();
									return false;
								}
							});

							$('#frmAddUser_txtUsername').keypress(function (e) {
								var regex = new RegExp("^[a-zA-Z0-9.]+$");
								var str = String.fromCharCode(!e.charCode ? e.which : e.charCode);
								if (regex.test(str)) {
									return true;
								}else{
									e.preventDefault();
									return false;
								}
							});
						}

						validateEntry();
					</script>	
				</div>

				<div class="modal-footer">
					<button
						type="submit" 
						class="btn btn-info btn-lg btn-rounded" 
						style="padding: 10px; font-size: 15px; background-color:#1caf9a; color: white;">
						<strong>
							<span class="glyphicon glyphicon-hand-right"></span> Add New User
						</strong>
					</button>

					<button 
						type="button" 
						class="btn btn-warning btn-rounded" 
						style="padding: 10px; font-size: 15px; background-color: orange; color: white;" 
						data-dismiss="modal">
						<strong>
							<span class="glyphicon glyphicon-remove-sign"></span> Close
						</strong>
					</button>
				</div>

			</form>
		</div>
	</div>
</div>