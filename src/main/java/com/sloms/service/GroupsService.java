package com.sloms.service;

import java.util.List;

import com.sloms.entity.Groups;
import com.sloms.entity.Notifications;
import com.sloms.entity.Users;

public interface GroupsService {
	List<Groups> getAllGroups();
	List<Groups> getAllGroupsThatIsActivated();
	List<Groups> getAllGroupsOfLoggedInUser(Users loggedInUser);
	Groups getGroupById(Integer groupID);
	Groups getGroupByGroupName(String groupName);
	void saveGroup(Groups groupsDAO, Notifications notificationsDAO);
	void updateGroup(Groups groupsDAO, Notifications notificationsDAO);
	void updateGroupPreviewImage(String group_preview_image,Integer group_id);
	void deleteGroup(Groups selectedGroup);
}
