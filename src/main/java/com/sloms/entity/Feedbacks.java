package com.sloms.entity;

import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;

import lombok.*;
import org.hibernate.validator.constraints.NotEmpty;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@Getter
@Setter
@ToString
@EqualsAndHashCode
@NoArgsConstructor
@Entity
public class Feedbacks{	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Integer feedback_id;

	@NotNull
	private String feedback_type;

	@NotNull
	@Column(length=5000)
	private String feedback;

	@NotNull
	@ManyToOne(fetch=FetchType.LAZY)
    @JoinColumn(name = "feedback_created_by")
	@JsonIgnoreProperties(
			{
				"hibernateLazyInitializer",
				"handler",
				"notes",
				"threads",
				"groups",
				"notes",
				"threads",
				"threadComments",
				"threadCommentReplies",
				"medias",
				"user_password",
				"user_phone_number",
				"user_status",
				"user_sms_notif",
				"user_email_notif",
				"user_security_question",
				"user_secret_answer",
				"user_dateAndTimeCreated"
			})
	private Users feedback_created_by;
	
	@Temporal(TemporalType.TIMESTAMP)
	private Date feedback_dateAndTimeCreated;
		
	@OneToMany(
			cascade = CascadeType.ALL,
			fetch = FetchType.LAZY,
			mappedBy = "feedbacks")
	@JsonIgnore
	private List<Notifications> notifications;

}
