package com.sloms.service;

import com.sloms.entity.Feedbacks;
import com.sloms.entity.Notifications;

import java.security.Principal;
import java.util.List;

public interface FeedbacksService {
	void saveFeedback(Feedbacks feedbacksDAO, Notifications notificationsDAO);
	List<Feedbacks> getAllFeedbacksOfLoggedInUser(Principal principal);
	List<Feedbacks> getAllFeedbacks();
}
