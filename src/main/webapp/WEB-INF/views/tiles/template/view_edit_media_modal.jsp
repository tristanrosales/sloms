<div class="modal" id="view_edit_media-modal" tabindex="-1" role="dialog" aria-labelledby="largeModalHead" aria-hidden="true" data-backdrop="static" data-keyboard="false">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<form class="form-horizontal frmUpdateDeleteMedia" role="form" method="POST">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">
						<span aria-hidden="true">&times;</span><span class="sr-only">Close</span>
					</button>

					<h3 class="modal-title" id="largeModalHead">
						Edit <span id="frmUpdateDeleteMedia_lblMediaName"></span>
					</h3>
				</div>

				<div class="modal-body" style="max-height: calc(100vh - 200px); overflow-y: auto;">
					<input 
						type="hidden" 
						id="frmUpdateDeleteMedia_lblMedia_id" 
						name="media_id">

					<div class="row">
						<div class="col-md-6">
							<div class="form-group">
								<label class="col-md-2 control-label">Name</label>
								<div class="col-md-10">
									<input 
										type="text" 
										class="form-control"
										placeholder="Enter name of media..." 
										required="required"
										name="media_name" 
										id="frmUpdateDeleteMedia_txtMediaName" 
										style="text-transform: capitalize; background-color:white !important;"/>
								</div>
							</div>

							<div class="form-group">
								<label class="col-md-2 control-label">Media Code</label>
								<div class="col-md-10">
									<input 
										type="text" 
										class="form-control"
										placeholder="Enter media code..." 
										required="required"
										name="media_code" 
										id="frmUpdateDeleteMedia_txtMediaCode" 
										style="text-transform: capitalize; background-color:white !important;"/>
								</div>
							</div>

							<div class="form-group">
								<label class="col-md-2 control-label">Description</label>
								<div class="col-md-10">
									<textarea 
										class="form-control" 
										rows="5"
										placeholder="Enter description of media..."
										required="required" 
										name="media_description"
										id="frmUpdateDeleteMedia_txtMediaDescription"
										style='resize: none; text-transform: capitalize; background-color:white !important;'></textarea>
								</div>
							</div>

							<div class="form-group">
								<label class="col-md-2 control-label">Status</label>
								<div class="col-md-10">
									<select 
										class="form-control select" 
										data-live-search="true"
										required="required" 
										name="media_status"
										id="frmUpdateDeleteUser_cmbMedia_Status">

										<option disabled="disabled" selected="selected">---SELECT STATUS---</option>
										<option value="Activated">Activated</option>
										<option value="Deactivated">Deactivated</option>
									
									</select>
								</div>
							</div>

							<div class="form-group">
								<label class="col-md-2 control-label">Group</label>
								<div class="col-md-10">
									<select 
										class="form-control select" 
										data-live-search="true" 
										name="media_group" 
										id="frmUpdateDeleteMedia_cmbMediaGroup">

										<option disabled="disabled" selected="selected">---SELECT GROUP---</option>
									
									</select>
								</div>
							</div>

							<div class="form-group">
								<label class="col-md-2 control-label">Price</label>
								<div class="col-md-10">
									<input 
										type="number" 
										class="form-control"
										placeholder="Enter price..." 
										required="required"
										name="media_price" 
										id="frmUpdateDeleteMedia_txtMediaPrice" 
										style="background-color:white !important;"/>
								</div>
							</div>

							<div class="form-group">
								<label class="col-md-2 control-label">Location</label>
								<div class="col-md-10">
									<textarea 
										class="form-control" 
										rows="5"
										placeholder="Enter location..."
										required="required" 
										name="media_location" 
										id="frmUpdateDeleteMedia_txtMediaLocation"
										style='resize: none; background-color:white !important;'></textarea>
								</div>
							</div>
						</div>

						<div class="col-md-6">
							<div class="form-group">
								<label class="col-md-2 control-label">Region</label>
								<div class="col-md-10">
									<select class="form-control select" data-live-search="true" name="media_region" id="frmUpdateDeleteMedia_cmbRegion" required>
									</select>
								</div>
							</div>

							<div class="form-group">
								<label class="col-md-2 control-label">Province</label>
								<div class="col-md-10">
									<select class="form-control select" data-live-search="true" name="media_province" id="frmUpdateDeleteMedia_cmbProvince" required>
									</select>
								</div>
							</div>

							<div class="form-group">
								<label class="col-md-2 control-label">City</label>
								<div class="col-md-10">
									<select class="form-control select" data-live-search="true" name="media_city" id="frmUpdateDeleteMedia_cmbCity" required>
									</select>
								</div>
							</div>

							<div class="form-group">
								<label class="col-md-2 control-label">Size H</label>
								<div class="col-md-10">
									<input 
										type="text" 
										class="form-control"
										placeholder="Enter Size H..." 
										required="required"
										name="media_size_h" 
										id="frmUpdateDeleteMedia_txtMediaSize_h" 
										style="background-color:white !important;"/>
								</div>
							</div>

							<div class="form-group">
								<label class="col-md-2 control-label">Size W</label>
								<div class="col-md-10">
									<input 
										type="text" 
										class="form-control"
										placeholder="Enter Size W..." 
										required="required"
										name="media_size_w" 
										id="frmUpdateDeleteMedia_txtMediaSize_w" 
										style="background-color:white !important;"/>
								</div>
							</div>

							<div class="form-group">
								<label class="col-md-2 control-label">Road Position</label>
								<div class="col-md-10">
									<select 
										class="form-control select" 
										data-live-search="true"
										name="media_road_position"
										id="frmUpdateDeleteMedia_cmbMediaRoadPosition">

										<option disabled="disabled" selected="selected">---SELECT ROAD POSITION---</option>
										<option value="Roadside_Left">Roadside Left</option>
										<option value="Roadside_Right">Roadside Right</option>

									</select>
								</div>
							</div>

							<div class="form-group">
								<label class="col-md-2 control-label">Panel Orientation</label>
								<div class="col-md-10">
									<select 
										class="form-control select" 
										data-live-search="true"
										name="media_panel_orientation"
										id="frmUpdateDeleteMedia_cmbMediaPanelOrientation">

										<option disabled="disabled" selected="selected">---SELECT PANEL ORIENTATION---</option>
										<option value="Vertical_or_Portrait">Vertical or Portrait</option>
										<option value="Horizontal_or_Landscape">Horizontal or Landscape</option>
									</select>
								</div>
							</div>

							<div class="form-group">
								<label class="col-md-2 control-label">Illumination</label>
								<div class="col-md-10">
									<select class="form-control select" data-live-search="true" name="media_illumination" id="frmUpdateDeleteMedia_cmbMediaIllumination">
										<option disabled="disabled" selected="selected">---SELECT ILLUMINATION---</option>
										
										<script language="javascript" type="text/javascript"> 
											for(var a=4; a<=30; a++){
												if(a % 2 == 0){
													document.write("<option value='" + a + "'>" + a + "</option>");
												}				
											}
										</script>
									
									</select>
								</div>
							</div>

							<div class="form-group">
								<label class="col-md-2 control-label">Structure Type</label>
								<div class="col-md-10">
									<select class="form-control select" data-live-search="true" name="media_structure_type" id="frmUpdateDeleteMedia_cmbMediaStructureType">
										<option disabled="disabled" selected="selected">---SELECT STRUCTURE TYPE---</option>
										<option value="Rooftop_Structure">Rooftop Structure</option>
										<option value="Wall-attached_Frame">Wall-attached Frame</option>
										<option value="Wall_Painting">Wall Painting</option>
										<option value="Freestanding">Freestanding</option>
										<option value="Wall_Sticker">Wall Sticker</option>
										<option value="Rooftop_LED">Rooftop LED</option>
										<option value="Wall-attached_LED">Wall-attached LED</option>
										<option value="Freestanding_LED">Freestanding LED</option>
									</select>
								</div>
							</div>

							<div class="form-group">
								<label class="col-md-2 control-label">Traffic Details</label>
								<div class="col-md-10">
									<select 
										class="form-control select" 
										data-live-search="true"
										name="media_traffic_details"
										id="frmUpdateDeleteMedia_cmbMediaTrafficDetails">

										<option disabled="disabled" selected="selected">---SELECT TRAFFIC DETAIL---</option>
										<option value="Northbound">Northbound</option>
										<option value="Eastbound">Eastbound</option>
										<option value="Southbound">Southbound</option>
										<option value="Westbound">Westbound</option>

									</select>
								</div>
							</div>

							<div class="form-group">
								<label class="col-md-2 control-label">Advertiser</label>
								<div class="col-md-10">
									<input 
										type="text" 
										class="form-control"
										placeholder="Enter advertiser..." 
										required="required"
										name="media_client"
										id="frmUpdateDeleteMedia_txtMediaClient" 
										style="background-color:white !important;"/>
								</div>
							</div>
							
							<div class="form-group">
								<label class="col-md-2 control-label">Contract Rate</label>
								<div class="col-md-10">
									<input 
										type="text" class="form-control"
										placeholder="Enter contract rate..." 
										required="required"
										name="media_contracted_rate"
										id="frmUpdateDeleteMedia_txtMediaContractedRate" 
										style="background-color:white !important;"/>
								</div>
							</div>

							<div class="alert alert-info" role="alert">
								<button type="button" class="close" data-dismiss="alert"></button>
								<strong>Instruction!</strong> Just click the textbox below to set the contract start date & contract end date.
							</div>

							<div class="form-group">
								<label class="col-md-2 control-label">Contract Start Date</label>
								<div class="col-md-10">
									<input 
										type="text" 
										id="frmUpdateDeleteMedia_txtMediaContractStart" 
										class="form-control" 
										name="media_contract_start" 
										placeholder="Click this textbox to set the contract start date.." 
										required 
										style="background-color:white !important; color: black;">
								</div>
							</div>

							<div class="form-group">
								<label class="col-md-2 control-label">Contract End Date</label>
								<div class="col-md-10">
									<input 
										type="text" 
										id="frmUpdateDeleteMedia_txtMediaExpiration" 
										class="form-control" 
										name="media_expiration" 
										placeholder="Click this textbox to set the contract end date" 
										required 
										style="background-color:white !important; color: black;">
								</div>
							</div>

							<script>
								document.getElementById("frmUpdateDeleteMedia_txtMediaContractStart").flatpickr();
								document.getElementById("frmUpdateDeleteMedia_txtMediaExpiration").flatpickr();
							</script>
						</div>
					</div>
				</div>

				<div class="modal-footer">
					<button
						type="submit" 
						id="btnUpdateMedia"
						class="btn btn-info btn-lg btn-rounded" 
						style="padding: 10px; font-size: 15px; background-color:#1caf9a; color: white;">
						<strong>
							<span class="glyphicon glyphicon-hand-right"></span> Update
						</strong>
					</button>

					<button 
						type="button"
						class="btn btn-warning btn-lg btn-rounded" 
						id="btnDeleteMedia"
						onclick="deleteMedia()"
						style="display: none;">
						Delete
					</button>

					<button 
						type="button" 
						class="btn btn-warning btn-rounded" 
						style="padding: 10px; font-size: 15px; background-color:orange; color: white;" 
						data-dismiss="modal">
						<strong>
							<span class="glyphicon glyphicon-remove-sign"></span> Close
						</strong>
					</button>
				</div>
			</form>
		</div>
	</div>
</div>