<div class="modal" id="view_edit_notes-modal" tabindex="-1" role="dialog" aria-labelledby="largeModalHead" aria-hidden="true" data-backdrop="static" data-keyboard="false">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<form id="frmUpdateDeleteNotes" class="form-horizontal" role="form" method="POST" action="/update-notes">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">
						<span aria-hidden="true">&times;</span><span class="sr-only">Close</span>
					</button>
					<h3 class="modal-title" id="largeModalHead"><span id="frmUpdateDeleteNotes_lblNotes_type"></span></h3>
				</div>

				<div class="modal-body">
					<input type="hidden" id="frmUpdateDeleteNotes_lblNotes_id" name="notes_id">
				
					<div class="form-group">
						<label class="col-md-2 control-label">Type</label>
						<div class="col-md-10">
							<input 
								type="text" 
								class="form-control"
								placeholder="Enter notes type..." 
								required="required"
								name="notes_type" 
								id="frmUpdateDeleteNotes_txtNotes_type" 
								style="resize: none; text-transform: capitalize; background-color:white !important;"/> 
							
							<span class="help-block">Required notes type</span>
						</div>
					</div>

					<div class="form-group">
						<label class="col-md-2 control-label">Description</label>
						<div class="col-md-10">

							<textarea 
								class="form-control" 
								rows="5"
								placeholder="Enter description of notes..." 
								required="required"
								name="notes_description" 
								id="frmUpdateDeleteNotes_txtNotes_description" 
								style="resize: none; text-transform: capitalize; background-color:white !important;"></textarea>

							<span class="help-block">Required notes description</span>
						</div>
					</div>
					
					<div class="form-group">
						<label class="col-md-2 control-label">Status</label>
						<div class="col-md-10">
							<select 
								class="form-control select" 
								data-live-search="true"
								required="required" 
								name="notes_status" 
								id="frmUpdateDeleteNotes_cmbNotes_status">

								<option disabled="disabled" selected="selected">---SELECT STATUS---</option>
								<option value="Activated">Activated</option>
								<option value="Deactivated">Deactivated</option>

							</select> 
							
							<span class="help-block">Required notes status</span> 
						</div>
					</div>
				</div>

				<div class="modal-footer">
					<button 
						type="submit" 
						class="btn btn-info btn-lg btn-rounded" 
						style="padding: 10px; font-size: 15px; background-color:#1caf9a; color: white;">
						<strong>
							<span class="fa fa-hand-o-right"></span> Update Note
						</strong>
					</button>

					<button 
						type="button" 
						id="btnDeleteNotes" 
						class="btn btn-warning btn-lg btn-rounded" 
						onclick="deleteNotes()" 
						style="display: none;">
						Delete
					</button>

					<button 
						type="button" 
						class="btn btn-warning btn-rounded" 
						style="padding: 10px; font-size: 15px; background-color: orange; color: white;" 
						data-dismiss="modal">
						<strong>
							<span class="glyphicon glyphicon-remove-sign"></span> Close
						</strong>
					</button>
				</div>
			</form>
		</div>
	</div>
</div>