<div class="modal" id="view_edit_faqs-modal" tabindex="-1" role="dialog" aria-labelledby="largeModalHead" aria-hidden="true" data-backdrop="static" data-keyboard="false">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
            <form id="frmUpdateFAQs" class="form-horizontal" role="form" action="/update-faqs" method="POST">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">
                        <span aria-hidden="true">&times;</span><span class="sr-only">Close</span>
                    </button>
                    <h3 class="modal-title" id="largeModalHead">Edit FAQs</h3>
                </div>
                
                <div class="modal-body" style="max-height: calc(100vh - 200px); overflow-y: auto;"> 
                    <input type="hidden" id="frmUpdateFAQs_lblID" name="faqs_id">

                    <div class="form-group">
                        <label class="col-md-2 control-label">Question</label>
                        <div class="col-md-10">
                            <textarea 
                                id="frmUpdateFAQs_txtQuestion"
                                class="form-control" 
                                rows="5"
                                placeholder="Enter FAQs question..."
                                required="required" 
                                name="faqs_question"
                                style="resize: none; background-color:white !important;"></textarea>

                            <span class="help-block">Required FAQs question</span>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-md-2 control-label">Answer</label>
                        <div class="col-md-10">
                            <textarea 
                                id="frmUpdateFAQs_answer"
                                class="form-control" 
                                rows="5"
                                placeholder="Enter FAQs answer..."
                                required="required" 
                                name="faqs_answer"
                                style="resize: none; background-color:white !important;"></textarea>

                            <span class="help-block">Required FAQs answer</span>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-md-2 control-label">Status</label>
                        <div class="col-md-10">
                            <select class="form-control select" data-live-search="true" required="required" name="faqs_status" id="frmUpdateFAQs_cmbStatus">
                                <option disabled="disabled" selected="selected">---SELECT STATUS---</option>
                                <option value="Enabled">Enabled</option>
                                <option value="Disabled">Disabled</option>
                            </select> 
                            
                            <span class="help-block">Required role</span>
                        </div>
                    </div>
                </div>
                
                <div class="modal-footer">
                    <button 
                        type="submit" 
                        class="btn btn-info btn-lg btn-rounded" 
                        style="padding: 10px; font-size: 15px; background-color:#1caf9a; color: white;">
                        <strong>
                            <span class="fa fa-hand-o-right"></span> Update FAQ
                        </strong>
                    </button>
                    
                    <button 
                        type="button" 
                        class="btn btn-warning btn-rounded" 
                        style="padding: 10px; font-size: 15px; background-color: orange; color: white;" 
                        data-dismiss="modal">
                        <strong>
                            <span class="glyphicon glyphicon-remove-sign"></span> Close
                        </strong>
                    </button>
                </div>
            </form>
		</div>
	</div>
</div>