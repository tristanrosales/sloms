package com.sloms.service.implementation;

import java.security.Principal;
import java.util.Date;
import java.util.List;

import com.sloms.service.MediaService;
import com.sloms.service.UsersService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.sloms.entity.Media;
import com.sloms.entity.MediaClientDetails;
import com.sloms.entity.MediaDetails;
import com.sloms.entity.Notifications;
import com.sloms.entity.Users;
import com.sloms.repository.MediaClientDetailsRepository;
import com.sloms.repository.MediaDetailsRepository;
import com.sloms.repository.MediaRepository;
import com.sloms.repository.NotificationsRepository;

@Service
@Transactional(readOnly=true)
public class MediaServiceImpl implements MediaService {

	@Autowired private MediaRepository mediaRepository;
	@Autowired private MediaDetailsRepository mediaDetailsRepository;
	@Autowired private MediaClientDetailsRepository mediaClientDetailsRepository;
	@Autowired private NotificationsRepository notificationsRepository;
	@Autowired private UsersService usersService;
	
	@Override
	@Transactional
	public void saveMedia(Media mediaDAO, MediaDetails mediaDetailsDAO, MediaClientDetails mediaClientDetailsDAO, Notifications notificationsDAO, Principal principal) {
		try {
			Media media = new Media();
			MediaDetails mediaDetails = new MediaDetails();
			MediaClientDetails mediaClientDetails = new MediaClientDetails();
			Notifications notifications = new Notifications();
			Users loggedInUser = usersService.findUserByUsername(principal.getName());
			
			final Date dateAndTimeCreated = new Date();
			
			//media
			media.setMedia_name(mediaDAO.getMedia_name().trim());
			media.setMedia_code(mediaDAO.getMedia_code().trim());
			media.setMedia_preview_image(mediaDAO.getMedia_preview_image());
			media.setMedia_images(mediaDAO.getMedia_images());
			media.setMedia_dateAndTime_created(dateAndTimeCreated);
			media.setMedia_created_by(loggedInUser);
			media.setMedia_status("Activated");
			
			//media details
			mediaDetails.setMedia(media);
			mediaDetails.setMedia_group(mediaDetailsDAO.getMedia_group());	
			mediaDetails.setMedia_description(mediaDetailsDAO.getMedia_description().trim());
			mediaDetails.setMedia_location(mediaDetailsDAO.getMedia_location().trim());
			mediaDetails.setMedia_region(mediaDetailsDAO.getMedia_region().trim());
			mediaDetails.setMedia_city(mediaDetailsDAO.getMedia_city().trim());
			mediaDetails.setMedia_size_h(mediaDetailsDAO.getMedia_size_h().trim());
			mediaDetails.setMedia_size_w(mediaDetailsDAO.getMedia_size_w().trim());
			mediaDetails.setMedia_unit("foot");
			mediaDetails.setMedia_price(mediaDetailsDAO.getMedia_price().trim());
			mediaDetails.setMedia_road_position(mediaDetailsDAO.getMedia_road_position());
			mediaDetails.setMedia_province(mediaDetailsDAO.getMedia_province());
			mediaDetails.setMedia_panel_orientation(mediaDetailsDAO.getMedia_panel_orientation());
			mediaDetails.setMedia_illumination(mediaDetailsDAO.getMedia_illumination());
			mediaDetails.setMedia_structure_type(mediaDetailsDAO.getMedia_structure_type());
			mediaDetails.setMedia_traffic_details(mediaDetailsDAO.getMedia_traffic_details());
			
			//client details
			mediaClientDetails.setMedia(media);
			mediaClientDetails.setMedia_client(mediaClientDetailsDAO.getMedia_client().trim());
			mediaClientDetails.setMedia_contract_start(mediaClientDetailsDAO.getMedia_contract_start().trim());
			mediaClientDetails.setMedia_expiration(mediaClientDetailsDAO.getMedia_expiration().trim());
			mediaClientDetails.setMedia_contracted_rate(mediaClientDetailsDAO.getMedia_contracted_rate().trim());
			
			//notification
			notifications.setMedia(media);
			notifications.setNotif_type("add_media");
			
			
			mediaRepository.save(media);
			mediaDetailsRepository.save(mediaDetails);
			mediaClientDetailsRepository.save(mediaClientDetails);
			notificationsRepository.save(notifications);
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
	}
	
	@Override
	@Transactional
	public void updateMedia(Media mediaDAO, MediaDetails mediaDetailsDAO, MediaClientDetails mediaClientDetailsDAO) {
		
		final Date dateAndTimeModified = new Date();
				
		mediaRepository.updateMediaBasicInformation(
				mediaDAO.getMedia_name().trim(),
				mediaDAO.getMedia_code().trim(),
				mediaDAO.getMedia_status().trim(),
				dateAndTimeModified,
				mediaDAO.getMedia_id());
		
		mediaDetailsRepository.updateMediaDetailsInformation(
				mediaDetailsDAO.getMedia_group(),
				mediaDetailsDAO.getMedia_description().trim(),
				mediaDetailsDAO.getMedia_price().trim(),
				mediaDetailsDAO.getMedia_location().trim(),
				mediaDetailsDAO.getMedia_region().trim(),
				mediaDetailsDAO.getMedia_city().trim(),
				mediaDetailsDAO.getMedia_size_h().trim(),
				mediaDetailsDAO.getMedia_size_w().trim(),
				mediaDetailsDAO.getMedia_road_position(),
				mediaDetailsDAO.getMedia_panel_orientation(),
				mediaDetailsDAO.getMedia_illumination(),
				mediaDetailsDAO.getMedia_structure_type(),
				mediaDetailsDAO.getMedia_traffic_details(),
				mediaDAO);
		
		mediaClientDetailsRepository.updateMediaClientDetailsInformation(
				mediaClientDetailsDAO.getMedia_client().trim(),
				mediaClientDetailsDAO.getMedia_contracted_rate().trim(),
				mediaClientDetailsDAO.getMedia_contract_start(),
				mediaClientDetailsDAO.getMedia_expiration(),
				mediaDAO);
	}
	
	@Override
	public List<Media> getAllMedia() {
		return mediaRepository.getAllMedia();
	}

	@Override
	public List<Media> getAllMediaOfLoggedInUser(Principal principal) {
		Users loggedInUser = usersService.findUserByUsername(principal.getName());
		return mediaRepository.getAllMediaOfLoggedInUser(loggedInUser);
	}

	@Override
	public Media getMediaById(Integer mediaID) {
		return mediaRepository.findMediaByMediaId(mediaID);
	}

	@Override
	public Media getAllMediaByMediaIdAndThreadNotesId(Integer media_id,Integer notes_id) {
		return mediaRepository.getAllMediaByMediaIdAndThreadNotesType(media_id,notes_id);
	}
}
