<div class="modal" id="update_thread_preview_image-modal" tabindex="-1" role="dialog" aria-labelledby="largeModalHead" aria-hidden="true" data-backdrop="static" data-keyboard="false">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
            <form id="frmUpdateThreadPreviewImage" class="form-horizontal" role="form" method="POST" action="/update-thread-preview-image">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">
                        <span aria-hidden="true">&times;</span><span class="sr-only">Close</span>
                    </button>
                    <h3 class="modal-title" id="largeModalHead">Update Thread Preview Image</h3>
                </div>
                
                <div class="modal-body">
                    <div class="alert alert-info" role="alert" style="font-size: 15px;">
                        <button type="button" class="close" data-dismiss="alert"></button>
                        <strong>Reminder!</strong><br>
                        <ul>
                            <li>Maximum file size: <b>50kb</b></li>
                            <li>Allowed file extension: <b>jpeg, jpg, and png</b></li>
                        </ul> 
                    </div>

                    <input type="hidden" id="frmUpdateThreadPreviewImage_groupId" name="thread_id">

                    <div class="form-group">
                        <label class="col-md-2 control-label">Thread Preview Image</label>
                        <div class="col-md-10">
                            <div class="panel panel-default">
                                <div class="panel-body">
                                    <h3>
                                        <span class="fa fa-download"></span> Thread Preview Image
                                    </h3>
                                    
                                    <div id="update-thread-preview-image-holder"></div>		
                                </div>
                            </div>
                        </div>
                    </div>
                    
                    <script>
                        var updateThreadPreviewImageUploader = new qq.FineUploader({
                            element: document.getElementById('update-thread-preview-image-holder'),
                            template: 'qq-template-manual-trigger_single-upload',
                            validation: {
                                allowedExtensions: ["jpeg","jpg","png"],
                                itemLimit: 1,
                                sizeLimit: 51200 // 50 kB = 50 * 1024 bytes
                            },
                            request: {
                                endpoint: '/add-thread/add-thread-preview-image',
                                method: "POST"
                            },
                            deleteFile: {
                                enabled: true,
                                forceConfirm: true,
                                endpoint: '/add-thread/delete-thread-preview-image',
                                method: "DELETE"
                            },
                            thumbnails: {
                                placeholders: {
                                    waitingPath: '/resources/js/plugins/fine-uploader/placeholders/waiting-generic.png',
                                    notAvailablePath: '/resources/js/plugins/fine-uploader/placeholders/not_available-generic.png'
                                }
                            },
                            autoUpload: false,
                            debug: true,
                            callbacks: {
                                onComplete: function (id,name,responseJSON,maybeXhr){
                                    console.log(responseJSON);
                                    $('.lblThreadPreviewImage_' + id).val(responseJSON.threadPreviewImageLink);
                                },
                                onCancel: function(id,name){
                                    console.log("cancel");
                                    $('.lblThreadPreviewImage_' + id).remove();
                                },
                                onError: function(id,name,reason,maybeXhrOrXdr){
                                    //logic here
                                },
                                onAllComplete: function(successful,failed){
                                    if(failed.length <= 0){
                                        //logic here
                                    }else{
                                        //logic here
                                    }
                                },
                                onSubmitted: function(id,name){
                                    $(".qq-uploader-selector").append("<input type='hidden' class='lblThreadPreviewImage_" + id + "' name='thread_preview_image' value='' />");
                                },
                                onUpload (id,name){
                                    console.log(name + " before uploading...");
                                },

                                onSubmitDelete: function(id){
                                    this.setDeleteFileParams({filename: this.getName(id)}, id);
                                },
                                onDeleteComplete: function(id){
                                    $('.lblThreadPreviewImage_' + id).remove();
                                }
                            }
                        });

                        qq(document.querySelector("#update-thread-preview-image-holder #trigger-upload")).attach("click", function () {
                            updateThreadPreviewImageUploader.uploadStoredFiles();
                        });
                        
                        $('#update_thread_preview_image-modal').on('hidden.bs.modal', function () {
                            updateThreadPreviewImageUploader.cancelAll();
                            updateThreadPreviewImageUploader.clearStoredFiles();
                        });
                    </script>
                </div>

                <div class="modal-footer">
                    <button 
                        type="submit" 
                        class="btn btn-info btn-lg btn-rounded" 
                        style="padding: 10px; font-size: 15px; background-color:#1caf9a; color: white;">
                        <strong>
                            <span class="fa fa-hand-o-right"></span> Update
                        </strong>
                    </button>

                    <button 
                        type="button" 
                        class="btn btn-warning btn-rounded" 
                        style="padding: 10px; font-size: 15px; background-color: orange; color: white;" 
                        data-dismiss="modal">
                        <strong>
                            <span class="glyphicon glyphicon-remove-sign"></span> Close
                        </strong>
                    </button>
                </div>
            </form>
		</div>
	</div>
</div>