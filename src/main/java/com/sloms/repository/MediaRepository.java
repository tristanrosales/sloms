package com.sloms.repository;

import java.util.Date;
import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.sloms.entity.Media;
import com.sloms.entity.Users;

@Repository
public interface MediaRepository extends JpaRepository<Media,Integer> {
	@Query("SELECT m FROM Media m WHERE m.media_id=m.media_id ORDER BY m.media_id DESC")
	List<Media> getAllMedia();
	
	@Query("SELECT m FROM Media m WHERE m.media_status=:media_status ORDER BY m.media_id DESC")
	List<Media> getAllAvailableMedia(@Param("media_status") String media_status);
	
	@Query(value="SELECT m FROM Media m "
			+ "INNER JOIN m.threads threads "
			+ "INNER JOIN threads.thread_notes_id thread_notes_id "
			+ "WHERE m.media_id=:media_id AND thread_notes_id.notes_id=:notes_id "
			+ "ORDER BY m.media_id DESC")
	Media getAllMediaByMediaIdAndThreadNotesType(
			@Param("media_id") Integer media_id,
			@Param("notes_id") Integer notes_id);
	
	@Query("SELECT m FROM Media m WHERE m.media_created_by=:userID ORDER BY m.media_id DESC")
	List<Media> getAllMediaOfLoggedInUser(@Param("userID") Users userID);
	
	@Query("SELECT m FROM Media m WHERE m.media_name=:media_name")
	Media findMediaByMediaName(@Param("media_name") String media_name);
	
	@Query("SELECT m FROM Media m WHERE m.media_code=:media_code")
	Media findMediaByMediaCode(@Param("media_code") String media_code);
	
	@Query("SELECT m FROM Media m WHERE m.media_id=:media_id")
	Media findMediaByMediaId(@Param("media_id") Integer media_id);
	
	@Transactional
	@Modifying
	@Query("UPDATE Media m SET "
			+ "m.media_name=:media_name,"
			+ "m.media_code=:media_code,"
			+ "m.media_status=:media_status,"
			+ "m.media_modified=:media_modified "
			+ "WHERE m.media_id=:media_id")
	void updateMediaBasicInformation(
			@Param("media_name") String media_name,
			@Param("media_code") String media_code,
			@Param("media_status") String media_status,
			@Param("media_modified") Date media_modified,
			@Param("media_id") Integer media_id);
	
	@Transactional
	@Modifying
	@Query("UPDATE Media m SET "
			+ "m.media_preview_image=:media_preview_image "
			+ "WHERE m.media_id=:media_id")
	void updateMediaPreviewImage(
			@Param("media_preview_image") String media_preview_image,
			@Param("media_id") Integer media_id);
}
