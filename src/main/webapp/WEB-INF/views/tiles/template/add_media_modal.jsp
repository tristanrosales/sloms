<div class="modal" id="add_new_media-modal" tabindex="-1" role="dialog" aria-labelledby="largeModalHead" aria-hidden="true" data-backdrop="static" data-keyboard="false">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<form id="frmAddMedia" class="form-horizontal" role="form" action="/add-media" method="post">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">
						<span aria-hidden="true">&times;</span><span class="sr-only">Close</span>
					</button>
					<h3 class="modal-title" id="largeModalHead">Add Media</h3>
				</div>

				<div class="modal-body" style="max-height: calc(100vh - 200px); overflow-y: auto;">
					<div class="alert alert-info" role="alert" style="font-size: 15px;">
						<button type="button" class="close" data-dismiss="alert"></button>
						<strong>Reminder!</strong><br>
						<ul>
							<li>Maximum file size: <b>10mb</b></li>
							<li>Allowed file extension: <b>jpeg, jpg, and png</b></li>
						</ul> 
					</div>
					
					<div class="row">
						<div class="col-md-6">
							<div class="form-group">
								<label class="col-md-2 control-label">Media Preview Image</label>
								<div class="col-md-10">
									<div class="panel panel-default">
										<div class="panel-body">
											<h3>
												<span class="fa fa-download"></span> Media Preview Image
											</h3>
											
											<div id="media-preview-image-holder"></div>
										</div>
									</div>
								</div>
							</div>

							<div class="form-group">
								<label class="col-md-2 control-label">Name</label>
								<div class="col-md-10">
									<input 
										type="text" 
										class="form-control"
										placeholder="Enter name of media..." 
										required="required"
										name="media_name"
										id="frmAddMedia_media_name"
										style="text-transform: capitalize; background-color:white !important;" 
										maxlength="50"/>
								</div>
							</div>

							<div class="form-group">
								<label class="col-md-2 control-label">Group</label>
								<div class="col-md-10">
									<select 
										class="form-control select" 
										data-live-search="true" 
										name="media_group" 
										id="cmbMedia_group" 
										required="required">
										<option selected="selected" disabled="disabled" value="">---SELECT GROUP---</option>
									</select>
								</div>
							</div>

							<div class="form-group">
								<label class="col-md-2 control-label">Description</label>
								<div class="col-md-10">
									<textarea 
										class="form-control" 
										rows="5"
										placeholder="Enter description of media..."
										required="required" 
										name="media_description"
										id="frmAddMedia_media_description"
										style="resize: none; background-color:white !important;"
										maxlength="5000"></textarea>
								</div>
							</div>

							<div class="form-group">
								<label class="col-md-2 control-label">Price</label>
								<div class="col-md-10">
									<input 
										type="number" 
										class="form-control"
										placeholder="Enter price..." 
										required="required"
										name="media_price" 
										style="background-color:white !important;"/>
								</div>
							</div>

							<div class="form-group">
								<label class="col-md-2 control-label">Media Code</label>
								<div class="col-md-10">
									<input 
										type="text" 
										class="form-control"
										placeholder="Enter media code..." 
										required="required"
										name="media_code"
										id="frmAddMedia_media_code"
										style="text-transform: capitalize; background-color:white !important;"
										maxlength="50" />
								</div>
							</div>

							<div class="form-group">
								<label class="col-md-2 control-label">Location</label>
								<div class="col-md-10">
									<textarea 
										class="form-control" 
										rows="5" 
										placeholder="Enter location..." 
										required="required" 
										name="media_location" 
										id="frmAddMedia_media_location" 
										style="resize: none; background-color:white !important;"
										maxlength="200"></textarea>
								</div>
							</div>

							<div class="form-group">
								<label class="col-md-2 control-label">Region</label>
								<div class="col-md-10">
									<select class="form-control select" data-live-search="true" name="media_region" id="frmAddMedia_cmbRegion" required>
										<option disabled="disabled" selected="selected" value="">---SELECT REGION---</option>
									</select>
								</div>
							</div>

							<div class="form-group">
								<label class="col-md-2 control-label">Province</label>
								<div class="col-md-10">
									<select class="form-control select" data-live-search="true" name="media_province" id="frmAddMedia_cmbProvince" required>
										<option disabled="disabled" selected="selected" value="">---SELECT PROVINCE---</option>
									</select>
								</div>
							</div>

							<div class="form-group">
								<label class="col-md-2 control-label">City</label>
								<div class="col-md-10">
									<select class="form-control select" data-live-search="true" name="media_city" id="frmAddMedia_cmbCity" required>
										<option disabled="disabled" selected="selected" value="">---SELECT CITY---</option>
									</select>
								</div>
							</div>

							<div class="form-group">
								<label class="col-md-2 control-label">Size H</label>
								<div class="col-md-10">
									<input 
										type="number" 
										class="form-control"
										placeholder="Enter Size H..." 
										required="required"
										name="media_size_h"
										id="frmAddMedia_media_size_h"
										maxlength="3" 
										style="background-color:white !important;"/>
								</div>
							</div>

							<div class="form-group">
								<label class="col-md-2 control-label">Size W</label>
								<div class="col-md-10">
									<input 
										type="number" 
										class="form-control"
										placeholder="Enter Size W..." 
										required="required"
										name="media_size_w"
										id="frmAddMedia_media_size_w"
										maxlength="3" 
										style="background-color:white !important;"/>
								</div>
							</div>
						</div>

						<div class="col-md-6">
							<div class="form-group">
								<label class="col-md-2 control-label">Road Position</label>
								<div class="col-md-10">
									<select id="frmAddMedia_cmbRoadPosition" class="form-control select" data-live-search="true" name="media_road_position" required="required">
										<option disabled="disabled" selected="selected" value="">---SELECT ROAD POSITION---</option>
										<option value="Roadside_Left">Roadside Left</option>
										<option value="Roadside_Right">Roadside Right</option>
									</select>
								</div>
							</div>

							<div class="form-group">
								<label class="col-md-2 control-label">Panel Orientation</label>
								<div class="col-md-10">
									<select id="frmAddMedia_cmbPanelOrientation" class="form-control select" data-live-search="true" name="media_panel_orientation" required="required">
										<option disabled="disabled" selected="selected" value="">---SELECT PANEL ORIENTATION---</option>
										<option value="Vertical_or_Portrait">Vertical or Portrait</option>
										<option value="Horizontal_or_Landscape">Horizontal or Landscape</option>
									</select>
								</div>
							</div>

							<div class="form-group">
								<label class="col-md-2 control-label">Illumination</label>
								<div class="col-md-10">
									<select id="frmAddMedia_cmbIllumination" class="form-control select" data-live-search="true" name="media_illumination" required="required">
										<option disabled="disabled" selected="selected" value="">---SELECT ILLUMINATION---</option>

										<script language="javascript" type="text/javascript"> 
											for(var a=4; a<=30; a++){
												if(a % 2 == 0){
													document.write("<option value='" + a + "'>" + a + "</option>");
												}				
											}
										</script>

									</select>
								</div>
							</div>

							<div class="form-group">
								<label class="col-md-2 control-label">Structure Type</label>
								<div class="col-md-10">
									<select id="frmAddMedia_cmbStructureType" class="form-control select" data-live-search="true" name="media_structure_type" required="required">
										<option disabled="disabled" selected="selected" value="">---SELECT STRUCTURE TYPE---</option>
										<option value="Rooftop_Structure">Rooftop Structure</option>
										<option value="Wall-attached_Frame">Wall-attached Frame</option>
										<option value="Wall_Painting">Wall Painting</option>
										<option value="Freestanding">Freestanding</option>
										<option value="Wall_Sticker">Wall Sticker</option>
										<option value="Rooftop_LED">Rooftop LED</option>
										<option value="Wall-attached_LED">Wall-attached LED</option>
										<option value="Freestanding_LED">Freestanding LED</option>
									</select>
								</div>
							</div>

							<div class="form-group">
								<label class="col-md-2 control-label">Traffic Details</label>
								<div class="col-md-10">
									<select id="frmAddMedia_cmbTrafficDetails" class="form-control select" data-live-search="true" name="media_traffic_details" required="required">
										<option disabled="disabled" selected="selected" value="">---SELECT TRAFFIC DETAIL---</option>
										<option value="Northbound">Northbound</option>
										<option value="Eastbound">Eastbound</option>
										<option value="Southbound">Southbound</option>
										<option value="Westbound">Westbound</option>
									</select>
								</div>
							</div>

							<div class="form-group">
								<label class="col-md-2 control-label">Advertiser</label>
								<div class="col-md-10">
									<input 
										type="text" 
										id="frmAddMedia_txtAdvertiser" 
										class="form-control"
										placeholder="Enter advertiser..." 
										required="required"
										name="media_client" 
										style="background-color:white !important;" 
										maxlength="50"/>
								</div>
							</div>

							<div class="form-group">
								<label class="col-md-2 control-label">Contract Rate</label>
								<div class="col-md-10">
									<input 
										type="number" 
										class="form-control"
										placeholder="Enter contract rate..." 
										required="required"
										name="media_contracted_rate" 
										style="background-color:white !important;"/>
								</div>
							</div>

							<div class="alert alert-info" role="alert">
								<button type="button" class="close" data-dismiss="alert"></button>
								<strong>Instruction!</strong> Just click the textbox below to set the contract start date & contract end date.
							</div>

							<div class="form-group">
								<label class="col-md-2 control-label">Contract Start Date</label>
								<div class="col-md-10">
									<input 
										type="text" 
										id="txtMediaContractStart" 
										class="form-control" 
										name="media_contract_start" 
										placeholder="Click this textbox to set the contract start date.." 
										style="background-color:white !important;" 
										required>
								</div>
							</div>

							<div class="form-group">
								<label class="col-md-2 control-label">Contract End Date</label>
								<div class="col-md-10">
									<input 
										type="text" 
										id="txtMediaExpiration" 
										class="form-control" 
										name="media_expiration" 
										placeholder="Click this textbox to set the contract end date" 
										style="background-color:white !important;" 
										required>
								</div>
							</div>

							<div class="form-group">
								<label class="col-md-2 control-label">Billboard Images</label>
								<div class="col-md-10">
									<div class="panel panel-default">
										<div class="panel-body">
											<h3>
												<span class="fa fa-download"></span> Billboard Images
											</h3>
											
											<div id="media-images-holder"></div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>	
				</div>

				<div class="modal-footer">
					<button
						type="submit" 
						id="btnUpdateMedia"
						class="btn btn-info btn-lg btn-rounded" 
						style="padding: 10px; font-size: 15px; background-color:#1caf9a; color: white;">
						<strong>
							<span class="glyphicon glyphicon-hand-right"></span> Add New Media
						</strong>
					</button>

					<button 
						type="button" 
						class="btn btn-warning btn-rounded" 
						style="padding: 10px; font-size: 15px; background-color: orange; color: white;" 
						data-dismiss="modal">
						<strong>
							<span class="glyphicon glyphicon-remove-sign"></span> Close
						</strong>
					</button>
				</div>
			</form>

			<script>
				function implementFlatPickr(){
					document.getElementById("txtMediaContractStart").flatpickr();
					document.getElementById("txtMediaExpiration").flatpickr();
				}

				var mediaPreviewImageUploader = new qq.FineUploader({
					element: document.getElementById('media-preview-image-holder'),
					template: 'qq-template-manual-trigger_single-upload',
					validation: {
						allowedExtensions: ["jpeg","jpg","png"],
						itemLimit: 1,
                        sizeLimit: 10000 * 1024 // 10000 kB = 10000 * 1024 bytes | 10mb
					},
					request: {
						endpoint: '/add-media/add-media-preview-image',
						method: "POST"
					},
					deleteFile: {
						enabled: true,
						forceConfirm: true,
						endpoint: '/add-media/delete-media-images',
						method: "DELETE"
					},
					thumbnails: {
						placeholders: {
							waitingPath: '/resources/js/plugins/fine-uploader/placeholders/waiting-generic.png',
							notAvailablePath: '/resources/js/plugins/fine-uploader/placeholders/not_available-generic.png'
						}
					},
					autoUpload: false,
					debug: true,
					callbacks: {
						onComplete: function (id,name,responseJSON,maybeXhr){
							console.log(responseJSON);
							$('#frmAddMedia_lblMediaPreviewImage_' + id).val(responseJSON.mediaPreviewImageLink);
						},
						onCancel: function(id,name){
							console.log("cancel");
							$('#frmAddMedia_lblMediaPreviewImage_' + id).remove();
						},
						onError: function(id,name,reason,maybeXhrOrXdr){
							//logic here
						},
						onAllComplete: function(successful,failed){
							if(failed.length <= 0){
								//logic here
							}else{
								//logic here
							}
						},
						onSubmitted: function(id,name){
							$(".qq-uploader-selector").append("<input type='hidden' id='frmAddMedia_lblMediaPreviewImage_" + id + "' name='media_preview_image' value='' />");
						},
						onUpload (id,name){
							console.log(name + " before uploading...");
						},

						onSubmitDelete: function(id){
							this.setDeleteFileParams({filename: this.getName(id)}, id);
						},
						onDeleteComplete: function(id){
							$('#frmAddMedia_lblMediaPreviewImage_' + id).remove();
						}
					}
				});

				qq(document.querySelector("#media-preview-image-holder #trigger-upload")).attach("click", function () {
					mediaPreviewImageUploader.uploadStoredFiles();
				});

				var mediaImagesMultiFileUploader = new qq.FineUploader({
					element: document.getElementById('media-images-holder'),
					template: 'qq-template-manual-trigger',
					validation: {
						allowedExtensions: ["jpeg","jpg","png"],
						itemLimit: 10,
                        sizeLimit: 10000 * 1024 // 10000 kB = 10000 * 1024 bytes | 10mb
					},
					request: {
						endpoint: '/add-media/add-media-images',
						method: "POST"
					},
					deleteFile: {
						enabled: true,
						forceConfirm: true,
						endpoint: '/add-media/delete-media-images',
						method: "DELETE"
					},
					thumbnails: {
						placeholders: {
							waitingPath: '/resources/js/plugins/fine-uploader/placeholders/waiting-generic.png',
							notAvailablePath: '/resources/js/plugins/fine-uploader/placeholders/not_available-generic.png'
						}
					},
					autoUpload: false,
					debug: true,
					callbacks: {
						onComplete: function (id,name,responseJSON,maybeXhr){
							console.log(responseJSON);
							$('.frmAddMedia_lblMediaImages_' + id).val(responseJSON.mediaImageLink);
						},
						onCancel: function(id,name){
							console.log("cancel");
							$('.frmAddMedia_lblMediaImages_' + id).remove();
						},
						onError: function(id,name,reason,maybeXhrOrXdr){
							//logic here
						},
						onAllComplete: function(successful,failed){
							if(failed.length <= 0){
								//logic here
							}else{
								//logic here
							}
						},
						onSubmitted: function(id,name){
							$(".qq-uploader-selector").append("<input type='hidden' class='frmAddMedia_lblMediaImages_" + id + "' name='media_images' value='' />");
						},
						onUpload (id,name){
							console.log(name + " before uploading...");
						},

						onSubmitDelete: function(id){
							this.setDeleteFileParams({filename: this.getName(id)}, id);
						},
						onDeleteComplete: function(id){
							$('.frmAddMedia_lblMediaImages_' + id).remove();
						}
					}
				});
												
				qq(document.querySelector("#media-images-holder #trigger-upload")).attach("click", function () {
					mediaImagesMultiFileUploader.uploadStoredFiles();
				});
				
				function resetDropdown(dropdownID,value){
					$(dropdownID).val(value);
					$(dropdownID).selectpicker('refresh');
					$(dropdownID).selectpicker('render');
				}

				$('#add_new_media-modal').on('hidden.bs.modal', function () {
					$('#frmAddMedia')[0].reset();
					
					resetDropdown('#cmbMedia_group','---SELECT GROUP---');

					resetDropdown('#frmAddMedia_cmbRegion','---SELECT REGION---');
					resetDropdown('#frmAddMedia_cmbProvince','---SELECT PROVINCE---');
					resetDropdown('#frmAddMedia_cmbCity','---SELECT CITY---');

					resetDropdown('#frmAddMedia_cmbRoadPosition','---SELECT ROAD POSITION---');
					resetDropdown('#frmAddMedia_cmbPanelOrientation','---SELECT PANEL ORIENTATION---');
					resetDropdown('#frmAddMedia_cmbIllumination','---SELECT ILLUMINATION---');
					resetDropdown('#frmAddMedia_cmbStructureType','---SELECT STRUCTURE TYPE---');
					resetDropdown('#frmAddMedia_cmbTrafficDetails','---SELECT TRAFFIC DETAIL---');
					
					mediaPreviewImageUploader.cancelAll();
					mediaPreviewImageUploader.clearStoredFiles();

					mediaImagesMultiFileUploader.cancelAll();
					mediaImagesMultiFileUploader.clearStoredFiles();
				});
				
				function addMaxLengthTo(input){
					$(input).maxlength({
						alwaysShow: true,
						threshold: 10,
						warningClass: "label label-success",
						limitReachedClass: "label label-danger",
						separator: ' of ',
						preText: 'You have ',
						postText: ' chars remaining.',
						validate: true
					});
				}

				function implementBootstrapMaxLength(){
					addMaxLengthTo('input#frmAddMedia_media_name');
					addMaxLengthTo('textarea#frmAddMedia_media_description');
					addMaxLengthTo('input#frmAddMedia_media_code');
					addMaxLengthTo('textarea#frmAddMedia_media_location');
					addMaxLengthTo('input#frmAddMedia_media_size_h');
					addMaxLengthTo('input#frmAddMedia_media_size_w');
					addMaxLengthTo('input#frmAddMedia_txtAdvertiser');
				}

				implementFlatPickr();
				implementBootstrapMaxLength();

				$.getJSON("/resources/js/plugins/philippines-master/regions.json", function(response) {
					$.each(response, function(key,data) {
						$('#frmAddMedia_cmbRegion').append("<option value='" + data.key + "'>" + data.long + " (" + data.name + ")</option>");
					});

					$('#frmAddMedia_cmbRegion').selectpicker('refresh');
					$('#frmAddMedia_cmbRegion').selectpicker('render');
				});

				$("#frmAddMedia_cmbRegion").change(function(){
					$('#frmAddMedia_cmbProvince').empty();
					$('#frmAddMedia_cmbProvince').append('<option disabled="disabled" selected="selected">---SELECT PROVINCE---</option>');
					$.getJSON("/resources/js/plugins/philippines-master/provinces.json", function(response) {
						$.each(response, function(key,data) {
							if(data.region == $('#frmAddMedia_cmbRegion option:selected').val()){
								$('#frmAddMedia_cmbProvince').append("<option value='" + data.key + "'>" + data.name + "</option>");
							}
						});

						$('#frmAddMedia_cmbProvince').selectpicker('refresh');
						$('#frmAddMedia_cmbProvince').selectpicker('render');
					});
				});

				$("#frmAddMedia_cmbProvince").change(function(){
					$('#frmAddMedia_cmbCity').empty();
					$('#frmAddMedia_cmbCity').append('<option disabled="disabled" selected="selected">---SELECT CITY---</option>');
					$.getJSON("/resources/js/plugins/philippines-master/cities.json", function(response) {
						$.each(response, function(key,data) {
							if(data.province == $('#frmAddMedia_cmbProvince option:selected').val() && data.city == true){
								$('#frmAddMedia_cmbCity').append("<option value='" + data.name + "'>" + data.name + "</option>");
							}
						});

						$('#frmAddMedia_cmbCity').selectpicker('refresh');
						$('#frmAddMedia_cmbCity').selectpicker('render');
					});
				});
			</script>
		</div>
	</div>
</div>