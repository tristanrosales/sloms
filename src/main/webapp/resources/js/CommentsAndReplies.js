var stompClient = null;

function connectWebSocket(){
    var socket = new SockJS('/sloms_web_socket');
    stompClient = Stomp.over(socket);

    stompClient.connect({}, function(frame) {
		stompClient.subscribe("/sloms/errors", function(message) {
            console.log("Web Socket Error Message : " + message.body);
            connectWebSocket();
        });
        
		stompClient.subscribe("/sloms/comment", function(message) {
			viewComments($('#frmAddComment_lblThread_id').val());
		});
		
		stompClient.subscribe("/sloms/reply", function(message) {
			//viewComments($('#frmAddComment_lblThread_id').val());
			viewReplies($('#frmAddReply_lblThread_comment_id').val());
		});
	},function(error) {
        console.log("Web Socket Error Message : " + error);
    });
}

connectWebSocket();

var comments = "";
var replies = "";

$("#frmAddComment").validate({
	ignore : [],
	rules : {
		thread_comment: {
			required : true,
			maxlength : 5000
		}
	},
	submitHandler: function(form) {
		addComment(form);
	}
});

$("#frmAddReply").validate({
	ignore : [],
	rules : {
		thread_reply: {
			required : true,
			maxlength : 5000
		}
	},
	submitHandler: function(form) {
		addReply(form);
	}
});

$('body').on('click', '#btnCloseCommentsModal',function(event){
	$('li#comments').empty();
});

$('body').on('click', '#btnCloseRepliesModal',function(event){
	$('li#view-replies_comments').empty();
});

function viewComments(data_id){
	comments = "";

	$.ajax({
		url: "/thread_comments/" + data_id,
		dataType: 'json',
		mimeType: 'json',
		success: function(response) {
			if(response.length <= 0){
				$('#emptyComment').css("display","inline");
				$('#btnAddNewComment').css("display","inline");
			}else{
				$('#emptyComment').css("display","none");
				$('#btnAddNewComment').css("display","inline");
			}
			
			if(response[0].thread_id.thread_status == "Closed"){
				$('#btnAddNewComment').css("display","none");
			}else{
				$('#btnAddNewComment').css("display","inline");
			}

			$.each(response, function(i, data){
				var imageSrc = '';

				if(data.thread_comment_by.user_profile_picture == null || data.thread_comment_by.user_profile_picture == ""){
					imageSrc = 'resources/assets/images/users/no-image.jpg';
				}else{
					imageSrc = '/SLOMS_server' + data.thread_comment_by.user_profile_picture;
				}

				comments += "" +
					"<li class='media' id='comments'>" +
						"<a class='pull-left' href='#'> " +
							"<img class='media-object img-text' " +
								"src='" + imageSrc + "' " +
								"alt='" + data.thread_comment_by.user_firstname + " " + data.thread_comment_by.user_lastname + "' width='64'>" +
						"</a>" +
						"" +
						"<div class='media-body'>" +
							"<h4 class='media-heading'>" + data.thread_comment_by.user_firstname + " " + data.thread_comment_by.user_lastname + "</h4>" +
							"<p>" + data.thread_comment + "</p>" +
							"<div class='gallery' id='links_thread-comment-images'>";

							for (var i = 0; i < data.thread_comment_images.length; i++) {
								if(data.thread_comment_images[i] != ""){
									console.log(data.thread_comment_images[i]);
					
									var image = document.createElement("img");
									image.setAttribute("src", "/SLOMS_server" + data.thread_comment_images[i]);
								
									comments += "<a href='/SLOMS_server" + data.thread_comment_images[i] + "' class='gallery-item' data-gallery><div class='image'><img src='/SLOMS_server" + data.thread_comment_images[i] + "' style='max-height: 70px; height: 70px; max-width: 100px; width: 100px;'/></div></a>";
								}
							}

							comments += "</div>";

				comments +=	"<p class='text-muted'>" + moment(new Date(data.thread_dateAndTime_posted)).format('LLLL') + "</p>" +
							"<p class='text-muted'>" +
								"<a data-toggle='modal' data-id='" + data.thread_comment_id + "' data-target='#edit_comment-modal' " +
									"style='cursor: pointer; display: none;' id='btnEditComment'>Edit</a>&nbsp;&nbsp;&nbsp;" +
									"";

									if(data.thread_id.thread_status == "Open"){
										comments += "<a data-toggle='modal' data-id='" + data.thread_comment_id + "' data-target='#add_new_reply-modal' " +
										"style='cursor: pointer;' id='btnReply'>Reply</a>";
									}

								var viewReplyText = "";
								
								if(data.threadCommentReplies.length == 1){
									viewReplyText = "View " + data.threadCommentReplies.length + " reply";

									comments +=	" | <a data-toggle='modal' data-id='" + data.thread_comment_id + "' data-target='#view_replies-modal' " +
										"style='cursor: pointer;' id='btnViewReplies'>" + viewReplyText + "</a>";

								}else if(data.threadCommentReplies.length > 1){
									viewReplyText = "View " + data.threadCommentReplies.length + " replies";

									comments +=	" | <a data-toggle='modal' data-id='" + data.thread_comment_id + "' data-target='#view_replies-modal' " +
										"style='cursor: pointer;' id='btnViewReplies'>" + viewReplyText + "</a>";
								}

				comments += "</p>" +
						"</div>" +
					"</li>";
			});
			
			$('ul#comments').html(comments);
		}
	});
}

//view comments
$('body').on('click', '#btnViewComments',function(){
	viewComments($(this).attr('data-id'));
});

function viewReplies(data_id){
	comments = "";
	replies = "";
	
	var thread_comment_id = data_id;

	$('#view-replies_btnAddNewReply').prop("data-id",thread_comment_id);

	$.ajax({
		url: "/thread_comment_replies/" + thread_comment_id,
		dataType: 'json',
		mimeType: 'json',
		success: function(response) {
			if(response.length <= 0){
				$('#emptyReply').css("display","inline");
				//$('#btnAddNewComment').css("display","inline");
			}else{
				$('#emptyReply').css("display","none");
				//$('#btnAddNewComment').css("display","inline");
			}

			var thread_comment_by_imageSrc = '';

			if(response[0].thread_comment_id.thread_comment_by.user_profile_picture == null || response[0].thread_comment_id.thread_comment_by.user_profile_picture == ""){
				thread_comment_by_imageSrc = 'resources/assets/images/users/no-image.jpg';
			}else{
				thread_comment_by_imageSrc = '/SLOMS_server' + response[0].thread_comment_id.thread_comment_by.user_profile_picture;
			}

			comments += "" +
					"<li class='media' id='view-replies_comments'>" +
						"<a class='pull-left' href='#'> " +
							"<img class='media-object img-text' " +
								"src='" + thread_comment_by_imageSrc + "' " +
								"alt='" + response[0].thread_comment_id.thread_comment_by.user_firstname + " " + response[0].thread_comment_id.thread_comment_by.user_lastname + "' width='64'>" +
						"</a>" +
						"" +
						"<div class='media-body'>" +
							"<h4 class='media-heading'>" + response[0].thread_comment_id.thread_comment_by.user_firstname + " " + response[0].thread_comment_id.thread_comment_by.user_lastname + "</h4>" +
							"<p>" + response[0].thread_comment_id.thread_comment + "</p>" +
							"<div class='gallery' id='links_thread-comment-images'>";

							for (var i = 0; i < response[0].thread_comment_id.thread_comment_images.length; i++) {
								if(response[0].thread_comment_id.thread_comment_images[i] != ""){
									console.log(response[0].thread_comment_id.thread_comment_images[i]);
					
									var image = document.createElement("img");
									image.setAttribute("src", "/SLOMS_server" + response[0].thread_comment_id.thread_comment_images[i]);
								
									comments += "<a href='/SLOMS_server" + response[0].thread_comment_id.thread_comment_images[i] + "' class='gallery-item' data-gallery><div class='image'><img src='/SLOMS_server" + response[0].thread_comment_id.thread_comment_images[i] + "' style='max-height: 70px; height: 70px; max-width: 100px; width: 100px;'/></div></a>";
								}
							}

							comments += "</div>";

				comments +=	"<p class='text-muted'>" + moment(new Date(response[0].thread_comment_id.thread_dateAndTime_posted)).format('LLLL') + "</p>" +
							"<p class='text-muted'>" +
								"<a data-toggle='modal' data-id='" + response[0].thread_comment_id.thread_comment_id + "' data-target='#edit_comment-modal' " +
									"style='cursor: pointer; display: none;' id='btnEditComment'>Edit</a>&nbsp;&nbsp;&nbsp;";

								if(response[0].thread_comment_id.thread_id.thread_status == "Open"){
									comments += "<a data-toggle='modal' data-id='" + response[0].thread_comment_id.thread_comment_id + "' data-target='#add_new_reply-modal' " +
									"style='cursor: pointer;' id='btnReply'>Reply</a>";
								}
								
				comments += "</p>" +
						"<div class='media' id='replies'></div>" +
						"</div>" +
					"</li>";

				$('ul#replies').html(comments);

			$.each(response, function(i, thread_comment_replies_data){

				var thread_reply_by_imageSrc = '';

				if(thread_comment_replies_data.thread_reply_by.user_profile_picture == null || thread_comment_replies_data.thread_reply_by.user_profile_picture == ""){
					thread_reply_by_imageSrc = 'resources/assets/images/users/no-image.jpg';
				}else{
					thread_reply_by_imageSrc = '/SLOMS_server' + thread_comment_replies_data.thread_reply_by.user_profile_picture;
				}

				replies += "" +
						"<a class='pull-left' href='#'> " +
							"<img " +
								"class='media-object img-text' " +
								"src='" + thread_reply_by_imageSrc + "' " +
								"alt='" + thread_comment_replies_data.thread_reply_by.user_firstname + " " + thread_comment_replies_data.thread_reply_by.user_lastname + "' width='64'>" +
						"</a>" +
						"" +
						"<div class='media-body'>" +
							"<h4 class='media-heading'>" + thread_comment_replies_data.thread_reply_by.user_firstname + " " + thread_comment_replies_data.thread_reply_by.user_lastname + "</h4>" +
							"<p>" + thread_comment_replies_data.thread_reply + "</p>" +
							"<div class='gallery' id='links_thread-comment-reply-images'>";

							for (var i = 0; i < thread_comment_replies_data.thread_comment_reply_images.length; i++) {
								if(thread_comment_replies_data.thread_comment_reply_images[i] != ""){
									console.log(thread_comment_replies_data.thread_comment_reply_images[i]);
					
									var image = document.createElement("img");
									image.setAttribute("src", "/SLOMS_server" + thread_comment_replies_data.thread_comment_reply_images[i]);
								
									replies += "<a href='/SLOMS_server" + thread_comment_replies_data.thread_comment_reply_images[i] + "' class='gallery-item' data-gallery><div class='image'><img src='/SLOMS_server" + thread_comment_replies_data.thread_comment_reply_images[i] + "' style='max-height: 70px; height: 70px; max-width: 100px; width: 100px;'/></div></a>";
								}
							}

							replies += "</div>";

				replies += "<p class='text-muted'>" + moment(new Date(thread_comment_replies_data.thread_dateAndTime_replied)).format('LLLL') + "</p>" +
							"<p class='text-muted'>" +
								"<a " +
									"data-toggle='modal' " +
									"data-id='" + thread_comment_replies_data.thread_reply_id + "' " +
									"data-target='#edit_reply-modal' " +
									"style='cursor: pointer; display: none;' id='btnEditReply'>Edit</a>&nbsp;&nbsp;&nbsp;" +
							"</p>" +
						"</div>";
			});	
			
			$('div#replies').html(replies);
		}
	});
}

//view replies
$('body').on('click', '#btnViewReplies',function(){
	viewReplies($(this).attr('data-id'));
});

//click reply button
$('body').on('click', '#btnReply',function(){
	$.ajax({
		url: "/thread_comment/" + $(this).attr('data-id'),
		dataType: 'json',
		mimeType: 'json',
		success: function(data) {
			$('#lblReply_to').text(data.thread_comment_by.user_firstname + " " + data.thread_comment_by.user_lastname);
			$('#frmAddReply_lblThread_comment_id').val(data.thread_comment_id);
			
			$('#frmEditDeleteComment_lblThread_comment_id').val(data.thread_comment_id);
			$('#frmEditDeleteComment_lblThread_id').val(data.thread_id.thread_id);
			$('#frmEditDeleteComment_lblThread_commentBy').val(data.thread_comment_by.user_id);
			$('#frmEditDeleteComment_lblThread_dateAndTime_posted').val(moment(new Date(data.thread_dateAndTime_posted)).format('LLLL'));
			
			$('#frmEditDeleteComment_txtThread_comment').val(data.thread_comment);
		},
		error: function(e){
			console.log(e);
		}
	});
});

//click edit comment button
$('body').on('click', '#btnEditComment',function(){
	$.ajax({
		url: "/thread_comment/" + $(this).attr('data-id'),
		dataType: 'json',
		mimeType: 'json',
		success: function(data) {
			$('#lblReply_to').text(data.thread_comment_by.user_firstname + " " + data.thread_comment_by.user_lastname);
			$('#frmAddReply_lblThread_comment_id').val(data.thread_comment_id);
			
			$('#frmEditDeleteComment_lblThread_comment_id').val(data.thread_comment_id);
			$('#frmEditDeleteComment_lblThread_id').val(data.thread_id.thread_id);
			$('#frmEditDeleteComment_lblThread_commentBy').val(data.thread_comment_by.user_id);
			$('#frmEditDeleteComment_lblThread_dateAndTime_posted').val(moment(new Date(data.thread_dateAndTime_posted)).format('LLLL'));
			
			$('#frmEditDeleteComment_txtThread_comment').val(data.thread_comment);
		},
		error: function(e){
			console.log(e);
		}
	});
});

//click edit reply button
$('body').on('click', '#btnEditReply',function(){
	$.ajax({
		url: "/thread_comment_reply/" + $(this).attr('data-id'),
		dataType: 'json',
		mimeType: 'json',
		success: function(response) {
			$('#frmEditDeleteReply_lblThread_reply_id').val(response.thread_reply_id);
			$('#frmEditDeleteReply_lblThread_comment_id').val(response.thread_comment_id.thread_comment_id);
			$('#frmEditDeleteReply_lblThread_reply_by').val(response.thread_reply_by.user_id);
			$('#frmEditDeleteReply_lblThread_dateAndTime_replied').val(moment(new Date(response.thread_dateAndTime_replied)).format('LLLL'));
			
			$('#frmEditDeleteReply_txtThread_reply').val(response.thread_reply);
		},
		error: function(e){
			console.log(e);
		}
	});
});

function addComment(form){
	//event.preventDefault();
	var formEl = $(form);
	
	swal({
		title: "Do you still want to continue?",
		icon: "warning",
		buttons: ["No!", "Yes!"],
		dangerMode: true
	})
	.then((willAdd) => {
		if (willAdd) {
			$('body').loadingModal({
				position: 'auto',
				text: 'Please wait while sending your comment...',
				color: '#fff',
				opacity: '0.7',
				backgroundColor: 'rgb(0,0,0)',
				animation: 'doubleBounce'
			});

			$.ajax({
				type: 'POST',
				dataType : 'JSON',
				url: formEl.prop('action'),
				data: formEl.serialize(),
				success: function(data) {
                    $('body').loadingModal('destroy');

					if(data.isAddingCommentSuccess){
						iziToast.success({
							title: 'Congratulations!',
							theme: 'light', // dark
							color: 'green', // blue, red, green, yellow
							position: 'bottomRight', // bottomRight, bottomLeft, topRight, topLeft, topCenter, bottomCenter, center
							message: "Comment has been added successfully!"
						});

						$("#add_new_comment-modal").modal('toggle');
						//viewComments($('#frmAddComment_lblThread_id').val());
					}
				},
				error: function(e){
					console.log(e);
                    $('body').loadingModal('destroy');
                    iziToast.error({
                        title: "Oops! We're sorry!",
                        theme: 'light',
                        color: 'red',
                        position: 'bottomRight',
                        message: "Cannot send your comment. Please try again!"
                    });
				}
			});
		}
	});
}

//edit and delete comment
$('.frmEditDeleteComment').submit(function(event) {
    event.preventDefault();
    var formEl = $(this);
    
    swal({
		  title: "Are you sure?",
		  text: "Once updated, you will not be able to undo this action!",
		  icon: "warning",
		  buttons: ["No, not sure yet!", "Yes, I'm Sure!"],
		  dangerMode: true
	})
	.then((willUpdate) => {
		if (willUpdate) {
			$.ajax({
                type: 'POST',
                dataType : 'JSON',
                url: "/frmEditComment",
                data: formEl.serialize(),
                // if received a response from the server
                success: function( data, textStatus, jqXHR) {
					 if(data.status){
						 swal("Comment has been updated successfully!", {
		          		    icon: "success"
		          		 }).then((willUpdate) => {
		          			 if(willUpdate){
		          				location.reload(); 
		          			 }
		          		 });
					 }
                },
                error: function(e){
					console.log(e.responseText);
				}
            });
		}
	});
});

function deleteComment(){
	$('body').on('click', '#btnDeleteComment',function(){	
		swal({
			  title: "Are you sure?",
			  text: "Once deleted, you will not be able to recover this group!",
			  icon: "warning",
			  buttons: ["No, not sure yet!", "Yes, I'm Sure!"],
			  dangerMode: true,
		})
		.then((willDelete) => {
			if (willDelete) {
				$.ajax({
	              type: 'POST',
	              dataType : 'JSON',
	              url: "/frmDeleteComment",
	              data: $('.frmEditDeleteComment').serialize(),
	              // if received a response from the server
	              success: function( data, textStatus, jqXHR) {
						 if(data.status){
							 swal("Comment has been deleted successfully!", {
			          		    icon: "success"
			          		 }).then((willDelete) => {
			          			 if(willDelete){
			          				location.reload(); 
			          			 }
			          		 });
						 }
	              },
				  error: function(e){
					console.log(e.responseText);
				}
	          });
			}
		});
	});
}

function addReply(form){
	var formEl = $(form);

	swal({
		title: "Do you still want to continue?",
		icon: "warning",
		buttons: ["No!", "Yes!"],
		dangerMode: true
	})
	.then((willAdd) => {
		if (willAdd) {
			$('body').loadingModal({
				position: 'auto',
				text: 'Please wait while sending your reply...',
				color: '#fff',
				opacity: '0.7',
				backgroundColor: 'rgb(0,0,0)',
				animation: 'doubleBounce'
			});

			$.ajax({
				type: 'POST',
				dataType : 'JSON',
				url: formEl.prop('action'),
				data: formEl.serialize(),
				success: function(data) {
					$('body').loadingModal('destroy');

					if(data.isAddingReplySuccess){
						iziToast.success({
							title: 'Congratulations!',
							theme: 'light', // dark
							color: 'green', // blue, red, green, yellow
							position: 'bottomRight', // bottomRight, bottomLeft, topRight, topLeft, topCenter, bottomCenter, center
							message: "Reply has been added successfully!"
						});

						$("#add_new_reply-modal").modal('toggle');
						//viewComments($('#frmAddComment_lblThread_id').val());
						//viewReplies($('#frmAddReply_lblThread_comment_id').val());
					}
				},
				error: function(e){
					console.log(e);
                    $('body').loadingModal('destroy');
                    iziToast.error({
                        title: "Oops! We're sorry!",
                        theme: 'light',
                        color: 'red',
                        position: 'bottomRight',
                        message: "Cannot send your reply. Please try again!"
                    });
				}
			});
		}
	});
}

//edit and delete reply
$('.frmEditDeleteReply').submit(function(event) {
    event.preventDefault();
    var formEl = $(this);
    
    swal({
		  title: "Are you sure?",
		  text: "Once updated, you will not be able to undo this action!",
		  icon: "warning",
		  buttons: ["No, not sure yet!", "Yes, I'm Sure!"],
		  dangerMode: true
	})
	.then((willUpdate) => {
		if (willUpdate) {
			$.ajax({
                type: 'POST',
                dataType : 'JSON',
                url: "/frmEditReply",
                data: formEl.serialize(),
                // if received a response from the server
                success: function( data, textStatus, jqXHR) {
					if(data.status){
						swal("Reply has been updated successfully!", {
						icon: "success"
						}).then((willUpdate) => {
							if(willUpdate){
							location.reload(); 
							}
						});
					}
                },
                error: function(e){
					console.log(e.responseText);
				}
            });
		}
	});
});

function deleteReply(){
	$('body').on('click', '#btnDeleteReply',function(){	
		swal({
			  title: "Are you sure?",
			  text: "Once deleted, you will not be able to recover this group!",
			  icon: "warning",
			  buttons: ["No, not sure yet!", "Yes, I'm Sure!"],
			  dangerMode: true,
		})
		.then((willDelete) => {
			if (willDelete) {
				$.ajax({
	              type: 'POST',
	              dataType : 'JSON',
	              url: "/frmDeleteReply",
	              data: $('.frmEditDeleteReply').serialize(),
	              // if received a response from the server
					success: function( data, textStatus, jqXHR) {
							if(data.status){
								swal("Reply has been deleted successfully!", {
									icon: "success"
								}).then((willDelete) => {
									if(willDelete){
										location.reload(); 
									}
								});
							}
					},
					error: function(e){
						console.log(e.responseText);
					}
	          });
			}
		});
	});
}