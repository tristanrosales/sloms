var stompClient = null;

function connectWebSocket(){
    var socket = new SockJS('/sloms_web_socket');
    stompClient = Stomp.over(socket);

    stompClient.connect({}, function(frame) {
		stompClient.subscribe("/sloms/errors", function(message) {
            iziToast.error({
                title: 'Oops!',
                theme: 'light', // dark
                color: 'blue', // blue, red, green, yellow
                position: 'bottomRight', // bottomRight, bottomLeft, topRight, topLeft, topCenter, bottomCenter, center
                message: 'Error: ' + message.body
            });
            connectWebSocket();
        });
        
		stompClient.subscribe("/sloms/all_media", function(message) {
			//refreshMedia();
		});

		stompClient.subscribe("/sloms/available_media", function(message) {
			//refreshMedia();
		});
	},function(error) {
		iziToast.error({
            title: 'Oops!',
            theme: 'light', // dark
            color: 'blue', // blue, red, green, yellow
            position: 'bottomRight', // bottomRight, bottomLeft, topRight, topLeft, topCenter, bottomCenter, center
            message: 'STOMP error: ' + error
        });
    });
}

connectWebSocket();

var all_media_count = 0;
var available_media_count = 0;

var live_contract_count = 0;
var soon_to_expire_count = 0;
var needs_update_count = 0;

function generateAllMediaExpiringInNDays(data,rows){
	console.log("Media Name: " + data.media_name);

	var allMediaExpiringInNDays_body = "<li value='" + data.media_name + " (" + data.media_code + ")'>";

	allMediaExpiringInNDays_body += "<a href='/media_details?media_id=" + data.media_id + "'>" + data.media_name + " <b>(" + data.media_code + ")</b></a>";

	allMediaExpiringInNDays_body += "</li>";

	$(rows).append(allMediaExpiringInNDays_body);
}

function generateAllMediaNeedsUpdateInNDays(data,rows){
	var allMediaNeedsUpdateInNDays_body = "<li value='" + data.media_name + " (" + data.media_code + ")'>";

		allMediaNeedsUpdateInNDays_body += "<a href='/media_details?media_id=" + data.media_id + "'>" + data.media_name + " <b>(" + data.media_code + ")</b></a>";

	allMediaNeedsUpdateInNDays_body += "</li>";

	$(rows).append(allMediaNeedsUpdateInNDays_body);
}

function addConditionInExpiringMedia(data,days){
	if(days == 15){
		console.log(days);

		if(moment(data.mediaClientDetails.media_expiration,"YYYY-MM-DD").diff(moment(),'days') <= days){
			soon_to_expire_count++;
			generateAllMediaExpiringInNDays(data,'ol#orderedList_expiring_' + days + 'days');
			$('#expiring_in_' + days + 'days_no_data_available').css("display","none");
			$('#email_btn_expiring_in_' + days + 'days').css("display","inline");
			$('#sms_btn_expiring_in_' + days + 'days').css("display","inline");
		}
	}else if(days == 30){
		console.log(days + " to " + (days + 15));

		if(moment(data.mediaClientDetails.media_expiration,"YYYY-MM-DD").diff(moment(),'days') > 15 && moment(data.mediaClientDetails.media_expiration,"YYYY-MM-DD").diff(moment(),'days') <= 30){
			soon_to_expire_count++;
			generateAllMediaExpiringInNDays(data,'ol#orderedList_expiring_' + days + 'days');
			$('#expiring_in_' + days + 'days_no_data_available').css("display","none");
			$('#email_btn_expiring_in_' + days + 'days').css("display","inline");
			$('#sms_btn_expiring_in_' + days + 'days').css("display","inline");
		}
	}else{
		console.log(days + " to " + (days + 15));

		if(moment(data.mediaClientDetails.media_expiration,"YYYY-MM-DD").diff(moment(),'days') > days && moment(data.mediaClientDetails.media_expiration,"YYYY-MM-DD").diff(moment(),'days') <= (days + 15)){
			soon_to_expire_count++;
			generateAllMediaExpiringInNDays(data,'ol#orderedList_expiring_' + days + 'days');
			$('#expiring_in_' + days + 'days_no_data_available').css("display","none");
			$('#email_btn_expiring_in_' + days + 'days').css("display","inline");
			$('#sms_btn_expiring_in_' + days + 'days').css("display","inline");
		}
	}
}

function loadSoonToExpire(data){
	for(var a=15; a<=90; a++){
		if(a % 15 == 0 && a != 90){
			addConditionInExpiringMedia(data,a);
		}
	}
}

function addConditionInNeedsUpdate(data,days){
	if(data.threads.length <= 0 && (moment(data.mediaClientDetails.media_expiration,"YYYY-MM-DD").diff(moment(new Date(data.media_dateAndTime_created)).add(days, 'days').format('LL'),'days')) > 0){
		needs_update_count++;
		generateAllMediaNeedsUpdateInNDays(data,'ol#orderedList_needsUpdate_' + days + 'days');
		$('#needs_update_in_' + days + 'days_no_data_available').css("display","none");

		$('#email_btn_needs_update_in_' + days + 'days').css("display","inline");
		$('#sms_btn_needs_update_in_' + days + 'days').css("display","inline");
	}
}

function loadNeedsUpdate(data){
	for(var a=15; a<=90; a++){
		if(a % 15 == 0){
			addConditionInNeedsUpdate(data,a);
		}
	}
}

function sendEmailNotificationToExpiringMediaInDays(days){
	var media = [];
	var output = '';

	$('ol#orderedList_expiring_' + days + 'days li').each(function (i) {
        var index = $(this).index();
        var text = $(this).text();
        var value = $(this).attr('value');
		
		media.push("-" + value + "\n");
	});

	for (var i = 0; i < media.length; i++) { 
		output += media[i];
	}

	submitEmailOrSMSForExpiringMedia(days,output,"/send-email_expiring-media");
}

function sendSMSNotificationToExpiringMediaInDays(days){
	var media = [];
	var output = '';

	$('ol#orderedList_expiring_' + days + 'days li').each(function (i) {
        var index = $(this).index();
        var text = $(this).text();
        var value = $(this).attr('value');
		
		media.push("-" + value + "\n");
	});

	for (var i = 0; i < media.length; i++) { 
		output += media[i];
	}

	submitEmailOrSMSForExpiringMedia(days,output,"/send-sms_expiring-media");
}

function submitEmailOrSMSForExpiringMedia(days,output,url){
	var values = {
		'days': days,
		'output': output
	};

	swal({
		title: "Are you sure?",
		text: "Once sended, you will not be able to undo this action!",
		icon: "warning",
		buttons: ["No, not sure yet!", "Yes, I'm Sure!"],
		dangerMode: true
	})
	.then((willLoad) => {
		if (willLoad) {
			$('body').loadingModal({
				position: 'auto',
				text: 'Please wait while notification is sending :)',
				color: '#fff',
				opacity: '0.7',
				backgroundColor: 'rgb(0,0,0)',
				animation: 'doubleBounce'
			});

			$.ajax({
				url: url,
				type: "POST",
				dataType : 'JSON',
				data: values,
				success: function(data) {
					$('body').loadingModal('hide');

					if(data.isEmailSent){
						iziToast.success({
							title: 'Congratulations!',
							theme: 'light', // dark
							color: 'green', // blue, red, green, yellow
							position: 'bottomRight', // bottomRight, bottomLeft, topRight, topLeft, topCenter, bottomCenter, center
							message: "Notification about expiring media has been sent successfully!"
						});
					}else{
						iziToast.error({
							title: 'Oops!',
							theme: 'light', // dark
							color: 'red', // blue, red, green, yellow
							position: 'bottomRight', // bottomRight, bottomLeft, topRight, topLeft, topCenter, bottomCenter, center
							message: "Notification not sent successfully!"
						});
					}
				},
				error: function(e){
					console.log(e);
				}
			});
		}
	});
}

function sendEmailNotificationToNeedsUpdateInDays(days){
	var media = [];
	var output = '';
	
	$('ol#orderedList_needsUpdate_' + days + 'days li').each(function (i) {
        var index = $(this).index();
        var text = $(this).text();
        var value = $(this).attr('value');
		media.push("-" + value + "\n");
	});

	for (var i = 0; i < media.length; i++) { 
		output += media[i];
	}

	submitEmailForNeedsUpdateMedia(days,output);
}

function sendSMSNotificationToNeedsUpdateInDays(days){
	var media = [];
	var output = '';
	
	$('ol#orderedList_needsUpdate_' + days + 'days li').each(function (i) {
        var index = $(this).index();
        var text = $(this).text();
        var value = $(this).attr('value');
		media.push("-" + value + "\n");
	});

	for (var i = 0; i < media.length; i++) { 
		output += media[i];
	}

	submitSMSForNeedsUpdateMedia(days,output);
}

function submitEmailForNeedsUpdateMedia(days,output){
	var values = {
		'days': days,
		'output': output
	};

	swal({
		title: "Are you sure?",
		text: "Once sended, you will not be able to undo this action!",
		icon: "warning",
		buttons: ["No, not sure yet!", "Yes, I'm Sure!"],
		dangerMode: true
	})
	.then((willLoad) => {
		if (willLoad) {
			$('body').loadingModal({
				position: 'auto',
				text: 'Please wait while email notification is sending :)',
				color: '#fff',
				opacity: '0.7',
				backgroundColor: 'rgb(0,0,0)',
				animation: 'doubleBounce'
			});

			$.ajax({
				url: "/send-email_needs-update-media",
				type: "POST",
				dataType : 'JSON',
				data: values,
				success: function(data) {
					$('body').loadingModal('hide');

					if(data.isEmailSent){
						iziToast.success({
							title: 'Congratulations!',
							theme: 'light', // dark
							color: 'green', // blue, red, green, yellow
							position: 'bottomRight', // bottomRight, bottomLeft, topRight, topLeft, topCenter, bottomCenter, center
							message: "Email notification has been sent successfully!"
						});
					}else{
						iziToast.error({
							title: 'Oops!',
							theme: 'light', // dark
							color: 'red', // blue, red, green, yellow
							position: 'bottomRight', // bottomRight, bottomLeft, topRight, topLeft, topCenter, bottomCenter, center
							message: "Email not sent successfully!"
						});
					}
				},
				error: function(e){
					console.log(e);
				}
			});
		}
	});
}

function submitSMSForNeedsUpdateMedia(days,output){
	var values = {
		'days': days,
		'output': output
	};

	swal({
		title: "Are you sure?",
		text: "Once sended, you will not be able to undo this action!",
		icon: "warning",
		buttons: ["No, not sure yet!", "Yes, I'm Sure!"],
		dangerMode: true
	})
	.then((willLoad) => {
		if (willLoad) {
			$('body').loadingModal({
				position: 'auto',
				text: 'Please wait while sms notification is sending :)',
				color: '#fff',
				opacity: '0.7',
				backgroundColor: 'rgb(0,0,0)',
				animation: 'doubleBounce'
			});

			$.ajax({
				url: "/send-sms_needs-update-media",
				type: "POST",
				dataType : 'JSON',
				data: values,
				success: function(data) {
					$('body').loadingModal('hide');

					if(data.isSMSSent){
						iziToast.success({
							title: 'Congratulations!',
							theme: 'light', // dark
							color: 'green', // blue, red, green, yellow
							position: 'bottomRight', // bottomRight, bottomLeft, topRight, topLeft, topCenter, bottomCenter, center
							message: "SMS notification has been sent successfully!"
						});
					}else{
						iziToast.error({
							title: 'Oops!',
							theme: 'light', // dark
							color: 'red', // blue, red, green, yellow
							position: 'bottomRight', // bottomRight, bottomLeft, topRight, topLeft, topCenter, bottomCenter, center
							message: "SMS not sent successfully!"
						});
					}
				},
				error: function(e){
					console.log(e);
				}
			});
		}
	});
}

function showAllMedia(){
	$('#all_media_row').html('');

	$.ajax({
		url: '/media',
		dataType: 'json',
		mimeType: 'json',
		success: function(response) {
	
			if(response.length <= 0){
				$('#all-media_no_data_available').css("display","inline");
				$('#for-update_no_data_available').css("display","inline");
			}else{
				$('#all-media_no_data_available').css("display","none");
				$('#for-update_no_data_available').css("display","none");
			}
	
			$.each(response, function(i, data) {
				var body = "<div class='col-md-6'>";
				
				body += "<div class='post-item'>" +
						"<div class='post-title'>" +
							"<a href='/media_details?media_id=" + data.media_id + "'>" + data.media_name + "</a>" +
						"</div>" +
						"<div class='post-date'>" +
							"<span class='fa fa-calendar'></span> " + moment(new Date(data.media_dateAndTime_created)).format('LLLL') + " / " +
							"by <a href='pages-profile.html'>" + data.media_created_by.user_firstname + " " + data.media_created_by.user_lastname + "</a>" +
						"</div>" +
						"<div class='post-text'>";
	
						if(data.media_preview_image === "" || data.media_preview_image === null){
							body += "<img src='/resources/img/no-image-available.jpg' class='img-responsive img-text' style='margin: 0 auto; max-height: 220px; height: 220px; max-width: 270px; width: 270px;'>";
						}else{
							body += "<img src='/SLOMS_server" + data.media_preview_image + "' class='img-responsive img-text' style='margin: 0 auto; max-height: 220px; height: 220px; max-width: 270px; width: 270px;'/>";
						}
							
				body += "<p style='text-align: justify; text-justify: inter-word; text-overflow: ellipsis; display: -webkit-box; overflow: hidden; -webkit-line-clamp: 2; -webkit-box-orient: vertical; line-height: 16px; max-height: 32px;'>" + data.mediaDetails.media_description + "</p>";
	
						if(moment(data.mediaClientDetails.media_expiration,"YYYY-MM-DD").diff(moment(),'days') <= 0){
							body += "<p style='color: red;'><b>EXPIRED</b></p>";
						}else if(moment(data.mediaClientDetails.media_expiration,"YYYY-MM-DD").diff(moment(),'days') == 1){
							body += "<p>Expiring in <b>" + moment(data.mediaClientDetails.media_expiration,"YYYY-MM-DD").diff(moment(),'days') + " day</b></p>";
						}else{
							body += "<p>Expiring in <b>" + moment(data.mediaClientDetails.media_expiration,"YYYY-MM-DD").diff(moment(),'days') + " days</b></p>";
						}
	
				body +=	"</div>" +
						"<div class='post-row'>" +
							"<a href='/media_details?media_id=" + data.media_id + "'><button class='btn btn-default btn-rounded pull-right'>Read more &RightArrow;</button></a>" +
						"</div>" +
						"</div>" +
						"</div>";
							
				$("#all_media_row").append(body);
	
				if(!moment(data.mediaClientDetails.media_expiration,"YYYY-MM-DD").diff(moment(),'days') <= 0){
					live_contract_count++;
				}
				
				loadSoonToExpire(data);
				loadNeedsUpdate(data);
	
				all_media_count++;
	
				/*
				if(data.threads.length <= 0){
					$("#for_update_row").append(body);
				}
				*/
	
				console.log("Media Date Created: " + moment(new Date(data.media_dateAndTime_created)).format('LL'))
				console.log("Media Date Created + 15 days: " + moment(new Date(data.media_dateAndTime_created)).add(15, 'days').format('LL'));
				console.log("Media Expiration: " + moment(data.mediaClientDetails.media_expiration,"YYYY-MM-DD").format('LL'));
	
				console.log("Remaining days: " + moment(data.mediaClientDetails.media_expiration,"YYYY-MM-DD").diff(moment(new Date(data.media_dateAndTime_created)).add(15, 'days').format('LL'),'days'));
			});
	
			$('#all_media_row').easyPaginate({
				paginateElement: 'div#all_media',
				elementsPerPage: 2,
				effect: 'climb',
				firstButtonText: 'First',
				lastButtonText: 'Last',
				prevButtonText: 'Previous',
				nextButtonText: 'Next'
			});
	
			$('#all_media_count').text(all_media_count);
			$('#live_contract_count').text(live_contract_count);
			$('#soon_to_expire_count').text(soon_to_expire_count);
			$('#needs_update_count').text(needs_update_count);
		},
		error: function(e){
			console.log(e);
		}
	});
}

function showAllAvailableMedia(){
	$('#available_media_row').html('');

	$.ajax({
		url: '/available_media',
		dataType: 'json',
		mimeType: 'json',
		success: function(response) {
	
			if(response.length <= 0){
				$('#available-media_no_data_available').css("display","inline");
			}else{
				$('#available-media_no_data_available').css("display","none");
			}
	
			$.each(response, function(i, data) {
				var body = "<div class='col-md-6' id='available_media'>";
				
				body += "<div class='post-item'>" +
						"<div class='post-title'>" +
							"<a href='/media_details?media_id=" + data.media_id + "'>" + data.media_name + "</a>" +
						"</div>" +
						"<div class='post-date'>" +
							"<span class='fa fa-calendar'></span> " + moment(new Date(data.media_dateAndTime_created)).format('LLLL') + " / " +
							"by <a href='pages-profile.html'>" + data.media_created_by.user_firstname + " " + data.media_created_by.user_lastname + "</a>" +
						"</div>" +
						"<div class='post-text'>";
	
						if(data.media_preview_image === "" || data.media_preview_image === null){
							body += "<img src='/resources/img/no-image-available.jpg' class='img-responsive img-text' style='margin: 0 auto; max-height: 220px; height: 220px; max-width: 270px; width: 270px;'>";
						}else{
							body += "<img src='/SLOMS_server" + data.media_preview_image + "' class='img-responsive img-text' style='margin: 0 auto; max-height: 220px; height: 220px; max-width: 270px; width: 270px;'/>";
						}
							
				body += "<p style='text-align: justify; text-justify: inter-word; text-overflow: ellipsis; display: -webkit-box; overflow: hidden; -webkit-line-clamp: 2; -webkit-box-orient: vertical; line-height: 16px; max-height: 32px;'>" + data.mediaDetails.media_description + "</p>";
	
						if(moment(data.mediaClientDetails.media_expiration,"YYYY-MM-DD").diff(moment(),'days') <= 0){
							body += "<p style='color: red;'><b>EXPIRED</b></p>";
						}else if(moment(data.mediaClientDetails.media_expiration,"YYYY-MM-DD").diff(moment(),'days') == 1){
							body += "<p>Expiring in <b>" + moment(data.mediaClientDetails.media_expiration,"YYYY-MM-DD").diff(moment(),'days') + " day</b></p>";
						}else{
							body += "<p>Expiring in <b>" + moment(data.mediaClientDetails.media_expiration,"YYYY-MM-DD").diff(moment(),'days') + " days</b></p>";
						}
	
				body +=	"</div>" +
						"<div class='post-row'>" +
							"<a href='/media_details?media_id=" + data.media_id + "'><button class='btn btn-default btn-rounded pull-right'>Read more &RightArrow;</button></a>" +
						"</div>" +
						"</div>" +
						"</div>";
				
				$("#available_media_row").append(body);
	
				available_media_count++;
			});
			
			/*
			$('#available_media_row').easyPaginate({
				paginateElement: 'div#available_media',
				elementsPerPage: 2,
				effect: 'climb',
				firstButtonText: 'First',
				lastButtonText: 'Last',
				prevButtonText: 'Previous',
				nextButtonText: 'Next'
			});
			*/
	
			$('#available_media_count').text(available_media_count);
		},
		error: function(e){
			console.log(e);
		}
	});
}

showAllMedia();
showAllAvailableMedia();

function refreshMedia(){
	all_media_count = 0;
	available_media_count = 0;

	live_contract_count = 0;
	soon_to_expire_count = 0;
	needs_update_count = 0;

	showAllMedia();
	showAllAvailableMedia();
}