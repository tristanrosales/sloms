package com.sloms.controller;

import java.io.IOException;
import java.security.Principal;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.sloms.classes.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.thymeleaf.context.Context;
import org.thymeleaf.spring4.SpringTemplateEngine;

import com.clockworksms.ClockWorkSmsService;
import com.clockworksms.ClockworkException;
import com.clockworksms.ClockworkSmsResult;
import com.clockworksms.SMS;
import com.google.gson.JsonObject;
import com.sloms.entity.Media;
import com.sloms.entity.MediaClientDetails;
import com.sloms.entity.MediaDetails;
import com.sloms.entity.Notifications;
import com.sloms.entity.Users;
import com.sloms.repository.MediaClientDetailsRepository;
import com.sloms.repository.MediaDetailsRepository;
import com.sloms.repository.MediaRepository;
import com.sloms.service.EmailService;
import com.sloms.service.MediaService;
import com.sloms.service.UsersService;

@RestController
public class MediaController{
	@Autowired private MediaRepository mediaRepository;
	@Autowired private MediaDetailsRepository mediaDetailsRepository;
	@Autowired private MediaClientDetailsRepository mediaClientDetailsRepository;
	@Autowired private UsersService usersService;
	@Autowired private MediaService mediaService;
	@Autowired private EmailService emailService;
	@Autowired private SpringTemplateEngine templateEngine;
	@Autowired private SimpMessagingTemplate template;
	private JsonObject status = new JsonObject();
	
	public String getMediaDetails(Integer days,String output,String user_fullname,String type_of_message) {		
		String message =  "Hi, " + user_fullname + "!\n\n" + 
				"These are the media or billboard " + type_of_message + " " + days + " days:\n\n" + output + "\n\n" +
				"Regards, SLOMS, Inc.\nDo not reply. This message is computer generated from SLOMS, Inc.";
						
		return message;
	}

	@PostMapping("/send-email_expiring-media")
	@ResponseBody
	public String sendEmailForExpiringMedia(
			@RequestParam Integer days,
			@RequestParam String output,
			HttpSession httpSession) {
		
		List<UsersDao> allUsers = usersService.getAllUsers();
		
		for (UsersDao user: allUsers) {
			if(user.getUser_email_notif().equalsIgnoreCase("Enabled")) {
				try {
					System.out.println("user: " + user.getUser_email_address());
					
					Mail mail = new Mail();
			        mail.setFrom("sloms.inc03@gmail.com");
			        mail.setTo(user.getUser_email_address());
			        mail.setSubject("SLOMS, Inc. | Soon To Expire In " + days + " days");
			        
			        Map<String,String> model = new HashMap<String,String>();
			        model.put("user_fullname", httpSession.getAttribute("LOGGED-IN_USER_FIRSTNAME") + " " + httpSession.getAttribute("LOGGED-IN_USER_LASTNAME"));
			        model.put("days", days.toString());
			        model.put("output", output);
			       
			        Context context = new Context();
			        context.setVariables(model);
			        String html = templateEngine.process("send-notification-for-all-expiring-media-template", context);
			        
			        mail.setContent(html);
			        
			        emailService.sendSimpleMessage(mail);
			        
			        status.addProperty("isEmailSent", true);
				} catch (Exception e) {
					System.out.println("Exception: " + e.getMessage());
					status.addProperty("isEmailSent", false);
				}
			}
		}
		
		return status.toString();
	}

	@PostMapping("/send-email_needs-update-media")
	@ResponseBody
	public String sendEmailForNeedsUpdateMedia(
			@RequestParam Integer days,
			@RequestParam String output,
			HttpSession httpSession) {
		
		List<UsersDao> allUsers = usersService.getAllUsers();
		
		for (UsersDao user: allUsers) {
			if(user.getUser_email_notif().equalsIgnoreCase("Enabled")) {
				try {
					System.out.println("user: " + user.getUser_email_address());
					
					Mail mail = new Mail();
			        mail.setFrom("sloms.inc03@gmail.com");
			        mail.setTo(user.getUser_email_address());
			        mail.setSubject("SLOMS, Inc. | Needs Update In " + days + " days");
			        
			        Map<String,String> model = new HashMap<String,String>();
			        model.put("user_fullname", httpSession.getAttribute("LOGGED-IN_USER_FIRSTNAME") + " " + httpSession.getAttribute("LOGGED-IN_USER_LASTNAME"));
			        model.put("days", days.toString());
			        model.put("output", output);
			       
			        Context context = new Context();
			        context.setVariables(model);
			        String html = templateEngine.process("send-notification-for-all-needs-update-media-template", context);
			        
			        mail.setContent(html);
			        
			        emailService.sendSimpleMessage(mail);
			        
			        status.addProperty("isEmailSent", true);
				} catch (Exception e) {
					System.out.println("Exception: " + e.getMessage());
					status.addProperty("isEmailSent", false);
				}
			}
		}
		
		return status.toString();
	}

	@PostMapping("/send-sms_expiring-media")
	@ResponseBody
	public String sendSMSForExpiringMedia(
			@RequestParam Integer days,
			@RequestParam String output,
			HttpSession httpSession) {
		
		List<UsersDao> allUsers = usersService.getAllUsers();
		
		System.out.println("days: " + days);
		System.out.println("output: " + output);
		
		for (UsersDao user: allUsers) {
			if(user.getUser_sms_notif().equalsIgnoreCase("Enabled")) {
				try {			        
			        //real api key = 9c6adc7f85d45aed33f7b57477d93ebdbe6e563c
					//free api key = c443973a3e8e16e98091448e768738fce9799dd5
					
			        String API_KEY = "9c6adc7f85d45aed33f7b57477d93ebdbe6e563c";
			        
					ClockWorkSmsService clockWorkSmsService = new ClockWorkSmsService(API_KEY);
					
					String honorifics = null;
					
					if(user.getUser_gender().equalsIgnoreCase("Male")) {
						honorifics = "Mr.";
					}else if(user.getUser_gender().equalsIgnoreCase("Female")){
						honorifics = "Ms.";
					}
					
					String user_fullname = user.getUser_firstname() + " " + user.getUser_lastname();
					
					SMS sms = new SMS(user.getUser_phone_number(), getMediaDetails(days,output,honorifics + " " + user_fullname,"expiring in the next"));
					ClockworkSmsResult result = clockWorkSmsService.send(sms);
					
					if(result.isSuccess()) {
						System.out.println("Sent with ID: " + result.getId());
						
						status.addProperty("isSMSSent", true);
					}else {
						System.out.println("Error: " + result.getErrorMessage());
						status.addProperty("isSMSSent", false);
					}
					
				} catch (ClockworkException e) {
					e.printStackTrace();
				}
			}
		}
		
		return status.toString();
	}

	@PostMapping("/send-sms_needs-update-media")
	@ResponseBody
	public String sendSMSForNeedsUpdateMedia(
			@RequestParam Integer days,
			@RequestParam String output,
			HttpSession httpSession) {
		
		List<UsersDao> allUsers = usersService.getAllUsers();
		
		System.out.println("days: " + days);
		System.out.println("output: " + output);
		
		for (UsersDao user: allUsers) {
			if(user.getUser_sms_notif().equalsIgnoreCase("Enabled")) {
				try {			        
			        //real api key = 9c6adc7f85d45aed33f7b57477d93ebdbe6e563c
					//free api key = c443973a3e8e16e98091448e768738fce9799dd5
					
			        String API_KEY = "9c6adc7f85d45aed33f7b57477d93ebdbe6e563c";
			        
					ClockWorkSmsService clockWorkSmsService = new ClockWorkSmsService(API_KEY);
					
					String honorifics = null;
					
					if(user.getUser_gender().equalsIgnoreCase("Male")) {
						honorifics = "Mr.";
					}else if(user.getUser_gender().equalsIgnoreCase("Female")){
						honorifics = "Ms.";
					}
					
					String user_fullname = user.getUser_firstname() + " " + user.getUser_lastname();
					
					SMS sms = new SMS(user.getUser_phone_number(), getMediaDetails(days,output,honorifics + " " + user_fullname,"that needs update in the next"));
					ClockworkSmsResult result = clockWorkSmsService.send(sms);
					
					if(result.isSuccess()) {
						System.out.println("Sent with ID: " + result.getId());
						
						status.addProperty("isSMSSent", true);
					}else {
						System.out.println("Error: " + result.getErrorMessage());
						status.addProperty("isSMSSent", false);
					}
					
				} catch (ClockworkException e) {
					e.printStackTrace();
				}
			}
		}
		
		return status.toString();
	}

	@PostMapping("/add-media")
	@ResponseBody
	public String addMedia(
			@ModelAttribute("media") Media media,
			@ModelAttribute("mediaDetails") MediaDetails mediaDetails,
			@ModelAttribute("mediaClientDetails") MediaClientDetails mediaClientDetails,
			@RequestParam(value="media_preview_image",required=false) String media_preview_image,
			@RequestParam(value="media_images",required=false) List<String> mediaImageLinks,
			@ModelAttribute("notifications") Notifications notifications,
			Principal principal) {

		Users loggedInUser = usersService.findUserByUsername(principal.getName());

		if(RoleConstants.SYS_SPECIALIST.equals(loggedInUser.getUser_role())){
			status.addProperty("status", "accessDenied");
		}else{
			Media mediaNameExists = mediaRepository.findMediaByMediaName(media.getMedia_name());
			Media mediaCodeExists = mediaRepository.findMediaByMediaCode(media.getMedia_code());

			if(mediaNameExists != null) {
				status.addProperty("status", "mediaNameExists");
			}else if (mediaCodeExists != null) {
				status.addProperty("status", "mediaCodeExists");
			}else if(mediaNameExists == null && mediaCodeExists == null){
				if(media_preview_image != null) {
					media.setMedia_preview_image(media_preview_image.replaceAll(",", ""));
					System.out.println(media_preview_image.replaceAll(",", ""));
				}

				media.setMedia_images(mediaImageLinks);

				mediaService.saveMedia(media,mediaDetails,mediaClientDetails,notifications,principal);

				status.addProperty("status", true);

				template.convertAndSend("/sloms/bulletin_board", "bulletin_board");
				template.convertAndSend("/sloms/all_media", "all_media");
				template.convertAndSend("/sloms/available_media", "available_media");
				template.convertAndSend("/sloms/all_media_table", "all_media_table");
			}
		}
		
		return status.toString();
	}

	@PostMapping("/add-media/add-media-preview-image")
	@ResponseBody
	public void addMediaPreviewImage(
			@RequestParam("qqfile") MultipartFile mediaPreviewImage,
			@RequestParam("qquuid") String uuid,
			@RequestParam("qqfilename") String fileName,
			HttpServletResponse response) throws IOException {
		
		String uploadedFilename = UploadImage.uploadImage(uuid, mediaPreviewImage, "src\\main\\webapp\\SLOMS_server\\media", "/media/");
		
		System.out.println("Uploading " + mediaPreviewImage.getOriginalFilename());
		System.out.println("UUID=" + uuid);
		
		JsonObject jsonObject = new JsonObject();
		
		jsonObject.addProperty("success", true);
		jsonObject.addProperty("mediaPreviewImageLink", uploadedFilename);
		
		response.setCharacterEncoding("UTF-8");
		response.setContentType("text/plain");
		response.getWriter().print(jsonObject);
		response.flushBuffer();	
	}

	@PostMapping("/add-media/add-media-images")
	@ResponseBody
	public void addMediaImages(
			@RequestParam("qqfile") MultipartFile mediaImage,
			@RequestParam("qquuid") String uuid,
			@RequestParam("qqfilename") String fileName,
			HttpServletResponse response) throws IOException {
		
		String uploadedFilename = UploadImage.uploadImage(uuid, mediaImage, "src\\main\\webapp\\SLOMS_server\\media", "/media/");
		
		System.out.println("Uploading " + mediaImage.getOriginalFilename());
		System.out.println("UUID=" + uuid);
		
		JsonObject jsonObject = new JsonObject();
		
		jsonObject.addProperty("success", true);
		jsonObject.addProperty("mediaImageLink", uploadedFilename);
		
		response.setCharacterEncoding("UTF-8");
		response.setContentType("text/plain");
		response.getWriter().print(jsonObject);
		response.flushBuffer();
	}

	@DeleteMapping("/add-media/delete-media-images/{qquuid}")
	public void deleteMediaImages(
			HttpServletResponse response,
			HttpServletRequest request,
			@PathVariable("qquuid") String qquuid,
			@RequestParam("filename") String filename) throws IOException {
			
		System.out.println("uploadDelete() called " + filename);
		
		DeleteImage.deleteImage(qquuid, "src\\main\\webapp\\SLOMS_server\\media\\");
		
		response.setStatus(200);
		response.flushBuffer();
	}

	@PostMapping("/update-media")
	@ResponseBody
	public String updateMedia(
			@ModelAttribute("media") Media media,
			@ModelAttribute("mediaDetails") MediaDetails mediaDetails, 
			@ModelAttribute("mediaClientDetails") MediaClientDetails mediaClientDetails,
			Principal principal) {

		Users loggedInUser = usersService.findUserByUsername(principal.getName());

		if(RoleConstants.GUEST.equals(loggedInUser.getUser_role())){
			status.addProperty("status", "accessDenied");
		}else{
			mediaService.updateMedia(media, mediaDetails, mediaClientDetails);
			status.addProperty("isUpdateMediaSuccess", true);
		}

		return status.toString();
	}

	@PostMapping("/update-media-preview-image")
	@ResponseBody
	public String updateMediaPreviewImage(
			@RequestParam Integer media_id,
			@RequestParam(value="media_preview_image",required=false) String media_preview_image) {
		
		Media media = mediaRepository.findMediaByMediaId(media_id);
		
		System.out.println(media.getMedia_preview_image());
		
		if(media.getMedia_preview_image() != null || media.getMedia_preview_image() != "") {
			DeleteImage.deleteImageInFolder("src\\main\\webapp\\SLOMS_server\\" + media.getMedia_preview_image());
		}
		
		mediaRepository.updateMediaPreviewImage(media_preview_image, media_id);
		
		status.addProperty("isUpdatingMediaPreviewImage", true);
		
		return status.toString();
	}

	@PostMapping("/delete-media")
	@ResponseBody
	public String deleteMedia(
			@ModelAttribute("media") Media media,
			@ModelAttribute("mediaDetails") MediaDetails mediaDetails, 
			@ModelAttribute("mediaClientDetails") MediaClientDetails mediaClientDetails,
			Principal principal) {
		
		Media mediaIdExists = mediaRepository.findMediaByMediaId(media.getMedia_id());
		MediaDetails mediaDetails_mediaIdExists = mediaDetailsRepository.findMediaDetailsByMediaId(media.getMedia_id());
		MediaClientDetails mediaClientDetails_mediaIdExists = mediaClientDetailsRepository.findMediaClientDetailsByMediaId(media.getMedia_id());

		Users loggedInUser = usersService.findUserByUsername(principal.getName());

		if(RoleConstants.GUEST.equals(loggedInUser.getUser_role())){
			status.addProperty("status", "accessDenied");
		}else{
			if(mediaIdExists != null) {
				mediaRepository.delete(mediaIdExists);
				mediaDetailsRepository.delete(mediaDetails_mediaIdExists);
				mediaClientDetailsRepository.delete(mediaClientDetails_mediaIdExists);

				status.addProperty("status", true);
			}
		}

		return status.toString();
	}

	@GetMapping("/media")
	public List<Media> getAllMedia(){
		return mediaService.getAllMedia();
	}

	@GetMapping("/media/{mediaID}")
	public Media getMediaById(@PathVariable("mediaID") Integer mediaID) {
		return mediaService.getMediaById(mediaID);
	}

	@GetMapping("/available_media")
	public List<Media> getAllAvailableMedia(){
		return mediaRepository.getAllAvailableMedia("Activated");
	}

	@GetMapping("/media/media_id/{media_id}/thread_notes_id/{notes_id}/")
	public Media getAllMediaByMediaIdAndThreadNotesId(
			@PathVariable("media_id") Integer media_id, 
			@PathVariable("notes_id") Integer notes_id){
		
		return mediaService.getAllMediaByMediaIdAndThreadNotesId(media_id,notes_id);
	}

	@GetMapping("/my_media")
	public List<Media> getAllMediaOfLoggedInUser(Principal principal){
		return mediaService.getAllMediaOfLoggedInUser(principal);
	}
}
