package com.sloms.controller;

import java.io.IOException;
import java.security.Principal;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.sloms.service.UsersService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.google.gson.JsonObject;
import com.sloms.classes.DeleteImage;
import com.sloms.classes.UploadImage;
import com.sloms.entity.Notifications;
import com.sloms.entity.ThreadComments;
import com.sloms.entity.Users;
import com.sloms.repository.ThreadCommentsRepository;
import com.sloms.service.ThreadService;

@RestController
public class ThreadCommentController{
	@Autowired private ThreadCommentsRepository threadCommentsRepository;
	@Autowired private ThreadService threadService;
	@Autowired private UsersService usersService;
	@Autowired private SimpMessagingTemplate template;
	private JsonObject status = new JsonObject();
	
	@RequestMapping(value="/add-comment",method=RequestMethod.POST)
	@ResponseBody
	public String addComment(
			@RequestParam(value="thread_comment_images",required=false) List<String> threadCommentImageLink,
			@ModelAttribute("threadComments") ThreadComments threadComments,
			@ModelAttribute("notifications") Notifications notifications,
			Principal principal){

		Users loggedInUser = usersService.findUserByUsername(principal.getName());

		threadComments.setThread_comment_images(threadCommentImageLink);
		threadComments.setThread_comment_by(loggedInUser);
		
		threadService.saveThreadComments(threadComments,notifications);
		
		status.addProperty("isAddingCommentSuccess", true);
		
		template.convertAndSend("/sloms/bulletin_board","bulletin_board");
		template.convertAndSend("/sloms/comment","comment");
		
		return status.toString();
	}
	
	@RequestMapping(value="/add-comment/add-thread-comment-images",method=RequestMethod.POST)
	@ResponseBody
	public void addThreadCommentImages(
			@RequestParam("qqfile") MultipartFile threadCommentImage,
			@RequestParam("qquuid") String uuid,
			@RequestParam("qqfilename") String fileName,
			HttpServletResponse response) throws IOException {

		String uploadedFilename = UploadImage.uploadImage(uuid, threadCommentImage, "src\\main\\webapp\\SLOMS_server\\comment", "/comment/");
		
		System.out.println("Uploading " + threadCommentImage.getOriginalFilename());
		System.out.println("UUID=" + uuid);
		
		JsonObject jsonObject = new JsonObject();
		
		jsonObject.addProperty("success", true);
		jsonObject.addProperty("threadCommentImageLink", uploadedFilename);
		
		response.setCharacterEncoding("UTF-8");
		response.setContentType("text/plain");
		response.getWriter().print(jsonObject);
		response.flushBuffer();
	}
	
	@RequestMapping(value="/add-comment/delete-thread-comment-images/{qquuid}",method={RequestMethod.DELETE})
	public void deleteThreadCommentImages(
			HttpServletResponse response,
			HttpServletRequest request,
			@PathVariable("qquuid") String qquuid,
			@RequestParam("filename") String filename) throws IOException {
			
		System.out.println("uploadDelete() called " + filename);
		
		DeleteImage.deleteImage(qquuid, "src\\main\\webapp\\SLOMS_server\\comment\\");
		
		response.setStatus(200);
		response.flushBuffer();
	}
	
	@RequestMapping(value="/frmEditComment",method=RequestMethod.POST)
	@ResponseBody
	public String editComment(
			@ModelAttribute("threadComments") ThreadComments threadComments,
			@ModelAttribute("notifications") Notifications notifications) {
		
		ThreadComments threadCommentsIdExists = threadCommentsRepository.findThreadCommentsById(threadComments.getThread_comment_id());
		
		final Date dateAndTimeToday = new Date();
		
		if(threadCommentsIdExists != null) {
			threadComments.setThread_comment_modified(dateAndTimeToday);
			
			threadCommentsRepository.save(threadComments);
			
			status.addProperty("status", true);
			
			template.convertAndSend("/sloms/bulletin_board", "bulletin_board");
			template.convertAndSend("/sloms/comment","comment");
		}
		
		return status.toString();
	}
	
	@RequestMapping(value="/frmDeleteComment",method=RequestMethod.POST)
	@ResponseBody
	public String deleteComment(@ModelAttribute("threadComments") ThreadComments threadComments) {
		ThreadComments threadCommentsIdExists = threadCommentsRepository.findThreadCommentsById(threadComments.getThread_comment_id());
		
		if(threadCommentsIdExists != null) {
			threadCommentsRepository.delete(threadCommentsIdExists);
			status.addProperty("status", true);
			
			template.convertAndSend("/sloms/bulletin_board", "bulletin_board");
			template.convertAndSend("/sloms/comment","comment");
		}
		
		return status.toString();
	}
	
	@RequestMapping(path="/thread_comments/{threadID}", method=RequestMethod.GET)
	@ResponseBody
	public List<ThreadComments> getAllThreadCommentsByThreadId(@PathVariable("threadID") Integer threadID){		
		return threadCommentsRepository.getAllThreadCommentsByThreadId(threadID);
	}
	
	@RequestMapping(path="/thread_comment/{thread_comment_id}", method=RequestMethod.GET)
	@ResponseBody
	public ThreadComments getThreadCommentsByThreadCommentId(@PathVariable("thread_comment_id") Integer thread_comment_id){
		return threadCommentsRepository.getThreadCommentsByThreadCommentId(thread_comment_id);
	}
	
	@RequestMapping(path="/thread_comments", method=RequestMethod.GET)
	@ResponseBody
	public List<ThreadComments> getAllThreadComments(){
		return threadCommentsRepository.getAllThreadComments();
	}
}

