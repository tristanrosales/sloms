package com.sloms.controller;

import java.io.IOException;
import java.security.Principal;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.sloms.service.MediaService;
import com.sloms.service.UsersService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import com.google.gson.JsonObject;
import com.sloms.classes.DeleteImage;
import com.sloms.classes.UploadImage;
import com.sloms.entity.Media;
import com.sloms.entity.Notifications;
import com.sloms.entity.Threads;
import com.sloms.entity.Users;
import com.sloms.service.ThreadService;

@RestController
public class ThreadController {
	@Autowired private ThreadService threadService;
	@Autowired private UsersService usersService;
	@Autowired private MediaService mediaService;
	@Autowired private SimpMessagingTemplate template;
	private JsonObject status = new JsonObject();

	@PostMapping("/add-thread")
	@ResponseBody
	public String addThread(
			@RequestParam Integer thread_billboard,
			@RequestParam(value="thread_preview_image",required=false) String thread_preview_image,
			@ModelAttribute("threads") Threads threads,
			@ModelAttribute("notifications") Notifications notifications,
			Principal principal) {
		
		Threads threadCaptionExists = threadService.findThreadByThreadCaption(threads.getThread_caption());
		Users loggedInUser = usersService.findUserByUsername(principal.getName());
		Media mediaID = mediaService.getMediaById(thread_billboard);
		
		final Date dateAndTimeToday = new Date();
		
		if(threadCaptionExists != null) {
			status.addProperty("isAddingThreadSuccess", "threadCaptionExists");
		}else {
			if(thread_preview_image != null) {
				threads.setThread_preview_image(thread_preview_image.replaceAll(",", ""));
			}
			
			threads.setThread_billboard(mediaID);
			threads.setThread_created_by(loggedInUser);
			threads.setThread_dateAndTimeCreated(dateAndTimeToday);
			
			threadService.saveThread(threads, notifications);
			
			status.addProperty("isAddingThreadSuccess", true);
			
			template.convertAndSend("/sloms/bulletin_board","bulletin_board");
			template.convertAndSend("/sloms/thread","thread");
		}
		
		return status.toString();
	}

	@PostMapping("/add-thread/add-thread-preview-image")
	@ResponseBody
	public void addThreadPreviewImage(
			@RequestParam("qqfile") MultipartFile threadPreviewImage,
			@RequestParam("qquuid") String uuid,
			@RequestParam("qqfilename") String fileName,
			HttpServletResponse response) throws IOException {
		
		String uploadedFilename = UploadImage.uploadImage(uuid, threadPreviewImage, "src\\main\\webapp\\SLOMS_server\\thread", "/thread/");
		
		System.out.println("Uploading " + threadPreviewImage.getOriginalFilename());
		System.out.println("UUID=" + uuid);
		
		JsonObject jsonObject = new JsonObject();
		
		jsonObject.addProperty("success", true);
		jsonObject.addProperty("threadPreviewImageLink", uploadedFilename);
		
		response.setCharacterEncoding("UTF-8");
		response.setContentType("text/plain");
		response.getWriter().print(jsonObject);
		response.flushBuffer();		
	}

	@DeleteMapping("/add-thread/delete-thread-preview-image/{qquuid}")
	public void deleteThreadPreviewImage(
			HttpServletResponse response,
			HttpServletRequest request,
			@PathVariable("qquuid") String qquuid,
			@RequestParam("filename") String filename) throws IOException {
			
		System.out.println("uploadDelete() called " + filename);
		
		DeleteImage.deleteImage(qquuid, "src\\main\\webapp\\SLOMS_server\\thread\\");
		
		response.setStatus(200);
		response.flushBuffer();
	}

	@PostMapping("/update-thread")
	@ResponseBody
	public String updateThread(@ModelAttribute("threads") Threads threads) {
		threadService.updateThread(
				threads.getThread_notes_id(), 
				threads.getThread_caption(), 
				threads.getThread_status(), 
				threads.getThread_id());
		
		status.addProperty("isUpdatingThreadSuccess", true);
		
		template.convertAndSend("/sloms/bulletin_board", "bulletin_board");
		
		return status.toString();
	}

	@PostMapping("/update-thread-status")
	@ResponseBody
	public String openOrUpdateSelectedThread(@RequestParam String thread_status,@RequestParam Integer thread_id) {
		threadService.openOrCloseSelectedThread(thread_status, thread_id);
		
		status.addProperty("isUpdatingThreadStatusSuccess", true);
		
		template.convertAndSend("/sloms/bulletin_board", "bulletin_board");
		template.convertAndSend("/sloms/thread_status", "thread_status");
		
		return status.toString();
	}

	@PostMapping("/update-thread-preview-image")
	@ResponseBody
	public String updateThreadPreviewImage(
			@RequestParam Integer thread_id,
			@RequestParam(value="thread_preview_image",required=false) String thread_preview_image) {
		
		Threads threads = threadService.findThreadByThreadId(thread_id);
		
		System.out.println(threads.getThread_preview_image());
		
		if(threads.getThread_preview_image() != null || threads.getThread_preview_image() != ""){
			DeleteImage.deleteImageInFolder("src\\main\\webapp\\SLOMS_server\\" + threads.getThread_preview_image());
		}
		
		threadService.updateThreadPreviewImage(thread_preview_image, thread_id);
		
		status.addProperty("isUpdatingThreadPreviewImage", true);
		
		return status.toString();
	}

	@DeleteMapping("/delete-thread")
	@ResponseBody
	public String deleteThread(@ModelAttribute("threads") Threads threads) {
		Threads threadIdExists = threadService.findThreadByThreadId(threads.getThread_id());
		
		if(threadIdExists != null) {
			threadService.deleteThread(threadIdExists);
			status.addProperty("status", true);
			
			template.convertAndSend("/sloms/bulletin_board", "bulletin_board");
		}
		
		return status.toString();
	}

	@GetMapping("/threads")
	public List<Threads> getAllThreads(){
		return threadService.getAllThreads();
	}

	@GetMapping("/threads/{threadID}")
	public Threads getThreadById(@PathVariable("threadID") Integer threadID) {
		return threadService.getThreadById(threadID);
	}

	@GetMapping("/my_thread")
	public List<Threads> getAllThreadsOfLoggedInUser(Principal principal){
		Users loggedInUser = usersService.findUserByUsername(principal.getName());
		return threadService.getAllThreadsOfLoggedInUser(loggedInUser);
	}
}
