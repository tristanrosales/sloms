package com.sloms.launcher;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Profile;
import org.springframework.core.task.TaskExecutor;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;

@SpringBootApplication
@EnableJpaRepositories({ "com.sloms" })
@ComponentScan({ "com.sloms" })
@EntityScan({ "com.sloms" })
@EnableAsync
public class SlomsApplication {
	public static void main(String[] args) {
		SpringApplication.run(SlomsApplication.class, args);
	}
	
    @Bean(name="processExecutor")
    public TaskExecutor workExecutor() {
        ThreadPoolTaskExecutor threadPoolTaskExecutor = new ThreadPoolTaskExecutor();
        threadPoolTaskExecutor.setThreadNamePrefix("Async-");
        threadPoolTaskExecutor.setCorePoolSize(3);
        threadPoolTaskExecutor.setMaxPoolSize(3);
        threadPoolTaskExecutor.setQueueCapacity(600);
        threadPoolTaskExecutor.afterPropertiesSet();
        
        return threadPoolTaskExecutor;
    }

    @Profile("dev")
    @Bean
    public String devBean(){
	    return "dev";
    }

    @Profile("prod")
    @Bean
    public String prodBean(){
	    return "prod";
    }

}
