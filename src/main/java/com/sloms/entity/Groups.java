package com.sloms.entity;

import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.*;

@Getter
@Setter
@ToString
@EqualsAndHashCode
@NoArgsConstructor
@Entity
public class Groups {
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Integer group_id;

	@NotNull
	@Column(unique=true)
	private String group_name;

	@NotNull
	@Column(length=5000)
	private String group_description;
	
	private String group_preview_image;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(updatable=false)
	private Date group_dateAndTime_created;
	
	@ManyToOne(fetch=FetchType.LAZY)
    @JoinColumn(name = "group_created_by",updatable=false)
	@JsonIgnoreProperties(
			{
				"hibernateLazyInitializer",
				"handler",
				"groups",
				"user_email_address",
				"user_password",
				"user_role",
				"user_status",
				"user_sms_notif",
				"user_email_notif",
				"user_security_question",
				"user_secret_answer",
				"user_dateAndTimeCreated"
			})
	private Users group_created_by;
	
	@Temporal(TemporalType.TIMESTAMP)
	private Date group_modified;
	
	private String group_status;
	
	@OneToMany(
			cascade = CascadeType.ALL,
			fetch = FetchType.LAZY,
			mappedBy = "groups")
	@JsonIgnore
	private List<Notifications> notifications;

}
