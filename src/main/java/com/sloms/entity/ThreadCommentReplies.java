package com.sloms.entity;

import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;

import lombok.*;
import org.hibernate.validator.constraints.NotEmpty;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@Getter
@Setter
@ToString
@EqualsAndHashCode
@NoArgsConstructor
@Entity
public class ThreadCommentReplies{
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Integer thread_reply_id;
	
	@ManyToOne(fetch=FetchType.LAZY)
    @JoinColumn(name = "thread_comment_id")
	@JsonIgnoreProperties(
			{
				"hibernateLazyInitializer",
				"handler",
				"threadCommentReplies"
			})
	private ThreadComments thread_comment_id;
	
	@NotEmpty
	@NotNull
	@Column(length=5000)
	private String thread_reply;
	
	@ElementCollection(fetch=FetchType.EAGER)
	private List<String> thread_comment_reply_images;
	
	@ManyToOne(fetch=FetchType.LAZY)
    @JoinColumn(name = "thread_reply_by")
	@JsonIgnoreProperties(
			{
				"hibernateLazyInitializer",
				"handler",
				"threadCommentReplies",
				"user_password",
				"user_role",
				"user_status",
				"user_sms_notif",
				"user_email_notif",
				"user_dateAndTimeCreated"
			})
	private Users thread_reply_by;
	
	@Temporal(TemporalType.TIMESTAMP)
	private Date thread_dateAndTime_replied;
	
	@Temporal(TemporalType.TIMESTAMP)
	private Date thread_reply_modified;
	
	@OneToMany(
			cascade = CascadeType.ALL,
			fetch = FetchType.LAZY,
			mappedBy = "threadCommentReplies")
	@JsonIgnore
	private List<Notifications> notifications;

}
