<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<div class="modal" id="view_edit_group-modal" tabindex="-1" role="dialog" aria-labelledby="largeModalHead" aria-hidden="true" data-backdrop="static" data-keyboard="false">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<form id="frmUpdateGroup" method="POST" class="form-horizontal" role="form" action="/update-group">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">
						<span aria-hidden="true">&times;</span><span class="sr-only">Close</span>
					</button>
					<h3 class="modal-title" id="largeModalHead">
						Edit <span id="frmUpdateGroup_lblGroup_name"></span>
					</h3>
				</div>

				<div class="modal-body">
					<input type="hidden" name="group_id" id="frmUpdateGroup_lblGroup_id">
					
					<div class="form-group">
						<label class="col-md-2 control-label">Name</label>
						<div class="col-md-10">
							<input 
								type="text"
								id="frmUpdateGroup_txtGroup_name"
								class="form-control"
								placeholder="Enter name of group..." 
								required="required"
								name="group_name"
								style="text-transform: capitalize; background-color:white !important;"/>
						</div>
					</div>

					<div class="form-group">
						<label class="col-md-2 control-label">Description</label>
						<div class="col-md-10">
							<textarea
									id="frmUpdateGroup_txtGroup_description"
									class="form-control"
									rows="5"
									placeholder="Enter description of group..."
									required="required"
									name="group_description"
									style="resize: none; text-transform: capitalize; background-color:white !important;"></textarea>
						</div>
					</div>
					
					<div class="form-group">
						<label class="col-md-2 control-label">Status</label>
						<div class="col-md-10">
							<select id="frmUpdateGroup_cmbGroup_status" class="form-control select" name="group_status" data-live-search="true" required="required">
								<option value="Activated">Activated</option>
								<option value="Deactivated">Deactivated</option>
							</select>
						</div>
					</div>
				</div>

				<div class="modal-footer">
					<button 
						type="submit" 
						id="btnUpdateGroup" 
						class="btn btn-info btn-lg btn-rounded" 
						style="padding: 10px; font-size: 15px; background-color:#1caf9a; color: white;">
						<strong>
							<span class="fa fa-hand-o-right"></span> Update Group
						</strong>
					</button>

					<button 
						type="button" 
						class="btn btn-warning btn-rounded" 
						style="padding: 10px; font-size: 15px; background-color: orange; color: white;" 
						data-dismiss="modal">
						<strong>
							<span class="glyphicon glyphicon-remove-sign"></span> Close
						</strong>
					</button>
				</div>
			</form>
		</div>
	</div>
</div>