package com.sloms.entity;

import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;

import lombok.*;
import org.hibernate.validator.constraints.NotEmpty;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@Getter
@Setter
@ToString
@EqualsAndHashCode
@NoArgsConstructor
@Entity
public class ThreadComments{
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Integer thread_comment_id;
	
	@ManyToOne(fetch=FetchType.LAZY)
    @JoinColumn(name = "thread_id")
	@JsonIgnoreProperties(
			{
				"hibernateLazyInitializer",
				"handler",
				"threadComments",
				"thread_notes_id",
				"thread_dateAndTimeCreated",
				"thread_created_by",
				"thread_modified"
			})
	private Threads thread_id;
	
	@NotEmpty
	@NotNull
	@Column(length=5000)
	private String thread_comment;
	
	@ElementCollection(fetch=FetchType.EAGER)
	private List<String> thread_comment_images;
	
	@ManyToOne(fetch=FetchType.LAZY)
    @JoinColumn(name = "thread_comment_by")
	@JsonIgnoreProperties(
			{
				"hibernateLazyInitializer",
				"handler",
				"threadComments",
				"user_password",
				"user_role",
				"user_status",
				"user_sms_notif",
				"user_email_notif",
				"user_dateAndTimeCreated"
			})
	private Users thread_comment_by;
	
	@Temporal(TemporalType.TIMESTAMP)
	private Date thread_dateAndTime_posted;
	
	@Temporal(TemporalType.TIMESTAMP)
	private Date thread_comment_modified;
	
	@OneToMany(
			cascade = CascadeType.ALL,
            fetch = FetchType.LAZY,
            mappedBy = "thread_comment_id")
	@JsonIgnoreProperties("thread_comment_id")
	private List<ThreadCommentReplies> threadCommentReplies;
	
	@OneToMany(
			cascade = CascadeType.ALL,
			fetch = FetchType.LAZY,
			mappedBy = "threadComments")
	@JsonIgnore
	private List<Notifications> notifications;

}
