package com.sloms.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.sloms.entity.Feedbacks;
import com.sloms.entity.Users;

@Repository
public interface FeedbacksRepository extends JpaRepository<Feedbacks,Integer>{
	@Query("SELECT f FROM Feedbacks f ORDER BY f.feedback_id DESC")
	List<Feedbacks> getAllFeedbacks();
	
	@Query("SELECT f FROM Feedbacks f WHERE f.feedback_created_by=:userID ORDER BY f.feedback_id DESC")
	List<Feedbacks> getAllFeedbacksOfLoggedInUser(@Param("userID") Users userID);
}
