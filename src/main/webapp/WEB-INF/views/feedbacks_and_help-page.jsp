<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!-- PAGE CONTENT -->
<div class="page-content">

	<tiles:insertAttribute name="x_navigationVertical" />

	<!-- START BREADCRUMB -->
	<ul class="breadcrumb">
		<li><a href="${pageContext.request.contextPath}/home">Home</a></li>
		<li class="active">Feedbacks & Help</li>
	</ul>
	<!-- END BREADCRUMB -->

	<!-- PAGE TITLE -->
	<div class="page-title">
		<h2>
			<span class="fa fa-thumbs-o-up"></span> Feedbacks & Help
		</h2>
	</div>
	<!-- END PAGE TITLE -->

	<!-- PAGE CONTENT WRAPPER -->
	<div class="page-content-wrap">

		<div class="row">
			<div class="col-md-12">
				<div class="panel panel-default">
					<div class="panel-heading">
						<c:choose>
							<c:when test="${user.user_role != 'IT_ADMIN'}">
								<button
										type="button"
										class="btn btn-info btn-lg btn-rounded"
										style="padding: 10px; font-size: 15px; background-color:#1caf9a; color: white;"
										data-toggle="modal"
										data-target="#add_feedback-modal">
									<span class="fa fa-plus-circle"></span> <strong>Add New Feedback</strong>
								</button>
							</c:when>
						</c:choose>

						<ul class="panel-controls">
							<li>
								<a href="#" class="panel-collapse">
									<span class="fa fa-angle-down"></span>
								</a>
							</li>

							<li>
								<a href="#" class="panel-refresh">
									<span class="fa fa-refresh"></span>
								</a>
							</li>
						</ul>
					</div>

					<c:choose>
						<c:when test="${user.user_role == 'IT_ADMIN'}">
							<div class="panel-body" style="font-size: 15px;">
								<table id="tblFeedbacks" class="display" style="width:100%;">
									<thead style="background-color: #1caf9a; color: white;">
									<tr>
										<th style="text-align: center;">Details</th>
										<th style="text-align: center;">Respondent</th>
										<th style="text-align: center;">Type</th>
									</tr>
									</thead>

									<tfoot style="background-color: #1caf9a; color: white;">
									<tr>
										<th style="text-align: center;">Details</th>
										<th style="text-align: center;">Respondent</th>
										<th style="text-align: center;">Type</th>
									</tr>
									</tfoot>
								</table>
							</div>
						</c:when>
					</c:choose>

					<div class="panel-body">
						<div class="panel-group accordion">
							<div class="panel panel-info">
								<div class="panel-heading">
									<h4 class="panel-title">
										<a href="#reasons_why_feedback_is_important">
											<b>5 Reasons Why Feedback is Important</b>
										</a>
									</h4>
								</div>
								<div class="panel-body" id="reasons_why_feedback_is_important" class="row">
									<h3>5 Reasons Why Feedback is Important</h3>
									<h4 style="line-height: 1.6;">
										<ol>
											<li>
												<b>Feedback is always there.</b> 
												If you ask someone in your organization when feedback occurs, 
												they will typically mention an employee survey, performance appraisal, 
												or training evaluation. In actuality, feedback is around us all the time. 
												Every time we speak to a person, employee, customer, vendor, etc., 
												we communicate feedback. In actuality, it’s impossible not to give feedback.
											</li>
	
											<li>
												<b>Feedback is effective listening.</b> 
												Whether the feedback is done verbally or via a feedback survey, 
												the person providing the feedback needs to know they have been understood (or received) 
												and they need to know that their feedback provides some value. 
												When conducting a survey, always explain why respondents’ feedback is important and how their feedback will be used.
											</li>
	
											<li>
												<b>Feedback can motivate.</b> 
												By asking for feedback, it can actually motivate employees to perform better. 
												Employees like to feel valued and appreciate being asked to provide feedback 
												that can help formulate business decisions. And feedback from client, suppliers, 
												vendors, and stakeholders can be used to motivate to build better working relations
											</li>
											
											<li>
												<b>Feedback can improve performance.</b> 
												Feedback is often mistaken for criticism. In fact, what is viewed as negative criticism 
												is actually constructive criticism and is the best find of feedback that can help to 
												formulate better decisions to improve and increase performance.
											</li>
	
											<li>
												<b>Feedback is a tool for continued learning.</b> 
												Invest time in asking and learning about how others experience working with your organization. 
												Continued feedback is important across the entire organization in order to remain aligned to goals, 
												create strategies, develop products and services improvements, improve relationships, and much more. 
												Continued learning is the key to improving.
											</li>
										</ol>
	
										<b>Source:</b> <a href="https://www.snapsurveys.com/blog/5-reasons-feedback-important/" target="_blank">https://www.snapsurveys.com/blog/5-reasons-feedback-important/</a>
									</h4>
								</div>
							</div>
						</div>

						
					</div>
				</div>
			</div>
		</div>

	</div>
	<!-- END PAGE CONTENT WRAPPER -->

</div>
<!-- END PAGE CONTENT -->

<tiles:insertAttribute name="add_feedback_modal" />

<script type="text/javascript" src="${pageContext.request.contextPath}/resources/js/Feedbacks.js"></script>