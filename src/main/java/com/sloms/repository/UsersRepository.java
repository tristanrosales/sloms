package com.sloms.repository;

import java.util.Date;
import java.util.List;

import javax.transaction.Transactional;

import com.sloms.classes.UsersDao;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.sloms.entity.Users;

@Repository
public interface UsersRepository extends JpaRepository<Users,Integer>{
	@Query("SELECT u.user_id as user_id,u.username as username,u.user_firstname as user_firstname," +
			"u.user_lastname as user_lastname,u.user_email_address as user_email_address," +
			"u.user_phone_number as user_phone_number,u.user_gender as user_gender," +
			"u.user_profile_picture as user_profile_picture,u.user_role as user_role," +
			"u.user_status as user_status,u.user_sms_notif as user_sms_notif," +
			"u.user_email_notif as user_email_notif,u.user_dateAndTimeApproved as user_dateAndTimeApproved," +
			"u.user_dateAndTimeCreated as user_dateAndTimeCreated FROM Users u ORDER BY user_id DESC")
	List<UsersDao> getAllUsers();
	
	@Query("SELECT u FROM Users u WHERE u.username=:username")
	Users getAllUserInfoByUsername(@Param("username") String username);
	
	@Query("SELECT u FROM Users u WHERE u.user_email_address=:email")
	Users findUserByEmail(@Param("email") String email);
	
	@Query("SELECT u FROM Users u WHERE u.user_id=:user_id")
	Users findUserByUserId(@Param("user_id") Integer user_id);
	
	@Transactional
	@Modifying
	@Query("UPDATE Users u SET u.user_profile_picture=:user_profile_picture WHERE u.user_id=:user_id")
	void updateUserProfilePicture(
			@Param("user_profile_picture") String user_profile_picture,
			@Param("user_id") Integer user_id);

	@Transactional
	@Modifying
	@Query("UPDATE Users u SET u.user_password=:user_password WHERE u.username=:username")
	void updateUserPassword(@Param("user_password") String user_password, @Param("username") String username);
	
	@Transactional
	@Modifying
	@Query("UPDATE Users u SET u.username=:username WHERE u.user_id=:user_id")
	void updateUsername(@Param("username") String username, @Param("user_id") Integer user_id);
	
	@Transactional
	@Modifying
	@Query("UPDATE Users u SET "
			+ "u.user_email_address=:user_email_address,"
			+ "u.user_role=:user_role,"
			+ "u.user_status=:user_status "
			+ "WHERE u.user_id=:user_id")
	void updateUserEmail_Role_Status(
			@Param("user_email_address") String user_email_address,
			@Param("user_role") String user_role,
			@Param("user_status") String user_status,
			@Param("user_id") Integer user_id);
	
	@Transactional
	@Modifying
	@Query("UPDATE Users u SET "
			+ "u.user_firstname=:user_firstname,"
			+ "u.user_lastname=:user_lastname,"
			+ "u.user_email_address=:user_email_address,"
			+ "u.user_phone_number=:user_phone_number,"
			+ "u.user_gender=:user_gender,"
			+ "u.user_sms_notif=:user_sms_notif,"
			+ "u.user_email_notif=:user_email_notif "
			+ "WHERE u.user_id=:user_id")
	void updateProfileInformation(
			@Param("user_firstname") String user_firstname,
			@Param("user_lastname") String user_lastname,
			@Param("user_email_address") String user_email_address,
			@Param("user_phone_number") String user_phone_number,
			@Param("user_gender") String user_gender,
			@Param("user_sms_notif") String user_sms_notif,
			@Param("user_email_notif") String user_email_notif,
			@Param("user_id") Integer user_id);

	@Transactional
	@Modifying
	@Query("UPDATE Users u SET "
			+ "u.user_status=:user_status,"
			+ "u.user_dateAndTimeApproved=:user_dateAndTimeApproved "
			+ "WHERE u.user_id=:user_id")
	void approveOrDecline(
			@Param("user_id") Integer user_id,
			@Param("user_status") String user_status,
			@Param("user_dateAndTimeApproved") Date user_dateAndTimeApproved
	);
}
