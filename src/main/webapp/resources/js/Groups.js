var stompClient = null;

var userID = document.getElementById("loggedInUserId").innerText;
var userRole = document.getElementById("loggedInUserRole").innerText;

String.prototype.replaceAll = function (searchText, replacementText) {
    return this.split(searchText).join(replacementText);
};

function connectWebSocket(){
    var socket = new SockJS('/sloms_web_socket');
    stompClient = Stomp.over(socket);

    stompClient.connect({}, function(frame) {
		stompClient.subscribe("/sloms/errors", function(message) {
			console.log("Web Socket Error Message : " + message.body);
            connectWebSocket();
		});

		stompClient.subscribe("/sloms/all_groups_of_logged_in_user", function(message) {
            $('#tblGroups').DataTable().ajax.reload();
		});

		stompClient.subscribe("/sloms/all_activated_groups", function(message) {
            $('#tblGroups').DataTable().ajax.reload();
		});
	}, function(error) {
        console.log("Web Socket Error Message : " + error);
    });
}

function showAllGroups(){
    $("#tblGroups tbody").empty();

    /* DataTables instantiation. */
    var table = $("#tblGroups").DataTable({
        "ajax": {
            "url": "/groups",
            "dataSrc": ""
        },
        "columns": [
            {
                "className": 'details-control',
                "orderable": false,
                "data": null,
                "defaultContent": ''
            },
            {
                "data": null,
                render: function (data,type,row) {
                    var imageSrc = '';

                    if(data.group_preview_image == null || data.group_preview_image == ""){
                        imageSrc = '/resources/img/no-image-available.jpg';
                    }else{
                        imageSrc = '/SLOMS_server' + data.group_preview_image;
                    }

                    return '<img src="' + imageSrc + '" style="width: 30px; height: 30px; border-radius: 50%;" />&nbsp;&nbsp;' + data.group_name;
                }
            }
        ],
        "iDisplayLength": 5,
        "aLengthMenu": [[5, 10, 25, 50, 100, -1], [5, 10, 25, 50, 100, "All"]],
        "pagingType": "full_numbers"
    });

    $('#tblGroups tbody').on('click', 'td.details-control', function () {
        var tr = $(this).closest('tr');
        var row = table.row(tr);

        if (row.child.isShown()) {
            row.child.hide();
            tr.removeClass('shown');
        }
        else {
            row.child(showGroupDetails(row.data())).show();
            tr.addClass('shown');
        }
    });
}

function showGroupDetails(details) {
    var table,access,creatorProfilePic;

    if(details.group_created_by.user_profile_picture == null || details.group_created_by.user_profile_picture == ""){
        creatorProfilePic = '/resources/img/no-image-available.jpg';
    }else{
        creatorProfilePic = '/SLOMS_server' + details.group_created_by.user_profile_picture;
    }

    table = '<table cellpadding="5" cellspacing="0" border="0" style="padding-left:50px;" class="table table-bordered table-striped display">'+
        '<tr>'+
        '<td><strong>DESCRIPTION</strong></td>'+
        '<td>'+details.group_description+'</td>'+
        '</tr>'+
        '<tr>'+
        '<td><strong>CREATOR</strong></td>'+
        '<td><img src="' + creatorProfilePic + '" style="width: 30px; height: 30px; border-radius: 50%;" />&nbsp;&nbsp;' + details.group_created_by.user_firstname + ' ' + details.group_created_by.user_lastname + ' <b>(' + details.group_created_by.username + ')</b></td>'+
        '</tr>'+
        '<tr>'+
        '<td><strong>CREATED</strong></td>'+
        '<td>'+ moment(new Date(details.group_dateAndTime_created)).format('LLLL') +'</td>'+
        '</tr>'+
        '<tr>'+
        '<td><strong>MODIFIED</strong></td>' +
        '<td>'+ ((details.group_modified == null) ? "None" : moment(new Date(details.group_modified)).format('LLLL')) +'</td>'+
        '</tr>'+
        '<tr>'+
        '<td><strong>STATUS</strong></td>'+
        '<td>'+ details.group_status +'</td>'+
        '</tr>'+
        '<tr>';

    if(userRole == "SYS_ADMIN" || userRole == "IT_ADMIN"){
        if(userID == details.group_created_by.user_id){
            access = '<button type="button" class="btn btn-info btn-lg btn-rounded" style="padding: 10px; font-size: 15px; background-color:#1caf9a; color: white;" data-toggle="modal" data-target="#view_edit_group-modal"><strong><span class="fa fa-edit"></span> Edit This Group</strong></button>&nbsp;' +
                '<button type="button" class="btn btn-warning btn-lg btn-rounded" style="padding: 10px; font-size: 15px; background-color:#FF8000; color: white;" data-toggle="modal" data-target="#update_group_preview_image-modal"><strong><span class="fa fa-picture-o"></span> Change Preview Image</strong></button>';
        }else{
            access = "<span class='fa fa-ban' style='font-size: 18px;'><br>Access Denied</span>";
        }
    }else{
        access = "<span class='fa fa-ban' style='font-size: 18px;'><br>Access Denied</span>";
    }

    table += '<td colspan="2" style="text-align: center;">' + access + '</td>'+
        '</tr>'+
        '</table>';

    $('#frmUpdateGroup_lblGroup_id').val(details.group_id);
    $('#frmUpdateGroup_txtGroup_name').val(details.group_name);
    $('#frmUpdateGroup_txtGroup_description').val(details.group_description);
    $('#frmUpdateGroup_lblGroup_name').text(details.group_name);
			
    $('#frmUpdateGroup_cmbGroup_status')
		.removeAttr('selected')
		.find('option[value="' + details.group_status + '"]')
		.attr("selected",true)
		.change();

    $('#frmUpdateGroupPreviewImage_groupId').val(details.group_id);

    return table;
}

function validateGroup(){
    $("#frmAddGroup").validate({
        ignore : [],
        rules : {
            group_name: {
                required: true,
                maxlength: 50
            },
            group_description: {
                required: true,
                maxlength: 5000
            }
        },
        submitHandler: function(form) {
            addGroup(form);
        }
    });
}

function addGroup(form){
	var formEl = $(form);
	
	swal({
		title: "Do you still want to continue?",
		icon: "warning",
		buttons: ["No!", "Yes!"],
		dangerMode: true
	})
	.then((willAdd) => {
		if (willAdd) {
			$('body').loadingModal({
				position: 'auto',
				text: 'Please wait while creating this group...',
				color: '#fff',
				opacity: '0.7',
				backgroundColor: 'rgb(0,0,0)',
				animation: 'doubleBounce'
			});

			$.ajax({
				type: 'POST',
				dataType : 'JSON',
				url: formEl.prop('action'),
				data: formEl.serialize(),
				success: function(data) {
                    $('body').loadingModal('destroy');

					if(data.status == "groupNameExists"){
                        iziToast.error({
                            title: "Oops! We're sorry!",
                            theme: 'light',
                            color: 'red',
                            position: 'bottomRight',
                            message: "Group name exists. Please try again!"
                        });
					}else if(data.status == "accessDenied"){
                        iziToast.error({
                            title: "Oops! We're sorry!",
                            theme: 'light',
                            color: 'red',
                            position: 'bottomRight',
                            message: "Permission denied. Please try again!"
                        });
					}else if(data.status){
						iziToast.success({
							title: 'Congratulations!',
							theme: 'light', // dark
							color: 'green', // blue, red, green, yellow
							position: 'bottomRight', // bottomRight, bottomLeft, topRight, topLeft, topCenter, bottomCenter, center
							message: "Group has been added successfully!"
						});

						$("#add_new_group-modal").modal('toggle');
						//$('#tblGroups').DataTable().ajax.reload();
					}
				},
				error: function(e){
					console.log(e);
                    $('body').loadingModal('destroy');
                    iziToast.error({
                        title: "Oops! We're sorry!",
                        theme: 'light',
                        color: 'red',
                        position: 'bottomRight',
                        message: "Failed to create a group. Please try again!"
                    });
				}
			});
		}
	});
}

function updateGroup(){
	$('#frmUpdateGroup').submit(function(event) {
		event.preventDefault();
		var formEl = $(this);
		
		swal({
			  title: "Are you sure?",
			  text: "Once updated, you will not be able to undo this action!",
			  icon: "warning",
			  buttons: ["No, not sure yet!", "Yes, I'm Sure!"],
			  dangerMode: true
		})
		.then((willUpdate) => {
			if (willUpdate) {
				$.ajax({
					type: 'POST',
					dataType : 'JSON',
					url: formEl.prop('action'),
					data: formEl.serialize(),
					// if received a response from the server
					success: function( data, textStatus, jqXHR) {
						if(data.isUpdateGroupSuccess == "groupNameExists"){
							swal("We're sorry!", "Group name exists!", "error");
						}else if(data.isUpdateGroupSuccess){
							iziToast.success({
								title: 'Congratulations!',
								theme: 'light', // dark
								color: 'green', // blue, red, green, yellow
								position: 'bottomRight', // bottomRight, bottomLeft, topRight, topLeft, topCenter, bottomCenter, center
								message: "Group has been updated successfully!"
							});
	
							$("#view_edit_group-modal").modal('toggle');
							$('#tblGroups').DataTable().ajax.reload();
						}
					},
					error: function(e){
						console.log(e);
					}
				});
			}
		});
	});
}

function updateGroupPreviewImage(){
	$('#frmUpdateGroupPreviewImage').submit(function(event) {
		event.preventDefault();
		var formEl = $(this);
		
		swal({
			  title: "Are you sure?",
			  text: "Once updated, you will not be able to undo this action!",
			  icon: "warning",
			  buttons: ["No, not sure yet!", "Yes, I'm Sure!"],
			  dangerMode: true
		})
		.then((willUpdate) => {
			if (willUpdate) {
				$.ajax({
					type: 'POST',
					dataType : 'JSON',
					url: formEl.prop('action'),
					data: formEl.serialize(),
					// if received a response from the server
					success: function(data) {
						if(data.isUpdatingGroupPreviewImage){
							iziToast.success({
								title: 'Congratulations!',
								theme: 'light', // dark
								color: 'green', // blue, red, green, yellow
								position: 'bottomRight', // bottomRight, bottomLeft, topRight, topLeft, topCenter, bottomCenter, center
								message: "Group preview image has been updated successfully!"
							});
	
							$("#update_group_preview_image-modal").modal('toggle');
							$('#tblGroups').DataTable().ajax.reload();
						}
					},
					error: function(e){
						console.log(e);
					}
				});
			}
		});
	});
}

connectWebSocket();
showAllGroups();
validateGroup();
updateGroup();
updateGroupPreviewImage();