package com.sloms.repository;

import java.util.Date;
import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.sloms.entity.Notes;

@Repository
public interface NotesRepository extends JpaRepository<Notes,Integer>{
	@Query("SELECT n FROM Notes n WHERE n.notes_type=:notes_type")
	Notes findNotesByNotesType(@Param("notes_type") String notes_type);
	
	@Query("SELECT n FROM Notes n ORDER BY n.notes_id DESC")
	List<Notes> getAllNotes();
	
	@Query("SELECT n FROM Notes n "
			+ "INNER JOIN n.notes_billboard notes_billboard "
			+ "WHERE notes_billboard.media_id=:media_id "
			+ "ORDER BY n.notes_id DESC")
	List<Notes> getAllNotesByNotesBillboardMediaId(@Param("media_id") Integer media_id);
	
	@Query("SELECT n FROM Notes n WHERE n.notes_status='Activated' ORDER BY n.notes_id DESC")
	List<Notes> getAllActivatedNotes();
	
	@Query("SELECT n FROM Notes n WHERE n.notes_id=:notes_id")
	Notes findNotesByNotesId(@Param("notes_id") Integer notes_id);
	
	@Transactional
	@Modifying
	@Query("UPDATE Notes n SET "
			+ "n.notes_type=:notes_type,"
			+ "n.notes_description=:notes_description,"
			+ "n.notes_status=:notes_status,"
			+ "n.notes_dateAndTimeModified=:notes_dateAndTimeModified "
			+ "WHERE n.notes_id=:notes_id")
	void updateNotes(
			@Param("notes_type") String notes_type,
			@Param("notes_description") String notes_description,
			@Param("notes_status") String notes_status,
			@Param("notes_dateAndTimeModified") Date notes_dateAndTimeModified,
			@Param("notes_id") Integer notes_id);
}
