package com.sloms.service;

import java.security.Principal;
import java.util.List;

import com.sloms.entity.FAQs;
import com.sloms.entity.Notifications;

public interface FAQsService {
	List<FAQs> getAllFAQs();
	void saveFAQ(FAQs faqsDAO, Notifications notificationsDAO, Principal principal);
	void updateFAQs(String faqs_question, String faqs_answer, String faqs_status, Integer faqs_id);
	FAQs findFAQsByFAQsQuestion(String faqs_question);
}
