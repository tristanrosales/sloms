package com.sloms.entity;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.*;

@Getter
@Setter
@ToString
@EqualsAndHashCode
@NoArgsConstructor
@Entity
public class Notifications{	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Integer id;

	@NotNull
	private String notif_type;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="user_id")
	@JsonIgnoreProperties(
			{
				"handler",
				"hibernateLazyInitializer",
				"notifications",
				"user_password",
				"user_email_address",
				"user_phone_number",
				"user_status",
				"user_sms_notif",
				"user_email_notif",
				"user_email_notif",
				"user_security_question",
				"user_secret_answer"
			})
	private Users users;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="changelog_id")
	@JsonIgnoreProperties(
			{
				"handler",
				"hibernateLazyInitializer"
			})
	private Changelog changelog;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="faqs_id")
	@JsonIgnoreProperties(
			{
				"handler",
				"hibernateLazyInitializer"
			})
	private FAQs faQs;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="feedback_id")
	@JsonIgnoreProperties(
			{
				"handler",
				"hibernateLazyInitializer"
			})
	private Feedbacks feedbacks;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="group_id")
	@JsonIgnoreProperties(
			{
				"handler",
				"hibernateLazyInitializer"
			})
	private Groups groups;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="media_id")
	@JsonIgnoreProperties(
			{
				"handler",
				"hibernateLazyInitializer"
			})
	private Media media;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="notes_id")
	@JsonIgnoreProperties(
			{
				"handler",
				"hibernateLazyInitializer"
			})
	private Notes notes;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="thread_id")
	@JsonIgnoreProperties(
			{
				"handler",
				"hibernateLazyInitializer"
			})
	private Threads threads;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="thread_comment_id")
	@JsonIgnoreProperties(
			{
				"handler",
				"hibernateLazyInitializer"
			})
	private ThreadComments threadComments;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="thread_reply_id")
	@JsonIgnoreProperties(
			{
				"handler",
				"hibernateLazyInitializer"
			})
	private ThreadCommentReplies threadCommentReplies;

}
